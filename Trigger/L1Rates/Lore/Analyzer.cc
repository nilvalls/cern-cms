/*
    Author:			Nil Valls <nil.valls@cern.ch>
    Date:			30 Aug 2011
    Description:	L1 rate estimator
    Notes:

*/

#define Analyzer_cxx
#include "Analyzer.h"

using namespace std;


// Default constructor
Analyzer::Analyzer(long long iMaxEvents, string iNtuplePath,  map<string,L1Trigger*>* iTriggers, map<string,L1Trigger*>* iCompTriggers){

	triggers = iTriggers;
	compTriggers = iCompTriggers;
	maxEvents = iMaxEvents;

	ntuplePath = iNtuplePath;
	fChainL1Tree = GetTChain(iNtuplePath,"l1NtupleProducer/L1Tree");
	fChainL1Menu = GetTChain(iNtuplePath,"l1MenuTreeProducer/L1MenuTree");
	reportRate = 1000;
	lumiByLS = new lumiMap(); lumiByLS->clear();

	maxInstLumiByRun	= new map<int,double>(); maxInstLumiByRun->clear();	
	minInstLumiByRun	= new map<int,double>(); minInstLumiByRun->clear();	
	avgInstLumiByRun	= new map<int,double>(); avgInstLumiByRun->clear();	
	numLSByRun			= new map<int,int>(); 	 numLSByRun->clear();		
	runsProcessed		= new vector<int>(); 	 runsProcessed->clear();		
	
	Init();
}

// Default destructor
Analyzer::~Analyzer(){}

Int_t Analyzer::GetEntry(Long64_t entry){
	Int_t result;
	// Read contents of entry.
	if (!fChainL1Tree) return 0;
	result = fChainL1Tree->GetEntry(entry);


	return result;
}


void Analyzer::Loop(){



	cout << ">>> Starting loop... "; cout.flush();
	if (fChainL1Tree == 0){ cout << endl << "ERROR: empty TChain. Exiting."; return; }


	Long64_t nentries = fChainL1Tree->GetEntries();
	numEvInNtuple	= nentries;
	cout << " " << nentries << " entries available: ";
	if(maxEvents <= 0 || maxEvents >= nentries){ cout << "Processing all of them..." << string(14,'.') << " "; }
	else{ cout << "Stopping at " << maxEvents << " as per-user request" << string(14,'.') << " "; }
	cout.flush();

	Long64_t nbytes = 0, nb = 0;
	numEvAnalyzed=0;
	for (Long64_t jentry=0; jentry<nentries; jentry++) {

		// Keep user informed of the number of events processed and if there is a termination due to reaching of the limit
		/*******************************************************************************/
			int prevLength = 0;
			if (jentry>0 && (jentry+1)%reportRate == 0){ 
				stringstream jentryss; jentryss.str("");
				jentryss << (jentry+1);
				cout << string((jentryss.str()).length(),'\b') << jentryss.str(); cout.flush(); 
				prevLength = (jentryss.str()).length();
			}
			if ( maxEvents > 0 && jentry >= (unsigned int)(maxEvents-1)){ cout << "\n>>> INFO: Reached user-imposed event number limit (" << maxEvents << "), skipping the rest." << endl; break; }

			Long64_t ientry = LoadTree(jentry);
			if (ientry < 0) break;
			//nb = GetEntry(jentry);   nbytes += nb;
			fChainL1Tree->GetEntry(jentry);
			fChainL1Menu->GetEntry(jentry);
		/*******************************************************************************/
		cout << "test index: " << l1menu_->AlgoTrig_PrescaleFactorIndex << endl;
		cout << "test pf5,10,50 " << l1menu_->AlgoTrig_PrescaleFactors.at(5)
			<< " " << l1menu_->AlgoTrig_PrescaleFactors.at(10)
			<< " " << l1menu_->AlgoTrig_PrescaleFactors.at(50)
			<< endl;

	

		// Skip first 100 lumisections (potentially before stable beams)
		if(event_->lumi < 100){ continue; }

		if(runsProcessed->size() == 0){ 
			runsProcessed->push_back((int)event_->run); 
		}else if (runsProcessed->back() != (int)event_->run){
			runsProcessed->push_back((int)event_->run);
		}

		// Loop over every defined trigger and make itself check this event
		map<string,L1Trigger*>::iterator triggerIt;
		for(triggerIt = triggers->begin(); triggerIt != triggers->end(); ++triggerIt){
			(triggerIt->second)->CheckEvent(event_, l1menu_, gt_, GetInstLumi(event_->run,event_->lumi));
		}
		// Loop over every defined composite trigger and make itself check this event
		for(triggerIt = compTriggers->begin(); triggerIt != compTriggers->end(); ++triggerIt){
			(triggerIt->second)->CheckEvent(event_, l1menu_, triggers, GetInstLumi(event_->run,event_->lumi));
		}

		numEvAnalyzed++;

	}
	cout << endl;


	
	cout << string(30,' ') << "Instantaneous Luminosity [ 1E33 /(cm^2-s) ]" << endl;
	cout << string(29,' ') << string(45,'-') << endl;
	cout << string(10,' ') << "Runs processed" << string(6,' ') << "Average" << string(14,' ') << "Min" << string(12,' ') << "Max" << endl;
	cout << string(5,' ') << string(78,'-') << endl;
	for(unsigned int r=0; r<runsProcessed->size(); r++){
		((*avgInstLumiByRun)[r]) /= (double)((*numLSByRun)[r]); // Average run instantaneous luminosity x 1E30/(cm^2-s)	
		int run = runsProcessed->at(r);
		cout << string(10,' ') 
			 << run << string(15,' ') << "\t"
			 << (*maxInstLumiByRun)[run]/1000.0 << string(10,' ') << "\t"
			 << (*minInstLumiByRun)[run]/1000.0 << string(6,' ') << "\t"
			 << (*avgInstLumiByRun)[run]/1000.0 << string(6,' ') 
			 << endl;
	}
	cout << "\n\n" << endl;


	cout << string(10,' ') << "Trigger name" << string(40,' ') << "Average Rate [kHz]" << endl;
	cout << string(5,' ') << string(78,'-') << endl;

	// Loop over every defined trigger and get average rates
	map<string,L1Trigger*>::iterator triggerIt;
	for(triggerIt = triggers->begin(); triggerIt != triggers->end(); ++triggerIt){
		string triggerName = (triggerIt->first);
		cout << string(7,' ') << triggerName << string(60-triggerName.length(),'.') << " " << setprecision(5) << (triggerIt->second)->GetAverageRatekHz() << endl;
	}
	// Loop over every defined composite trigger and get average rates
	for(triggerIt = compTriggers->begin(); triggerIt != compTriggers->end(); ++triggerIt){
		string triggerName = (triggerIt->first);
		cout << string(7,' ') << triggerName << string(60-triggerName.length(),'.') << " " << setprecision(5) << (triggerIt->second)->GetAverageRatekHz() << endl;
	}

}

Long64_t Analyzer::LoadTree(Long64_t entry){
	// Set the environment to read one entry
	if (!fChainL1Tree) return -5;
	Long64_t centry = fChainL1Tree->LoadTree(entry);
	if (centry < 0) return centry;
	if (!fChainL1Tree->InheritsFrom(TChain::Class()))  return centry;
	TChain *chain = (TChain*)fChainL1Tree;
	if (chain->GetTreeNumber() != fCurrentL1Tree) {
		fCurrentL1Tree = chain->GetTreeNumber();
		Notify();
	}
	return centry;
}

TChain* Analyzer::GetTChain(string iPath, string iTreeName){

	// Chain to return
	TChain* result = new TChain(iTreeName.c_str());

	result->Add((string(iPath+"/*.root")).c_str()); // Otherwise add the file to the TChain

	// Return TChain
	return result;
}

void  Analyzer::SetMaxEvents(int iMaxEvents){
	maxEvents = iMaxEvents;
}


vector<string>* Analyzer::ParseString(const string& iS, char c) {
	vector<string>* v = new vector<string>(); v->clear();

	string s = string(iS);

	string::size_type curr = 0;
	string::size_type next = min(s.find(c), s.length());
	while (next < s.length()){
		string toAdd = string(s.substr(curr, next-curr));
		v->push_back(toAdd);

		curr = next+1;
		next = s.find(c, next+1);
	}
	v->push_back(s.substr(curr, next-curr));

	return v;
}



// Reads the lumis.root file with the info of the luminosity per run per LS and returns it in a map
lumiMap* Analyzer::GetLumiMap(int iRun){

	lumiMap* result = new lumiMap(); result->clear();

	TFile* lumiFile = new TFile((string(ntuplePath+"/lumis/lumis.root")).c_str());
	TTree* lumiTree = (TTree*)lumiFile->Get("ntuple");

	Float_t         run;
	Float_t         ls;
	Float_t         lumiDelivered; // Not used currently
	Float_t         lumiReported;

	TBranch        *b_run;
	TBranch        *b_ls;
	TBranch        *b_lumiDelivered; // Not used currently
	TBranch        *b_lumiReported;

	lumiTree->SetBranchAddress("run", &run, &b_run);
	lumiTree->SetBranchAddress("ls", &ls, &b_ls);
	lumiTree->SetBranchAddress("lumiDelivered", &lumiDelivered, &b_lumiDelivered); // INSTANTANEOUS delivered lumi (per LS) 
	lumiTree->SetBranchAddress("lumiReported", &lumiReported, &b_lumiReported); // INSTANTANEOUS live lumi (per LS)

	// Loop and fill the map
	map<int,bool> runsDone; runsDone.clear();
	Long64_t nentries = lumiTree->GetEntries();
	for (Long64_t jentry=0; jentry < nentries; jentry++) {

		lumiTree->GetEntry(jentry);

		if( !runsDone[(int)run] ){
			(*maxInstLumiByRun)[(int)run]	= 0;
			(*minInstLumiByRun)[(int)run]	= 99999;
			(*avgInstLumiByRun)[(int)run]	= 0;
			(*numLSByRun)[(int)run]			= 0;
			runsDone[(int)run] = true;
		}
	
		if(lumiReported > (*maxInstLumiByRun)[(int)run]){ (*maxInstLumiByRun)[(int)run] = lumiReported; }
		if((lumiReported < (*minInstLumiByRun)[(int)run]) && 0 < lumiReported){ (*minInstLumiByRun)[(int)run] = lumiReported; }
		(*avgInstLumiByRun)[(int)run] = lumiReported;
		(*numLSByRun)[(int)run]++;
		

		pair<double,int> toInsert = make_pair(run, ls);
		(*result)[toInsert] = lumiReported;
	}


	lumiFile->Close();

	return result;

}

// Return the instantaneous luminosity for a run and LS number
double Analyzer::GetInstLumi(int iRun, int iLS){

	double result;

	// If lumi map is not filled, do it
	if(lumiByLS->size()==0 ){
		lumiByLS = GetLumiMap(iRun);
	}

	// Obtain integrated lumi for the Run:LS in question and compute the instantaneous lumi for it
	pair<int,int> keyPair = make_pair(iRun,iLS);
	result = ((*lumiByLS)[keyPair])/1000.0; // Return instantaneous luminosity x 1E33/(cm^2-s)

	// Some values are negative, return zero instead
	if(result <= 0){
		return 0;
	}

	// Else return the instantaneous lumi for the Run:LS asked
	return result;

}

