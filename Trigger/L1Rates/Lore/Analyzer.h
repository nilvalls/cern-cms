//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Jun  7 03:28:02 2011 by ROOT version 5.27/06b
// from TTree HMTTree/TauTauTree
// found on file: ditau_nTuple.root
//////////////////////////////////////////////////////////

#ifndef Analyzer_h
#define Analyzer_h


#include <TROOT.h>
#include <TSystem.h>
#include <TLatex.h>
#include <TChain.h>
#include <TFile.h>
#include <TBranch.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TPaveText.h>
#include <TGraphAsymmErrors.h>

#include <stdio.h>
#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <limits.h>
#include <map>
#include <vector>
#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <utility>

#include "L1Trigger.h"

#include "L1AnalysisEventDataFormat.h"
#include "L1AnalysisL1MenuDataFormat.h"
#include "L1AnalysisGTDataFormat.h"

#include "TDataType.h"

using namespace std;


typedef map< pair<int,int>, double> lumiMap;


//class Analyzer : public L1Ntuple{
class Analyzer{ 

	private:
		map<string,L1Trigger*>* triggers;
		map<string,L1Trigger*>* compTriggers;

	public :

		L1Analysis::L1AnalysisEventDataFormat*	event_;
		L1Analysis::L1AnalysisL1MenuDataFormat*	l1menu_;
		L1Analysis::L1AnalysisGTDataFormat*		gt_;

		TChain*          	fChainL1Tree;   //!pointer to the analyzed TTree or TChain
		TChain*          	fChainL1Menu;   //!pointer to the analyzed TTree or TChain
		Int_t           	fCurrentL1Tree; //!current Tree number in a TChain
		Int_t           	fCurrentL1Menu; //!current Tree number in a TChain
		int					numEvAnalyzed;
		int					numEvInNtuple;
		int 				maxEvents;
		int					reportRate;
		string				ntuplePath;

		lumiMap*			lumiByLS;
		map<int,double>*	maxInstLumiByRun;
		map<int,double>*	minInstLumiByRun;
		map<int,double>*	avgInstLumiByRun;
		map<int,int>*		numLSByRun;
		vector<int>*		runsProcessed;




		Analyzer(long long, string, map<string,L1Trigger*>*, map<string,L1Trigger*>*);
		~Analyzer();

		virtual TChain*					GetTChain(string, string iTreeName="l1NtupleProducer/L1Tree");
		
		virtual void					SetMaxEvents(int);
		virtual	vector<string>*			ParseString(const string&, char);

		virtual Int_t					Cut(Long64_t entry);
		virtual Bool_t					Notify();
		virtual void					Show(Long64_t entry = -1);
		virtual void					SpacePad(stringstream&, int );
		
		Int_t							GetEntry(Long64_t entry);
		Long64_t						LoadTree(Long64_t entry);
		void							Init();
		void							Loop();
		void							FillObjects();
		string 							Dec2Bin(long double);
		unsigned long long				BitMaskForBit(unsigned int);
		bool 							IsBitSet(int);
		lumiMap*						GetLumiMap(int);
		double							GetInstLumi(int, int);
		
	private:
		Long64_t nentriesL1Tree_;
		Long64_t nentriesL1Menu_;

};

#endif

#ifdef Analyzer_cxx

void Analyzer::SpacePad(stringstream& iSS, int iNum){
	for (int i = 0; i < iNum; i++){ iSS << " "; }
}

void Analyzer::Init(){

   if (!fChainL1Tree) return;
   fCurrentL1Tree = -1;
   
   nentriesL1Tree_ = fChainL1Tree->GetEntries();
   
   event_  = new L1Analysis::L1AnalysisEventDataFormat();
   gt_     = new L1Analysis::L1AnalysisGTDataFormat();

   fChainL1Tree->SetBranchAddress("Event", &event_);
   fChainL1Tree->SetBranchAddress("GT",    &gt_);


   if (!fChainL1Menu) return;
   fCurrentL1Menu = -1;
   
   nentriesL1Menu_ = fChainL1Tree->GetEntries();

   if(nentriesL1Tree_ != nentriesL1Menu_){
	   cerr << "ERROR: L1Tree and L1Menu have different number of entries: " << nentriesL1Tree_ << " and " << nentriesL1Menu_ << endl; exit(1);	
   }

   l1menu_ = new L1Analysis::L1AnalysisL1MenuDataFormat();
   fChainL1Menu->SetBranchAddress("L1Menu", &l1menu_);

}


Bool_t Analyzer::Notify(){
	// The Notify() function is called when a new file is opened. This
	// can be either for a new TTree in a TChain or when when a new TTree
	// is started when using PROOF. It is normally not necessary to make changes
	// to the generated code, but the routine can be extended by the
	// user if needed. The return value is currently not used.

	return kTRUE;
}

void Analyzer::Show(Long64_t entry){
	// Print contents of entry.
	// If entry is not specified, print current entry
	if (!fChainL1Tree) return;
	fChainL1Tree->Show(entry);
}


Int_t Analyzer::Cut(Long64_t entry){
	// This function may be called from Loop.
	// returns  1 if entry is accepted.
	// returns -1 otherwise.
	return 1;
}


#endif
