
#define HistoWrapper_cxx
#include "HistoWrapper.h"
#include <TStopwatch.h>
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <stdlib.h>
#include <fstream>
#include <iomanip>

using namespace std;



// Default constructor
HistoWrapper::HistoWrapper(){
	cutsApplied		= "";
	NumEvAnalyzed	= 0;
	NumEvInNtuple	= 0;
	CrossSection	= 0;
	logx			= false;
	logy			= false;
}

HistoWrapper::HistoWrapper(TH1F* iHisto){
	HistoWrapper();
	SetHisto(iHisto);
}

HistoWrapper::HistoWrapper(TH1F* iHisto, int iNumEvAnalyzed){
	HistoWrapper();
	SetHisto(iHisto);
	NumEvAnalyzed = iNumEvAnalyzed;
}

int HistoWrapper::GetNumEvAnalyzed(){
	return NumEvAnalyzed;
}

int HistoWrapper::GetNumEvInNtuple(){
	return NumEvInNtuple;
}

void HistoWrapper::SetCutsApplied(string iCutsApplied){
	cutsApplied = iCutsApplied;
}

string HistoWrapper::GetCutsApplied(){
	return cutsApplied;
}

bool HistoWrapper::ChargeProductApplied(){
	size_t found = cutsApplied.find("ChargeProduct");
	return ((0 <= found) && (found < cutsApplied.length()));
}

void HistoWrapper::SetNumEvInNtuple(int iNumEvInNtuple){
	NumEvInNtuple = iNumEvInNtuple;
}

float HistoWrapper::GetCrossSection(){
	return CrossSection;
}
void HistoWrapper::SetCrossSection(float iCrossSection){
	CrossSection = iCrossSection;
}

string HistoWrapper::GetOutputFilename(){
	return outputFilename;
}

void HistoWrapper::SetOutputFilename(string iOutputFilename){
	outputFilename = iOutputFilename;
}

void HistoWrapper::SetRangeUser(float ixMinVis, float ixMaxVis){
	xMinVis	= ixMinVis;	
	xMaxVis	= ixMaxVis;	
}

void HistoWrapper::SetXlabel(string ixLabel){
	xLabel = ixLabel;
}
void HistoWrapper::SetYlabel(string iyLabel){
	yLabel = iyLabel;
}

void HistoWrapper::SetLogxy(bool iLogx, bool iLogy){
	logx = iLogx;
	logy = iLogy;
}

void HistoWrapper::SetCenterLabels(bool iCenterLabels){
	centerLabels = iCenterLabels;
}

bool HistoWrapper::DoLogx(){
	return logx;
}

bool HistoWrapper::DoLogy(){
	return logy;
}

bool HistoWrapper::CenterLabels(){
	return centerLabels;
}

pair<float,float> HistoWrapper::GetRangeUser(){
	return pair<float, float>(xMinVis,xMaxVis);
}
float HistoWrapper::GetXminVis(){
	return xMinVis;
}
float HistoWrapper::GetXmaxVis(){
	return xMaxVis;
}

string HistoWrapper::GetXlabel(){
	return xLabel;
}

string HistoWrapper::GetYlabel(){
	return yLabel;
}

// Default destructor
HistoWrapper::~HistoWrapper(){
}

void HistoWrapper::SetHisto(TH1F* iHisto){
	histo = new TH1F(*iHisto);	
}

void HistoWrapper::SetHisto(TH1F iHisto){
	histo = new TH1F(iHisto);	
}

void HistoWrapper::SetName(string iName){
	histo->SetName(iName.c_str());
}

void HistoWrapper::SetTitle(string iTitle){
	histo->SetTitle(iTitle.c_str());
}

void HistoWrapper::SetNumEvAnalyzed(int iNumEvAnalyzed){
	NumEvAnalyzed = iNumEvAnalyzed;
}

string HistoWrapper::GetName(){
	return string(histo->GetName());
}

string HistoWrapper::GetTitle(){
	return string(histo->GetTitle());
}

TH1F* HistoWrapper::GetHisto(){
	return histo;
}

