
#ifndef HistoWrapper_h
#define HistoWrapper_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TGraphAsymmErrors.h>
#include <math.h>
#include <map>
#include <vector>
#include <sstream>
#include <iostream>
#include <utility>


using namespace std;




class HistoWrapper {
	public :
		TH1F* histo;
		string cutsApplied;
		bool isBackground;
		bool isSignal;
		int NumEvAnalyzed;
		int NumEvInNtuple;
		float CrossSection;
		float yMinVis;
		float yMaxVis;
		float xMinVis;
		float xMaxVis;
		string xLabel;
		string yLabel;
		string outputFilename;
		bool logx;
		bool logy;
		bool centerLabels;



		// Default constructor
		HistoWrapper();
		HistoWrapper(TH1F*);
		HistoWrapper(TH1F*, int); 
		virtual ~HistoWrapper();
		virtual void SetHisto(TH1F* iHisto);
		virtual void SetHisto(TH1F iHisto);
		virtual void SetName(string iName);
		virtual void SetCutsApplied(string);
		virtual string GetCutsApplied();
		virtual bool ChargeProductApplied();
		virtual void SetTitle(string iTitle);
		virtual void SetRangeUser(float,float);
		virtual void SetXlabel(string);
		virtual void SetYlabel(string);
		virtual void SetLogxy(bool, bool);
		virtual void SetCenterLabels(bool);
		virtual bool DoLogx();
		virtual bool DoLogy();
		virtual bool CenterLabels();
		virtual void SetOutputFilename(string);
		virtual int GetNumEvInNtuple();
		virtual void SetNumEvInNtuple(int iNumEvInNtuple);
		virtual float GetCrossSection();
		virtual void SetCrossSection(float iCrossSection);
		virtual string GetOutputFilename();
		virtual pair<float,float> GetRangeUser();
		virtual float GetXminVis();
		virtual float GetXmaxVis();
		virtual string GetXlabel();
		virtual string GetYlabel();
		virtual void SetNumEvAnalyzed(int);
		virtual int GetNumEvAnalyzed();
		virtual string GetName();
		virtual string GetTitle();
		virtual TH1F* GetHisto();


};

#endif

#ifdef HistoWrapper_cxx



#endif
