#ifndef __L1Analysis_L1AnalysisL1MenuDataFormat_H__
#define __L1Analysis_L1AnalysisL1MenuDataFormat_H__

#include <vector>

namespace L1Analysis
{
  struct L1AnalysisL1MenuDataFormat
  {
    L1AnalysisL1MenuDataFormat() {Reset();}
    ~L1AnalysisL1MenuDataFormat() {}
    
    void Reset()
    {
      AlgoTrig_PrescaleFactorIndexValid = false;
      AlgoTrig_PrescaleFactorIndex = 0;
      TechTrig_PrescaleFactorIndexValid = false;
      TechTrig_PrescaleFactorIndex = 0;

	  AlgoTrig_PrescaleFactorsValid.clear();
	  AlgoTrig_PrescaleFactors.clear();
	  TechTrig_PrescaleFactorsValid.clear();
	  TechTrig_PrescaleFactors.clear();
    }

    bool AlgoTrig_PrescaleFactorIndexValid;
    int  AlgoTrig_PrescaleFactorIndex;
    bool TechTrig_PrescaleFactorIndexValid;
    int  TechTrig_PrescaleFactorIndex;

    std::vector<bool> AlgoTrig_PrescaleFactorsValid;
    std::vector<int>  AlgoTrig_PrescaleFactors;
    std::vector<bool> TechTrig_PrescaleFactorsValid;
    std::vector<int>  TechTrig_PrescaleFactors;



  }; 
}
#endif


