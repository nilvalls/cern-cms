
#define L1Trigger_cxx
#include "L1Trigger.h"
#include <TStopwatch.h>
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <stdlib.h>
#include <fstream>
#include <iomanip>

using namespace std;



// Default constructor
L1Trigger::L1Trigger(string iName, string iDefinition, map<string, L1Trigger*>* iFundTriggers){

	name = iName;
	mathParser	= new MathParser("");
	histo = new TH1F(name.c_str(),name.c_str(),1000,0,10);
	definition = string(" " + ReplaceString(iDefinition,"\t"," ") + " ");
	triggersInDef = NULL;
	rawEventsPassingFundTrigger = NULL;
	fundTriggerPrescale = NULL;

	// Reset counters;
	rawEventsPassing		= 0;
	rescaledEventsPassing	= 0;
	eventsAnalyzed			= 0;
	averageRate				= 0;
	lumiSectionsAnalyzed.clear();

	userPrescale = 1;
	cutOnJetMult = false;	
	cutOnTau = false;		
	cutOnFwd = false;		
	cutOnJetEt = false;	
	cutOnJetEta = false;	
	cutOnJetIeta = false;	
	cutOnMET = false;		


	size_t found = definition.find(":");
	isComposite = !( (0 <= found) && (found <= definition.length()) );

	if(!isComposite){

		etaLowerEdge[0] = -5.0;
		etaLowerEdge[1] = -4.5;	
		etaLowerEdge[2] = -4.0;
		etaLowerEdge[3] = -3.5;	
		etaLowerEdge[4] = -3.0;	
		etaLowerEdge[5] = -2.17;
		etaLowerEdge[6] = -1.74;
		etaLowerEdge[7] = -1.39;
		etaLowerEdge[8] = -1.0425;
		etaLowerEdge[9] = -0.6950;
		etaLowerEdge[10] = -0.3475;
		etaLowerEdge[11] = 0.0000;
		etaLowerEdge[12] = 0.3475;
		etaLowerEdge[13] = 0.6950;
		etaLowerEdge[14] = 1.044;
		etaLowerEdge[15] = 1.39;
		etaLowerEdge[16] = 1.74;
		etaLowerEdge[17] = 2.17;
		etaLowerEdge[18] = 3;
		etaLowerEdge[19] = 3.5;
		etaLowerEdge[20] = 4.0;
		etaLowerEdge[21] = 4.5;
		etaLowerEdge[22] = 5.0;


		if(!CutOn("L1bit")){ cerr << "ERROR: bit not set" << endl; exit(1); }

		bit				= GetThresholds("L1bit").first;
		bitPrescale		= GetThresholds("bitPrescale").first;
		userPrescale	= GetThresholds("userPrescale").first;


		if(CutOn("jetMult")){
			cutOnJetMult= true;
			minNjets	= GetThresholds("jetMult").first;
			maxNjets	= GetThresholds("jetMult").second;
		}

		if(CutOn("tau")){
			cutOnTau	= true;
			beTau		= GetThresholds("tau").first;
		}

		if(CutOn("fwd")){
			cutOnFwd	= true;
			beFwd		= GetThresholds("fwd").first;
		}

		if(CutOn("jetEt")){
			cutOnJetEt	= true;
			minJetEt	= GetThresholds("jetEt").first;
			maxJetEt	= GetThresholds("jetEt").second;
		}

		if(CutOn("jetEta")){
			cutOnJetEta	= true;
			minJetEta	= GetThresholds("jetEta").first;
			maxJetEta	= GetThresholds("jetEta").second;
		}
		if(CutOn("jetIeta")){
			cutOnJetIeta	= true;
			minJetIeta	= GetThresholds("jetIeta").first;
			maxJetIeta	= GetThresholds("jetIeta").second;
		}

		if(CutOn("met")){
			cutOnMET	= true;
			minMET		= GetThresholds("met").first;
			maxMET		= GetThresholds("met").second;
		}
	}else{
		// Scan definition and store trigger names
		maxBitPrescale	= 1;
		triggersInDef = ScanForTriggers(definition);
		definition = ReplaceString(definition, "&&", " && ");
		definition = ReplaceString(definition, "||", " || ");
		definition = ReplaceString(definition, "(", "(  ");
		definition = ReplaceString(definition, ")", " )");

		// Is this for a new L1 trigger of for an HLT seed?
		isNewL1trigger = true;

		rawEventsPassingFundTrigger = new vector<int>();
		rawEventsPassingFundTrigger->clear();

		fundTriggerPrescale = new vector<int>();
		fundTriggerPrescale->clear();
		
		// Loop over every fundamental trigger in the definition
		for(unsigned int t = 0; t < triggersInDef->size(); t++){
			L1Trigger* fundTrigger = (*iFundTriggers)[triggersInDef->at(t)];

			// Check that requested trigger was defined as a fundamental trigger
			if( fundTrigger == NULL){
				cerr << "\nERROR: composite trigger '" << name << "' requires fundamental trigger '" << triggersInDef->at(t) << "' which has not been defined in the config file" << endl;
				exit(1);
			}

			if(t==0){
				userPrescale = fundTrigger->GetUserPrescale();	
			}else{
				if(userPrescale != fundTrigger->GetUserPrescale()){ isNewL1trigger = false; }	
			}

			// Reset counters for fundamental triggers
			rawEventsPassingFundTrigger->push_back(0);

			// Figure out maximum prescale of triggers in definition
			int defTriggerBitPrescale = fundTrigger->GetBitPrescale();

			fundTriggerPrescale->push_back(defTriggerBitPrescale);

			if( defTriggerBitPrescale > maxBitPrescale ){
				maxBitPrescale = defTriggerBitPrescale;
			}
		}

		if(!isNewL1trigger){ userPrescale = 1; }

	}

}


// Provided the event, gt, and instantaneous luminosity it determines whether this trigger should have been fired. If so, it fills the histo.
void L1Trigger::CheckEvent(L1Analysis::L1AnalysisEventDataFormat* iEvent, L1Analysis::L1AnalysisL1MenuDataFormat* iL1Menu, L1Analysis::L1AnalysisGTDataFormat* iGt, double iInstLumi){

	passedEvent = false;
	
	gt_ = iGt;
	event_ = iEvent;
	FillObjects();

	pair<int,int> runLumiPair = make_pair(runNumber,lumiSection);
	lumiSectionsAnalyzed[runLumiPair] = true;

	eventsAnalyzed++;

	if(!IsBitSet(bit)){ return; }

	if(cutOnJetMult){ 
		int numPassingJets = 0;
		for(unsigned j = 0; j<numJets; j++){
			if(cutOnJetEt	){ 
				//cout << jetET.at(j) << " " <<  minJetEt << " " <<  maxJetEt << endl;
				if(OutOfRange(jetET.at(j), minJetEt, maxJetEt)){ continue; }
				//cout << "passed" << endl;
			}
			if(cutOnJetEta	){ if(OutOfRange(jetEta.at(j), minJetEta, maxJetEta)){ continue; } }
			if(cutOnJetIeta	){ if(OutOfRange(jetIeta.at(j), minJetIeta, maxJetIeta)){ continue; } }
			if(cutOnTau     ){ if(isTauJet.at(j) != beTau){ continue; } }
			if(cutOnFwd     ){ if(isFwdJet.at(j) != beFwd){ continue; } }

			numPassingJets++;
		}
		if(OutOfRange(numPassingJets, minNjets, maxNjets)){ return; }
	}

	if(cutOnMET	){ if(OutOfRange(MET,minMET,maxMET)){ return; } }

	rawEventsPassing++;
	passedEvent = true;
	rescaledEventsPassing += bitPrescale;

	histo->Fill(iInstLumi, bitPrescale);

}



// Check to see if the fundamental triggers that form this composite trigger were set
void L1Trigger::CheckEvent(L1Analysis::L1AnalysisEventDataFormat* iEvent, L1Analysis::L1AnalysisL1MenuDataFormat* iL1Menu, map<string, L1Trigger*>* iTriggers, double iInstLumi){

	passedEvent = false;
	string expression = definition;

	eventsAnalyzed++;

	pair<int,int> runLumiPair = make_pair(iEvent->run, iEvent->lumi);
	lumiSectionsAnalyzed[runLumiPair] = true;


	// Check set bits and substitute for trigger name
	stringstream nameToVal; nameToVal.str("");
	for (unsigned int t = 0; t < triggersInDef->size(); t++){

		// Reset ss
		nameToVal.str("");

		// Check that requested trigger was defined as a fundamental trigger
		if(((*iTriggers)[triggersInDef->at(t)]) == NULL){
			cerr << "\nERROR: composite trigger '" << name << "' requires fundamental trigger '" << triggersInDef->at(t) << "' which has not been defined in the config file" << endl;
			exit(1);
		}

		// Store result of fundamental trigger
		if(isNewL1trigger){
			nameToVal << (((*iTriggers)[triggersInDef->at(t)])->PassedEventModPrescale(maxBitPrescale/fundTriggerPrescale->at(t)));
		}else{
			nameToVal << (((*iTriggers)[triggersInDef->at(t)])->PassedEvent());
		}

		// Replace name of the fundamental trigger by its result
		expression = ReplaceString(expression, string(" " + triggersInDef->at(t) + " "), nameToVal.str());
	}

	// Evaluate the overall definition of this composite trigger
	if(mathParser->Evaluate(expression)){
		passedEvent = true;
		rawEventsPassing++;

		if(isNewL1trigger){
			rescaledEventsPassing += ((maxBitPrescale));
		}else{
			rescaledEventsPassing++;
		}

		histo->Fill(iInstLumi);
	}
	
}

// Return whether the event has fired this trigger or not
bool L1Trigger::PassedEvent(){
	return passedEvent;
}

bool L1Trigger::PassedEventModPrescale(int iPrescale){

	if(passedEvent){
		if( ((long int)rawEventsPassing % iPrescale) == 0 ){ return true; }	
	}

	return false;
}


// Fills "friendly" variables
void L1Trigger::FillObjects(){

		runNumber	= event_->run;
		lumiSection	= event_->lumi;
		eventNumber = event_->event;
		numJets		= gt_->Njet;
		MET 		= (gt_->RankETM)/(double)4;

		jetET.clear();
		jetPhi.clear();
		jetEta.clear();
		jetIeta.clear();
		isTauJet.clear();
		isCenJet.clear();
		isFwdJet.clear();

		for(unsigned j = 0; j<numJets; j++){
			jetET.push_back		( ((gt_->Rankjet).at(j))*2*1.8 );	
			jetPhi.push_back	( ((gt_->Phijet).at(j))	); 
			jetIeta.push_back	( ((gt_->Etajet).at(j))	);
			jetEta.push_back	( Ieta2InnerEta((gt_->Etajet).at(j))	);
			isTauJet.push_back	( (gt_->Taujet).at(j)	);
			isCenJet.push_back	(!(gt_->Fwdjet).at(j)	);
			isFwdJet.push_back	( (gt_->Fwdjet).at(j)	);
		}

		tw1 = gt_->tw1.at(2);
		tw2 = gt_->tw2.at(2);

}

// Decimal to binary converter
string L1Trigger::Dec2Bin64(unsigned long int iNum){
	stringstream resultSS; resultSS.str("");

	unsigned long int num = iNum;

	for(int b=63; b>=0; b--){
		bool isSet = (num >= pow(2,b));
		resultSS << isSet;
		if(isSet){ num -= pow(2,b); }
	}
	
	string result;
	result = string((64-resultSS.str().length()),'0') + resultSS.str();


	return result;;

}


// returns a bitmask to extract bit i from another bitmask
unsigned long long  L1Trigger::BitMaskForBit(unsigned int bit) {
	unsigned long long tmp = 1;
	tmp = tmp << bit;
	return tmp;

}

// Returns true if 'bit' is set in tw1/2
bool L1Trigger::IsBitSet(unsigned int bit) {


	unsigned long int val;
	if(bit < 64){
		val = tw1;
	}else if(bit < 128){
		val = tw2;
		bit -= 64;
	}else{
		cerr << "\nERROR: requested bit greater than 128." << endl; exit(1);
	}

	return bool((val&(unsigned long int)pow(2,bit)));
}

// Checks that 'input' is between 'min' and 'max'
bool L1Trigger::OutOfRange(double input, double min, double max){
	return ((min > input) || (input > max));
}


// Returns the inner eta value for the given 'ieta'
double L1Trigger::Ieta2InnerEta(double ieta){
	
	if( (ieta < 0) || (21 < ieta) ){ cerr << "\nERROR: requested ieta " << ieta << " out of bounds"; exit(1); }

	if(ieta<11){
		return (etaLowerEdge[ieta+1]);
	}else{
		return (etaLowerEdge[ieta]);
	}

}


// Returns the outer eta value for the given 'ieta'
double L1Trigger::Ieta2OuterEta(double ieta){
	
	if( (ieta < 0) || (21 < ieta) ){ cerr << "\nERROR: requested ieta " << ieta << " out of bounds"; exit(1); }

	if(ieta<11){
		return (etaLowerEdge[ieta]);
	}else{
		return (etaLowerEdge[ieta+1]);
	}

}

// Returns the ieta corresponding to 'eta'
double L1Trigger::Eta2Ieta(double eta){
	
	if( (eta < -5) || (5 < eta) ){ cerr << "\nERROR: requested eta " << eta << " out of bounds"; exit(1); }

	map<int,float>::iterator ietaIt;
	for(ietaIt = etaLowerEdge.begin(); ietaIt != etaLowerEdge.end(); ++ietaIt){
		float edge = ietaIt->second;	
		if(edge<eta){ return ietaIt->first; }
	}

	return 999;

}

// Determines whether 'iCut' was requested in the definition of the trigger
bool L1Trigger::CutOn(string iCut){

	string tempDef	= " " + definition + " ";
	size_t foundNDef	= tempDef.find(" " + iCut + ":");
	size_t length		= tempDef.length();

	return ( 0 <= foundNDef && foundNDef <= length );

}

// Extracts the thresholds of 'iCut' from the definition of this trigger
pair<float,float> L1Trigger::GetThresholds(string iCut){

	float min = 0;
	float max = 0;

	pair<float,float> result = make_pair(-FLT_MAX,FLT_MAX);

	string tempDef	= " " + ReplaceString(definition,"\t"," ") + " ";


	size_t foundNDef	= tempDef.find(" " + iCut + ":");
	size_t length		= tempDef.length();

	if ( 0 <= foundNDef && foundNDef <= length ){
		string thresholds = tempDef.substr(foundNDef+iCut.length()+2);
		thresholds = thresholds.substr(0,thresholds.find(" "));


		float min, max;
		string smin = thresholds.substr(0,thresholds.find(":"));
		string smax = string(thresholds.substr(thresholds.find(":")+1));

		if(smin.compare("") == 0){ min = -FLT_MAX; }
		else{ min = atof(smin.c_str()); }

		if(smax.compare("") == 0){ max = FLT_MAX; }
		else{ max = atof(smax.c_str()); }

		if( min > max ){
			cout << "ERROR: Min threshold in cut named \"" << iCut << "\" has a greater value than the max: (" << min << ", " << max << ")"  << endl; exit(1);
		}

		result = make_pair(min,max);

	}else{
			cout << "ERROR: Definition of \"" << iCut << "\" is missing threshold definition." << endl; exit(1);
	}

	return result;
}

// Return the average instantaneous rate in kHz
double L1Trigger::GetAverageRatekHz(){

	int nanoDSTprescale	= 10;
	float HzToKhz		= 0.001;

	long int numerator	= (rescaledEventsPassing * nanoDSTprescale * HzToKhz / (double)userPrescale);
	double denominator	= (double)(lumiSectionsAnalyzed.size() * 23.31);

	return (numerator/(double)denominator);
}

// Scan definition string and add trigger names to vector, then return vector
vector<string>* L1Trigger::ScanForTriggers(string iDefinition){

	vector<string>* result = new vector<string>(); result->clear();
	string tempDef = iDefinition + " ";

	// Replace logical operators by spaces
	tempDef = ReplaceString(tempDef, "|", " ");
	tempDef = ReplaceString(tempDef, "&", " ");
	tempDef = ReplaceString(tempDef, "(", " ");
	tempDef = ReplaceString(tempDef, ")", " ");

	size_t found = -1;
	while(tempDef.length() != 0){

		string toAdd; 
		found = tempDef.find(' ');	

		if(!InRange(found,tempDef)){ cerr << "ERROR: invalid definition of composite trigger: " << iDefinition << endl; exit(1); }

		found = tempDef.find(' ');
		if( found == 0 ){
			tempDef = tempDef.substr(1);
		}else{
			result->push_back(string(tempDef.substr(0,found))); // Add trigger name just found
			tempDef = tempDef.substr(found);
		}
	}

	if(result->size() == 0){ cerr << "ERROR: invalid definition of composite trigger: " << iDefinition << endl; exit(1); }
	return result;
}

// String replacement function
string L1Trigger::ReplaceString(string input, string oldStr, string newStr){
	string toReturn = string(input);
	size_t pos = 0;
	while((pos = toReturn.find(oldStr, pos)) != std::string::npos){
		toReturn.replace(pos, oldStr.length(), newStr);
		pos += newStr.length();
	}

	return toReturn;
}

// Check to see whether the string position is within the string
bool L1Trigger::InRange(size_t iPos, string iString){
	return ((0 <= iPos) && (iPos <= iString.length()));
}

int L1Trigger::GetBitPrescale(){
	return bitPrescale;
}

int L1Trigger::GetUserPrescale(){
	return userPrescale;
}

TH1F* L1Trigger::GetHisto(){
	return histo;
}

void L1Trigger::SetHisto(TH1F* iHisto){
	histo = new TH1F(*iHisto);
}

L1Trigger* L1Trigger::Clone(){
	L1Trigger* result = new L1Trigger(*this);
	result->SetHisto(this->GetHisto());
	return result;

}
