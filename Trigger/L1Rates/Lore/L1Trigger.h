
#ifndef L1Trigger_h
#define L1Trigger_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TGraphAsymmErrors.h>
#include <math.h>
#include <map>
#include <vector>
#include <sstream>
#include <iostream>
#include <utility>
#include "L1AnalysisEventDataFormat.h"
#include "L1AnalysisL1MenuDataFormat.h"
#include "L1AnalysisGTDataFormat.h"
#include "mathParser/mathParser.h"


using namespace std;




class L1Trigger {
	private:
		string				name;
		map<int, float> etaLowerEdge;
		string				definition;
		vector<string>*		triggersInDef;
		bool				isComposite;
		bool				isNewL1trigger;



		long double			rawEventsPassing;
		long double			rescaledEventsPassing;
		vector<int>*		rawEventsPassingFundTrigger;
		vector<int>*		fundTriggerPrescale;
		int					firstUserPrescale;
		long int			eventsAnalyzed;
		long int			averageRate;

		map<pair<int,int>,bool> lumiSectionsAnalyzed;


		TH1F*				histo;
		int					runNumber;
		int					lumiSection;
		int					eventNumber;
		int					maxBitPrescale;
		int					numJets;
		vector<float>		jetET;
		vector<float>		jetPhi;
		vector<float>		jetEta;
		vector<float>		jetIeta;
		vector<bool>		isTauJet;
		vector<bool>		isCenJet;
		vector<bool>		isFwdJet;
		double				MET;
		long long int		tw1;
		long long int		tw2;

		int bit;			int bitPrescale;	int userPrescale;
		bool cutOnJetMult;	float minNjets;		float maxNjets;
		bool cutOnTau;		bool beTau;
		bool cutOnFwd;		bool beFwd;
		bool cutOnJetEt;	float minJetEt;		float maxJetEt;
		bool cutOnJetEta;	float minJetEta;	float maxJetEta;
		bool cutOnJetIeta;	float minJetIeta;	float maxJetIeta;
		bool cutOnMET;		float minMET;		float maxMET;	

		MathParser* mathParser;


		long int numEventsAnalyzed;
		long int numEventsPassing;
		L1Analysis::L1AnalysisEventDataFormat*	event_;
		L1Analysis::L1AnalysisL1MenuDataFormat*	l1menu_;
		L1Analysis::L1AnalysisGTDataFormat*		gt_;
		bool passedEvent;
		void FillObjects();
		string Dec2Bin64(unsigned long int);
		unsigned long long BitMaskForBit(unsigned int);
		bool IsBitSet(unsigned int);
		bool OutOfRange(double, double, double);
		double Ieta2InnerEta(double);
		double Ieta2OuterEta(double);
		double Eta2Ieta(double);
		bool CutOn(string);
		pair<float,float> GetThresholds(string);
		string ReplaceString(string,string,string);
		vector<string>* ScanForTriggers(string);
		bool InRange(size_t, string);


	
	public:

		// Default constructor
				L1Trigger(string,string,map<string, L1Trigger*>* iFundTriggers=NULL);
		void	CheckEvent(L1Analysis::L1AnalysisEventDataFormat*, L1Analysis::L1AnalysisL1MenuDataFormat*, L1Analysis::L1AnalysisGTDataFormat*, double);
		void	CheckEvent(L1Analysis::L1AnalysisEventDataFormat*, L1Analysis::L1AnalysisL1MenuDataFormat*, map<string, L1Trigger*>*, double);
		bool	PassedEvent();
		bool	PassedEventModPrescale(int);
		double	GetAverageRatekHz();
		int		GetBitPrescale();
		int		GetUserPrescale();
		TH1F*	GetHisto();
		L1Trigger*	Clone();
		void	SetHisto(TH1F*);

};

#endif

