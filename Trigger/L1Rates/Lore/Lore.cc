/*
	Author..........Nil Valls <nil.valls@cern.ch>
	Date started....29 Aug 2011
	Last mod........23 Oct 2011
	Description:
		L1 trigger rate estimator based on nanoDST ntuples.
*/

#include "Lore.h"

#define Lore_cxx
using namespace std;


int main(int argc, char **argv){

	// Set up nice plot style
	gROOT->Reset();
	gROOT->SetStyle("Plain");
	setTDRStyle();

	// Supress ROOT warnings
	gErrorIgnoreLevel = kError;

	// Keep track of how long things take
	TDatime clock;
	TStopwatch* stopwatch = new TStopwatch();

	print(CYAN, ">>> Starting analysis...");
	stopwatch->Start();

	// Instatiate configuration parser and take the first argument as the config file
	Config theConfig(argv[1], argv);

	// Read in global parameters 
	int	maxEvents					= theConfig.pDouble("maxEvents");

	// Read in output parameters and check for existence of dirs, otherwise create them
	string webDir					= theConfig.pString("webDir");			ReMakeDir(webDir);
	string ntuplesDir				= theConfig.pString("ntuplesDir");

	cout << "\n\t"; PrintURL(webDir);
	cout << "\n\tLoading ntuples from " << ntuplesDir << "\n" << endl;

	// Copy original config file to output dir
	BackUpConfigFile(string(argv[1]), webDir);
	

	
	
	// Read in and add triggers
	vector<pair<string, string> > triggersFromConfig = theConfig.getTemplates("L1trigger_");
	map<string,L1Trigger*>* triggers = new map<string,L1Trigger*>(); triggers->clear();
	for (int t=0; t < triggersFromConfig.size(); t++){
		string triggerName	= triggersFromConfig.at(t).first;
		string triggerDef	= triggersFromConfig.at(t).second;

		(*triggers)[triggerName] = new L1Trigger(triggerName, triggerDef);

		cout << "Found and added trigger \"" << triggerName << "\" with definition:\t" << triggerDef << endl;
	}
	cout << "\n" << endl;

	// Read in and add composite triggers
	vector<pair<string, string> > compTriggersFromConfig = theConfig.getTemplates("compL1trigger_");
	map<string,L1Trigger*>* compTriggers = new map<string,L1Trigger*>(); compTriggers->clear();
	for (int t=0; t < compTriggersFromConfig.size(); t++){
		string triggerName	= compTriggersFromConfig.at(t).first;
		string triggerDef	= compTriggersFromConfig.at(t).second;

		(*compTriggers)[triggerName] = new L1Trigger(triggerName, triggerDef, triggers);

		cout << "Found and added composite trigger \"" << triggerName << "\" with definition:\t" << triggerDef << endl;
	}
	cout << "\n" << endl;

	/*	
	// Read in and add plots
	map<string, Config*> plots = theConfig.getGroups();
	const string plotPrefix = "L1plot_";
	for (map<string, Config*>::iterator i = plots.begin(); i != plots.end(); ++i) {
		string groupName = i->first;
		Config* group = i->second;
		if (groupName.substr(0, plotPrefix.length()) == plotPrefix) {
			groupName = groupName.substr(plotPrefix.length());
			cout << "Found and added plot \"" << groupName << "\"" << endl;

			// Read in and add curves
			vector<pair<string, string> > curvesFromConfig = group->getTemplates("curve");
			map<string,string>* curves = new map<string,string>(); curves->clear();
			for (int t=0; t < curvesFromConfig.size(); t++){
				string curveName	= curvesFromConfig.at(t).first;
				string curveDef	= curvesFromConfig.at(t).second;

				(*triggers)[curveDef] = new L1Trigger(curveDef);
				(*curves)[curveName] = curveDef;

				cout << "Found and added curve \"" << curveName << "\" with definition:\t" << curveDef << endl;
			}

			string xrange	= group->pString("xrange");
			string title	= group->pString("title");

		}
	}
	cout << "\n" << endl;
	//*/

	NewSection(stopwatch);
	
	// Main part of Lore, the analyzer, where the triggers get filled
	Analyzer* 		L1analysis	= new Analyzer(maxEvents, ntuplesDir, triggers, compTriggers);
	L1analysis->Loop();

	NewSection(stopwatch);
	// Plotting
	Plotter*		L1plotter	= new Plotter("",webDir);
	L1plotter->AddTriggers(triggers);
	L1plotter->AddTriggers(compTriggers);
	L1plotter->DrawPlots();

	NewSection(stopwatch);
	cout << ">>> Finished!" << endl;

	// Print output dirs
	cout << "\n" << endl;
	PrintURL(webDir);

	return 0;
}


