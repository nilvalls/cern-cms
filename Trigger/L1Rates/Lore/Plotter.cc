/*
    Author:			Nil Valls <nil.valls@cern.ch>
    Date:			11 Jun 2011
    Description:	Plot stacking class.
    Notes:

*/

#include "Plotter.h"

#define Plotter_cxx
using namespace std;

#define AT __LINE__

// Default constructor
Plotter::Plotter(string iOutputRoot, string iOutputDir){

	// Draw horizontal error bars
	gStyle->SetErrorX(0.5);

	outputDir = iOutputDir;
	outputRoot = iOutputRoot;

	triggers = new map<string, L1Trigger*>(); triggers->clear();

}

// Default destructor
Plotter::~Plotter(){
}


// Add triggers passed from Lore
void Plotter::AddTriggers(map<string, L1Trigger*>* iTriggers){

	map<string,L1Trigger*>::iterator triggerIt;
	for(triggerIt = iTriggers->begin(); triggerIt != iTriggers->end(); ++triggerIt){
		(*triggers)[triggerIt->first] = triggerIt->second->Clone();
	}

}


void Plotter::DrawPlots(){
	cout << ">>> Making plots..." << endl;

	TCanvas* canvas;

	// Loop over all the triggers
	map<string,L1Trigger*>::iterator triggerIt;
	for(triggerIt = triggers->begin(); triggerIt != triggers->end(); ++triggerIt){
		L1Trigger* trigger = triggerIt->second;
		TH1F* histo		= (TH1F*)(trigger->GetHisto()->Clone());
		string plotName = histo->GetName();
		canvas = new TCanvas(plotName.c_str(), plotName.c_str(), 800, 800);
		canvas->cd();
		
		float xLegend	= 0.9;
		float yLegend	= 0.88;
		float dxLegend	= 0.50; float dyLegend	= 0.20; 
		TLegend* legend = new TLegend(xLegend-dxLegend, yLegend-dyLegend, xLegend, yLegend, NULL, "brNDC");
		legend->SetFillColor(kWhite);
		legend->SetFillStyle(0);
		legend->SetBorderSize(1);
		legend->AddEntry(histo,histo->GetName(),"P");

	
		histo->GetXaxis()->SetTitle("Inst Lumi [#times 10^{33}/(cm^{2} s)]");
		histo->GetYaxis()->SetTitle("Rate [kHz]");
		histo->Sumw2();
		histo->SetMarkerStyle(20);
		histo->Draw("E1");
		legend->Draw();
		SaveCanvas(canvas, outputDir, plotName);
	}
	

}



// Save canvas
void Plotter::SaveCanvas(TCanvas* canvas, string dir, string filename){

	// Create output dir if it doesn't exists already
	TString sysCommand = "if [ ! -d " + dir + " ]; then mkdir -p " + dir + "; fi";
	if(gSystem->Exec(sysCommand) > 0){ cout << ">>> ERROR: problem creating dir for plots " << dir << endl; exit(1); }// exit(0);

	// Loop over all file format extensions choosen and save canvas
	vector<string> extension; extension.push_back(".png");
	for( unsigned int ext = 0; ext < extension.size(); ext++){
		canvas->SaveAs( (dir + filename + extension.at(ext)).c_str() );
	}

}















/*
void Plotter::SetAllMCscaleFactor(float iFactor){
	allMCscaleFactor	= iFactor;
}

void Plotter::SetLumi(float iLumi, string iUnits){
	lumi	= iLumi;
	units	= iUnits;
}

void Plotter::SetEffectiveLumi(float iLumi){
	effectiveLumi = iLumi;
}

// Add collision data plots
void Plotter::AddCollisions(vector<HistoWrapper*>* plots, float iScaleFactor){

	// Set of check consistent number of plots
	if (numberOfPlots == 0){
		numberOfPlots	= plots->size();
	}else{
		if ( plots->size() != numberOfPlots ){
			cout << "ERROR: imported vector of collision plots does not have the same amount of plots as the other topologies" << endl;
			exit(1);
		}
	}

	chargeProductApplied = plots->at(0)->ChargeProductApplied();
	collisionHistos		= plots;
	doCollisions		= true;
	normCollisionsDir	= normHistoDir->mkdir("collisions");
	unnormCollisionsDir	= unnormHistoDir->mkdir("collisions");
	collisionsSF		= iScaleFactor;
	topologies.push_back(pair<string,string>("collisions","Collisions"));
}

// Add signal plots
void Plotter::AddSignal(vector<HistoWrapper*>* plots, string iLabel, string iLabelForLegend, int iLineColor, float iCrossSection, float iOtherSF, int iNumEvInDS){

	// Set of check consistent number of plots
	if (numberOfPlots == 0){
		numberOfPlots	= plots->size();
	}else{
		if ( plots->size() != numberOfPlots ){
			cout << "ERROR: imported vector of signal plots does not have the same amount of plots as the other topologies" << endl;
			exit(1);
		}
	}

	chargeProductApplied = plots->at(0)->ChargeProductApplied();
	signals.push_back( pair< string, vector<HistoWrapper*>* >(iLabelForLegend, plots) );
	signalColor.push_back(iLineColor);
	signalOtherSF.push_back(iOtherSF);
	signalCrossSection.push_back(iCrossSection);
	signalNumEvInDS.push_back(iNumEvInDS);
	doSignals	= true;
	normSignalDir.push_back(normHistoDir->mkdir(iLabel.c_str()));
	unnormSignalDir.push_back(unnormHistoDir->mkdir(iLabel.c_str()));
	signalLabels.push_back(iLabel);
	topologies.push_back(pair<string,string>(iLabel,iLabelForLegend));

}

void Plotter::AddQCD( vector<HistoWrapper*>* plots, string iQCDLabel, float iScaleFactor){

	// Set of check consistent number of plots
	if (numberOfPlots == 0){
		numberOfPlots	= plots->size();
	}else{
		if ( plots->size() != numberOfPlots ){
			cout << "ERROR: imported vector of QCD plots does not have the same amount of plots as the other topologies" << endl;
			exit(1);
		}
	}

	QCDHistos		= plots;
	QCDLabel		= iQCDLabel;
	doQCD			= true;
	normQCDDir		= normHistoDir->mkdir("QCD");
	unnormQCDDir	= unnormHistoDir->mkdir("QCD");
	QCDSF			= iScaleFactor;
	topologies.push_back(pair<string,string>("QCD",iQCDLabel));
}

// Subtract LS backgrounds from LS collisions to obtain dd-QCD distros
TH1F* Plotter::BuildQCD(int plotNum){
	
	TH1F* result = NULL;
	int p = plotNum;

	if(!doQCD){ return NULL; }

	// Get LS collisions histo
	result = new TH1F(*(QCDHistos->at(p)->GetHisto()));

	// Loop over all backgrounds
	for ( unsigned int bkg = 0; bkg < LSbackgrounds.size(); bkg++){
		HistoWrapper* backgroundWrapper = ((LSbackgrounds.at(bkg)).second)->at(p);
		TH1F* backgroundPlot = new TH1F(*(((LSbackgrounds.at(bkg)).second)->at(p)->GetHisto())); 


		// Normalize histo
		long double expectedEv = (effectiveLumi*LSbackgroundCrossSection.at(bkg));
		long double obtainedEv = (long double)LSbackgroundNumEvInDS.at(bkg)*((long double)backgroundWrapper->GetNumEvAnalyzed()/(double)backgroundWrapper->GetNumEvInNtuple());
		long double normalization = (expectedEv/obtainedEv)*LSbackgroundOtherSF.at(bkg);
		backgroundPlot->Scale(normalization);

		// Subtract this background from LS collisions
		result->Add(backgroundPlot,-1);
	}

	// Make sure no bins acquire negative values
	for(int bin=1; bin<result->GetNbinsX(); bin++){
		if(result->GetBinContent(bin) < 0){
			result->SetBinContent(bin, 0);
			result->SetBinError(bin, 0);
		}
	}//*/
/*

	if( chargeProductApplied ){
		// Fix charge product plot
		if((string(result->GetName())).compare("ChargeProduct")==0){
			int osBin = -1;
			int lsBin = -1;
			for(int bin=1; bin <= result->GetNbinsX(); bin++){
				if(result->GetBinLowEdge(bin) == -1){ osBin = bin; }	
				if(result->GetBinLowEdge(bin) ==  1){ lsBin = bin; }	
			}
			result->SetBinContent(osBin, result->GetBinContent(lsBin)*QCDSF);
			result->SetBinContent(lsBin, 0);
		}else{
			result->Scale(QCDSF);
		}
	}else{
		// Fix charge product plot
		if((string(result->GetName())).compare("ChargeProduct")==0){
			int osBin = -1;
			int lsBin = -1;
			for(int bin=1; bin <= result->GetNbinsX(); bin++){
				if(result->GetBinLowEdge(bin) == -1){ osBin = bin; }	
				if(result->GetBinLowEdge(bin) ==  1){ lsBin = bin; }	
			}
			result->SetBinContent(osBin, result->GetBinContent(lsBin)*QCDSF);
		}else{
			result->Scale(1+QCDSF);
		}
	}


	return result;

}


// Add plots to subtract from QCD
void Plotter::AddLSBackground(vector<HistoWrapper*>* plots, string labelForLegend, float iCrossSection, float iOtherSF, int iNumEvInDS){


	// Set of check consistent number of plots
	if (numberOfPlots == 0){
		numberOfPlots	= plots->size();
	}else{
		if ( plots->size() != numberOfPlots ){
			cout << "ERROR: imported vector of background plots does not have the same amount of plots as the other topologies" << endl;
			exit(1);
		}
	}

	LSbackgrounds.push_back( pair< string, vector<HistoWrapper*>* >(labelForLegend, plots) );
	LSbackgroundOtherSF.push_back(iOtherSF);
	LSbackgroundCrossSection.push_back(iCrossSection);
	LSbackgroundNumEvInDS.push_back(iNumEvInDS);
}

// Add background plots
void Plotter::AddBackground(vector<HistoWrapper*>* plots, string iLabel, string iLabelForLegend, int iFillColor, float iCrossSection, float iOtherSF, int iNumEvInDS){

	// Set of check consistent number of plots
	if (numberOfPlots == 0){
		numberOfPlots	= plots->size();
	}else{
		if ( plots->size() != numberOfPlots ){
			cout << "ERROR: imported vector of background plots does not have the same amount of plots as the other topologies" << endl;
			exit(1);
		}
	}

	chargeProductApplied = plots->at(0)->ChargeProductApplied();
	backgrounds.push_back( pair< string, vector<HistoWrapper*>* >(iLabelForLegend, plots) );
	backgroundColor.push_back(iFillColor);
	backgroundOtherSF.push_back(iOtherSF);
	backgroundCrossSection.push_back(iCrossSection);
	backgroundNumEvInDS.push_back(iNumEvInDS);
	doBackgrounds	= true;
	normBackgroundDir.push_back(normHistoDir->mkdir(iLabel.c_str()));
	unnormBackgroundDir.push_back(unnormHistoDir->mkdir(iLabel.c_str()));
	backgroundLabels.push_back(iLabel);
	topologies.push_back(pair<string,string>(iLabel,iLabelForLegend));
}

// Perform the stacking and plotting here
EventCounter* Plotter::DrawPlots(){
	EventCounter* eventCounter = new EventCounter();

	// Check that there is at least something to plot
	if ( !(doCollisions || doSignals || doBackgrounds) ){
		cout << "ERROR: nothing to plot." << endl;
		return NULL;
	}

	// Declare canvas and legend
	TCanvas* canvas;
	TLegend *legend;

	// Loop over all plots
	for (unsigned int p = 0; p < numberOfPlots; p++){
		bool doLogx = false;
		bool doLogy = false;
		bool centerLabels = false;
		string outputFilename = "";
		vector<TH1F*> signalHistos; signalHistos.clear();

		float xmin, xmax;
		string xlabel, ylabel;

		// Set up canvas and legend
		float xLegend	= 0.92;
		float yLegend	= 0.88;

		float dxLegend	= 0.20; float dyLegend	= 0.30; 
		legend = new TLegend(xLegend-dxLegend, yLegend-dyLegend, xLegend, yLegend, NULL, "brNDC");
		legend->SetBorderSize(1);
		legend->SetFillColor(kWhite);
		legend->SetFillStyle(1001);
		string plotname = "test";
		canvas = new TCanvas(plotname.c_str(), plotname.c_str(), 800, 800);
		canvas->cd();


		TH1F* collisions;
		if (doCollisions){
			if(outputFilename.length() < 1){ outputFilename = collisionHistos->at(p)->GetOutputFilename(); }
			collisions	= new TH1F(*(collisionHistos->at(p)->GetHisto()));

			// Add collisions to event count
			string histoName = string(collisions->GetName());
			if((histoName.compare(plotNameToCount)==0) && (histoName.length() == plotNameToCount.length())){
				TH1F* toAdd = new TH1F(*collisions);
				eventCounter->AddCollisions("Collisions",toAdd);	
			}

			doLogx = collisionHistos->at(p)->DoLogx();
			doLogy = collisionHistos->at(p)->DoLogy();
			centerLabels = collisionHistos->at(p)->CenterLabels();

			normCollisionsDir->cd();
			collisions->Write();
			unnormCollisionsDir->cd();
			collisions->Write();

			canvas->SetName(collisions->GetName());
			xmin = collisionHistos->at(p)->GetXminVis();
			xmax = collisionHistos->at(p)->GetXmaxVis();
			xlabel = collisionHistos->at(p)->GetXlabel().c_str();
			ylabel = collisionHistos->at(p)->GetYlabel().c_str();
			collisions->GetXaxis()->SetRangeUser(xmin,xmax);
			collisions->GetXaxis()->CenterLabels(centerLabels);
			collisions->GetXaxis()->SetTitle(xlabel.c_str());
			collisions->GetYaxis()->SetTitle(ylabel.c_str());
			legend->AddEntry(collisions, "Collisions", "lep");
			collisions->SetMarkerStyle(20);
			collisions->Scale(collisionsSF);
			//cout << " > " << collisions->GetName() << " " << collisions->Integral() << endl;

		}

		// Setup background
		THStack* stack;
		TH1F* errorh = NULL;
		if (doBackgrounds){

			stack = new THStack( plotname.c_str(), plotname.c_str() );

			// Loop over all backgrounds
			for ( unsigned int bkg = 0; bkg < backgrounds.size(); bkg++){
				if(outputFilename.length() < 1){ outputFilename = ((backgrounds.at(bkg)).second)->at(p)->GetOutputFilename(); }
				TH1F* backgroundPlot = new TH1F(*(((backgrounds.at(bkg)).second)->at(p)->GetHisto())); 
				HistoWrapper* backgroundWrapper = ((backgrounds.at(bkg)).second)->at(p);


				doLogx = ((backgrounds.at(bkg)).second)->at(p)->DoLogx();
				doLogy = ((backgrounds.at(bkg)).second)->at(p)->DoLogy();
				centerLabels = ((backgrounds.at(bkg)).second)->at(p)->CenterLabels();
				backgroundPlot->GetXaxis()->SetRangeUser(((backgrounds.at(bkg)).second)->at(p)->GetXminVis(),((backgrounds.at(bkg)).second)->at(p)->GetXmaxVis());
				backgroundPlot->GetXaxis()->SetTitle(((backgrounds.at(bkg)).second)->at(p)->GetXlabel().c_str());
				backgroundPlot->GetXaxis()->CenterLabels(centerLabels);
				backgroundPlot->GetYaxis()->SetTitle(((backgrounds.at(bkg)).second)->at(p)->GetYlabel().c_str());
				if(!doCollisions){
					xmin = ((backgrounds.at(bkg)).second)->at(p)->GetXminVis();
					xmax = ((backgrounds.at(bkg)).second)->at(p)->GetXmaxVis();
					xlabel = ((backgrounds.at(bkg)).second)->at(p)->GetXlabel().c_str();
					ylabel = ((backgrounds.at(bkg)).second)->at(p)->GetYlabel().c_str();
				}

				// Save unormalized histo to file
				(unnormBackgroundDir.at(bkg))->cd();
				backgroundPlot->Write();


				// Normalize histo and save to file
				//long double expectedEv = (effectiveLumi*backgroundCrossSection.at(bkg));
				//long double obtainedEv = (long double)backgroundNumEvInDS.at(bkg)*((long double)backgroundWrapper->GetNumEvAnalyzed()/(double)backgroundWrapper->GetNumEvInNtuple());
				long double expectedEv = (effectiveLumi*backgroundCrossSection.at(bkg));
				long double obtainedEv = (long double)backgroundNumEvInDS.at(bkg)*((long double)backgroundWrapper->GetNumEvAnalyzed()/(double)backgroundWrapper->GetNumEvInNtuple());
				long double normalization = (expectedEv/obtainedEv)*backgroundOtherSF.at(bkg)*allMCscaleFactor;
				backgroundPlot->Scale(normalization);
				/*if(p==0){ 
					cout << "Normalization for " << (backgrounds.at(bkg).first) << ":\t" << normalization << endl;
					cout << "Expected events:\t" <<  expectedEv << "\tObtained events: " << obtainedEv << endl;
					cout << "Events in dataset:\t" << backgroundNumEvInDS.at(bkg) << endl;
					cout << "Events analyzed:\t" << backgroundWrapper->GetNumEvAnalyzed() << endl;
					cout << "Events in nTuple:\t" << backgroundWrapper->GetNumEvInNtuple() << endl;
					
				}//*/

				/*
			
				// Add background to event count
				string histoName = string(backgroundPlot->GetName());
				if((histoName.compare(plotNameToCount)==0) && (histoName.length() == plotNameToCount.length())){
					eventCounter->AddBackground(backgroundLabels.at(bkg), backgroundPlot);	
				}

				(normBackgroundDir.at(bkg))->cd();
				backgroundPlot->Write();

				// Prepare for canvas plotting
				canvas->SetName(backgroundPlot->GetName());
				backgroundPlot->SetFillColor(backgroundColor.at(bkg));

				// Statistical errors on backgrounds
				if (errorh == NULL && (backgroundPlot->Integral() > 0) ){ errorh = (TH1F*)backgroundPlot->Clone(); }
				else if( backgroundPlot->Integral() > 0 ){ errorh->Add(backgroundPlot); }

				// Add background to stack, and legend
				if (backgroundPlot->Integral() > 0){ stack->Add(backgroundPlot);
				}
				legend->AddEntry(backgroundPlot, (backgrounds.at(bkg).first).c_str(), "f");
			}

			if(!doCollisions){
				if( stack->GetMaximum() > 0){
					stack->Draw();
					stack->GetXaxis()->SetRangeUser(xmin,xmax);
					stack->GetYaxis()->SetRangeUser(0.01,1.5*stack->GetMaximum());
					stack->GetXaxis()->SetTitle(xlabel.c_str());
					stack->GetXaxis()->CenterLabels(centerLabels);
					stack->GetYaxis()->SetTitle(ylabel.c_str());
				}
			}

			// QCD plot
			if (doQCD){
				TH1F* QCD 		= BuildQCD(p);


				doLogx = QCDHistos->at(p)->DoLogx();
				doLogy = QCDHistos->at(p)->DoLogy();
				centerLabels = QCDHistos->at(p)->CenterLabels();

				xlabel = QCDHistos->at(p)->GetXlabel().c_str();
				ylabel = QCDHistos->at(p)->GetYlabel().c_str();
				QCD->GetXaxis()->SetTitle(xlabel.c_str());
				QCD->GetXaxis()->CenterLabels(centerLabels);
				QCD->GetYaxis()->SetTitle(ylabel.c_str());

				unnormQCDDir->cd();
				QCD->Write();

				normQCDDir->cd();
				QCD->Write();

				// Add background to event count
				string histoName = string(QCD->GetName());
				if((histoName.compare(plotNameToCount)==0) && (histoName.length() == plotNameToCount.length())){
					eventCounter->AddBackground("QCD",QCD);	
				}

				canvas->SetName(QCD->GetName());
				QCD->SetFillColor(kGreen-5);
				if (errorh == NULL && QCD->Integral() > 0 ){ errorh = (TH1F*)QCD->Clone(); }
				else if( QCD->Integral() > 0 ){ errorh->Add(QCD); }
				if( QCD->Integral() > 0){ stack->Add(QCD); }
				legend->AddEntry(QCD, QCDLabel.c_str(), "f");
			}
		}

		if (doSignals){

			// Loop over all signals
			for ( unsigned int signal = 0; signal < signals.size(); signal++){
				if(outputFilename.length() < 1){ outputFilename = ((signals.at(signal)).second)->at(p)->GetOutputFilename(); }
				TH1F* signalPlot = new TH1F(*(((signals.at(signal)).second)->at(p)->GetHisto())); 
				HistoWrapper* signalWrapper = ((signals.at(signal)).second)->at(p);
				doLogx = ((signals.at(signal)).second)->at(p)->DoLogx();
				doLogy = ((signals.at(signal)).second)->at(p)->DoLogy();
				centerLabels = ((signals.at(signal)).second)->at(p)->CenterLabels();
				signalPlot->GetXaxis()->SetRangeUser(((signals.at(signal)).second)->at(p)->GetXminVis(),((signals.at(signal)).second)->at(p)->GetXmaxVis());
				signalPlot->GetXaxis()->SetTitle(((signals.at(signal)).second)->at(p)->GetXlabel().c_str());
				signalPlot->GetXaxis()->CenterLabels(centerLabels);
				signalPlot->GetYaxis()->SetTitle(((signals.at(signal)).second)->at(p)->GetYlabel().c_str());

				// Save unormalized histo to file
				(unnormSignalDir.at(signal))->cd();
				signalPlot->Write();

				// Normalize histo and save to file
				long double expectedEv = (effectiveLumi*signalCrossSection.at(signal));
				long double obtainedEv = (long double)signalNumEvInDS.at(signal)*((long double)signalWrapper->GetNumEvAnalyzed()/(double)signalWrapper->GetNumEvInNtuple());
				long double normalization = expectedEv/obtainedEv;
				signalPlot->Scale(normalization);


				// Add signal to event count
				string histoName = string(signalPlot->GetName());
				if((histoName.compare(plotNameToCount)==0) && (histoName.length() == plotNameToCount.length())){
					eventCounter->AddSignal(signalLabels.at(signal),signalPlot);	
				}

				(normSignalDir.at(signal))->cd();
				signalPlot->Write();


				// Prepare for canvas plotting
				canvas->SetName(signalPlot->GetName());
				signalPlot->SetFillStyle(0);
				signalPlot->SetLineWidth(3);
				signalPlot->SetLineColor(signalColor.at(signal));

				legend->AddEntry(signalPlot, (signals.at(signal).first).c_str(), "l");
				signalHistos.push_back(signalPlot);
			}
		}


		// Error on backgrounds
		if (errorh != NULL){
			for(int bin = 0; bin <= errorh->GetNbinsX(); bin++){ errorh->SetBinError(bin,sqrt(errorh->GetBinContent(bin))); }
			errorh->SetLineColor(kGray+3);
			errorh->SetLineWidth(1);
			errorh->SetLineStyle(3);
			errorh->SetMarkerStyle(0);
			errorh->SetFillColor(kGray+3);
			errorh->SetFillStyle(3001);
		}
	
		// Draw stack of signal + backgrounds
		float collMax = 0;
		float stackMax = 0;
		if(doCollisions){	collMax	= collisions->GetMaximum() + sqrt(collisions->GetMaximum()); }
		if(doBackgrounds){	stackMax	= stack->GetMaximum() + sqrt( stack->GetMaximum()); }
		if(doCollisions){ collisions->GetYaxis()->SetRangeUser(0.01,1.1*max(collMax,stackMax)); }
		if(stackMax > 0 ){ stack->Draw("HIST"); stack->GetYaxis()->SetRangeUser(0.01,1.5*max(collMax,stackMax)); }

		bool collisionsFirst = true;
		if ((doBackgrounds || doSignals)){
			if ((!doCollisions) || collMax < stackMax ){
				collisionsFirst = false;
			}else{
				collisionsFirst = true;
			}
		}else{
			collisionsFirst = true;
		}

		if (doCollisions && collisionsFirst){
			collisions->Draw("EP");
			if(doBackgrounds || doSignals){
				if(stackMax > 0 ){ stack->Draw("sameHIST"); } if (errorh != NULL){ errorh->Draw("sameE2"); }
				collisions->Draw("sameEP");
				collisions->Draw("sameAXIS"); 
			}
		}else if((doBackgrounds || doSignals) && (!collisionsFirst)){
			if(doCollisions){ 
				collisions->Draw("EP"); 
				if(stackMax > 0 ){ stack->Draw("sameHIST"); } if (errorh != NULL){ errorh->Draw("sameE2"); } 
				collisions->Draw("sameEP"); 
				collisions->Draw("sameAXIS"); 
			}else{
				if(stackMax > 0 ){ stack->Draw("HIST"); } if (errorh != NULL){ errorh->Draw("sameE2"); }
				if(stackMax > 0 ){
					stack->GetHistogram()->Draw("sameAXIS");
					stack->GetXaxis()->CenterLabels(centerLabels);
					stack->GetXaxis()->SetTitle(xlabel.c_str());
					stack->GetYaxis()->SetTitle(ylabel.c_str());
					stack->GetXaxis()->SetRangeUser(xmin,xmax);
					stack->GetYaxis()->SetRangeUser(0.01,1.5*stack->GetMaximum());
				}
			}
		}
		if (doSignals){
			for ( unsigned int signalHisto = 0; signalHisto < signalHistos.size(); signalHisto++){
				(signalHistos.at(signalHisto))->Draw("same");
			}
		}
			
		// Extra plot info
		float xPlotInfo		= 1.00;
		float yPlotInfo		= 0.95;

		float dxPlotInfo	= 0.40; float dyPlotInfo	= 0.07; 
		TPaveText *plotInfo = new TPaveText(xPlotInfo-dxPlotInfo, yPlotInfo-dyPlotInfo, xPlotInfo, yPlotInfo, "brNDC");
		plotInfo->SetBorderSize(0);
		plotInfo->SetLineColor(0);
		plotInfo->SetFillColor(kWhite);
		stringstream lumiSS;
		lumiSS.str("");
		lumiSS << "#tau_{h}#tau_{h} channel " << endl;
		if( effectiveLumi>0 ){lumiSS << "(" << GetNiceLumi(effectiveLumi,units) << ")"; }
		plotInfo->AddText((lumiSS.str()).c_str());
		plotInfo->AddText("CMS Preliminary");
		plotInfo->SetFillStyle(0);
		plotInfo->Draw();
		legend->Draw();

		// Save canvas
		SaveCanvas(canvas, outputDir, outputFilename);
		if(doLogx || doLogy){
			if(doLogx){ canvas->SetLogx(1); }
			if(doLogy){ canvas->SetLogy(1); }
			SaveCanvas(canvas, outputDir, string(outputFilename+"_log"));
			canvas->SetLogx(0);
			canvas->SetLogy(0);
		}

		stacksDir->cd();
		canvas->Write();

		// MET scale factor
		/*if(doCollisions && doBackgrounds){
			if((string(collisions->GetName())).compare("MET") == 0){
				TH1F* sf = (TH1F*)collisions->Clone();
				sf->Divide(errorh);
				for(int i=1; i<=sf->GetNbinsX(); i++){ 
					float met = collisions->GetBinLowEdge(i);
					if (met>100){ break; }
					cout << met << ",";
				}
				cout << endl;
				cout << "\n" << endl;

				for(int i=1; i<=sf->GetNbinsX(); i++){
					float met = collisions->GetBinLowEdge(i);
					if (met>100){ break; }
					//cout << collisions->GetBinLowEdge(i) << ": ";
					//cout << collisions->GetBinContent(i) << " ";
					//cout << errorh->GetBinContent(i) << " = ";
					//cout << sf->GetBinContent(i) << endl;
					cout << sf->GetBinContent(i) << ","; 
				}
				cout << endl;
			}
		}//*/
		/*
		
	}
	histoFile->Close();

	return eventCounter;
}


string Plotter::GetNiceLumi(float iLumi, string iUnits){
	string result;

	map<string,int> suf2mag; suf2mag["mb"] = 12; suf2mag["ub"] = 9; suf2mag["ub"] = 6; suf2mag["nb"] = 3; suf2mag["pb"] = 0; suf2mag["fb"] = -3; suf2mag["ab"] = -6;
	map<int,string> mag2suf; mag2suf[12] = "mb"; mag2suf[9] = "ub"; mag2suf[6] = "ub"; mag2suf[3] = "nb"; mag2suf[0] = "pb"; mag2suf[-3] = "fb"; mag2suf[-6] = "ab";

	int lumiMag = 3*((int)(((int)log10(iLumi)) / 3));
	int unitsMag = suf2mag[iUnits];
	int targetMag = unitsMag - lumiMag;

	float targetLumi = ((iLumi/(double)pow(10,lumiMag)) + 0.05)*10;
	targetLumi = ((int)targetLumi)/(double)10.0;

	stringstream ssout; ssout.str("");	
	if( ((int)(targetLumi*10)) == (((int)targetLumi)*10) ){ ssout << ((int)targetLumi) << ".0"; } 
	else{ ssout << targetLumi; }
	ssout << "/" << mag2suf[targetMag];
	result = ssout.str();

	return result;
}

*/
