#include "L1AnalysisGTDataFormat.h"
#include "L1AnalysisEventDataFormat.h"

//#ifdef __MAKECINT__
#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all structs;
#pragma link off all functions;


#pragma link C++ struct L1Analysis::L1AnalysisGTDataFormat+;
#pragma link C++ struct L1Analysis::L1AnalysisEventDataFormat+;
#pragma link C++ struct L1Analysis::L1AnalysisL1MenuDataFormat+;

#endif
