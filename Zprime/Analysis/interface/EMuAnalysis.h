#include <memory>
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDAnalyzer.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/MessageLogger/interface/MessageLogger.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "CommonTools/UtilAlgos/interface/TFileService.h"
#include "HighMassAnalysis/Analysis/interface/HiMassTauAnalysis.h"
#include "TFile.h"
#include "TTree.h"

class EMuAnalysis: public HiMassTauAnalysis{
  public:
    //explicit EMuAnalysis();			      
    explicit EMuAnalysis(const edm::ParameterSet&);  
    ~EMuAnalysis();
    
  
  private:
    void fillNtuple();
    void setupBranches();
    void initializeVectors();
    void clearVectors();
    
    //define tree variables
    TTree* _EMuTree;
        
    vector<double> pdfWeightVector;
    reco::Candidate::LorentzVector Muon;
    reco::Candidate::LorentzVector Electron;
    reco::Candidate::LorentzVector MET;
    
    //event info
    int _runNumber;
    int _eventNumber;
    int _passedHLTMu9;
    int _passedHLTMu11;
    int _passedHLTMu15;
    int _passedHLTIsoMu9;
    int _passedHLTIsoMu11;
    int _passedHLTIsoMu13;
    int _passedHLTIsoMu15;
    int _passedHLTMu5Elec9;
    int _passedHLTMu8Elec8;
    int _passedHLTMu11Elec8;

    vector<double> _PDFWeights;
    double _ISRGluonWeight;
    double _ISRGammaWeight;
    double _FSRWeight;
    
    vector<unsigned int> *_muonMotherId;
    vector<int> *_muonMatched;
    vector<float> *_muonE;
    vector<float> *_muonEt; 
    vector<float> *_muonPt; 
    vector<float> *_muonCharge;
    vector<float> *_muonEta;
    vector<float> *_muonPhi;
    vector<float> *_muonIpVtx;
    vector<float> *_muonIpVtxError;
    vector<unsigned int> *_muonValidHits;
    vector<float> *_muonNormChiSqrd;
    
    vector<float> *_muonEcalIsoDr03;
    vector<float> *_muonTrkIsoDr03;
    vector<float> *_muonCaloIsoDr03;
    
    vector<float> *_muonEcalIsoDr04;
    vector<float> *_muonTrkIsoDr04;
    
    vector<float> *_muonEcalIsoDr05;   
    vector<float> *_muonHadIsoDr05;    
    vector<float> *_muonTrkIsoDr05;	
    vector<float> *_muonCaloIsoDr05;	
    vector<float> *_muonCaloComp;    
    
    vector<int> *_muonIsGlobalMuon;
    vector<int> *_muonisTrackerMuon;
    vector<int> *_muonIsGlobalPromptTight;
    vector<int> *_muonIsTMLastStationLoose;
    vector<int> *_muonIsTMLastStationTight;
    vector<int> *_muonIsTM2DCompatibilityLoose;
    vector<int> *_muonIsTM2DCompatibilityTight;
    vector<float> *_muonPionVeto;
    vector<unsigned int> *_muonInJet;

    vector<unsigned int> *_eMotherId;
    vector<float> *_zeeMass;
    vector<float> *_zeePtAsymm;
    vector<int> *_eMatched;
    vector<int> *_ePdgId;
    vector<int> *_eMotherPdgId;
    vector<float> *_eE;
    vector<float> *_eEt;
    vector<float> *_ePt;
    vector<float> *_eCharge;
    vector<float> *_eEta;
    vector<float> *_ePhi;
    vector<float> *_eSigmaEtaEta;
    vector<float> *_eSigmaIEtaIEta;
    vector<float> *_eEOverP;
    vector<float> *_eHOverEm;
    vector<float> *_eDeltaPhiIn;
    vector<float> *_eDeltaEtaIn;
    
    vector<float> *_eEcalIsoPat;
    vector<float> *_eHcalIsoPat;
    vector<float> *_eTrkIsoPat;
    vector<float> *_eIsoPat;
    
    vector<float> *_heepTrkIso;
    vector<float> *_heepEcalIso;
    vector<float> *_heepHcalIso;
    vector<float> *_heepHcalIsoDepth1;  			  						    
    vector<float> *_heepHcalIsoDepth2;  			  						    
    vector<float> *_heepEcalHcalIsoDepth1;			  						    

    
    vector<float> *_eUserEcalIso;
    vector<float> *_eUserHcalIso;
    vector<float> *_eUserTrkIso;
    
    vector<float> *_eSCE1x5;
    vector<float> *_eSCE2x5;
    vector<float> *_eSCE5x5;
    vector<float> *_eIp;
    vector<float> *_eIpError;
    vector<float> *_eIpError_ctf;
    vector<float> *_eIp_ctf;
    vector<int> *_eClass;
    vector<float> *_eIdRobustTight;
    vector<float> *_eIdRobustLoose;
    vector<float> *_eIdTight;
    vector<float> *_eIdLoose;
    vector<float> *_eIdHighEnergy;
    
    vector<int> *_heepPassedEt;
    vector<int> *_heepPassedPt;
    vector<int> *_heepPassedDetEta;
    vector<int> *_heepPassedCrack;
    vector<int> *_heepPassedDEtaIn;
    vector<int> *_heepPassedDPhiIn;
    vector<int> *_heepPassedHadem;
    vector<int> *_heepPassedSigmaIEtaIEta;
    vector<int> *_heepPassed2by5Over5By5;
    vector<int> *_heepPassedEcalHad1Iso;
    vector<int> *_heepPassedHad2Iso;
    vector<int> *_heepPassedTrkIso;
    vector<int> *_heepPassedEcalDriven;
    vector<int> *_heepPassedAllCuts;
    
    vector<unsigned int> *_eMissingHits;
    
    vector<float> *_mEt;
    vector<float> *_eMEtMass;
    vector<float> *_muMEtMass;
    vector<float> *_eMEtMt;
    vector<float> *_muMEtMt;
    
    vector<float> *_eMuMass;
    vector<float> *_eMuCosDPhi;
    vector<float> *_eMuDelatR;
    vector<float> *_eMuMetMass;
    vector<float> *_eMuCollMass;
    vector<float> *_eMuPZetaVis;
    vector<float> *_eMuPZeta;

    //-------Jet info
    vector<float> *_jetPt;
    vector<float> *_jetEt;
    vector<float> *_jetE;
    vector<float> *_jetEta;
    vector<float> *_jetPhi;
    vector<float> *_jetEmFraction;
    
    //------Gen resolution
    vector<float> *_egenE;
    vector<float> *_egenPt;
    vector<float> *_egenEta;
    vector<float> *_egenPhi; 
    vector<float> *_muongenE;
    vector<float> *_muongenPt;
    vector<float> *_muongenEta;
    vector<float> *_muongenPhi; 

    vector<float> *_gsfElectronE;
    vector<int> *_isEEv;
    vector<int> *_isEBv;
    vector<int> *_isEBEEGap;   
    vector<int> *_isEBEtaGap;  
    vector<int> *_isEBPhiGap;  
    vector<int> *_isEEDeeGap;  
    vector<int> *_isEERingGap; 
 
    vector<bool> *_eEcalDrivenv;
    vector<bool> *_eTrkDrivenv;

    vector<float> *_bJetDiscrByTrkCnt;
    vector<float> *_bJetDiscrByTrkCntHiPurity;
    vector<float> *_bJetDiscrBySimpleSecVtx;
    vector<float> *_bJetDiscrBySimpleSecVtxHiPurity;
    vector<float> *_bJetDiscrByComSecVtx;
    vector<unsigned int> *_nBtagsHiEffTrkCnt;
    vector<unsigned int> *_nBtagsHiPurityTrkCnt;
    vector<unsigned int> *_nBTagsHiEffSimpleSecVtx;
    vector<unsigned int> *_nBTagsHiPuritySimpleSecVtx;
    vector<unsigned int> *_nBTagsCombSecVtx;
    
    vector<float> *_extraTrkPtSum;
    vector<float> *_leptonMetCosDphi;
    
    float _jetBtagHiDiscByTrkCntHiEff;
    float _jetBtagHiDiscByTrkCntHiPurity;
    float _jetBtagHiDiscBySimpleSecVtxHiEff;
    float _jetBtagHiDiscBySimpleSecVtxHiPurity;
    float _jetBtagHiDiscByCombSecVtx;
    vector<float> *_jetSumEt;
    vector<float> *_jetMETSumEt;

    vector<unsigned int> *_nJets;

};
