#include <memory>
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDAnalyzer.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/MessageLogger/interface/MessageLogger.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "CommonTools/UtilAlgos/interface/TFileService.h"
#include "HighMassAnalysis/Analysis/interface/HiMassTauAnalysis.h"
#include "DataFormats/Common/interface/ValueMap.h"
#include "CommonTools/ParticleFlow/test/PFIsoReaderDemo.h"
#include "TFile.h"
#include "TTree.h"

class ElecTauTauAnalysis: public HiMassTauAnalysis{
  public:
    //explicit ElecTauTauAnalysis();			      
    explicit ElecTauTauAnalysis(const edm::ParameterSet&);  
    ~ElecTauTauAnalysis();
    
  
  private:
    void fillNtuple();
    void setupBranches();
    void initializeVectors();
    void clearVectors();
    
    //define tree variables
    TTree* _MuTauTauTree;
    
    //event info

    vector<double> _PDFWeights;
    double _ISRGluonWeight;
    double _ISRGammaWeight;
    double _FSRWeight;
    
    unsigned int _nVtx;
    unsigned int _nGoodVtx;
    
    float _MEt;
    
    bool _zEvent;
    float _zMassDiff;
    float _zPtAsymm;
    
    unsigned int _nBtagsHiEffTrkCnt ;
    unsigned int _nBTagsHiEffSimpleSecVtx ;
    unsigned int _nBtagsHiPurityTrkCnt ;
    unsigned int _nBTagsHiPuritySimpleSecVtx ;
    unsigned int _nBTagsCombSecVtx;
    
    float _genLevelElecPt;
    float _genLevelElecPhi;
    float _genLevelElecEta;
    float _genLevelTau1Pt;
    float _genLevelTau1Phi;
    float _genLevelTau1Eta;
    float _genLevelTau2Pt;
    float _genLevelTau2Phi;
    float _genLevelTau2Eta;
    
    //LeadTau Info
    vector<unsigned int> *_leadTauMatched;
    vector<unsigned int> *_leadTauMotherId;
    vector<float> *_leadTauGenPt;
    vector<float> *_leadTauGenE;
    vector<float> *_leadTauGenEta;
    vector<float> *_leadTauGenPhi;
    
    vector<float> *_leadTauPt;
    vector<float> *_leadTauEnergy;
    vector<float> *_leadTauEta;
    vector<float> *_leadTauPhi;
    vector<float> *_leadTauCharge;
    vector<float> *_leadTauLeadChargedCandPt;
    vector<float> *_leadTauLeadChargedCandCharge;
    vector<unsigned int> *_leadTauLeadChargedCandRecHitsSize;
    vector<double> *_leadTauLeadChargedCandDxyVtx;
    vector<double> *_leadTauLeadChargedCandDxyBS;
    vector<double> *_leadTauLeadChargedCandDxyError;
    vector<double> *_leadTauLeadChargedCandDzVtx;
    vector<double> *_leadTauLeadChargedCandDzBS;
    vector<double> *_leadTauLeadChargedCandDzError;
    vector<double> *_leadTauLeadChargedCandVz;
    vector<double> *_leadTauLeadChargedCandVy;
    vector<double> *_leadTauLeadChargedCandVx;
    vector<double> *_leadTauDeltaZVtx;
    vector<int> *_leadTauNProngs;
    vector<float> *_leadTauEmFraction;
    vector<float> *_leadTauHcalTotOverPLead;
    vector<float> *_leadTauHcalMaxOverPLead;
    vector<float> *_leadTauHcal3x3OverPLead;
    vector<float> *_leadTauElectronPreId;
    vector<float> *_leadTauModifiedEOverP;
    vector<float> *_leadTauBremsRecoveryEOverPLead;
    vector<float> *_leadTauLTSignedIp;
    vector<float> *_leadTauTrkIsoSumPt;
    vector<float> *_leadTauECALIsoSumEt;
    vector<unsigned int> *_leadTauIdByDecayModeFinding;
    vector<unsigned int> *_leadTauIdByVLooseIsolation;
    vector<unsigned int> *_leadTauIdByLooseIsolation;
    vector<unsigned int> *_leadTauIdByMediumIsolation;
    vector<unsigned int> *_leadTauIdByTightIsolation;
    vector<unsigned int> *_leadTauIdByVLooseCombinedIsolationDBSumPtCorr;
    vector<unsigned int> *_leadTauIdByLooseCombinedIsolationDBSumPtCorr;
    vector<unsigned int> *_leadTauIdByMediumCombinedIsolationDBSumPtCorr;
    vector<unsigned int> *_leadTauIdByTightCombinedIsolationDBSumPtCorr;
    vector<unsigned int> *_leadTauIdByLooseElectronRejection;
    vector<unsigned int> *_leadTauIdByMediumElectronRejection;
    vector<unsigned int> *_leadTauIdByTightElectronRejection;
    vector<unsigned int> *_leadTauIdByLooseElecRejection;
    vector<unsigned int> *_leadTauIdByTightElecRejection;
    vector<double> *_leadTauMEtMt;
    
    //SubLeadTau Info
    vector<unsigned int> *_subLeadTauMatched;
    vector<unsigned int> *_subLeadTauMotherId;
    vector<float> *_subLeadTauGenPt;
    vector<float> *_subLeadTauGenE;
    vector<float> *_subLeadTauGenEta;
    vector<float> *_subLeadTauGenPhi;
    
    vector<float> *_subLeadTauPt;
    vector<float> *_subLeadTauEnergy;
    vector<float> *_subLeadTauEta;
    vector<float> *_subLeadTauPhi;
    vector<float> *_subLeadTauCharge;
    vector<float> *_subLeadTauLeadChargedCandPt;
    vector<float> *_subLeadTauLeadChargedCandCharge;
    vector<unsigned int> *_subLeadTauLeadChargedCandRecHitsSize;
    vector<double> *_subLeadTauLeadChargedCandDxyVtx;
    vector<double> *_subLeadTauLeadChargedCandDxyBS;
    vector<double> *_subLeadTauLeadChargedCandDxyError;
    vector<double> *_subLeadTauLeadChargedCandDzVtx;
    vector<double> *_subLeadTauLeadChargedCandDzBS;
    vector<double> *_subLeadTauLeadChargedCandDzError;
    vector<double> *_subLeadTauLeadChargedCandVx;
    vector<double> *_subLeadTauLeadChargedCandVy;
    vector<double> *_subLeadTauLeadChargedCandVz;
    vector<double> *_subLeadTauDeltaZVtx;
    vector<int> *_subLeadTauNProngs;
    vector<float> *_subLeadTauEmFraction;
    vector<float> *_subLeadTauHcalTotOverPLead;
    vector<float> *_subLeadTauHcalMaxOverPLead;
    vector<float> *_subLeadTauHcal3x3OverPLead;
    vector<float> *_subLeadTauElectronPreId;
    vector<float> *_subLeadTauModifiedEOverP;
    vector<float> *_subLeadTauBremsRecoveryEOverPLead;
    vector<float> *_subLeadTauLTSignedIp;
    vector<float> *_subLeadTauTrkIsoSumPt;
    vector<float> *_subLeadTauECALIsoSumEt;
    vector<unsigned int> *_subLeadTauIdByDecayModeFinding;
    vector<unsigned int> *_subLeadTauIdByVLooseIsolation;
    vector<unsigned int> *_subLeadTauIdByLooseIsolation;
    vector<unsigned int> *_subLeadTauIdByMediumIsolation;
    vector<unsigned int> *_subLeadTauIdByTightIsolation;
    vector<unsigned int> *_subLeadTauIdByVLooseCombinedIsolationDBSumPtCorr;
    vector<unsigned int> *_subLeadTauIdByLooseCombinedIsolationDBSumPtCorr;
    vector<unsigned int> *_subLeadTauIdByMediumCombinedIsolationDBSumPtCorr;
    vector<unsigned int> *_subLeadTauIdByTightCombinedIsolationDBSumPtCorr;
    vector<unsigned int> *_subLeadTauIdByLooseElectronRejection;
    vector<unsigned int> *_subLeadTauIdByMediumElectronRejection;
    vector<unsigned int> *_subLeadTauIdByTightElectronRejection;
    vector<unsigned int> *_subLeadTauIdByLooseElecRejection;
    vector<unsigned int> *_subLeadTauIdByTightElecRejection;
    vector<double> *_subLeadTauMEtMt;
    
    //diTau Info
    vector<float> *_diHadTauCosDPhi;
    vector<float> *_diHadTauDelatR;
    vector<double> *_diHadTauPZeta;
    vector<double> *_diHadTauPZetaVis;
    vector<double> *_diHadTauMass;
    vector<double> *_diHadTauMetMass;
    vector<double> *_diHadTauCollMass;
    
    //from candidate
    vector<double> *_diHadCandPt;
    vector<double> *_diHadCandEt;
    vector<double> *_diHadCandEta;
    vector<double> *_diHadCandPhi;
    vector<double> *_diHadCandMass;
    vector<double> *_diHadCandCharge;
  
    //Elec Info
    vector<unsigned int> *_elecMatched;
    vector<unsigned int> *_elecMotherId;
    vector<float> *_elecGenPt;
    vector<float> *_elecGenE;
    vector<float> *_elecGenEta;
    vector<float> *_elecGenPhi;
    
    vector<float> *_elecPt;
    vector<float> *_elecE;
    vector<float> *_elecEta;
    vector<float> *_elecPhi;
    vector<float> *_elecCharge;
    vector<double> *_elecDxyVtx;
    vector<double> *_elecDxyBS;
    vector<double> *_elecDxyError;
    vector<double> *_elecDzVtx;
    vector<double> *_elecDzBS;
    vector<double> *_elecDzError;
    vector<double> *_elecVx;
    vector<double> *_elecVy;
    vector<double> *_elecVz;
    vector<float> *_elecEcalIsoDr03;
    vector<float> *_elecTrkIsoDr03;
    vector<float> *_elecCaloIsoDr03;
    vector<float> *_elecEcalIsoDr04;
    vector<float> *_elecTrkIsoDr04;
    vector<float> *_elecEcalIsoDr05;
    vector<float> *_elecHadIsoDr05;
    vector<float> *_elecTrkIsoDr05;
    vector<float> *_elecCaloIsoDr05;
    vector<float> *_elecPFIsoDR03SumChargedHadronPt;
    vector<float> *_elecPFIsoDR03SumChargedParticlePt;
    vector<float> *_elecPFIsoDR03SumNeutralHadronPt;
    vector<float> *_elecPFIsoDR03SumPhotonHadronPt;
    vector<float> *_elecPFIsoDR03SumPUPt;
    vector<float> *_elecPFIsoDR04SumChargedHadronPt;
    vector<float> *_elecPFIsoDR04SumChargedParticlePt;
    vector<float> *_elecPFIsoDR04SumNeutralHadronPt;
    vector<float> *_elecPFIsoDR04SumPhotonHadronPt;
    vector<float> *_elecPFIsoDR04SumPUPt;
    vector<int> *_elecClass;
    vector<float> *_elecIdLoose;
    vector<float> *_elecIdTight;
    vector<float> *_elecIdRobustHighEnergy;
    vector<float> *_elecIdRobustLoose;
    vector<float> *_elecIdRobustTight;
    vector<int> *_elecMissingHits;
    vector<bool> *_heepPassedEt;
    vector<bool> *_heepPassedPt;
    vector<bool> *_heepPassedDetEta;
    vector<bool> *_heepPassedCrack;
    vector<bool> *_heepPassedDEtaIn;
    vector<bool> *_heepPassedDPhiIn;
    vector<bool> *_heepPassedHadem;
    vector<bool> *_heepPassedSigmaIEtaIEta;
    vector<bool> *_heepPassed2by5Over5By5;
    vector<bool> *_heepPassedEcalHad1Iso;
    vector<bool> *_heepPassedHad2Iso;
    vector<bool> *_heepPassedTrkIso;
    vector<bool> *_heepPassedEcalDriven;
    vector<bool> *_heepPassedAllCuts;
    vector<double> *_elecMEtMt;
    
    //mu-LeadTau Info
    vector<float> *_elecLeadTauCosDPhi;
    vector<float> *_elecLeadTauDelatR;
    vector<double> *_elecLeadTauPZeta;
    vector<double> *_elecLeadTauPZetaVis;
    vector<double> *_elecLeadTauMass;
    vector<double> *_elecLeadTauMetMass;    
    vector<double> *_elecLeadTauCollMass;
  
    //mu-SubLeadTau Info
    vector<float> *_elecSubLeadTauCosDPhi;
    vector<float> *_elecSubLeadTauDelatR;
    vector<double> *_elecSubLeadTauPZeta;
    vector<double> *_elecSubLeadTauPZetaVis;
    vector<double> *_elecSubLeadTauMass;
    vector<double> *_elecSubLeadTauMetMass;
    vector<double> *_elecSubLeadTauCollMass;
    
    vector<double> *_elecDiHadCandCosDPhi;
    vector<double> *_elecDiHadCandMass;
    vector<double> *_elecMetCosDPhi;
    vector<double> *_diHadTauCandMetCosDPhi;
};
