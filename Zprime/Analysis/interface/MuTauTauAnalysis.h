#include <memory>
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDAnalyzer.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/MessageLogger/interface/MessageLogger.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "CommonTools/UtilAlgos/interface/TFileService.h"
#include "HighMassAnalysis/Analysis/interface/HiMassTauAnalysis.h"
#include "TFile.h"
#include "TTree.h"

class MuTauTauAnalysis: public HiMassTauAnalysis{
  public:
    //explicit MuTauTauAnalysis();			      
    explicit MuTauTauAnalysis(const edm::ParameterSet&);  
    ~MuTauTauAnalysis();
    
  
  private:
    void fillNtuple();
    void setupBranches();
    void initializeVectors();
    void clearVectors();
    
    //define tree variables
    TTree* _MuTauTauTree;
    
    //event info

    vector<double> _PDFWeights;
    double _ISRGluonWeight;
    double _ISRGammaWeight;
    double _FSRWeight;
    
    unsigned int _nVtx;
    unsigned int _nGoodVtx;
    
    float _MEt;
    
    bool _zEvent;
    float _zMassDiff;
    float _zPtAsymm;
    
    unsigned int _nBtagsHiEffTrkCnt ;
    unsigned int _nBTagsHiEffSimpleSecVtx ;
    unsigned int _nBtagsHiPurityTrkCnt ;
    unsigned int _nBTagsHiPuritySimpleSecVtx ;
    unsigned int _nBTagsCombSecVtx;
    
    float _genLevelMuonPt;
    float _genLevelMuonPhi;
    float _genLevelMuonEta;
    float _genLevelTau1Pt;
    float _genLevelTau1Phi;
    float _genLevelTau1Eta;
    float _genLevelTau2Pt;
    float _genLevelTau2Phi;
    float _genLevelTau2Eta;
    
    //LeadTau Info
    vector<unsigned int> *_leadTauMatched;
    vector<unsigned int> *_leadTauMotherId;
    vector<float> *_leadTauGenPt;
    vector<float> *_leadTauGenE;
    vector<float> *_leadTauGenEta;
    vector<float> *_leadTauGenPhi;
    
    vector<float> *_leadTauPt;
    vector<float> *_leadTauEnergy;
    vector<float> *_leadTauEta;
    vector<float> *_leadTauPhi;
    vector<float> *_leadTauCharge;
    vector<float> *_leadTauLeadChargedCandPt;
    vector<float> *_leadTauLeadChargedCandCharge;
    vector<unsigned int> *_leadTauLeadChargedCandRecHitsSize;
    vector<double> *_leadTauLeadChargedCandDxyVtx;
    vector<double> *_leadTauLeadChargedCandDxyBS;
    vector<double> *_leadTauLeadChargedCandDxyError;
    vector<double> *_leadTauLeadChargedCandDzVtx;
    vector<double> *_leadTauLeadChargedCandDzBS;
    vector<double> *_leadTauLeadChargedCandDzError;
    vector<double> *_leadTauLeadChargedCandVz;
    vector<double> *_leadTauLeadChargedCandVy;
    vector<double> *_leadTauLeadChargedCandVx;
    vector<double> *_leadTauDeltaZVtx;
    vector<int> *_leadTauNProngs;
    vector<float> *_leadTauEmFraction;
    vector<float> *_leadTauHcalTotOverPLead;
    vector<float> *_leadTauHcalMaxOverPLead;
    vector<float> *_leadTauHcal3x3OverPLead;
    vector<float> *_leadTauElectronPreId;
    vector<float> *_leadTauModifiedEOverP;
    vector<float> *_leadTauBremsRecoveryEOverPLead;
    vector<float> *_leadTauLTSignedIp;
    vector<float> *_leadTauTrkIsoSumPt;
    vector<float> *_leadTauECALIsoSumEt;
    vector<unsigned int> *_leadTauIdByDecayModeFinding;
    vector<unsigned int> *_leadTauIdByVLooseIsolation;
    vector<unsigned int> *_leadTauIdByLooseIsolation;
    vector<unsigned int> *_leadTauIdByMediumIsolation;
    vector<unsigned int> *_leadTauIdByTightIsolation;
    vector<unsigned int> *_leadTauIdByVLooseCombinedIsolationDBSumPtCorr;
    vector<unsigned int> *_leadTauIdByLooseCombinedIsolationDBSumPtCorr;
    vector<unsigned int> *_leadTauIdByMediumCombinedIsolationDBSumPtCorr;
    vector<unsigned int> *_leadTauIdByTightCombinedIsolationDBSumPtCorr;
    vector<unsigned int> *_leadTauIdByLooseElectronRejection;
    vector<unsigned int> *_leadTauIdByMediumElectronRejection;
    vector<unsigned int> *_leadTauIdByTightElectronRejection;
    vector<unsigned int> *_leadTauIdByLooseMuonRejection;
    vector<unsigned int> *_leadTauIdByTightMuonRejection;
    vector<double> *_leadTauMEtMt;
    
    //SubLeadTau Info
    vector<unsigned int> *_subLeadTauMatched;
    vector<unsigned int> *_subLeadTauMotherId;
    vector<float> *_subLeadTauGenPt;
    vector<float> *_subLeadTauGenE;
    vector<float> *_subLeadTauGenEta;
    vector<float> *_subLeadTauGenPhi;
    
    vector<float> *_subLeadTauPt;
    vector<float> *_subLeadTauEnergy;
    vector<float> *_subLeadTauEta;
    vector<float> *_subLeadTauPhi;
    vector<float> *_subLeadTauCharge;
    vector<float> *_subLeadTauLeadChargedCandPt;
    vector<float> *_subLeadTauLeadChargedCandCharge;
    vector<unsigned int> *_subLeadTauLeadChargedCandRecHitsSize;
    vector<double> *_subLeadTauLeadChargedCandDxyVtx;
    vector<double> *_subLeadTauLeadChargedCandDxyBS;
    vector<double> *_subLeadTauLeadChargedCandDxyError;
    vector<double> *_subLeadTauLeadChargedCandDzVtx;
    vector<double> *_subLeadTauLeadChargedCandDzBS;
    vector<double> *_subLeadTauLeadChargedCandDzError;
    vector<double> *_subLeadTauLeadChargedCandVx;
    vector<double> *_subLeadTauLeadChargedCandVy;
    vector<double> *_subLeadTauLeadChargedCandVz;
    vector<double> *_subLeadTauDeltaZVtx;
    vector<int> *_subLeadTauNProngs;
    vector<float> *_subLeadTauEmFraction;
    vector<float> *_subLeadTauHcalTotOverPLead;
    vector<float> *_subLeadTauHcalMaxOverPLead;
    vector<float> *_subLeadTauHcal3x3OverPLead;
    vector<float> *_subLeadTauElectronPreId;
    vector<float> *_subLeadTauModifiedEOverP;
    vector<float> *_subLeadTauBremsRecoveryEOverPLead;
    vector<float> *_subLeadTauLTSignedIp;
    vector<float> *_subLeadTauTrkIsoSumPt;
    vector<float> *_subLeadTauECALIsoSumEt;
    vector<unsigned int> *_subLeadTauIdByDecayModeFinding;
    vector<unsigned int> *_subLeadTauIdByVLooseIsolation;
    vector<unsigned int> *_subLeadTauIdByLooseIsolation;
    vector<unsigned int> *_subLeadTauIdByMediumIsolation;
    vector<unsigned int> *_subLeadTauIdByTightIsolation;
    vector<unsigned int> *_subLeadTauIdByVLooseCombinedIsolationDBSumPtCorr;
    vector<unsigned int> *_subLeadTauIdByLooseCombinedIsolationDBSumPtCorr;
    vector<unsigned int> *_subLeadTauIdByMediumCombinedIsolationDBSumPtCorr;
    vector<unsigned int> *_subLeadTauIdByTightCombinedIsolationDBSumPtCorr;
    vector<unsigned int> *_subLeadTauIdByLooseElectronRejection;
    vector<unsigned int> *_subLeadTauIdByMediumElectronRejection;
    vector<unsigned int> *_subLeadTauIdByTightElectronRejection;
    vector<unsigned int> *_subLeadTauIdByLooseMuonRejection;
    vector<unsigned int> *_subLeadTauIdByTightMuonRejection;
    vector<double> *_subLeadTauMEtMt;
    
    //diTau Info
    vector<float> *_diHadTauCosDPhi;
    vector<float> *_diHadTauDelatR;
    vector<double> *_diHadTauPZeta;
    vector<double> *_diHadTauPZetaVis;
    vector<double> *_diHadTauMass;
    vector<double> *_diHadTauMetMass;
    vector<double> *_diHadTauCollMass;
    
    //from candidate
    vector<double> *_diHadCandPt;
    vector<double> *_diHadCandEt;
    vector<double> *_diHadCandEta;
    vector<double> *_diHadCandPhi;
    vector<double> *_diHadCandMass;
    vector<double> *_diHadCandCharge;
  
    //muon Info
    vector<unsigned int> *_muonMatched;
    vector<unsigned int> *_muonMotherId;
    vector<float> *_muonGenPt;
    vector<float> *_muonGenE;
    vector<float> *_muonGenEta;
    vector<float> *_muonGenPhi;
    
    vector<float> *_muonPt;
    vector<float> *_muonE;
    vector<float> *_muonEta;
    vector<float> *_muonPhi;
    vector<float> *_muonCharge;
    vector<double> *_muonDxyVtx;
    vector<double> *_muonDxyBS;
    vector<double> *_muonDxyError;
    vector<double> *_muonDzVtx;
    vector<double> *_muonDzBS;
    vector<double> *_muonDzError;
    vector<double> *_muonVx;
    vector<double> *_muonVy;
    vector<double> *_muonVz;
    vector<unsigned int> *_muonValidHits;
    vector<double> *_muonNormChiSqrd;
    vector<int> *_muonChambersValidHits;
    vector<int> *_muonMatchedStations;
    vector<int> *_muonPixelHits;
    vector<int> *_muonTrkLayersWithHits;
    vector<float> *_muonEcalIsoDr03;
    vector<float> *_muonTrkIsoDr03;
    vector<float> *_muonCaloIsoDr03;
    vector<float> *_muonEcalIsoDr04;
    vector<float> *_muonTrkIsoDr04;
    vector<float> *_muonEcalIsoDr05;
    vector<float> *_muonHadIsoDr05;
    vector<float> *_muonTrkIsoDr05;
    vector<float> *_muonCaloIsoDr05;
    vector<float> *_muonPFIsoDR03SumChargedHadronPt;
    vector<float> *_muonPFIsoDR03SumChargedParticlePt;
    vector<float> *_muonPFIsoDR03SumNeutralHadronPt;
    vector<float> *_muonPFIsoDR03SumPhotonHadronPt;
    vector<float> *_muonPFIsoDR03SumPUPt;
    vector<float> *_muonPFIsoDR04SumChargedHadronPt;
    vector<float> *_muonPFIsoDR04SumChargedParticlePt;
    vector<float> *_muonPFIsoDR04SumNeutralHadronPt;
    vector<float> *_muonPFIsoDR04SumPhotonHadronPt;
    vector<float> *_muonPFIsoDR04SumPUPt;
    vector<float> *_muonCaloComp;
    vector<int> *_muonIsGlobalMuon;
    vector<int> *_muonIsTrackerMuon;
    vector<int> *_muonIsGlobalPromptTight;
    vector<int> *_muonIsTMLastStationLoose;
    vector<int> *_muonIsTMLastStationTight;
    vector<int> *_muonIsTM2DCompatibilityLoose;
    vector<int> *_muonIsTM2DCompatibilityTight;
    vector<float> *_muonPionVeto;
    vector<double> *_muonMEtMt;
    
    //mu-LeadTau Info
    vector<float> *_muonLeadTauCosDPhi;
    vector<float> *_muonLeadTauDelatR;
    vector<double> *_muonLeadTauPZeta;
    vector<double> *_muonLeadTauPZetaVis;
    vector<double> *_muonLeadTauMass;
    vector<double> *_muonLeadTauMetMass;    
    vector<double> *_muonLeadTauCollMass;
  
    //mu-SubLeadTau Info
    vector<float> *_muonSubLeadTauCosDPhi;
    vector<float> *_muonSubLeadTauDelatR;
    vector<double> *_muonSubLeadTauPZeta;
    vector<double> *_muonSubLeadTauPZetaVis;
    vector<double> *_muonSubLeadTauMass;
    vector<double> *_muonSubLeadTauMetMass;
    vector<double> *_muonSubLeadTauCollMass;
    
    vector<double> *_muonDiHadCandCosDPhi;
    vector<double> *_muonDiHadCandMass;
    vector<double> *_muonMetCosDPhi;
    vector<double> *_diHadTauCandMetCosDPhi;
};
