#ifndef _TauTauAnalysis_h
#define _TauTauAnalysis_h

#include <memory>
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDAnalyzer.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/MessageLogger/interface/MessageLogger.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "CommonTools/UtilAlgos/interface/TFileService.h"
#include "HighMassAnalysis/Analysis/interface/HiMassTauAnalysis.h"
#include "RecoTauTag/TauTagTools/interface/PFTauQualityCutWrapper.h"
#include "TauAnalysis/CandidateTools/interface/NSVfitStandaloneLikelihood.h"
#include "TauAnalysis/CandidateTools/interface/NSVfitStandaloneAlgorithm.h"
#include "DataFormats/GeometryVector/interface/Pi.h"
#include "Math/LorentzVector.h"
#include "TLorentzVector.h"
#include "TFile.h"
#include "TTree.h"

using namespace std;

struct Triplet{
	TLorentzVector tau1;
	TLorentzVector tau2;
	TLorentzVector parent;
};

class TauTauAnalysis: public HiMassTauAnalysis{
  public:
    //explicit TauTauAnalysis();			      
    explicit TauTauAnalysis(const edm::ParameterSet&);  
    ~TauTauAnalysis();
    
  
  private:
    void fillNtuple();
    void setupBranches();
    void initializeVectors();
    void clearVectors();
	TLorentzVector BoostAndRotateToRestFrame(TLorentzVector&, TVector3 const &);
	TLorentzVector GetClosestGenMatch(TLorentzVector const &);
	unsigned int NumberOfHadronicGenTaus();
	Triplet GetGenHadronicTaus();
    
    //define tree variables
    TTree* _TauTauTree;
    
    reco::Candidate::LorentzVector Muon;
    reco::Candidate::LorentzVector Electron;
    reco::Candidate::LorentzVector MET;
    float _theLeastSig;
    

	// Quality cuts
	PFTauQualityCutWrapper* _qualityCuts;

    //event info

	double				_runNumber;
	double				_eventNumber;
	double				_lumiBlock;
	int					_numInteractionsBXm1;
	int					_numInteractionsBX0;
	int					_numInteractionsBXp1;
	int					_NumberOfHadronicGenTaus;
	vector<float>*		_GenMET;
	vector<int>*		_comboNumber;
	vector<float>*		_GenMETphi;
	vector<float>*		_Tau1GenPt;
	vector<float>*		_Tau2GenPt;
	vector<float>*		_Tau1GenE;
	vector<float>*		_Tau2GenE;
	vector<float>*		_Tau1GenEta;
	vector<float>*		_Tau2GenEta;
	vector<float>*		_Tau1GenPhi;
	vector<float>*		_Tau2GenPhi;
	vector<float>*		_Tau1GenParentMass;
	vector<float>*		_Tau2GenParentMass;
	vector<float>*		_TauTauPlusMetGenMass;

	float		_GenTau1P;
	float		_GenTau2P;
	float		_GenTau1Pt;
	float		_GenTau2Pt;
	float		_GenTau1Phi;
	float		_GenTau2Phi;
	float		_GenTau1Theta;
	float		_GenTau2Theta;
	float		_GenTausOmega;

	float		_BoostedGenTau1P;
	float		_BoostedGenTau2P;
	float		_BoostedGenTau1Pt;
	float		_BoostedGenTau2Pt;
	float		_BoostedGenTau1Phi;
	float		_BoostedGenTau2Phi;
	float		_BoostedGenTau1Theta;
	float		_BoostedGenTau2Theta;
	float		_BoostedGenTausOmega;

	vector<bool>*		_Tau1Matched;
	vector<bool>*		_Tau2Matched;
	vector<int>*		_Tau1MotherId;
	vector<int>*		_Tau2MotherId;
	vector<int>*		_Tau1PdgId;
	vector<int>*		_Tau2PdgId;
	vector<int>*		_Tau1MotherPdgId;
	vector<int>*		_Tau2MotherPdgId;
	vector<bool>*		_Tau1_ParentTauMatched;
	vector<bool>*		_Tau2_ParentTauMatched;
	vector<bool>*		_Tau1_ZtauMatched;
	vector<bool>*		_Tau2_ZtauMatched;
	vector<bool>*		_Tau1_ZeMatched;
	vector<bool>*		_Tau2_ZeMatched;
	vector<bool>*		_Tau1_MatchesGenHadronic;
	vector<bool>*		_Tau2_MatchesGenHadronic;
	vector<int>*		_NumPV;


	vector<float>*	_DeltaR_recoTau1_genTau1;
	vector<float>*	_DeltaR_recoTau2_genTau1;
	vector<float>*	_DeltaR_recoTau1_genTau2;
	vector<float>*	_DeltaR_recoTau2_genTau2;

	vector<float>*		_Tau1E;
	vector<float>*		_Tau2E;
	vector<float>*		_Tau1Et;
	vector<float>*		_Tau2Et;
	vector<float>*		_Tau1Pt;
	vector<float>*		_Tau2Pt;
	vector<bool>*		_Tau1LTvalid;
	vector<bool>*		_Tau2LTvalid;
	vector<float>*		_Tau1LTPt;
	vector<float>*		_Tau2LTPt;
	vector<int>*		_Tau1Charge;
	vector<int>*		_Tau2Charge;
	vector<float>*		_Tau1Eta;
	vector<float>*		_Tau2Eta;
	vector<float>*		_Tau1Phi;
	vector<float>*		_Tau2Phi;
	vector<float>*		_Tau1LTIpVtdxy;
	vector<float>*		_Tau1LTIpVtdz;
	vector<float>*		_Tau1LTIpVtdxyError;
	vector<float>*		_Tau1LTIpVtdzError;
	vector<float>*		_Tau1LTvx;
	vector<float>*		_Tau1LTvy;
	vector<float>*		_Tau1LTvz;
	vector<int>*		_Tau1LTValidHits;
	vector<float>*		_Tau1LTNormChiSqrd;
	vector<float>*		_Tau2LTIpVtdxy;
	vector<float>*		_Tau2LTIpVtdz;
	vector<float>*		_Tau2LTIpVtdxyError;
	vector<float>*		_Tau2LTIpVtdzError;
	vector<float>*		_Tau2LTvx;
	vector<float>*		_Tau2LTvy;
	vector<float>*		_Tau2LTvz;
	vector<int>*		_Tau2LTValidHits;
	vector<float>*		_Tau2LTNormChiSqrd;
	vector<int>*		_Tau1NProngs;
	vector<int>*		_Tau2NProngs;
	vector<int>*		_Tau1NSignalGammas;
	vector<int>*		_Tau2NSignalGammas;
	vector<int>*		_Tau1NSignalNeutrals;
	vector<int>*		_Tau2NSignalNeutrals;
	vector<int>*		_Tau1NSignalPiZeros;
	vector<int>*		_Tau2NSignalPiZeros;
	vector<int>*		_Tau1DecayMode;
	vector<int>*		_Tau2DecayMode;
	vector<float>*		_Tau1EmFraction;
	vector<float>*		_Tau2EmFraction;
	vector<int>*		_Tau1IsInTheCracks;
	vector<int>*		_Tau2IsInTheCracks;
	vector<float>*		_MET;
	vector<float>*		_METphi;
	vector<float>*		_METl1l2l3corr;
	vector<float>*		_METl1l2l3corrPhi;
	vector<float>*		_METsignificance;
	vector<float>*		_TauTauVisibleMass;
	vector<float>*		_TauTauVisPlusMetMass;
	vector<float>*		_TauTauCollinearMetMass;
	vector<float>*		_TauTauCosDPhi;
	vector<float>*		_TauTauDeltaR;
	vector<float>*		_TauTauPZeta;
	vector<float>*		_TauTauPZetaVis;
	vector<float>*		_Tau1MetCosDphi;
	vector<float>*		_Tau2MetCosDphi;
	vector<double>*		_Tau1MetMt;
	vector<double>*		_Tau2MetMt;
	vector<float>*		_nBtagsHiEffTrkCnt;
	vector<float>*		_nBtagsHiPurityTrkCnt;
	vector<float>*		_nBTagsHiEffSimpleSecVtx;
	vector<float>*		_nBTagsHiPuritySimpleSecVtx;
	vector<float>*		_nBTagsCombSecVtx;
	vector<float>*		_jetSumEt;
	vector<float>*		_jetMETSumEt;
	vector<int>*		_nJets;
	vector<float>*		_nExtraJets;
	vector<float>*		_Tau1Tau2LeadingJetPtSum;
	vector<float>*		_Tau1LeadingJetDeltaPhi;
	vector<float>*		_Tau2LeadingJetDeltaPhi;

	vector<bool>*		_Tau1hpsPFTauDiscriminationAgainstLooseElectron;
	vector<bool>*		_Tau1hpsPFTauDiscriminationAgainstLooseMuon;
	vector<bool>*		_Tau1hpsPFTauDiscriminationAgainstMediumElectron;
	vector<bool>*		_Tau1hpsPFTauDiscriminationAgainstMediumMuon;
	vector<bool>*		_Tau1hpsPFTauDiscriminationAgainstTightElectron;
	vector<bool>*		_Tau1hpsPFTauDiscriminationAgainstTightMuon;
	vector<bool>*		_Tau1hpsPFTauDiscriminationByDecayModeFinding;
	vector<bool>*		_Tau1hpsPFTauDiscriminationByLooseIsolation;
	vector<bool>*		_Tau1hpsPFTauDiscriminationByMediumIsolation;
	vector<bool>*		_Tau1hpsPFTauDiscriminationByTightIsolation;
	vector<bool>*		_Tau1hpsPFTauDiscriminationByVLooseIsolation;
	vector<bool>*		_Tau2hpsPFTauDiscriminationAgainstLooseElectron;
	vector<bool>*		_Tau2hpsPFTauDiscriminationAgainstLooseMuon;
	vector<bool>*		_Tau2hpsPFTauDiscriminationAgainstMediumElectron;
	vector<bool>*		_Tau2hpsPFTauDiscriminationAgainstMediumMuon;
	vector<bool>*		_Tau2hpsPFTauDiscriminationAgainstTightElectron;
	vector<bool>*		_Tau2hpsPFTauDiscriminationAgainstTightMuon;
	vector<bool>*		_Tau2hpsPFTauDiscriminationByDecayModeFinding;
	vector<bool>*		_Tau2hpsPFTauDiscriminationByLooseIsolation;
	vector<bool>*		_Tau2hpsPFTauDiscriminationByMediumIsolation;
	vector<bool>*		_Tau2hpsPFTauDiscriminationByTightIsolation;
	vector<bool>*		_Tau2hpsPFTauDiscriminationByVLooseIsolation;

	vector<bool>*		_Tau1hpsPFTauDiscriminationByVLooseIsolationDBSumPtCorr;
	vector<bool>*		_Tau1hpsPFTauDiscriminationByLooseIsolationDBSumPtCorr;
	vector<bool>*		_Tau1hpsPFTauDiscriminationByMediumIsolationDBSumPtCorr;
	vector<bool>*		_Tau1hpsPFTauDiscriminationByTightIsolationDBSumPtCorr;
	vector<bool>*		_Tau1hpsPFTauDiscriminationByVLooseCombinedIsolationDBSumPtCorr;
	vector<bool>*		_Tau1hpsPFTauDiscriminationByLooseCombinedIsolationDBSumPtCorr;
	vector<bool>*		_Tau1hpsPFTauDiscriminationByMediumCombinedIsolationDBSumPtCorr;
	vector<bool>*		_Tau1hpsPFTauDiscriminationByTightCombinedIsolationDBSumPtCorr;
	vector<bool>*		_Tau2hpsPFTauDiscriminationByVLooseIsolationDBSumPtCorr;
	vector<bool>*		_Tau2hpsPFTauDiscriminationByLooseIsolationDBSumPtCorr;
	vector<bool>*		_Tau2hpsPFTauDiscriminationByMediumIsolationDBSumPtCorr;
	vector<bool>*		_Tau2hpsPFTauDiscriminationByTightIsolationDBSumPtCorr;
	vector<bool>*		_Tau2hpsPFTauDiscriminationByVLooseCombinedIsolationDBSumPtCorr;
	vector<bool>*		_Tau2hpsPFTauDiscriminationByLooseCombinedIsolationDBSumPtCorr;
	vector<bool>*		_Tau2hpsPFTauDiscriminationByMediumCombinedIsolationDBSumPtCorr;
	vector<bool>*		_Tau2hpsPFTauDiscriminationByTightCombinedIsolationDBSumPtCorr;

	vector<float>*		_Tau1hpsPFTauDiscriminationByRawCombinedIsolationDBSumPtCorr;
	vector<float>*		_Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorr;
	vector<float>*		_Tau1hpsPFTauDiscriminationByRawGammaIsolationDBSumPtCorr;
	vector<float>*		_Tau2hpsPFTauDiscriminationByRawCombinedIsolationDBSumPtCorr;
	vector<float>*		_Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorr;
	vector<float>*		_Tau2hpsPFTauDiscriminationByRawGammaIsolationDBSumPtCorr;

/*	vector<float>*		_Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p0;
	vector<float>*		_Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p1;
	vector<float>*		_Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p2;
	vector<float>*		_Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p3;
	vector<float>*		_Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p4;
	vector<float>*		_Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p0;
	vector<float>*		_Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p1;
	vector<float>*		_Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p2;
	vector<float>*		_Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p3;
	vector<float>*		_Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p4; //*/


	vector<bool>*	_NSVFitStatus;
	vector<float>*	_NSVFitMass;
	vector<float>*	_NSVFitMassUncert;
	vector<float>*	_NSVFitMET;
	vector<float>*	_NSVFitMETphi;
	vector<float>*	_NSVFitSystemPt;
	vector<float>*	_NSVFitSystemMag;
	vector<float>*	_NSVFitSystemPhi;
	vector<float>*	_NSVFitSystemEta;


	vector<float>*	_SVFitTau1P	;
	vector<float>*	_SVFitTau2P	;
	vector<float>*	_SVFitTau1Pt	;
	vector<float>*	_SVFitTau2Pt	;
	vector<float>*	_SVFitTau1Phi;
	vector<float>*	_SVFitTau2Phi;
	vector<float>*	_SVFitTau1Theta;
	vector<float>*	_SVFitTau2Theta;
	vector<float>*	_SVFitTausOmega;


	vector<float>*	_BoostedSVFitTau1P	;
	vector<float>*	_BoostedSVFitTau2P	;
	vector<float>*	_BoostedSVFitTau1Pt	;
	vector<float>*	_BoostedSVFitTau2Pt	;
	vector<float>*	_BoostedSVFitTau1Phi;
	vector<float>*	_BoostedSVFitTau2Phi;
	vector<float>*	_BoostedSVFitTau1Theta;
	vector<float>*	_BoostedSVFitTau2Theta;
	vector<float>*	_BoostedSVFitTausOmega;

	vector<float>*	_DeltaR_recoTau1_SVFitTau1;
	vector<float>*	_DeltaR_recoTau2_SVFitTau1;
	vector<float>*	_DeltaR_recoTau1_SVFitTau2;
	vector<float>*	_DeltaR_recoTau2_SVFitTau2;



};


#endif
