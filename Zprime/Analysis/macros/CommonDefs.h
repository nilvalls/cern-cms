#ifndef __CommonDefs_H__
#define __CommonDefs_H__

using namespace std;

namespace Common{

	struct cutResult{
		string cutName;
		string preName;
		string postName;
		long double preEvents;
		long double postEvents;
		long double oriEvents;
	};



}

#endif
