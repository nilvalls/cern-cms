#include "Driver.h"

#define Driver_cxx
using namespace std;


int main(int argc, char **argv){

	// Set up nice plot style
	gROOT->Reset();
	//gROOT->SetStyle("Plain");
	setTDRStyle();

	// Supress ROOT warnings
	gErrorIgnoreLevel = kError;

	// Keep track of how long things take
	TDatime clock;
	TStopwatch* stopwatch = new TStopwatch();

	print(CYAN, ">>> Starting analysis...");
	stopwatch->Start();

	// Instatiate configuration parser and take the first argument as the config file
	Config theConfig(argv[1], argv);

	// Read in global parameters 
	int	maxEvents					= theConfig.pDouble("maxEvents");
	string lumiExpression			= theConfig.pString("luminosity");
	string tauTriggerEffLowEdges	= theConfig.pString("tauTriggerEffLowEdge");
	string tauTriggerEfficiency 	= theConfig.pString("tauTriggerEfficiency");
	string tauTriggerEffFunction	= theConfig.pString("tauTriggerEffFunction");
	string metSFedge				= theConfig.pString("metSFedge");
	string metSF				 	= theConfig.pString("metSF");
	float allMCscaleFactor			= theConfig.pDouble("allMCscaleFactor");
	float OS2LS						= theConfig.pDouble("OS2LS");
	string puFile					= theConfig.pString("puFile");
	string triggerConfig			= theConfig.pString("triggerConfig");
	double luminosity 				= atof(lumiExpression.substr(0,lumiExpression.find('/')).c_str());
	string lumiUnits				= lumiExpression.substr(lumiExpression.find('/'));
	string toDo						= theConfig.pString("toDo");
	string enabledTopologies		= theConfig.pString("enabledTopologies");
	string flags					= theConfig.pString("flags");
	string countMasses				= theConfig.pString("countMasses");

	// Read in output parameters and check for existence of dirs, otherwise create them
	string webDir					= theConfig.pString("webDir");			ReMakeDir(webDir);
	string bigDir					= theConfig.pString("bigDir");			ReMakeDir(bigDir);
	string output_stacks			= theConfig.pString("output_stacks");	ReMakeDir(output_stacks);
	string output_sb				= theConfig.pString("output_sb");		ReMakeDir(output_sb);
	string output_stamps			= theConfig.pString("output_stamps");	ReMakeDir(output_stamps);
	string output_osls				= theConfig.pString("output_osls");   	ReMakeDir(output_osls);
	string output_effHTML			= theConfig.pString("output_effHTML");	CheckAndRemove(output_effHTML);
	string output_effTex			= theConfig.pString("output_effTex");	CheckAndRemove(output_effTex);
	string output_cntHTML			= theConfig.pString("output_cntHTML");	CheckAndRemove(output_cntHTML);
	string output_cntTex			= theConfig.pString("output_cntTex");	CheckAndRemove(output_cntTex);
	string output_events			= theConfig.pString("output_events");	CheckAndRemove(output_events);
	string output_cuts				= theConfig.pString("output_cuts");		ReMakeDir(output_cuts);
	string output_flags				= theConfig.pString("output_flags");	ReMakeDir(output_flags);
	string output_root				= theConfig.pString("output_root");		CheckAndRemove(output_root);
	string output_fitter			= theConfig.pString("output_fitter");	CheckAndRemove(output_fitter);

	cout << "\n\t"; PrintURL(webDir);
	cout << "\t"; PrintLocal(bigDir);
	cout << "\n\tLoading ntuples from " << theConfig.pString("ntuplesDir") << "\n" << endl;
	cout << "\n\tUsing the following flags: " << GREEN << flags << NOCOLOR << "\n" << endl;

	// Copy original config file to output dir
	BackUpConfigFile(string(argv[1]), webDir);
	
	// Instatiate helper classes
	Config* testConfig = new Config(theConfig.pString("histoCfg"));
	TauTauAnalyzer*		tautauAnalysis	= new TauTauAnalyzer(maxEvents, output_events, output_cuts, output_flags, luminosity, testConfig);
	PlotStacker*		stacker;
	PlotSignificance*	significancePlotter;
	PlotStamper*		stamper;
	OS2LSplotter*		os2ls;
	if(DoThisAnalysis(toDo, "stacks")){			stacker				= new PlotStacker(output_root, output_stacks); }
	if(DoThisAnalysis(toDo, "significance")){	significancePlotter	= new PlotSignificance("trash.root", output_sb); }
	if(DoThisAnalysis(toDo, "stamps")){			stamper				= new PlotStamper(output_root, output_stamps); }
	if(DoThisAnalysis(toDo, "OStoLS")){			os2ls				= new OS2LSplotter(output_root, output_osls); }

	// Set up trigger efficiencies
	tautauAnalysis->SetFlags(flags);
	if(tauTriggerEffFunction.length() == 0){	tautauAnalysis->SetTriggerEfficiency(tauTriggerEffLowEdges, tauTriggerEfficiency);	}
	else{										tautauAnalysis->SetTriggerEfficiency(tauTriggerEffFunction);						}
	tautauAnalysis->SetMETSF(metSFedge,metSF);
	tautauAnalysis->LoadPuFile(puFile);
	if(triggerConfig.length()>0){ tautauAnalysis->LoadTriggerConfig(triggerConfig); }

	if(DoThisAnalysis(toDo, "stacks")){ stacker->SetFlags(flags); }
	if(DoThisAnalysis(toDo, "significance")){ significancePlotter->SetFlags(flags); }
	if(DoThisAnalysis(toDo, "stamps")){ stamper->SetFlags(flags); }

	// Set OS2LS ratio in analyzer
	tautauAnalysis->SetOS2LS(OS2LS);

	// Set report rate
	tautauAnalysis->SetReportRate(theConfig.pInt("reportEvery"));

	// Switch on/off specified selection cuts
	tautauAnalysis->SetSelectEvents(theConfig.pString("selectEvents"));
	tautauAnalysis->SetCutsToApply(theConfig.pString("cutsToApply"));

	float effectiveLumi	= luminosity;
	// Read in and add topologies
	map<string, Config*> topologies = theConfig.getGroups();
	const string topologyPrefix = "topology_";
	for (map<string, Config*>::iterator i = topologies.begin(); i != topologies.end(); ++i) {
		string groupName = i->first;
		Config* group = i->second;
		if (groupName.substr(0, topologyPrefix.length()) == topologyPrefix) {
			NewSection(stopwatch);
			groupName = groupName.substr(topologyPrefix.length());
			cout << CYAN << ">>> INFO: Found topology \"" << BGRAY << GREEN << groupName << NOCOLOR << CYAN << "\"" << NOCOLOR << endl;

			string rootLabel		= group->pString("rootLabel");
			if(!doTopology(enabledTopologies,groupName)){ print(PURPLE, ">>> INFO: Skipped"); continue; }

			string inputFile		= group->pString("inputFile");
			string treeName			= group->pString("treeName");
			bool isQCD				= group->pBool("isQCD");
			bool isSignal			= group->pBool("isSignal");
			bool isMC				= group->pBool("isMC");
			int color				= group->pInt("color");
			float crossSection		= group->pDouble("crossSection");
			float branchingRatio	= group->pDouble("branchingRatio");
			float otherSF			= group->pDouble("otherSF");
			int numEvInPAT			= group->pInt("numEvInPAT");
			int numEvInDS			= group->pInt("numEvInDS");

			
			effectiveLumi = tautauAnalysis->GetEffectiveLumi(luminosity); /*
			if((!isSignal) && (!isMC) && (!isQCD)){
				cout << ">>> INFO: Calculating effective luminosity... "; cout.flush();
				effectiveLumi = tautauAnalysis->GetEffectiveLumi(inputFile, treeName, luminosity);
				cout << BLUE << effectiveLumi << lumiUnits << NOCOLOR << endl;

			}//*/

			// Set topology name
			tautauAnalysis->SetTopology(groupName, crossSection, branchingRatio);


			// Make plots and classify them by topology
			vector<HistoWrapper*>* OSplotBuffer; 
			vector<HistoWrapper*>* LSplotBuffer; 

				cout << "Is signal in driver? " << isSignal << endl;
				 if ((isMC || isSignal)){   OSplotBuffer = tautauAnalysis->GetOSPlots(inputFile, treeName, (isMC || isSignal), isSignal, numEvInPAT, numEvInDS, otherSF, allMCscaleFactor);
											LSplotBuffer = tautauAnalysis->GetQCDPlots(inputFile, treeName, isMC, isSignal, numEvInPAT,  numEvInDS, otherSF, allMCscaleFactor);
		}   else if (isQCD)				 {	LSplotBuffer = tautauAnalysis->GetQCDPlots(inputFile, treeName, isMC, isSignal, numEvInPAT,  numEvInDS, otherSF, allMCscaleFactor);	
		}	else if ((!isQCD) && (!isMC)){	OSplotBuffer = tautauAnalysis->GetOSPlots(inputFile, treeName, isMC, isSignal, numEvInPAT,  numEvInDS, otherSF, allMCscaleFactor);	
		}   else 						 {	cout << ">>> INFO: Skipped" << endl; }



			// Make stacks
			if(DoThisAnalysis(toDo,"stacks")){	
					 if (isMC && (!isSignal)){	stacker->AddBackground(OSplotBuffer, groupName, rootLabel, color, crossSection, otherSF, numEvInDS);	
												stacker->AddLSBackground(LSplotBuffer, groupName, crossSection, otherSF, numEvInDS);				}
				else if (isSignal){				stacker->AddSignal(OSplotBuffer, groupName, rootLabel, color, crossSection, otherSF, numEvInDS); 	}	
				else if (isQCD){				stacker->AddQCD(LSplotBuffer, rootLabel, otherSF);													}
				else{							stacker->AddCollisions(OSplotBuffer, groupName, otherSF);											}
			}

			if(DoThisAnalysis(toDo,"significance")){	
					 if (isMC && (!isSignal)){	significancePlotter->AddBackground(OSplotBuffer, groupName, rootLabel, color, crossSection, otherSF, numEvInDS);	
												significancePlotter->AddLSBackground(LSplotBuffer, groupName, crossSection, otherSF, numEvInDS);				}
				else if (isSignal){				significancePlotter->AddSignal(OSplotBuffer, groupName, rootLabel, color, crossSection, otherSF, numEvInDS); 	}	
				else if (isQCD){				significancePlotter->AddQCD(LSplotBuffer, rootLabel, otherSF);													}
				else{							significancePlotter->AddCollisions(OSplotBuffer, groupName, otherSF);											}
			}


			// Make stamps
			if(DoThisAnalysis(toDo,"stamps")){	
					 if (isMC && (!isSignal)){	stamper->AddBackground(OSplotBuffer, groupName, rootLabel, color, crossSection, otherSF, numEvInDS);	
												stamper->AddLSBackground(LSplotBuffer, groupName, crossSection, otherSF, numEvInDS);				}
				else if (isSignal){				stamper->AddSignal(OSplotBuffer, groupName, rootLabel, color, crossSection, otherSF, numEvInDS);	}	
				else if (isQCD){				stamper->AddQCD(LSplotBuffer, rootLabel, otherSF);													}
				else{							stamper->AddCollisions(OSplotBuffer, groupName, otherSF);											}
			}


			// Make OS2LS
			if(DoThisAnalysis(toDo,"OStoLS")){	
					 if (isMC && (!isSignal)){	os2ls->AddOSBackground(OSplotBuffer);
					 							os2ls->AddLSBackground(LSplotBuffer); }
				else if (isQCD){				os2ls->AddLSCollisions(LSplotBuffer); }
				else{ 							os2ls->AddOSCollisions(OSplotBuffer); }
			}

		}
	}
	//tautauAnalysis->puCorrector->CloseFile();

	
	if(DoThisAnalysis(toDo, "stacks")){ 		stacker->SetEffectiveLumi(effectiveLumi); 
												stacker->SetLumi(luminosity, lumiUnits);	
												stacker->SetAllMCscaleFactor(allMCscaleFactor); }
	if(DoThisAnalysis(toDo, "significance")){ 	significancePlotter->SetEffectiveLumi(effectiveLumi); 
												significancePlotter->SetLumi(luminosity, lumiUnits);	
												significancePlotter->SetAllMCscaleFactor(allMCscaleFactor); }
	if(DoThisAnalysis(toDo, "stamps")){ 		stamper->SetEffectiveLumi(effectiveLumi); 
												stamper->SetLumi(luminosity, lumiUnits);	}
	if(DoThisAnalysis(toDo,"OStoLS")){			os2ls->SetEffectiveLumi(effectiveLumi);
												os2ls->SetLumi(luminosity,lumiUnits);		}



	// Print efficiencies to file
	NewSection(stopwatch);
	cout << BLUE << ">>> INFO: Printing efficiencies..." << endl;
	tautauAnalysis->PrintEfficienciesHTML(output_effHTML);
	tautauAnalysis->effCounter->PrintEfficienciesTex(output_effTex);
	cout << GREEN << " done!" << NOCOLOR << endl;

	// Draw stacks and print integrals
	if(DoThisAnalysis(toDo,"stacks")){	
		NewSection(stopwatch);
		cout << BLUE << ">>> INFO: Drawing stacks..." << endl;
		EventCounter* eventCounter = stacker->DrawPlots();
		eventCounter->PrintToHTML(output_cntHTML,countMasses);
		//eventCounter->PrintToTex(output_cntTex,countMasses);
		cout << GREEN << " done!" << NOCOLOR << endl;
	}

	// Draw stacks and print integrals
	if(DoThisAnalysis(toDo,"significance")){	
		NewSection(stopwatch);
		cout << BLUE << ">>> INFO: Drawing significance..." << endl;
		significancePlotter->DrawPlots(true);
		significancePlotter->DrawPlots(false);
		cout << GREEN << " done!" << NOCOLOR << endl;
	}

	// Draw stamps
	if(DoThisAnalysis(toDo,"stamps")){	
		NewSection(stopwatch);
		cout << BLUE << ">>> INFO: Drawing stamps..." << endl;
		stamper->DrawPlots();
		cout << GREEN <<  " done!" << NOCOLOR << endl;
	}

	// OS to LS plots
	if(DoThisAnalysis(toDo,"OStoLS")){	
		NewSection(stopwatch);
		cout << BLUE << ">>> INFO: Drawing OS to LS plots..."; cout.flush();
		os2ls->DrawPlots();
		cout << GREEN << " done!" << NOCOLOR << endl;
	}

	// Print output dirs
	NewSection(stopwatch);
	cout << "\n" << endl;
	PrintURL(webDir);
	PrintLocal(bigDir);
	cout << "\a";
	sleep(1);
	cout << "\a\n" << endl;

	return 0;
}


