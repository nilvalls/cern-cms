#include "configParser/config.h"
#include <iomanip>
#include <iostream>
#include <fstream>
#include "TError.h"
#include "TSystem.h"
#include "TauTauAnalyzer.h"
#include "TStopwatch.h"
#include "PlotStacker.h"
#include "PlotSignificance.h"
#include "PlotStamper.h"
#include "OS2LSplotter.h"
#include "HistoWrapper.h"
#include "EfficiencyCounter.h"
#include "EventCounter.h"
#include "TDatime.h"
#include "style-CMSTDR.h"
#include "ColoredOutput.h"

#ifndef Driver_h
#define Driver_h

using namespace std;

// ========== Function declarations ========== //

void print(string,string);
bool DoThisAnalysis(string, string);
void NewSection(TStopwatch*);
void ReMakeDir(string);
void CheckAndRemove(string);
void PrintURL(string);
void PrintLocal(string);
void print(string, string);
void BackUpConfigFile(string, string);
bool doTopology(string, string);
HistoWrapper* FindByName(vector<HistoWrapper*>*, string);



// ========== Function definitions ========== //

bool DoThisAnalysis(string analysisList, string thisAnalysis){
	bool result = false;
	analysisList = " " + analysisList + " ";
	size_t found = analysisList.find(" " + thisAnalysis + " ");
	size_t length = analysisList.length();
	if ( 0 <= found && found <= length ){ result = true; }
	else{ result = false;}
	return result;
}

void NewSection(TStopwatch* iStopWatch){

	float realSecs = iStopWatch->RealTime();
	float cpuSecs = iStopWatch->CpuTime();
	iStopWatch->Continue();

	TDatime clock;
	
	cout << BGRAY << "\n--- " << clock.AsString() << " ----- Elapsed: " << setw(7) << setfill(' ') << setprecision(3) << realSecs << "s ("
	<< setw(7) << setfill(' ')  << cpuSecs << " CPUs) "  << string(50, '-') << NOCOLOR << endl;
}

void ReMakeDir(string iPath){
	TString sysCommand;
	
	if(iPath.substr(iPath.length()-1).compare("/")!=0){
		iPath = iPath.substr(0,iPath.rfind("/"));
	}

	sysCommand = "rm -rf " + iPath;
	if(gSystem->Exec(sysCommand) > 0){ cout << ">>> ERROR: problem deleting \"" << iPath << "\" -- Check permissions." << endl; exit(1); }
	sysCommand = "if [ ! -d " + iPath + " ]; then mkdir -p " + iPath + "; fi";
	if(gSystem->Exec(sysCommand) > 0){ cout << ">>> ERROR: problem creating dir \"" << iPath << "\" -- Check input path and permissions." << endl; exit(1); }
}


void BackUpConfigFile(string iConfigPath, string iOutputDir){
	TString sysCommand;
	string filename = iConfigPath.substr(iConfigPath.rfind("/")+1);
	filename = filename.substr(0,iConfigPath.find(".cfg"));
	sysCommand = "cp " + iConfigPath + " " + iOutputDir + "/config_" + filename + ".txt";
	if(gSystem->Exec(sysCommand) > 0){ cout << ">>> ERROR: problem copying config file " << iConfigPath << " to output dir " << iOutputDir << endl; exit(1); }
}

void CheckAndRemove(string iPath){
	TString sysCommand;
	sysCommand = "rm -rf " + iPath;
	if(gSystem->Exec(sysCommand) > 0){ cout << ">>> ERROR: problem deleting \"" << iPath << "\" -- Check permissions." << endl; exit(1); }
}


void PrintURL(string iPath){
	TString sysCommand;
	cout << "Web dir: " << ORANGE; cout.flush();
	sysCommand = "echo \"http://$USER.web.cern.ch/$USER/" + iPath.substr(iPath.find("www/")+4) +"\"";
	cout << NOCOLOR;
	gSystem->Exec(sysCommand);
}

void PrintLocal(string iPath){
	cout << "Big dir: ";
	print(ORANGE, iPath);
}

void print(string color, string iString){
	cout << color << iString << NOCOLOR << endl;
}

bool doTopology(string iEnabledTopologies, string iThisTopo){
	string enabledTopos = " " + iEnabledTopologies + " ";
	string thisTopo = " " + iThisTopo + " ";
	return (enabledTopos.find(thisTopo) < enabledTopos.length());
}

HistoWrapper* FindByName(vector<HistoWrapper*>* iVec, string iName){
	for(int pos=0; pos < iVec->size(); pos++){
		HistoWrapper* histoWrapper = iVec->at(pos);
		string name = histoWrapper->GetHisto()->GetName();
		cout << name << " " << iName << endl;
		if((name.compare(iName) == 0) && (name.length() == iName.length())){ return histoWrapper; }
	}

	return NULL;
}

#endif
