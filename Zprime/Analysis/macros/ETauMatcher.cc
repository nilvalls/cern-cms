#define ETauMatcher_cxx
#include "ETauMatcher.h"


		// Default constructor
		ETauMatcher::ETauMatcher(){
			goodEvents="etau_May10ReReco_goodEvents.txt";
			goodEvents="etau_PromptReco-v4_goodEvents.txt";


			runLSevent	= new vector<string>();
			tauPt		= new vector<float>();
			tauEta		= new vector<float>();
			tauPhi		= new vector<float>();
			ePt			= new vector<float>();
			eEta		= new vector<float>();
			ePhi		= new vector<float>();

			runLSevent->clear();
			tauPt->clear();
			tauEta->clear();
			tauPhi->clear();
			ePt->clear();
			eEta->clear();
			ePhi->clear();


			string line;
			ifstream inputFile(goodEvents.c_str());
			if (inputFile.is_open()){
				while ( inputFile.good() ){
					getline (inputFile,line);
					if(line.length() > 0 ){ ParseString(line, runLSevent, tauPt, tauEta, tauPhi, ePt, eEta, ePhi); }
				}
				inputFile.close();
			}else{
				cout << "ERROR: Cannot open file " << goodEvents << " for reading good eTau events." << endl; exit(1);
			}


		}

		void ETauMatcher::ParseString(string iLine, vector<string>* iRunLSeventVec,
										vector<float>* iTauPtVec, vector<float>* iTauEtaVec, vector<float>* iTauPhiVec,
										vector<float>* iEPtVec, vector<float>* iEEtaVec, vector<float>* iEPhiVec){

			string line = string(iLine);
			line = line.substr(line.find("  ")+2);
			string sRun = line.substr(0,line.find("  "));
			line = line.substr(line.find("  ")+2);
			string sLS = line.substr(0,line.find("  "));
			line = line.substr(line.find("  ")+2);
			string sEvent = line.substr(0,line.find("  "));
			line = line.substr(line.find("  tau-pT  ")+10);
			string sTauPt = line.substr(0,line.find("  "));
			line = line.substr(line.find("  eta  ")+7);
			string sTauEta = line.substr(0,line.find("  "));
			line = line.substr(line.find("  phi  ")+7);
			string sTauPhi = line.substr(0,line.find("  "));
			line = line.substr(line.find("  e-pT  ")+8);
			string sEPt = line.substr(0,line.find("  "));
			line = line.substr(line.find("  e-eta  ")+9);
			string sEEta = line.substr(0,line.find("  "));
			line = line.substr(line.find("  e-phi  ")+9);
			string sEPhi = line;


			iRunLSeventVec->push_back(string(sRun+":"+sLS+":"+sEvent));
			iTauPtVec->push_back(atof(sTauPt.c_str()));
			iTauEtaVec->push_back(atof(sTauEta.c_str()));
			iTauPhiVec->push_back(atof(sTauPhi.c_str()));
			iEPtVec->push_back(atof(sEPt.c_str()));
			iEEtaVec->push_back(atof(sEEta.c_str()));
			iEPhiVec->push_back(atof(sEPhi.c_str()));

		}

		float ETauMatcher::GetDeltaRWithTau(float iEta, float iPhi){
			float result = 999;		
	
				if( thisEvent != -1 ){
					return DeltaR(tauEta->at(thisEvent), tauPhi->at(thisEvent), iEta, iPhi);
				}	

			return result;
		} 

		float ETauMatcher::GetDeltaRWithE(float iEta, float iPhi){
			float result = 999;		
	
				if( thisEvent != -1 ){
					return DeltaR(eEta->at(thisEvent), ePhi->at(thisEvent), iEta, iPhi);
				}	

			return result;

		} 

		float ETauMatcher::GetEPt(){
					return ePt->at(thisEvent);
		} 

		int ETauMatcher::GetElectronPos( vector<float>* iEtaVec, vector<float>* iPhiVec){
			int result = -1;

				if( thisEvent != -1 ){

					float minDeltaR = 999.9;
					int minDeltaRi	= -1;

					for(unsigned int j=0; j < iEtaVec->size(); j++){

						float currentDeltaR = DeltaR(eEta->at(thisEvent), ePhi->at(thisEvent), iEtaVec->at(j), iPhiVec->at(j));
						//cout << currentDeltaR << endl;

						if(currentDeltaR < minDeltaR){
							minDeltaR = currentDeltaR;	
							minDeltaRi = j;
						}
					}

					return minDeltaRi;
				}	

			return result;
		}
		
		float ETauMatcher::DeltaR(float iEta1, float iPhi1, float iEta2, float iPhi2){
		
			float deltaEta2 = pow(iEta1-iEta2,2); 
			float deltaPhi2 = pow(iPhi1-iPhi2,2); 
			return sqrt(deltaEta2+deltaPhi2);

		}

		void ETauMatcher::SetEvent(string iRunLSevent){
			thisEvent = -1;
			for(unsigned int i=0; i < runLSevent->size(); i++){
				if( iRunLSevent.compare(runLSevent->at(i))==0 ){
					thisEvent = i;
				}
			}
		
		}


