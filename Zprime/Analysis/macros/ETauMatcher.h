#ifndef ETauMatcher_h
#define ETauMatcher_h

#include <iostream>
#include <utility>
#include <algorithm>
#include <TFile.h>
#include <math.h>
#include <iostream>
#include <fstream>

using namespace std;

class ETauMatcher{
	private:

	public:
		string goodEvents;
		vector<string>* runLSevent;
		vector<float>* tauPt;
		vector<float>* tauEta;
		vector<float>* tauPhi;
		vector<float>* ePt;
		vector<float>* eEta;
		vector<float>* ePhi;
		int thisEvent;

		ETauMatcher();
		void ParseString(string, vector<string>*, vector<float>*, vector<float>*, vector<float>*, vector<float>*, vector<float>*, vector<float>*);
		float GetDeltaRWithTau(float, float);
		float GetDeltaRWithE(float, float);
		int GetElectronPos(vector<float>*, vector<float>*);

		float GetEPt();
		float DeltaR(float, float, float, float);
		void SetEvent(string);
};

#endif

