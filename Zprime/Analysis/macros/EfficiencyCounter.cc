
#define EfficiencyCounter_cxx
#include "EfficiencyCounter.h"

using namespace std;



// Default constructor
EfficiencyCounter::EfficiencyCounter(){
	results.clear();
	results_LS.clear();

	results_QCD = NULL;
}


// Default destructor
EfficiencyCounter::~EfficiencyCounter(){
}

// Add the cut results for a new topology
void EfficiencyCounter::AddTopology(string iTopology, vector<cutResult*>* iCutResults){
	pair<string,vector<cutResult*>*>* toInsert = new pair<string,vector<cutResult*>*>();
	toInsert->first		= iTopology;
	toInsert->second	=  new vector<cutResult*>(*iCutResults);
	results.push_back(toInsert);
}

void EfficiencyCounter::AddTopology_LS(string iTopology, vector<cutResult*>* iCutResults){
	pair<string,vector<cutResult*>*>* toInsert = new pair<string,vector<cutResult*>*>();
	toInsert->first		= iTopology;
	vector<cutResult*>* normalizedCutResults = new vector<cutResult*>(*iCutResults);

	int sumFactor = 1;
	if (iTopology.compare("QCD")==0){ sumFactor = 1;	}
	else{	sumFactor = -1;	}

	for(unsigned int c=0; c<iCutResults->size(); c++){
		(normalizedCutResults->at(c))->preEvents	*= (sumFactor);
		(normalizedCutResults->at(c))->postEvents	*= (sumFactor);
		(normalizedCutResults->at(c))->oriEvents	*= (sumFactor);
	}
	
	toInsert->second	=  new vector<cutResult*>(*normalizedCutResults);
	results_LS.push_back(toInsert);
}

void EfficiencyCounter::BuildQCD(){

	if(results_QCD != NULL){ 
		results_QCD->clear();
	}else{
		results_QCD = new vector<cutResult*>();
	}

	if(results_LS.size() == 0 ){ return; }
	

	
	cutResult* toAdd;

	// Loop over every cut
	for(unsigned int c=0; c<(((results_LS.at(0))->second)->size()); c++){
		cutResult* toAdd		= new cutResult();
		
		toAdd->cutName			= ((results_LS.at(0))->second)->at(c)->cutName;
		toAdd->preEvents		= ((results_LS.at(0))->second)->at(c)->preEvents;
		toAdd->postEvents		= ((results_LS.at(0))->second)->at(c)->postEvents;
		toAdd->oriEvents		= ((results_LS.at(0))->second)->at(c)->oriEvents;

		for(unsigned int t=1; t<(results_LS.size()); t++){
			string thisTopo = (results_LS.at(t))->first;
			toAdd->preEvents		+= ((results_LS.at(t))->second)->at(c)->preEvents;
			toAdd->postEvents		+= ((results_LS.at(t))->second)->at(c)->postEvents;
			toAdd->oriEvents		+= ((results_LS.at(t))->second)->at(c)->oriEvents;
		}

		results_QCD->push_back(toAdd);
	}
	//*/

	pair<string,vector<cutResult*>*>* toInsert = new pair<string,vector<cutResult*>*>();
	toInsert->first		= "QCD";
	toInsert->second	= results_QCD;
	vector<pair<string,vector<cutResult*>*>*>::iterator position;
	position = results.begin();
	position++;
	results.insert(position, 1, toInsert);


	for(unsigned int r=0; r<results_LS.size(); r++){
		results.push_back(results_LS.at(r));
	}

}

// Return cut name
string EfficiencyCounter::GetCutString(cutResult* iCutResult){
	return iCutResult->cutName;
}

// Get the number of events surviving this cut
string EfficiencyCounter::GetEvents(cutResult* iCutResult, int iPrecision){
	stringstream toReturn; toReturn.str("");
	toReturn << setprecision(iPrecision) << iCutResult->postEvents;
	return (toReturn.str());
}

// Get relative efficiency of this cut
string EfficiencyCounter::GetRelativeEfficiency(cutResult* iCutResult, int iPrecision){
	stringstream toReturn; toReturn.str("");
	float efficiency = iCutResult->postEvents/(double)iCutResult->preEvents;
	toReturn << setprecision(iPrecision) << efficiency*100;
	return (toReturn.str());
}
string EfficiencyCounter::GetRelativeEfficiencyWerror(cutResult* iCutResult, int iPrecision){
	stringstream toReturn; toReturn.str("");
	float num = iCutResult->postEvents;
	float den = iCutResult->preEvents;
	float efficiency = num/(double)den;
	float error = efficiency*sqrt((1/(double)num)-(1/(double)den));

	error = ((int)((error*pow(10,4))+0.5))/(double)pow(10,4);
	efficiency = ((int)((efficiency*pow(10,4))+0.5))/(double)pow(10,4);

	toReturn << "$ " << efficiency << " \\pm " << error << " $";
	return (toReturn.str());
}

// Get cumulative efficiency of this cut
string EfficiencyCounter::GetCumulativeEfficiency(cutResult* iCutResult, int iPrecision){
	stringstream toReturn; toReturn.str("");
	float efficiency = iCutResult->postEvents/(double)iCutResult->oriEvents;
	toReturn << setprecision(iPrecision) << efficiency*100;
	return (toReturn.str());
}

string EfficiencyCounter::EfficienciesHTML(){
	stringstream result; result.str("");

	pair<string,vector<cutResult*>*>* currentTopo;
	vector<cutResult*>* currentCuts;

	if(results.size()==0){ return ""; }



	result << "<HTML><table border=1 cellspacing=0>" << endl;
	
	// Table headers
	result << "<tr><th></th>";
	for(unsigned int t=0; t<results.size(); t++){
		currentTopo = results.at(t);
		result << "<th colspan=3>" << currentTopo->first << "</th>";
	}
	result << "</tr>" << endl;

	// Subheaders
	result << "<tr><th></th>";
	for(unsigned int t=0; t<results.size(); t++){
		currentTopo = results.at(t);
		result << "<th>Events</th> ";
		result << "<th>Relative</th> ";
		result << "<th>Cumulative</th> ";
	}
	result << "</tr>" << endl;


	// Loop over all the cuts
	currentTopo = results.at(0);
	currentCuts = currentTopo->second;
	for(unsigned int c=0; c<currentCuts->size(); c++){
		result << "<tr> ";
		string cutName = currentCuts->at(c)->cutName;
		result << "<td>" << cutName << "</td> ";

		// Loop over all the topologies
		for(unsigned int t=0; t<results.size(); t++){
			currentTopo = results.at(t);
			currentCuts = currentTopo->second;
			result << "<td>" << GetEvents(currentCuts->at(c),8) << "</td> ";
			result << "<td>" << GetRelativeEfficiency(currentCuts->at(c)) << "</td> ";
			result << "<td>" << GetCumulativeEfficiency(currentCuts->at(c)) << "</td> ";
		}
		result << "</tr>" << endl;
	}
	result << "</table></HTML>" << endl;
	result << "<style type=\"text/css\">td{text-align: center; } </style>" << endl;

	string output = Replace(result.str(),"<=","&le;");
	//output = Replace(output,"==","=");

	return output;
}


string EfficiencyCounter::EfficienciesCSV(){
	stringstream result; result.str("");

	pair<string,vector<cutResult*>*>* currentTopo;
	vector<cutResult*>* currentCuts;

	if(results.size()==0){ return ""; }
	
	// Table headers
	result << ",";
	for(unsigned int t=0; t<results.size(); t++){
		currentTopo = results.at(t);
		result << "" << currentTopo->first << ",,,";
	}
	result << "" << endl;

	// Subheaders
	result << ",";
	for(unsigned int t=0; t<results.size(); t++){
		currentTopo = results.at(t);
		result << "Events,";
		result << "Relative,";
		result << "Cumulative,";
	}
	result << "" << endl;


	// Loop over all the cuts
	currentTopo = results.at(0);
	currentCuts = currentTopo->second;
	for(unsigned int c=0; c<currentCuts->size(); c++){
		result << "";
		string cutName = currentCuts->at(c)->cutName;
		result << "" << cutName << ",";

		// Loop over all the topologies
		for(unsigned int t=0; t<results.size(); t++){
			currentTopo = results.at(t);
			currentCuts = currentTopo->second;
			result << "" << GetEvents(currentCuts->at(c),8) << ",";
			result << "" << GetRelativeEfficiency(currentCuts->at(c)) << ",";
			result << "" << GetCumulativeEfficiency(currentCuts->at(c)) << ",";
		}
		result << "" << endl;
	}
	result << "" << endl;

	string output = result.str();

	return output;
}


void EfficiencyCounter::PrintEfficienciesHTML(string iOutputFile){

	if((results_QCD == NULL) && (results_LS.size()>0) ){ BuildQCD(); }

	WriteToFile(EfficienciesHTML(),iOutputFile);
}

void EfficiencyCounter::PrintEfficienciesCSV(string iOutputFile){

	if((results_QCD == NULL) && (results_LS.size()>0) ){ BuildQCD(); }

	WriteToFile(EfficienciesCSV(),iOutputFile);
}

string EfficiencyCounter::EfficienciesTex(){
	stringstream result; result.str("");

	pair<string,vector<cutResult*>*>* currentTopo;
	vector<cutResult*>* currentCuts;

	if(results.size()==0){ return ""; }



	result << "\\begin{sidewaystable}[htb!]" << endl;
	result << "\\caption{}" << endl;
	result << "\\label{dihad:tab:dihadEff}" << endl;
	result << "\\centering{" << endl;
	result << "\\begin{tabular}{|l|";

	for(unsigned int i = 0; i < results.size(); i++){
		result << "c";	
	}
	result << "}\\hline\\hline" << endl;
	result << "Cut";

	
	// Table headers
	for(unsigned int t=0; t<results.size(); t++){
		currentTopo = results.at(t);
		result << " & " << currentTopo->first;
	}
	result << "\\\\\\hline" << endl;

	// Loop over all the cuts
	currentTopo = results.at(0);
	currentCuts = currentTopo->second;
	for(unsigned int c=0; c<currentCuts->size(); c++){
		string cutName = currentCuts->at(c)->cutName;
		result << cutName;

		// Loop over all the topologies
		for(unsigned int t=0; t<results.size(); t++){
			currentTopo = results.at(t);
			currentCuts = currentTopo->second;
			result << " & " << GetRelativeEfficiencyWerror(currentCuts->at(c));
		}
		result << " \\\\" << endl;
	}
	result << "\\hline\\hline" << endl;
	result << "\\end{tabular}" << endl;
	result << "}" << endl;
	result << "\\end{sidewaystable}" << endl;
	result << "\\clearpage" << endl;

	string output;

	//string output = Replace(result.str(),"<=","&le;");
	//output = Replace(output,"==","=");
	output = result.str();

	return output;
}

void EfficiencyCounter::PrintEfficienciesTex(string iOutputFile){

	//if((results_QCD == NULL) && (results_LS.size()>0) ){ BuildQCD(); }

	WriteToFile(EfficienciesTex(),iOutputFile);
}

void EfficiencyCounter::WriteToFile(string iContent, string iOutputFile){

	if (iOutputFile.length() == 0){
		cout << iContent << endl;
	}else{
		ofstream fout(iOutputFile.c_str());
		if (fout.is_open()){
			fout << iContent << endl;
			fout.close();
		}else{ cout << ">>> ERROR: Unable to open file" << endl; exit(1); }
	}

}


string EfficiencyCounter::Replace(string input, string oldStr, string newStr){
	string toReturn = string(input);
	size_t pos = 0;
	while((pos = toReturn.find(oldStr, pos)) != std::string::npos){
		toReturn.replace(pos, oldStr.length(), newStr);
		pos += newStr.length();
	}

	return toReturn;
}


