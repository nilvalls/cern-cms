
#ifndef EfficiencyCounter_h
#define EfficiencyCounter_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TGraphAsymmErrors.h>
#include <math.h>
#include <map>
#include <vector>
#include <sstream>
#include <iostream>
#include <utility>
#include <algorithm>
#include <TStyle.h>
#include <TCanvas.h>
#include <stdlib.h>
#include <fstream>
#include <iomanip>
#include "CommonDefs.h"

using namespace std;
using namespace Common;


class EfficiencyCounter {
	public :
		vector<pair<string,vector<cutResult*>*>*> results;
		vector<pair<string,vector<cutResult*>*>*> results_LS;
		vector<cutResult*>* results_QCD;

		// Default constructor
		EfficiencyCounter();
		virtual ~EfficiencyCounter();
		virtual void AddTopology(string, vector<cutResult*>*);
		virtual void AddTopology_LS(string, vector<cutResult*>*);

		virtual string GetCutString(cutResult*);
		virtual string GetEvents(cutResult*,int iPrecision=3);
		virtual string GetRelativeEfficiency(cutResult*,int iPrecision=3);
		virtual string GetRelativeEfficiencyWerror(cutResult*,int iPrecision=3);
		virtual string GetCumulativeEfficiency(cutResult*,int iPrecision=3);

		virtual void BuildQCD();

		virtual string EfficienciesHTML();
		virtual string EfficienciesCSV();
		virtual string EfficienciesTex();
		virtual void WriteToFile(string iContent, string iOutputFile="");
		virtual void PrintEfficienciesHTML(string iOutputFile="");
		virtual void PrintEfficienciesCSV(string iOutputFile="");
		virtual void PrintEfficienciesTex(string iOutputFile="");

	//	virtual void PrintEfficiencies(string iOutputFile="");
	//	virtual void PrintEfficienciesHTML(string iOutputFile="");
	//	virtual void PrintEfficienciesTex(string iOutputFile="");
	//	virtual string EfficienciesHTML(bool,bool);
	//	virtual string EfficienciesTex(bool,bool);
	//	virtual string EfficienciesTwiki(bool,bool);

		virtual string Replace(string, string, string);


};

#endif

