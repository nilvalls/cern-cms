
#define EventCounter_cxx
#include "EventCounter.h"

using namespace std;

// Default constructor
EventCounter::EventCounter(){
	collisionsMap	= NULL;
	backgroundMap	= NULL;
	backgroundMaps.clear();
	signalMaps.clear();

	collisionsLabel	= "";
	backgroundLabels.clear();
	signalLabels.clear();
}

EventCounter::~EventCounter(){
}

void EventCounter::AddCollisions(string iLabel, TH1F* iHisto){
	collisionsMap = new map<float,float>();

	for(int bin=1; bin<=iHisto->GetNbinsX(); bin++){
		(*collisionsMap)[iHisto->GetBinLowEdge(bin)] = iHisto->GetBinContent(bin);	
	}

	collisionsLabel = iLabel;
}

void EventCounter::AddBackground(string iLabel, TH1F* iHisto){
	map<float, float>* tempMap = new map<float,float>();
	if(backgroundMaps.size() == 0){ backgroundMap = new map<float,float>(); backgroundMap->clear(); }

	for(int bin=1; bin<=iHisto->GetNbinsX(); bin++){
		float mass = iHisto->GetBinLowEdge(bin);
		float content = iHisto->GetBinContent(bin);

		(*tempMap)[mass] = content;
		(*backgroundMap)[mass] += content;
	}

	backgroundMaps.push_back(tempMap);
	backgroundLabels.push_back(iLabel);
}

void EventCounter::AddSignal(string iLabel, TH1F* iHisto){
	map<float, float>* tempMap = new map<float,float>();

	for(int bin=1; bin<=iHisto->GetNbinsX(); bin++){
		float mass = iHisto->GetBinLowEdge(bin);
		float content = iHisto->GetBinContent(bin);

		(*tempMap)[mass] = content;
	}

	signalMaps.push_back(tempMap);
	signalLabels.push_back(iLabel);
}

vector<float>* EventCounter::ParseBinString(const string& iS, char c) {
	vector<float>* v = new vector<float>(); v->clear();

	string s = string(iS);

	// Put every number in the vector
	string::size_type curr = 0;
	string::size_type next = min(s.find(c), s.length());
	while (next < s.length()){
		string toAdd = string(s.substr(curr, next-curr));
		v->push_back(atof(toAdd.c_str()));

		curr = next+1;
		next = s.find(c, next+1);
	}
	v->push_back(atof((s.substr(curr, next-curr)).c_str()));

	return v;
}

void EventCounter::PrintToHTML(string iPath, string iBins){

	stringstream ssout; ssout.str("");
	ofstream fout(iPath.c_str());
	if (fout.is_open()){

		map<float,float>* refMap;
		map<float,float>::const_iterator refMapIt;
		if( collisionsMap != NULL){ refMap = collisionsMap; }
		else if (backgroundMaps.size() > 0){ refMap = backgroundMaps.front(); }
		else{ cerr << "ERROR: No topologies from which to count events." << endl; return; }
		int numCols = 1;

		// Print table headers
		vector<float>* masses = ParseBinString(iBins,',');

		if(iBins.compare("-1")==0)	{ numCols = 1+refMap->size(); }
		else						{ numCols = 1+masses->size(); }

		ssout << "<html>" << endl;
		ssout << "<style>table td{text-align:center}</style>" << endl;
		ssout << "<table border=1 cellspacing=0>" << endl;
		ssout << "<tr><th></th><th colspan=" << numCols-1 << ">Events for Mass >= [GeV/c^2]</th>";
		ssout << "<tr><th>Topology</th>";
		if(iBins.compare("-1")==0){
			for(refMapIt = refMap->begin(); refMapIt != refMap->end(); ++refMapIt){ ssout << "<th>" << refMapIt->first << "</th>"; }
		}else{
			for(int massBin=0; massBin < masses->size(); massBin++){ ssout << "<th>" << masses->at(massBin) << "</th>"; }
		}
		ssout << "</tr>" << endl;

		// Print backgrounds
		for(int bkg = 0; bkg < backgroundLabels.size(); bkg++){ ssout << GetCount(backgroundLabels.at(bkg), backgroundMaps.at(bkg), iBins); }

		// Print total background
		ssout << "<tr><td bgcolor='#000000' colspan=" << numCols << "></td>&nbsp</tr>" << endl;
		if(backgroundLabels.size()>0){ ssout << GetCount("<b>Total expected<b>", backgroundMap, iBins, true); }

		// Print observed
		if( collisionsMap != NULL){ ssout << GetCount("<b>Total observed<b>", collisionsMap, iBins, false); }

		// Print signals
		for(int sig = 0; sig < signalLabels.size(); sig++){ ssout << GetSignalCount(signalLabels.at(sig), signalMaps.at(sig), iBins); }


		string ssoutString = ssout.str();
		size_t pos = 0;
		string oldStr = "+/-";
		string newStr = "&plusmn";
		while((pos = ssoutString.find(oldStr, pos)) != std::string::npos){
			ssoutString.replace(pos, oldStr.length(), newStr);
			pos += newStr.length();
		}

		fout << ssoutString;
		fout << "</table>" << endl;
		fout << "<br><br>\n" << GetObservedOverExpected() << endl;
		fout.close();
		ssout.str("");
	}else{ cerr << "ERROR in " << __FILE__ << ":" << __LINE__ << ": Could not open file " << iPath << " for writing event count." << endl; }
}


void EventCounter::PrintToTex(string iPath, string iBins){

	stringstream ssout; ssout.str("");
	ofstream fout(iPath.c_str());
	if (fout.is_open()){

		map<float,float>* refMap;
		map<float,float>::const_iterator refMapIt;
		if( collisionsMap != NULL){ refMap = collisionsMap; }
		else if (backgroundMaps.size() > 0){ refMap = backgroundMaps.front(); }
		else{ cerr << "ERROR: No topologies from which to count events." << endl; return; }
		int numCols = 1;

		// Print table headers
		vector<float>* masses = ParseBinString(iBins,',');

		if(iBins.compare("-1")==0)	{ numCols = 1+refMap->size(); }
		else						{ numCols = 1+masses->size(); }

		ssout << "\\begin{table}" << endl;
		ssout << "\\begin{tabular}{|l|";
		for(int i=0; i<(numCols-1); i++){
			ssout << "c|";
		}
		ssout << "}\\hline" << endl;
		ssout << "\\multicolumn{" << (numCols) << "}{|c|}{Number of events after cuts}\\\\\\hline" << endl;

		ssout << "$M(\\tau_1,\\tau_2,\\MET)>$"; 
		if(iBins.compare("-1")==0){
			for(refMapIt = refMap->begin(); refMapIt != refMap->end(); ++refMapIt){ ssout << " & " << refMapIt->first; }
		}else{
			for(int massBin=0; massBin < masses->size(); massBin++){ ssout << " & " << masses->at(massBin); }
		}
		ssout << "\\\\\\hline\\hline" << endl;

		// Print backgrounds
		for(int bkg = 0; bkg < backgroundLabels.size(); bkg++){ ssout << GetCountTex(backgroundLabels.at(bkg), backgroundMaps.at(bkg), iBins); }

		// Print total background
		ssout << "\\multicolumn{" << numCols << "}{|c|}{}\\\\\\hline" << endl;
		if(backgroundLabels.size()>0){ ssout << GetCountTex("<b>Total expected<b>", backgroundMap, iBins); }

		// Print observed
		if( collisionsMap != NULL){ ssout << GetCountTex("Total observed", collisionsMap, iBins); }

		// Print signals
		for(int sig = 0; sig < signalLabels.size(); sig++){ ssout << GetSignalCountTex(signalLabels.at(sig), signalMaps.at(sig), iBins); }

		ssout << "\\end{tabular}" << endl;
		ssout << "\\end{table}" << endl;


		string ssoutString = ssout.str();
		size_t pos = 0;
		string oldStr = "+/-";
		string newStr = "$\\pm$";
		while((pos = ssoutString.find(oldStr, pos)) != std::string::npos){
			ssoutString.replace(pos, oldStr.length(), newStr);
			pos += newStr.length();
		}

		fout << ssoutString;
		fout.close();
		ssout.str("");
	}else{ cerr << "ERROR in " << __FILE__ << ":" << __LINE__ << ": Could not open file " << iPath << " for writing event count." << endl; }
}


string EventCounter::GetCount(string iLabel, map<float,float>* iRefMap, string iBins, bool iPrintError){
		map<float,float>* refMap;
		refMap = iRefMap;
		vector<float>* masses = ParseBinString(iBins,',');
		stringstream ssret; ssret.str("");

		map<float,float>::const_iterator refMapIt;
		ssret << "<tr><td>" << iLabel << "</td>";
		if(iBins.compare("-1")==0){
			for(refMapIt = refMap->begin(); refMapIt != refMap->end(); ++refMapIt){ ssret << "<td>" << setprecision(99) << (nPMe(refMapIt->second)) << "</td>"; }
		}else{
			if(iPrintError){
				for(int massBin=0; massBin < masses->size(); massBin++){ ssret << "<td>" << setprecision(99) << (nPMe((*refMap)[(masses->at(massBin))])) << "</td>"; }
			}else{
				for(int massBin=0; massBin < masses->size(); massBin++){ ssret << "<td>" << setprecision(99) << (*refMap)[(masses->at(massBin))] << "</td>"; }
			}
		}
		ssret << "</tr>" << endl;

		return (ssret.str());
}


string EventCounter::GetSignalCount(string iLabel, map<float,float>* iRefMap, string iBins){
		map<float,float>* refMap;
		refMap = iRefMap;
		vector<float>* masses = ParseBinString(iBins,',');
		stringstream ssret; ssret.str("");
		int numCols = 0;
		if(iBins.compare("-1")==0)	{ numCols = 1+refMap->size(); }
		else						{ numCols = 1+masses->size(); }

		map<float,float>::const_iterator refMapIt;
		if(signalLabels.size()>0){ ssret << "<tr><td bgcolor='0000FF' colspan=" << numCols << "></td>&nbsp</tr>" << endl;}
		ssret << "<tr><td>" << iLabel << "</td>";
		if(iBins.compare("-1")==0){
			for(refMapIt = refMap->begin(); refMapIt != refMap->end(); ++refMapIt){ ssret << "<td>" << nPMe(refMapIt->second) << "</td>"; }
		}else{
			for(int massBin=0; massBin < masses->size(); massBin++){ ssret << "<td>" << nPMe((*refMap)[(masses->at(massBin))]) << "</td>"; }
		}
		ssret << "</tr>" << endl;

		if(backgroundMap!=NULL){
		if(backgroundMap->size() !=0){
			ssret << "<tr><td>" << "s/sqrt(s+b)" << "</td>";
			if(iBins.compare("-1")==0){
				for(refMapIt = refMap->begin(); refMapIt != refMap->end(); ++refMapIt){ 
					float s = (refMapIt->second);
					float b = (*backgroundMap)[(refMapIt->first)];
					ssret << "<td>" << setprecision(2) << (s/sqrt(s+b)) << "</td>";
				}
			}else{
				for(int massBin=0; massBin < masses->size(); massBin++){
					float s = (*refMap)[(masses->at(massBin))];
					float b = (*backgroundMap)[(masses->at(massBin))];
					ssret << "<td>" << setprecision(2) << (s/sqrt(s+b)) << "</td>";
				}
			}
			ssret << "</tr>" << endl;
		
		}
		}

		return (ssret.str());
}

string EventCounter::GetCountTex(string iLabel, map<float,float>* iRefMap, string iBins){
		map<float,float>* refMap;
		refMap = iRefMap;
		vector<float>* masses = ParseBinString(iBins,',');
		stringstream ssret; ssret.str("");

		map<float,float>::const_iterator refMapIt;
		ssret << iLabel;
		if(iBins.compare("-1")==0){
			for(refMapIt = refMap->begin(); refMapIt != refMap->end(); ++refMapIt){ ssret << " & " << (int)((refMapIt->second)+0.5); }  
		}else{
			for(int massBin=0; massBin < masses->size(); massBin++){ ssret << " & " << (int)((*refMap)[(masses->at(massBin))]+0.5); }
		}
		ssret << "\\\\\\hline" << endl;

		return (ssret.str());
}


string EventCounter::GetSignalCountTex(string iLabel, map<float,float>* iRefMap, string iBins){
		map<float,float>* refMap;
		refMap = iRefMap;
		vector<float>* masses = ParseBinString(iBins,',');
		stringstream ssret; ssret.str("");
		int numCols = 0;
		if(iBins.compare("-1")==0)	{ numCols = 1+refMap->size(); }
		else						{ numCols = 1+masses->size(); }

		map<float,float>::const_iterator refMapIt;
		if(signalLabels.size()>0){ 
			ssret << "\\multicolumn{" << numCols << "}{|c|}{}\\\\\\hline" << endl;
			ssret << iLabel;
			if(iBins.compare("-1")==0){
				for(refMapIt = refMap->begin(); refMapIt != refMap->end(); ++refMapIt){ ssret << " & " << (int)((refMapIt->second)+0.5); }
			}else{
				for(int massBin=0; massBin < masses->size(); massBin++){ ssret << " & " << (int)((*refMap)[(masses->at(massBin))]+0.5); }
			}
			ssret << "\\\\\\hline" << endl;

			if(backgroundMap->size() !=0){
				ssret << "$s/\\sqrt(s+b)$";
				if(iBins.compare("-1")==0){
					for(refMapIt = refMap->begin(); refMapIt != refMap->end(); ++refMapIt){ 
						float s = (refMapIt->second);
						float b = (*backgroundMap)[(refMapIt->first)];
						ssret << " & " << setprecision(2) << (s/sqrt(s+b));
					}
				}else{
					for(int massBin=0; massBin < masses->size(); massBin++){
						float s = (*refMap)[(masses->at(massBin))];
						float b = (*backgroundMap)[(masses->at(massBin))];
						ssret << " & " << setprecision(2) << (s/sqrt(s+b)); }
				}
			}
			ssret << "\\\\\\hline" << endl;

		}

		return (ssret.str());
}


string EventCounter::nPMe(float number){
	return nPMe(number,sqrt(number));
}

string EventCounter::nPMe(float number, float error){
	stringstream output;	
	stringstream errorSS;
	string ferror;
	stringstream numberSS;
	string fnumber;

	errorSS << setprecision(9) << error;
	ferror = errorSS.str();
	errorSS.str("");

	numberSS << number;
	fnumber = numberSS.str();
	numberSS.str("");

	int integersInError = 0;	

	if( error < 1){
		int decimalsInError;
		decimalsInError = (ferror.substr(ferror.find('.') + 1)).length();		

		int decimalsInNumber = 0;
		int integersInNumber = 0;

		if ( ((int)number) > 0 ){
			integersInNumber  = (fnumber.substr(0,fnumber.find('.'))).length();
		}

		integersInError = 1;

		numberSS << setprecision( integersInNumber + decimalsInError ) << number;

	}else{

		int integersInNumber = 0;

		numberSS << ((int)(number+0.5));
		integersInNumber = (numberSS.str()).length();
		numberSS.str("");

		integersInError = ferror.length();

		numberSS << setprecision( integersInNumber - integersInError + 1 ) << number;

	}   

	string errorPadding="";
	for (int i=1; i <=(5-integersInError); i++){
		errorPadding = errorPadding + string(" "); 
	}   

	output << numberSS.str() << " +/- " << ferror;

	return output.str();

}


string EventCounter::GetObservedOverExpected(){

		stringstream result; result.str("");

		if( (collisionsMap != NULL) && (backgroundMaps.size() > 0) ){

			float exp =	(*backgroundMap)[0];
			float obs = (*collisionsMap)[0];
			result << "Observed / Expected = " << setprecision(log10(obs)+1) << obs << " / " << setprecision((log10(exp)+3))  << exp << " = " << setprecision(3) << (obs/exp) << endl;

		}


		return (result.str());

}
