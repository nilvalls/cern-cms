
#ifndef EventCounter_h
#define EventCounter_h

#include <TH1F.h>
#include <math.h>
#include <map>
#include <vector>
#include <sstream>
#include <iostream>
#include <utility>
#include <fstream>
#include <iomanip>
#include <stdlib.h>


using namespace std;



class EventCounter {
	public :
		map<float,float>* collisionsMap;
		map<float,float>* backgroundMap;
		vector<map<float,float>*> backgroundMaps;
		vector<map<float,float>*> signalMaps;

		string collisionsLabel;
		vector<string> backgroundLabels;
		vector<string> signalLabels;

		// Default constructor
		EventCounter();
		virtual ~EventCounter();
		virtual void AddCollisions(string, TH1F*);
		virtual void AddBackground(string, TH1F*);
		virtual void AddSignal(string, TH1F*);
		virtual void PrintToHTML(string,string iBins="-1");
		virtual void PrintToTex(string,string iBins="-1");
		void Clear();
		vector<float>* ParseBinString(const string&, char);
		string GetCount(string, map<float,float>*, string iBins="-1", bool iPrintError=true);
		string GetSignalCount(string, map<float,float>*, string iBins="-1");
		string GetCountTex(string, map<float,float>*, string iBins="-1");
		string GetSignalCountTex(string, map<float,float>*, string iBins="-1");
		string GetObservedOverExpected();
		string nPMe(float);
		string nPMe(float,float);

};

#endif

