
#ifndef FitterNtupleProducer_h
#define FitterNtupleProducer_h

#include <TH1F.h>
#include <math.h>
#include <map>
#include <vector>
#include <sstream>
#include <iostream>
#include <utility>
#include <fstream>
#include <iomanip>
#include <stdlib.h>


using namespace std;



class FitterNtupleProducer {
	public :
		// Default constructor
		FitterNtupleProducer(string);
		virtual ~FitterNtupleProducer();
		virtual void AddCollisions(string, TH1F*);
};

#endif

