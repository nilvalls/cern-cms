#ifndef HistoIntegrator_h
#define HistoIntegrator_h

#include <float.h>
#include <iostream>
#include <utility>
#include <algorithm>
#include <TFile.h>
#include "HistoWrapper.h"

using namespace std;

class HistoIntegrator{
	private:
		map<int,double> weight;
		map<int,double> generated;

	public:
		// Default constructor
		HistoIntegrator(){
		}

		HistoWrapper* Integrate(HistoWrapper* iHistoWrapper, bool fromLeft){
			HistoWrapper* result = new HistoWrapper(*iHistoWrapper);
			TH1F* histo = new TH1F(*(TH1F*)(iHistoWrapper->GetHisto()));

			float sum = 0;
			float error = 0;
			if(fromLeft){
				for(int bin=1; bin <= histo->GetNbinsX(); bin++){
					sum += histo->GetBinContent(bin);	
					error = sqrt(pow(error,2)+pow(histo->GetBinContent(bin),2));	
					histo->SetBinContent(bin, sum);	
					histo->SetBinError(bin, error);	
				}
				histo->SetName((string(histo->GetName())+"_LeftIntegrated").c_str());
				result->SetYlabel("Accumulated Events");
				result->SetOutputFilename((result->GetOutputFilename())+"_LeftIntegrated");

			}else{
				for(int bin=histo->GetNbinsX(); bin >= 1; bin--){
					sum += histo->GetBinContent(bin);	
					error = sqrt(pow(error,2)+pow(histo->GetBinContent(bin),2));	
					histo->SetBinContent(bin, sum);	
					histo->SetBinError(bin, error);	
				}
				histo->SetName((string(histo->GetName())+"_RightIntegrated").c_str());
				result->SetYlabel("Accumulated Events");
				result->SetOutputFilename((result->GetOutputFilename())+"_RightIntegrated");
			}

			result->SetHisto(histo);

			return result;
		}

		TH1F* Integrate(TH1F* iHisto, bool fromLeft){

			TH1F* histo = (TH1F*)iHisto->Clone();
			//TH1F* histo = (TH1F*)iHisto->Clone((string(iHisto->GetName()+"_LeftIntegrated")).c_str());

			float sum = 0;
			float error = 0;
			if(fromLeft){
				for(int bin=1; bin <= histo->GetNbinsX(); bin++){
					sum += histo->GetBinContent(bin);	
					error = sqrt(pow(error,2)+pow(histo->GetBinContent(bin),2));	
					histo->SetBinContent(bin, sum);	
					histo->SetBinError(bin, error);	
				}

			}else{
				for(int bin=histo->GetNbinsX(); bin >= 1; bin--){
					sum += histo->GetBinContent(bin);	
					error = sqrt(pow(error,2)+pow(histo->GetBinContent(bin),2));	
					histo->SetBinContent(bin, sum);	
					histo->SetBinError(bin, error);	
				}
			}

			return histo;
		}


		HistoWrapper* IntegrateFromLeft(HistoWrapper* iMassWrapper){ return Integrate(iMassWrapper,true); }
		HistoWrapper* IntegrateFromRight(HistoWrapper* iMassWrapper){ return Integrate(iMassWrapper,false); }
		TH1F* IntegrateFromLeft(TH1F* iHisto){ 

			return Integrate(iHisto,true); }
		TH1F* IntegrateFromRight(TH1F* iHisto){ return Integrate(iHisto,false); }


		TH1F* SoverSqrtSpB_Right(TH1F* iSignal, TH1F* iBackground){
			TH1F* bkg	= IntegrateFromRight((TH1F*)(iBackground->Clone()));
			TH1F* sig	= IntegrateFromRight((TH1F*)(iSignal->Clone()));
			TH1F* result = (TH1F*)sig->Clone();

			for(int bin=1; bin <= bkg->GetNbinsX(); bin++){
				double bkgContent = bkg->GetBinContent(bin);
				double sigContent = sig->GetBinContent(bin);
				double content = 0;
				double error = 0;
				if((bkgContent+sigContent)>0){
					content = sigContent/(double)sqrt(sigContent+bkgContent);
					error = 0; 
				}
				result->SetBinContent(bin, content);	
				result->SetBinError(bin, error);	
			}

			return result;

		}

		TH1F* SoverSqrtSpB_Left(TH1F* iSignal, TH1F* iBackground){
			TH1F* bkg	= IntegrateFromLeft((TH1F*)(iBackground->Clone()));
			TH1F* sig	= IntegrateFromLeft((TH1F*)(iSignal->Clone()));
			TH1F* result = (TH1F*)sig->Clone();

			for(int bin=1; bin <= bkg->GetNbinsX(); bin++){
				double bkgContent = bkg->GetBinContent(bin);
				double sigContent = sig->GetBinContent(bin);
				double content = 0;
				double error = 0;

				if((bkgContent+sigContent)>0){
					content = sigContent/(double)sqrt(sigContent+bkgContent);
				}

				result->SetBinContent(bin, content);	
				result->SetBinError(bin, error);	
			}

			return result;

		}


		TH1F* SoverSqrtSpB(TH1F* iSignal, TH1F* iBackground){
			TH1F* bkg	= ((TH1F*)(iBackground->Clone()));
			TH1F* sig	= ((TH1F*)(iSignal->Clone()));
			TH1F* result = (TH1F*)sig->Clone();

			for(int bin=1; bin <= bkg->GetNbinsX(); bin++){
				double bkgContent = bkg->GetBinContent(bin);
				double sigContent = sig->GetBinContent(bin);
				double content	= 0;
				double error	= 0;
				if((bkgContent+sigContent)>0){
					content = sigContent/(double)sqrt(sigContent+bkgContent);
				}
				result->SetBinContent(bin, content);	
				result->SetBinError(bin, error);	
			}

			return result;

		}


		TH1F* SoverSqrtB_Right(TH1F* iSignal, TH1F* iBackground){
			TH1F* bkg	= IntegrateFromRight((TH1F*)(iBackground->Clone()));
			TH1F* sig	= IntegrateFromRight((TH1F*)(iSignal->Clone()));
			TH1F* result = (TH1F*)sig->Clone();

			for(int bin=0; bin <= bkg->GetNbinsX()+1; bin++){
				double bkgContent = bkg->GetBinContent(bin);
				double sigContent = sig->GetBinContent(bin);
				double bkgError = bkg->GetBinError(bin);
				double sigError = sig->GetBinError(bin);
				double content = 0;
				double error	= 0;
				if((bkgContent+sigContent)>0 && (bkgContent >0)){
					content = sigContent/(double)sqrt(bkgContent);
					error = sqrt(pow(sigError,2)/bkgContent+(0.25*pow(sigContent*bkgError,2))/pow(bkgContent,3));
				}
				result->SetBinContent(bin, content);	
				result->SetBinError(bin, error);	
			}

			return result;

		}

		TH1F* SoverSqrtB_Left(TH1F* iSignal, TH1F* iBackground){
			TH1F* bkg	= IntegrateFromLeft((TH1F*)(iBackground->Clone()));
			TH1F* sig	= IntegrateFromLeft((TH1F*)(iSignal->Clone()));
			TH1F* result = (TH1F*)sig->Clone();

			for(int bin=0; bin <= bkg->GetNbinsX()+1; bin++){
				double bkgContent = bkg->GetBinContent(bin);
				double sigContent = sig->GetBinContent(bin);
				double bkgError = bkg->GetBinError(bin);
				double sigError = sig->GetBinError(bin);
				double content = 0;
				double error	= 0;
				if((bkgContent+sigContent)>0 && bkgContent>0){
					content = sigContent/(double)sqrt(bkgContent);
					error = sqrt(pow(sigError,2)/bkgContent+(0.25*pow(sigContent*bkgError,2))/pow(bkgContent,3));
				}

				result->SetBinContent(bin, content);	
				result->SetBinError(bin, error);	
			}

			return result;

		}


		TH1F* SoverSqrtB(TH1F* iSignal, TH1F* iBackground){
			TH1F* bkg	= ((TH1F*)(iBackground->Clone()));
			TH1F* sig	= ((TH1F*)(iSignal->Clone()));
			TH1F* result = (TH1F*)sig->Clone();

			for(int bin=0; bin <= bkg->GetNbinsX()+1; bin++){
				double bkgContent = bkg->GetBinContent(bin);
				double sigContent = sig->GetBinContent(bin);
				double bkgError = bkg->GetBinError(bin);
				double sigError = sig->GetBinError(bin);
				double content	= 0;
				double error	= 0;

				if((bkgContent+sigContent)>0 && (bkgContent>0)){
					content = sigContent/(double)sqrt(bkgContent);
					error = sqrt((pow(sigError,2)/bkgContent)+(0.25*pow(sigContent*bkgError,2))/pow(bkgContent,3));
				}
				result->SetBinContent(bin, content);	
				result->SetBinError(bin, error);	
			}

			return result;

		}

};

#endif
