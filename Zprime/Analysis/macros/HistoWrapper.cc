
#define HistoWrapper_cxx
#include "HistoWrapper.h"
#include <TStopwatch.h>
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <stdlib.h>
#include <fstream>
#include <iomanip>

using namespace std;



// Default constructor
HistoWrapper::HistoWrapper(){
	cutsApplied		= "";
	isBackground	= false;
	isMC			= false;
	isSignal		= false;
	numEvInDS		= 0;
	numEvInNtuple	= 0;
	numEvAnalyzed	= 0;
	numEvPassing	= 0;
	crossSection	= 0;
	branchingRatio	= 0;
	effectiveLumi	= 0;
	logx			= false;
	logy			= false;
	logz			= false;
	showOF			= false;
	showText			= false;
	normalized		= false;
	hasVariableWidthBins = false;
}

HistoWrapper::HistoWrapper(TH1* iHisto){
	//HistoWrapper(); // WTF doesn't this work and I need to reset the vars manually??b
	cutsApplied		= "";
	isBackground	= false;
	isMC			= false;
	isSignal		= false;
	numEvInDS		= 0;
	numEvInNtuple	= 0;
	numEvAnalyzed	= 0;
	numEvPassing	= 0;
	crossSection	= 0;
	branchingRatio	= 0;
	effectiveLumi	= 0;
	logx			= false;
	logy			= false;
	logz			= false;
	showOF			= false;
	showText			= false;
	normalized		= false;
	hasVariableWidthBins = false;
	SetHisto(iHisto);
}


void HistoWrapper::SetMC(bool iSwitch){
	isMC = iSwitch;
	if(isMC){ SetBackground(true); }
}

void HistoWrapper::Fill(double iValue){
	((TH1F*)histo)->Fill(iValue);
	numEvPassing++;
}

void HistoWrapper::Fill(double iValue, double iWeight){
	((TH1F*)histo)->Fill(iValue,iWeight);
	numEvPassing++;
}

void HistoWrapper::Fill(double iValue1, double iValue2, double iWeight){
	((TH2F*)histo)->Fill(iValue1, iValue2, iWeight);
	numEvPassing++;
}

void HistoWrapper::SetBackground(bool iSwitch){
	isBackground = iSwitch;
}

void HistoWrapper::SetSignal(bool iSwitch){
	isSignal = iSwitch;
	if(isSignal){ SetMC(true); }
}

bool HistoWrapper::IsCollisions(){
	return (!(isMC || isBackground || isSignal));
}

bool HistoWrapper::IsMC(){
	return isMC;
}

bool HistoWrapper::IsBackground(){
	return isBackground;
}

bool HistoWrapper::IsSignal(){
	return isSignal;
}

int HistoWrapper::GetNumEvPassing(){
	return numEvPassing;
}

int HistoWrapper::GetNumEvAnalyzed(){
	return numEvAnalyzed;
}

int HistoWrapper::GetNumEvInNtuple(){
	return numEvInNtuple;
}

int HistoWrapper::GetNumEvInDS(){
	return numEvInDS;
}

void HistoWrapper::SetCutsApplied(string iCutsApplied){
	cutsApplied = iCutsApplied;
}

string HistoWrapper::GetCutsApplied(){
	return cutsApplied;
}

bool HistoWrapper::ChargeProductApplied(){
	size_t found = cutsApplied.find("ChargeProduct");
	return ((0 <= found) && (found < cutsApplied.length()));
}

void HistoWrapper::SetNumEvPassing(int iNumEvPassing){
	if(normalized){ cout << "ERROR: trying to setting numEvPassing but histo is already normalized!" << endl; exit(1); }
	numEvPassing = iNumEvPassing;
}

void HistoWrapper::SetNumEvInNtuple(int iNumEvInNtuple){
	if(normalized){ cout << "ERROR: trying to setting numEvInNtuple but histo is already normalized!" << endl; exit(1); }
	numEvInNtuple = iNumEvInNtuple;
}

void HistoWrapper::SetNumEvInDS(int iNumEvInDS){
	if(normalized){ cout << "ERROR: trying to setting numEvInDS but histo is already normalized!" << endl; exit(1); }
	numEvInDS = iNumEvInDS;
}

float HistoWrapper::GetCrossSection(){
	return crossSection;
}
void HistoWrapper::SetCrossSection(float iCrossSection){
	if(normalized){ cout << "ERROR: trying to setting crossSection but histo is already normalized!" << endl; exit(1); }
	crossSection = iCrossSection;
}

float HistoWrapper::GetBranchingRatio(){
	return branchingRatio;
}
void HistoWrapper::SetBranchingRatio(float iBranchingRatio){
	if(normalized){ cout << "ERROR: trying to setting branchingRatio but histo is already normalized!" << endl; exit(1); }
	branchingRatio = iBranchingRatio;
}

float HistoWrapper::GetEffectiveLumi(){
	return effectiveLumi;
}
void HistoWrapper::SetEffectiveLumi(float iEffectiveLumi){
	if(normalized){ cout << "ERROR: trying to setting effectiveLumi but histo is already normalized!" << endl; exit(1); }
	effectiveLumi = iEffectiveLumi;
}

float HistoWrapper::GetNormalization(){
	if(isMC){
		if(numEvAnalyzed	==0){ cout << "WARNING: numEvAnalyzed is set to zero!"	<< endl; };
		if(numEvInNtuple	==0){ cout << "WARNING: numEvInNtuple is set to zero!"	<< endl; };
		if(numEvInDS		==0){ cout << "WARNING: numEvInDS is set to zero!"		<< endl; };
		if(crossSection		==0){ cout << "WARNING: crossSection is set to zero!"	<< endl; };
		if(branchingRatio	==0){ cout << "WARNING: branchingRatio is set to zero!"	<< endl; };
		if(effectiveLumi	==0){ cout << "WARNING: effectiveLumi is set to zero!"	<< endl; };

		long double expectedEv = (effectiveLumi*crossSection*branchingRatio);
		long double obtainedEv = (long double)numEvInDS*(numEvAnalyzed/(double)numEvInNtuple);

		if(obtainedEv	==0){ cout << "ERROR: obtainedEv is set to zero. Normalization will be infinite!" << endl; exit(1); }
		normalization = (expectedEv/obtainedEv);
	}else{
		normalization = 1;
	}

	return normalization;
}

string HistoWrapper::GetOutputFilename(){
	return outputFilename;
}

void HistoWrapper::SetOutputFilename(string iOutputFilename){
	outputFilename = iOutputFilename;
}

void HistoWrapper::SetRangeUser(float ixMinVis, float ixMaxVis){
	xMinVis	= ixMinVis;	
	xMaxVis	= ixMaxVis;	
}

void HistoWrapper::SetRangeUser(float ixMinVis, float ixMaxVis, float iyMinVis, float iyMaxVis){
	xMinVis	= ixMinVis;	
	xMaxVis	= ixMaxVis;	
	yMinVis	= iyMinVis;	
	yMaxVis	= iyMaxVis;	
}

void HistoWrapper::SetRangeUser(float ixMinVis, float ixMaxVis, float iyMinVis, float iyMaxVis, float izMinVis, float izMaxVis){
	xMinVis	= ixMinVis;	
	xMaxVis	= ixMaxVis;	
	yMinVis	= iyMinVis;	
	yMaxVis	= iyMaxVis;	
	zMinVis	= izMinVis;	
	zMaxVis	= izMaxVis;	
}

void HistoWrapper::SetXlabel(string ixLabel){
	histo->GetXaxis()->SetTitle(ixLabel.c_str());
	xLabel = ixLabel;
}
void HistoWrapper::SetYlabel(string iyLabel){
	histo->GetYaxis()->SetTitle(iyLabel.c_str());
	yLabel = iyLabel;
}
void HistoWrapper::SetZlabel(string izLabel){
	zLabel = izLabel;
}

void HistoWrapper::SetLog(bool iLogx, bool iLogy, bool iLogz){
	logx = iLogx;
	logy = iLogy;
	logz = iLogz;
}

void HistoWrapper::SetShowOF(bool iSwitch){
	showOF = iSwitch;
}

void HistoWrapper::SetShowText(bool iSwitch){
	showText = iSwitch;
}

bool HistoWrapper::ShowOF(){
	return showOF;
}

bool HistoWrapper::ShowText(){
	return showText;
}

void HistoWrapper::SetOF(){
	int ofBin = GetOFbin(xMaxVis);
	pair<double,double> OFandError = GetOF(ofBin);

	histo->SetBinContent(ofBin,OFandError.first);
	histo->SetBinError(ofBin,OFandError.second);

}

int HistoWrapper::GetOFbin(float iLimit){
	int lowbin	= 0;

	if(histo == NULL){
		cout << "ERROR: trying to obtain OF from null histo." << endl; exit(1);
	}else{
		lowbin	= 0;
		for( unsigned int b = 1; b <= histo->GetNbinsX(); b++){
			if(histo->GetBinLowEdge(b) > iLimit){ lowbin = b-1; break; }
		}
	}

	return lowbin;
}

pair<double,double> HistoWrapper::GetOF(int iLowBin){
	pair<double, int> result;
	double of = 0;
	double ofError2 = 0;

	if(histo == NULL){
		cout << "ERROR: trying to obtain OF from null histo." << endl; exit(1);
	}else{
		for( unsigned int b = iLowBin; b <= histo->GetNbinsX(); b++){
			of += histo->GetBinContent(b);	
			ofError2 += pow(histo->GetBinError(b),2);	
		}
	}

	result.first = of;
	result.second = sqrt(ofError2);

	return result;
}

void HistoWrapper::SetCenterLabels(bool iCenterLabels){
	centerLabels = iCenterLabels;
}

bool HistoWrapper::DoLogx(){
	return logx;
}

bool HistoWrapper::DoLogy(){
	return logy;
}

bool HistoWrapper::DoLogz(){
	return logz;
}

bool HistoWrapper::CenterLabels(){
	return centerLabels;
}

pair<float,float> HistoWrapper::GetRangeUser(){
	return pair<float, float>(xMinVis,xMaxVis);
}
float HistoWrapper::GetXminVis(){
	return xMinVis;
}
float HistoWrapper::GetXmaxVis(){
	return xMaxVis;
}

float HistoWrapper::GetYminVis(){
	return yMinVis;
}
float HistoWrapper::GetYmaxVis(){
	return yMaxVis;
}

float HistoWrapper::GetZminVis(){
	return zMinVis;
}
float HistoWrapper::GetZmaxVis(){
	return zMaxVis;
}

string HistoWrapper::GetXlabel(){
	return xLabel;
}

string HistoWrapper::GetYlabel(){
	return yLabel;
}

string HistoWrapper::GetZlabel(){
	return zLabel;
}

double HistoWrapper::Integral(float iMin, float iMax){
	return (histo->Integral(iMin, iMax)); 
}

// Default destructor
HistoWrapper::~HistoWrapper(){
}

void HistoWrapper::SetHisto(TH1* iHisto){
	histo = (TH1*)iHisto->Clone();
	histo->Sumw2();
}

void HistoWrapper::SetName(string iName){
	histo->SetName(iName.c_str());
}

void HistoWrapper::SetTitle(string iTitle){
	histo->SetTitle(iTitle.c_str());
}

void HistoWrapper::SetNumEvAnalyzed(int iNumEvAnalyzed){
	if(normalized){ cout << "ERROR: trying to setting numEvAnalyzed but histo is already normalized!" << endl; exit(1); }
	numEvAnalyzed = iNumEvAnalyzed;
}

string HistoWrapper::GetName(){
	return string(histo->GetName());
}

string HistoWrapper::GetTitle(){
	return string(histo->GetTitle());
}

TH1* HistoWrapper::GetHisto(){
	return histo;
}

bool HistoWrapper::IsTH1F(){
	string className = string(histo->ClassName());
	return (className.compare("TH1F")==0);
}

bool HistoWrapper::IsTH2F(){
	string className = string(histo->ClassName());
	return (className.compare("TH2F")==0);
}

void HistoWrapper::Add(const HistoWrapper* iHW, Double_t iFactor){
	HistoWrapper* temp = new HistoWrapper(*iHW);
	histo->Add(temp->GetHisto(), iFactor);
	
	for(unsigned int i = 1; i <= histo->GetNbinsX(); i++){
		float da = histo->GetBinError(i);
		float db = temp->GetHisto()->GetBinError(i);
		float error = sqrt(pow(da,2) + pow(db,2));
		histo->SetBinError(i, error);

	}

}

void HistoWrapper::Multiply(const HistoWrapper* iHW){
	HistoWrapper* temp = new HistoWrapper(*iHW);
	histo->Multiply(temp->GetHisto());
}

void HistoWrapper::Divide(const HistoWrapper* iHW){
	HistoWrapper* temp = new HistoWrapper(*iHW);
	TH1* ori = (TH1*)histo->Clone();
	histo->Divide(temp->GetHisto());

	for(unsigned int i = 1; i <= histo->GetNbinsX(); i++){
		float a		= ori->GetBinContent(i);
		float da	= ori->GetBinError(i);
		float b		= temp->GetHisto()->GetBinContent(i);
		float db	= temp->GetHisto()->GetBinError(i);
		float error = (a/b)*sqrt(pow((da/a),2) + pow((db/b),2));
		histo->SetBinError(i, error);
	}


}

void HistoWrapper::Scale(Double_t iFactor){
	histo->Scale(iFactor);
}

HistoWrapper* HistoWrapper::Clone(){
	
	HistoWrapper* result = new HistoWrapper(*this);	
	result->histo = (TH2F*)histo->Clone();
	return result;
}


bool HistoWrapper::Normalized(){
	return normalized;
}

void HistoWrapper::Normalize(){
	if(!normalized){
		if(showOF){ SetOF(); }

		double puAndTriggerFactor = histo->Integral()/numEvPassing;
		histo->Scale(numEvPassing/histo->Integral());
		histo->Sumw2();
		histo->Scale(puAndTriggerFactor*GetNormalization());
		normalized = true;
	}	

	if(HasVariableWidthBins()){ 
		SmoothenVariableWidthBins(); }



}

void HistoWrapper::SetHasVariableWidthBins(bool iSwitch){
	hasVariableWidthBins = true;
}

bool HistoWrapper::HasVariableWidthBins(){
	return hasVariableWidthBins;
}

void HistoWrapper::SmoothenVariableWidthBins(){
	float firstBinWidth = histo->GetBinWidth(1);

	for(int b = 1; b <= histo->GetNbinsX(); b++){
		float currentBinWidth = histo->GetBinWidth(b);	

		if((currentBinWidth > 0) && (currentBinWidth != firstBinWidth)){
			float widthRatio = firstBinWidth/currentBinWidth;	
			histo->SetBinContent(b, widthRatio*(histo->GetBinContent(b)));
			histo->SetBinError(b, widthRatio*(histo->GetBinError(b)));
		}
	}

	string unit;
	size_t left = xLabel.find("GeV");
	if(0<= left && left < xLabel.length()){
		unit = xLabel.substr(left);
		size_t right = unit.find(")");
		if(0<= right && right < unit.length()){
			unit = unit.substr(0,right);
		}
	}

	stringstream labelSS; labelSS.str("");
	labelSS << yLabel << " /" << firstBinWidth << unit;
	yLabel = labelSS.str();

}
