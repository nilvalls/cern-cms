
#ifndef HistoWrapper_h
#define HistoWrapper_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TGraphAsymmErrors.h>
#include <math.h>
#include <map>
#include <vector>
#include <sstream>
#include <iostream>
#include <utility>


using namespace std;




class HistoWrapper {
	public :
		TH1* histo;
		string cutsApplied;
		bool isBackground;
		bool isMC;
		bool isSignal;

		int numEvPassing;
		int numEvAnalyzed;
		int numEvInNtuple;
		int numEvInDS;
		float crossSection;
		float branchingRatio;
		float effectiveLumi;
		float normalization;

		float xbins[100000];
		float xMinVis;
		float xMaxVis;
		float yMinVis;
		float yMaxVis;
		float zMinVis;
		float zMaxVis;
		string xLabel;
		string yLabel;
		string zLabel;
		string outputFilename;
		bool logx;
		bool logy;
		bool logz;
		bool showOF;
		bool showText;
		bool centerLabels;
		string th1Type;
		bool hasVariableWidthBins;
		bool normalized;



		// Default constructor
		HistoWrapper();
		HistoWrapper(TH1*);
		virtual ~HistoWrapper();
		virtual void Fill(double);
		virtual void Fill(double, double);
		virtual void Fill(double, double, double);
		virtual void SetHisto(TH1* iHisto);
		virtual void SetName(string iName);
		virtual void SetMC(bool iSwitch=true);
		virtual void SetBackground(bool iSwitch=true);
		virtual void SetSignal(bool iSwitch=true);
		virtual bool IsCollisions();
		virtual bool IsMC();
		virtual bool IsBackground();
		virtual bool IsSignal();
		virtual void SetCutsApplied(string);
		virtual string GetCutsApplied();
		virtual bool ChargeProductApplied();
		virtual void SetTitle(string iTitle);
		virtual void SetRangeUser(float,float);
		virtual void SetRangeUser(float,float,float,float);
		virtual void SetRangeUser(float,float,float,float,float,float);
		virtual void SetXlabel(string);
		virtual void SetYlabel(string);
		virtual void SetZlabel(string);
		virtual void SetLog(bool, bool, bool iLogz=false);
		virtual void SetShowOF(bool iSwitch=true);
		virtual void SetShowText(bool iSwitch=true);
		virtual void SetOF();
		virtual int GetOFbin(float);
		virtual pair<double,double> GetOF(int);
		virtual bool ShowOF();
		virtual bool ShowText();
		virtual void SetHasVariableWidthBins(bool iSwitch=true);
		virtual bool HasVariableWidthBins();
		virtual void SetCenterLabels(bool);
		virtual bool DoLogx();
		virtual bool DoLogy();
		virtual bool DoLogz();
		virtual bool CenterLabels();
		virtual void SetOutputFilename(string);
		virtual int GetNumEvPassing();
		virtual int GetNumEvInNtuple();
		virtual int GetNumEvInDS();
		virtual void SetNumEvPassing(int);
		virtual void SetNumEvInNtuple(int);
		virtual void SetNumEvInDS(int);
		virtual float GetCrossSection();
		virtual float GetBranchingRatio();
		virtual float GetEffectiveLumi();
		virtual float GetNormalization();
		virtual void SetCrossSection(float iCrossSection);
		virtual void SetBranchingRatio(float iBranchingRatio);
		virtual void SetEffectiveLumi(float iEffectiveLumi);
		virtual string GetOutputFilename();
		virtual pair<float,float> GetRangeUser();
		virtual float GetXminVis();
		virtual float GetXmaxVis();
		virtual float GetYminVis();
		virtual float GetYmaxVis();
		virtual float GetZminVis();
		virtual float GetZmaxVis();
		virtual string GetXlabel();
		virtual string GetYlabel();
		virtual string GetZlabel();
		virtual void SetNumEvAnalyzed(int);
		virtual int GetNumEvAnalyzed();
		virtual string GetName();
		virtual string GetTitle();
		virtual TH1* GetHisto();
		virtual bool IsTH1F();
		virtual bool IsTH2F();
		virtual double Integral(float iMin=-FLT_MAX, float iMax=FLT_MAX);
		virtual void Add(const HistoWrapper*, Double_t iFactor=1.0);
		virtual void Multiply(const HistoWrapper*);
		virtual void Divide(const HistoWrapper*);
		virtual void Scale(Double_t iFactor);
		virtual HistoWrapper* Clone();
		virtual bool Normalized();
		virtual void Normalize();

	private:
		void SmoothenVariableWidthBins();



};

#endif

#ifdef HistoWrapper_cxx



#endif
