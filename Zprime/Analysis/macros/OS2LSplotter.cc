/*
    Author:			Nil Valls <nil.valls@cern.ch>
    Date:			20 Jun 2011
    Description:	OS to LS calculator class
    Notes:

*/

#include "OS2LSplotter.h"

#define OS2LSplotter_cxx
using namespace std;

// Default constructor
OS2LSplotter::OS2LSplotter(string iOutputRoot, string iOutputDir){

	gStyle->SetErrorX(0.5);

	outputDir = iOutputDir;
	outputFile = iOutputRoot;
	lumi = 0;
	units = "";

	numberOfPlots	= 0;

	OSHistos = new vector<HistoWrapper*>();
	LSHistos = new vector<HistoWrapper*>();
	OSHistos -> clear();
	LSHistos -> clear();

}

// Default destructor
OS2LSplotter::~OS2LSplotter(){
}

void OS2LSplotter::SetLumi(float iLumi, string iUnits){
	lumi = iLumi;
	units = iUnits;
}

void OS2LSplotter::SetEffectiveLumi(float iLumi){
	effectiveLumi = iLumi;
}


void OS2LSplotter::AddHistos(vector<HistoWrapper*>* iBasePlots, vector<HistoWrapper*>* iNewPlots, float iScaleFactor){

	// Set of check consistent number of plots
	if (numberOfPlots == 0){
		numberOfPlots	= iNewPlots->size();
		for(unsigned int p=0; p < iNewPlots->size(); p++){
			HistoWrapper* toInsert = iNewPlots->at(p)->Clone();
			toInsert->Scale(iScaleFactor);
			iBasePlots->push_back(toInsert);
		}	
	}else{
		if ( iNewPlots->size() != numberOfPlots ){
			cout << "ERROR: imported vector of collision plots does not have the same amount of plots as the other topologies" << endl;
			exit(1);
		}else{
			if(iBasePlots->size() == 0){
				for(unsigned int p=0; p < iNewPlots->size(); p++){
					HistoWrapper* toInsert = iNewPlots->at(p)->Clone();
					iBasePlots->push_back(toInsert);
				}	
			}else{
				for(unsigned int p=0; p < iBasePlots->size(); p++){
					HistoWrapper* toAdd = iNewPlots->at(p)->Clone();
					iBasePlots->at(p)->Add(toAdd);
				}	
			}
		}
	}
	
}

void OS2LSplotter::AddOSCollisions(vector<HistoWrapper*>* iPlots){
	AddHistos(OSHistos, iPlots, 1);	
}
void OS2LSplotter::AddLSCollisions(vector<HistoWrapper*>* iPlots){
	AddHistos(LSHistos, iPlots, 1);	
}

void OS2LSplotter::AddOSBackground(vector<HistoWrapper*>* iPlots){
	AddHistos(OSHistos, iPlots, -1);	
}

void OS2LSplotter::AddLSBackground(vector<HistoWrapper*>* iPlots){
	AddHistos(LSHistos, iPlots, -1);	
}




void OS2LSplotter::DrawPlots(){

	HistoWrapper* toPlot;
	HistoWrapper* num;
	HistoWrapper* den;
	TCanvas* canvas;

	if((OSHistos != NULL) && (LSHistos != NULL)){
		if((OSHistos->size() != 0) && (LSHistos->size() != 0)){

			for(unsigned int h=0; h < OSHistos->size(); h++){
				string cutsApplied	= OSHistos->at(h)->GetCutsApplied();
				size_t found = cutsApplied.find("ChargeProduct");
				if(!((0 <= found) && (found < cutsApplied.length()))){
					cout << "\nERROR: trying to make OS2LS plots but \"ChargeProduct\" requirement was not requested!\n" << endl;
					exit(1);
				}

				if(OSHistos->at(h)->IsTH2F()){ continue; }
				toPlot	= OSHistos->at(h)->Clone();	
				den		= LSHistos->at(h)->Clone();	

				objectSaver.SaveToFile(toPlot->GetHisto(), "TH1F", outputFile,"os2ls/OS");
				objectSaver.SaveToFile(den->GetHisto(), "TH1F", outputFile,"os2ls/LS");

				toPlot->Divide(den);
				toPlot->GetHisto()->GetXaxis()->SetRangeUser(toPlot->GetXminVis(),toPlot->GetXmaxVis());
				toPlot->GetHisto()->GetXaxis()->SetTitle(toPlot->GetXlabel().c_str());
				toPlot->GetHisto()->GetYaxis()->SetTitle("OS/LS");

				objectSaver.SaveToFile(toPlot->GetHisto(), "TH1F", outputFile,"os2ls/ratios");


				string plotname = string(toPlot->GetHisto()->GetName());
				TFile* file;
				//OpenFile(file, outputFile, "os2ls/canvases");
				canvas = new TCanvas(("os2ls_"+plotname).c_str(), ("os2ls_"+plotname).c_str(), 800, 800);
				canvas->Clear();
				canvas->cd();
				canvas->SetName(toPlot->GetHisto()->GetName());
				toPlot->GetHisto()->Draw("E1");
				string outputFilename = toPlot->GetOutputFilename(); 
				SaveCanvas(canvas, outputDir, outputFilename);
				//CloseFile(file);

			}	
		}else{
			cout << "OS2LS has nothing to plot!" << endl;
		}
	}else{
		cout << "OS2LS has nothing to plot!" << endl;
	}
	//*/

}

// Save canvas
void OS2LSplotter::SaveCanvas(TCanvas* canvas, string dir, string filename){

	// Create output dir if it doesn't exists already
	TString sysCommand = "if [ ! -d " + dir + " ]; then mkdir -p " + dir + "; fi";
	if(gSystem->Exec(sysCommand) > 0){ cout << ">>> ERROR: problem creating dir for plots " << dir << endl; exit(1); }// exit(0);

	// Loop over all file format extensions choosen and save canvas
	vector<string> extension; extension.push_back(".png");
	for( unsigned int ext = 0; ext < extension.size(); ext++){
		canvas->SaveAs( (dir + "os2ls_" + filename + extension.at(ext)).c_str() );
	}
}

string OS2LSplotter::numberPMerror(float number, float error){
	stringstream output;	
	stringstream errorSS;
	string ferror;
	stringstream numberSS;
	string fnumber;

	errorSS << setprecision(1) << error;
	ferror = errorSS.str();
	errorSS.str("");

	numberSS << number;
	fnumber = numberSS.str();
	numberSS.str("");

	int integersInError = 0;	

	if( error < 1){
		int decimalsInError;
		decimalsInError = (ferror.substr(ferror.find('.') + 1)).length();		

		int decimalsInNumber = 0;
		int integersInNumber = 0;

		if ( ((int)number) > 0 ){
			integersInNumber  = (fnumber.substr(0,fnumber.find('.'))).length();
		}

		integersInError = 1;

		numberSS << setprecision( integersInNumber + decimalsInError ) << number;

	}else{

		int integersInNumber = 0;

		numberSS << ((int)(number+0.5));
		integersInNumber = (numberSS.str()).length();
		numberSS.str("");

		integersInError = ferror.length();

		numberSS << setprecision( integersInNumber - integersInError + 1 ) << number;

	}   

	string errorPadding="";
	for (int i=1; i <=(5-integersInError); i++){
		errorPadding = errorPadding + string(" "); 
	}   

	output << numberSS.str() << " +/- " << ferror;

	return output.str();

}

bool OS2LSplotter::isIndeterminate(const double x){ return (x != x); } 
bool OS2LSplotter::isInfinite(const double x){ return (x < -DBL_MAX || x > DBL_MAX); }
bool OS2LSplotter::isGoodNum(const double x){ return ((!isIndeterminate(x)) && (!isInfinite(x))); }





































































