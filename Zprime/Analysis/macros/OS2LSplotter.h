//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Jun  7 03:28:02 2011 by ROOT version 5.27/06b
// from TTree HMTTree/TauTauTree
// found on file: ditau_nTuple.root
//////////////////////////////////////////////////////////

#ifndef OS2LSplotter_h
#define OS2LSplotter_h

#include <TROOT.h>
#include <TSystem.h>
#include <TChain.h>
#include <TStyle.h>
#include <TFile.h>
#include <TF1.h>
#include <TH1F.h>
#include <TH2F.h>
#include <THStack.h>
#include <TCanvas.h>
#include "TVectorD.h"
#include <TLegend.h>
#include <TPaveText.h>
#include <TGraphAsymmErrors.h>
#include <math.h>
#include "limits.h"
#include "math.h"
#include <map>
#include <vector>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <utility>
#include <stdlib.h>
#include "HistoWrapper.h"
#include "TObjectSaver.h"



using namespace std;

class OS2LSplotter {
	public :

		vector<HistoWrapper*>* 		OSHistos;
		vector<HistoWrapper*>*		LSHistos;

		string outputDir;
		string outputFile;
		unsigned int numberOfPlots;
		float lumi;
		float effectiveLumi;
		string units;
		float OSSF;
		float LSSF;

		bool doOS;
		bool doLS;

		TObjectSaver objectSaver;

		TDirectory* os2lsCanvasesDir;
		TDirectory* os2lsHistosDir;
		TDirectory* osHistoDir;
		TDirectory* lsHistoDir;
		OS2LSplotter(string, string);
		virtual ~OS2LSplotter();
		
		virtual void SetLumi(float,string);
		virtual void SetEffectiveLumi(float);
		virtual string numberPMerror(float,float);

		virtual void AddHistos(vector<HistoWrapper*>*, vector<HistoWrapper*>*, float iScaleFactor=1.0);
		virtual void AddOSCollisions(vector<HistoWrapper*>*);
		virtual void AddLSCollisions(vector<HistoWrapper*>*);
		virtual void AddOSBackground(vector<HistoWrapper*>*);
		virtual void AddLSBackground(vector<HistoWrapper*>*);

		virtual void DrawPlots();
		virtual void SaveCanvas(TCanvas*, string, string);

		bool isIndeterminate(const double);
		bool isInfinite(const double);
		bool isGoodNum(const double);



};
#endif










