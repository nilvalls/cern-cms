/*
Author:			Nil Valls <nil.valls@cern.ch>
Date:			11 Jun 2011
Description:	Plot stacking class.
Notes:

 */

#include "PlotStacker.h"

#define PlotStacker_cxx
using namespace std;

#define AT __LINE__

// Default constructor
PlotStacker::PlotStacker(string iOutputRoot, string iOutputDir){

	// Draw horizontal error bars
	gStyle->SetErrorX(0.5);

	outputDir = iOutputDir;
	outputRoot = iOutputRoot;

	lumi	= 0;
	units	= "";

	doBackgrounds	= false;
	doSignals		= false;
	doQCD			= false;
	doCollisions	= false;

	topologies.clear();

	backgrounds.clear();
	backgroundColor.clear();
	backgroundLabels.clear();
	backgroundOtherSF.clear();
	backgroundCrossSection.clear();
	backgroundNumEvInDS.clear();

	signals.clear();
	signalColor.clear();
	signalLabels.clear();
	signalOtherSF.clear();
	signalCrossSection.clear();
	signalNumEvInDS.clear();

	LSbackgrounds.clear();
	LSbackgroundOtherSF.clear();
	LSbackgroundCrossSection.clear();
	LSbackgroundNumEvInDS.clear();

	allMCscaleFactor	= 1.0;
	collisionsSF		= 1.0;
	QCDSF				= 1.0;

	plotNameToCount = "InvariantMass_LSM_RightIntegrated";
	numberOfPlots	= 0;

	collisionHistos = NULL;
	QCDHistos		= NULL;

	bool chargeProductApplied = false;
	lastSavedPlotName="";

}

// Default destructor
PlotStacker::~PlotStacker(){
}

void PlotStacker::SetAllMCscaleFactor(float iFactor){
	allMCscaleFactor	= iFactor;
}

void PlotStacker::SetLumi(float iLumi, string iUnits){
	lumi	= iLumi;
	units	= iUnits;
}

// Set up analysis flags
void PlotStacker::SetFlags(string iFlags){
	flags = iFlags;
}

bool PlotStacker::IsFlagThere(string iFlag){
	size_t found = flags.find(iFlag);
	return ((0 <= found) && (found < flags.length()));
}

void PlotStacker::SetEffectiveLumi(float iLumi){
	effectiveLumi = iLumi;
}

// Add collision data plots
void PlotStacker::AddCollisions(vector<HistoWrapper*>* plots, string iLabel, float iScaleFactor){

	// Set of check consistent number of plots
	if (numberOfPlots == 0){
		numberOfPlots	= plots->size();
	}else{
		if ( plots->size() != numberOfPlots ){
			cout << "ERROR: imported vector of collision plots does not have the same amount of plots as the other topologies" << endl;
			exit(1);
		}
	}

	chargeProductApplied = plots->at(0)->ChargeProductApplied();
	collisionHistos		= plots;
	doCollisions		= true;
	collisionsSF		= iScaleFactor;
	collisionsLabel		= iLabel;
	topologies.push_back(pair<string,string>(iLabel,iLabel));
}

// Add signal plots
void PlotStacker::AddSignal(vector<HistoWrapper*>* plots, string iLabel, string iLabelForLegend, int iLineColor, float iCrossSection, float iOtherSF, int iNumEvInDS){

	// Set of check consistent number of plots
	if (numberOfPlots == 0){
		numberOfPlots	= plots->size();
	}else{
		if ( plots->size() != numberOfPlots ){
			cout << "ERROR: imported vector of signal plots does not have the same amount of plots as the other topologies" << endl;
			exit(1);
		}
	}


	chargeProductApplied = plots->at(0)->ChargeProductApplied();
	signals.push_back( pair< string, vector<HistoWrapper*>* >(iLabelForLegend, plots) );
	signalColor.push_back(iLineColor);
	signalOtherSF.push_back(iOtherSF);
	signalCrossSection.push_back(iCrossSection);
	signalNumEvInDS.push_back(iNumEvInDS);
	doSignals	= true;
	signalLabels.push_back(iLabel);
	topologies.push_back(pair<string,string>(iLabel,iLabelForLegend));

}

void PlotStacker::AddQCD( vector<HistoWrapper*>* plots, string iQCDLabel, float iScaleFactor){

	// Set of check consistent number of plots
	if (numberOfPlots == 0){
		numberOfPlots	= plots->size();
	}else{
		if ( plots->size() != numberOfPlots ){
			cout << "ERROR: imported vector of QCD plots does not have the same amount of plots as the other topologies" << endl;
			exit(1);
		}
	}

	QCDHistos		= plots;
	QCDLabel		= iQCDLabel;
	doQCD			= true;
	QCDSF			= iScaleFactor;
	topologies.push_back(pair<string,string>("QCD",iQCDLabel));
}

// Subtract LS backgrounds from LS collisions to obtain dd-QCD distros
TH1* PlotStacker::BuildQCD(int plotNum){

	TH1* result = NULL;
	int p = plotNum;

	if(!doQCD){ return NULL; }

	// Get LS collisions histo
	result = (TH1*)(QCDHistos->at(p)->GetHisto()->Clone());

	// Loop over all backgrounds
	for ( unsigned int bkg = 0; bkg < LSbackgrounds.size(); bkg++){
		HistoWrapper* backgroundWrapper = ((LSbackgrounds.at(bkg)).second)->at(p);
		TH1* backgroundPlot = (TH1*)(((LSbackgrounds.at(bkg)).second)->at(p)->GetHisto()->Clone()); 

		// Subtract this background from LS collisions
		result->Add(backgroundPlot,-1);
	}

	// Make sure no bins acquire negative values
	for(int bin=1; bin<result->GetNbinsX(); bin++){
		if(result->GetBinContent(bin) < 0){
			result->SetBinContent(bin, 0);
			result->SetBinError(bin, 0);
		}
	}//*/


	if( chargeProductApplied ){
		// Fix charge product plot
		if((string(result->GetName())).compare("ChargeProduct")==0){
			int osBin = -1;
			int lsBin = -1;
			for(int bin=1; bin <= result->GetNbinsX(); bin++){
				if(result->GetBinLowEdge(bin) == -1){ osBin = bin; }	
				if(result->GetBinLowEdge(bin) ==  1){ lsBin = bin; }	
			}
			result->SetBinContent(osBin, result->GetBinContent(lsBin)*QCDSF);
			result->SetBinContent(lsBin, 0);
		}else{
			result->Scale(QCDSF);
		}
	}else{
		// Fix charge product plot
		if((string(result->GetName())).compare("ChargeProduct")==0){
			int osBin = -1;
			int lsBin = -1;
			for(int bin=1; bin <= result->GetNbinsX(); bin++){
				if(result->GetBinLowEdge(bin) == -1){ osBin = bin; }	
				if(result->GetBinLowEdge(bin) ==  1){ lsBin = bin; }	
			}
			result->SetBinContent(osBin, result->GetBinContent(lsBin)*QCDSF);
		}else{
			result->Scale(1+QCDSF);
		}
	}


	return result;

}


// Add plots to subtract from QCD
void PlotStacker::AddLSBackground(vector<HistoWrapper*>* plots, string labelForLegend, float iCrossSection, float iOtherSF, int iNumEvInDS){


	// Set of check consistent number of plots
	if (numberOfPlots == 0){
		numberOfPlots	= plots->size();
	}else{
		if ( plots->size() != numberOfPlots ){
			cout << "ERROR: imported vector of background plots does not have the same amount of plots as the other topologies" << endl;
			exit(1);
		}
	}

	LSbackgrounds.push_back( pair< string, vector<HistoWrapper*>* >(labelForLegend, plots) );
	LSbackgroundOtherSF.push_back(iOtherSF);
	LSbackgroundCrossSection.push_back(iCrossSection);
	LSbackgroundNumEvInDS.push_back(iNumEvInDS);
}

// Add background plots
void PlotStacker::AddBackground(vector<HistoWrapper*>* plots, string iLabel, string iLabelForLegend, int iFillColor, float iCrossSection, float iOtherSF, int iNumEvInDS){

	// Set of check consistent number of plots
	if (numberOfPlots == 0){
		numberOfPlots	= plots->size();
	}else{
		if ( plots->size() != numberOfPlots ){
			cout << "ERROR: imported vector of background plots does not have the same amount of plots as the other topologies" << endl;
			exit(1);
		}
	}

	chargeProductApplied = plots->at(0)->ChargeProductApplied();
	backgrounds.push_back( pair< string, vector<HistoWrapper*>* >(iLabelForLegend, plots) );
	backgroundColor.push_back(iFillColor);
	backgroundOtherSF.push_back(iOtherSF);
	backgroundCrossSection.push_back(iCrossSection);
	backgroundNumEvInDS.push_back(iNumEvInDS);
	doBackgrounds	= true;
	backgroundLabels.push_back(iLabel);
	topologies.push_back(pair<string,string>(iLabel,iLabelForLegend));
}

// Perform the stacking and plotting here
EventCounter* PlotStacker::DrawPlots(){
	EventCounter* eventCounter = new EventCounter();



	// Check that there is at least something to plot
	if ( !(doCollisions || doSignals || doBackgrounds) ){
		cout << "ERROR: nothing to plot." << endl;
		return NULL;
	}

	// Declare canvas and legend
	TCanvas* canvas;
	TLegend *legend;

	float newMaxY = 0;

	// Loop over all plots
	for (unsigned int p = 0; p < numberOfPlots; p++){
		newMaxY = 0;
		TH1* basePlot = NULL;

		bool canvasEmpty = true;
		bool isTH2F	= false;
		bool doLogx = false;
		bool doLogy = false;
		bool doLogz = false;
		bool centerLabels = false;
		string outputFilename = "";
		vector<TH1*> signalHistos; signalHistos.clear();
		bool showText = false;

		float xmin, xmax, ymin, ymax;
		string xlabel, ylabel, zlabel;

		// Set up canvas and legend
		float xLegend	= 0.92;
		float yLegend	= 0.88;

		float dxLegend	= 0.20; float dyLegend	= 0.30; 
		legend = new TLegend(xLegend-dxLegend, yLegend-dyLegend, xLegend, yLegend, NULL, "brNDC");
		legend->SetBorderSize(1);
		legend->SetFillColor(kWhite);
		legend->SetFillStyle(1001);
		string plotname = "test";
		canvas = new TCanvas(plotname.c_str(), plotname.c_str(), 800, 800);
		canvas->cd();


		TH1* collisions = NULL;
		if (doCollisions){
			if(outputFilename.length() < 1){ outputFilename = collisionHistos->at(p)->GetOutputFilename(); }
			collisions	= (TH1*)collisionHistos->at(p)->GetHisto()->Clone();
			showText = collisionHistos->at(p)->ShowText();

			// Add collisions to event count
			string histoName = string(collisions->GetName());
			if((histoName.compare(plotNameToCount)==0) && (histoName.length() == plotNameToCount.length())){
				TH1* toAdd = (TH1*)(collisions->Clone());
				eventCounter->AddCollisions(collisionsLabel,(TH1F*)toAdd);	
			}

			doLogx = collisionHistos->at(p)->DoLogx();
			doLogy = collisionHistos->at(p)->DoLogy();
			centerLabels = collisionHistos->at(p)->CenterLabels();

			if(basePlot==NULL){ basePlot = (TH1*)(collisions->Clone()); }


			canvas->SetName(collisions->GetName());
			xmin = collisionHistos->at(p)->GetXminVis();
			xmax = collisionHistos->at(p)->GetXmaxVis();
			xlabel = collisionHistos->at(p)->GetXlabel().c_str();
			ylabel = collisionHistos->at(p)->GetYlabel().c_str();
			if(collisionHistos->at(p)->IsTH2F()){ zlabel = collisionHistos->at(p)->GetZlabel().c_str(); }
			collisions->GetXaxis()->SetRangeUser(xmin,xmax);
			collisions->GetXaxis()->CenterLabels(centerLabels);
			collisions->GetXaxis()->SetTitle(xlabel.c_str());
			collisions->GetYaxis()->SetTitle(ylabel.c_str());
			collisions->SetMarkerStyle(20);
			ymin = collisionHistos->at(p)->GetYminVis();
			ymax = collisionHistos->at(p)->GetYmaxVis();
			if((newMaxY < collisions->GetMaximum()) && (ymin==0) && (ymax==0)){	newMaxY = collisions->GetMaximum(); }
			if(collisionHistos->at(p)->IsTH2F()){ 
				isTH2F = true;
				doLogz = collisionHistos->at(p)->DoLogz();
				collisions->GetXaxis()->SetRangeUser(xmin,xmax);
				collisions->GetYaxis()->SetRangeUser(ymin,ymax);
				collisions->GetZaxis()->SetTitle(zlabel.c_str());
				objectSaver.SaveToFile(collisions, "TH2F", outputRoot, "stacker/histos/collisions");
			}else{
				objectSaver.SaveToFile(collisions, "TH1F", outputRoot, "stacker/histos/collisions");
			}
			legend->AddEntry(collisions, "Run2011AB", "lep");
			if(collisions->Integral()!=0){ canvasEmpty = false; }


		}

		// Setup background
		THStack* stack = NULL;
		TH1* errorh = NULL;
		if (doBackgrounds){

			stack = new THStack( plotname.c_str(), plotname.c_str() );

			// Loop over all backgrounds
			for ( unsigned int bkg = 0; bkg < backgrounds.size(); bkg++){
				if(outputFilename.length() < 1){ outputFilename = ((backgrounds.at(bkg)).second)->at(p)->GetOutputFilename(); }
				TH1* backgroundPlot = (TH1*)(((backgrounds.at(bkg)).second)->at(p)->GetHisto()->Clone()); 
				showText = ((backgrounds.at(bkg)).second)->at(p)->ShowText();
				if(basePlot==NULL){ basePlot = (TH1*)(((backgrounds.at(bkg)).second)->at(p)->GetHisto()->Clone()); }
				HistoWrapper* backgroundWrapper = ((backgrounds.at(bkg)).second)->at(p);


				doLogx = ((backgrounds.at(bkg)).second)->at(p)->DoLogx();
				doLogy = ((backgrounds.at(bkg)).second)->at(p)->DoLogy();
				centerLabels = ((backgrounds.at(bkg)).second)->at(p)->CenterLabels();
				backgroundPlot->GetXaxis()->SetRangeUser(((backgrounds.at(bkg)).second)->at(p)->GetXminVis(),((backgrounds.at(bkg)).second)->at(p)->GetXmaxVis());
				backgroundPlot->GetXaxis()->SetTitle(((backgrounds.at(bkg)).second)->at(p)->GetXlabel().c_str());
				backgroundPlot->GetXaxis()->CenterLabels(centerLabels);
				backgroundPlot->GetYaxis()->SetTitle(((backgrounds.at(bkg)).second)->at(p)->GetYlabel().c_str());
				ymin = ((backgrounds.at(bkg)).second)->at(p)->GetYminVis();
				ymax = ((backgrounds.at(bkg)).second)->at(p)->GetYmaxVis();
				if(((backgrounds.at(bkg)).second)->at(p)->IsTH2F()){
					isTH2F = true;
					doLogz = ((backgrounds.at(bkg)).second)->at(p)->DoLogz();
					backgroundPlot->GetZaxis()->SetTitle(((backgrounds.at(bkg)).second)->at(p)->GetZlabel().c_str());
					xmin = ((backgrounds.at(bkg)).second)->at(p)->GetXminVis();
					xmax = ((backgrounds.at(bkg)).second)->at(p)->GetXmaxVis();
					xlabel = ((backgrounds.at(bkg)).second)->at(p)->GetXlabel().c_str();
					ylabel = ((backgrounds.at(bkg)).second)->at(p)->GetYlabel().c_str();
					zlabel = ((backgrounds.at(bkg)).second)->at(p)->GetZlabel().c_str();
					//					backgroundPlot->GetYaxis()->SetRangeUser(ymin,ymax);
				}
				if(!doCollisions){
					xmin = ((backgrounds.at(bkg)).second)->at(p)->GetXminVis();
					xmax = ((backgrounds.at(bkg)).second)->at(p)->GetXmaxVis();
					xlabel = ((backgrounds.at(bkg)).second)->at(p)->GetXlabel().c_str();
					ylabel = ((backgrounds.at(bkg)).second)->at(p)->GetYlabel().c_str();
				}


				if(p==0){ 
					cout << "\tNormalization for " << backgroundLabels.at(bkg) << ":" 
						<< string(15-backgroundLabels.at(bkg).length(),' ') 
						<< setprecision(4) << backgroundWrapper->GetNormalization() << endl;
				}//*/

				// Add background to event count
				string histoName = string(backgroundPlot->GetName());
				if((histoName.compare(plotNameToCount)==0) && (histoName.length() == plotNameToCount.length())){
					eventCounter->AddBackground(backgroundLabels.at(bkg), (TH1F*)backgroundPlot);	
				}


				// Prepare for canvas plotting
				canvas->SetName(backgroundPlot->GetName());
				backgroundPlot->SetFillColor(backgroundColor.at(bkg));

				// Statistical errors on backgrounds
				if (errorh == NULL && (backgroundPlot->Integral() > 0) ){ 
					errorh = (TH1*)backgroundPlot->Clone();
					if(basePlot==NULL){ basePlot = (TH1*)backgroundPlot->Clone(); }
				}else if( backgroundPlot->Integral() > 0 ){
					errorh->Add(backgroundPlot);
				}

				if((ymin!=0) != (ymax!=0)){ newMaxY = ymax; }


				// Add background to stack, and legend
				if ((!isTH2F) && (backgroundPlot->Integral() > 0)){
					backgroundPlot->GetYaxis()->SetRangeUser(0.01,1.3*newMaxY);
					stack->Add(backgroundPlot);
					canvasEmpty = false; 
				}

				if(isTH2F){
					backgroundPlot->GetXaxis()->SetRangeUser(xmin,xmax);
					backgroundPlot->GetYaxis()->SetRangeUser(ymin,ymax);
					objectSaver.SaveToFile(backgroundPlot, "TH2F", outputRoot, string("stacker/histos/"+backgroundLabels.at(bkg)));
				}else{
					objectSaver.SaveToFile(backgroundPlot, "TH1F", outputRoot, string("stacker/histos/"+backgroundLabels.at(bkg)));
				}
				legend->AddEntry(backgroundPlot, (backgrounds.at(bkg).first).c_str(), "f");


			}


			if((!doCollisions) && (!isTH2F)){
				if( stack->GetMaximum() > 0){
					//stack->Draw("AXIS");
					stack->Draw("HIST");
					//			stack->GetHistogram()->GetYaxis()->SetRangeUser(0.01, 1.3*newMaxY);
					stack->GetXaxis()->SetRangeUser(xmin,xmax);
					//					stack->GetYaxis()->SetRangeUser(0.01,1.5*stack->GetMaximum());
					stack->GetXaxis()->SetTitle(xlabel.c_str());
					stack->GetXaxis()->CenterLabels(centerLabels);
					stack->GetYaxis()->SetTitle(ylabel.c_str());
				}
			}

			// QCD plot
			if (doQCD){
				TH1* QCD 		= BuildQCD(p);


				doLogx = QCDHistos->at(p)->DoLogx();
				doLogy = QCDHistos->at(p)->DoLogy();
				if(QCDHistos->at(p)->IsTH2F()){ doLogz = QCDHistos->at(p)->DoLogz(); }

				centerLabels = QCDHistos->at(p)->CenterLabels();

				xlabel = QCDHistos->at(p)->GetXlabel().c_str();
				ylabel = QCDHistos->at(p)->GetYlabel().c_str();
				if( QCDHistos->at(p)->IsTH2F()){ zlabel = QCDHistos->at(p)->GetZlabel().c_str(); }

				QCD->GetXaxis()->SetTitle(xlabel.c_str());
				QCD->GetXaxis()->CenterLabels(centerLabels);
				QCD->GetYaxis()->SetTitle(ylabel.c_str());
				ymin = QCDHistos->at(p)->GetYminVis();
				ymax = QCDHistos->at(p)->GetYmaxVis();
				if( QCDHistos->at(p)->IsTH2F()){ 
					isTH2F = true;
					QCD->GetZaxis()->SetTitle(zlabel.c_str());
					doLogz = QCDHistos->at(p)->DoLogz();
					//					QCD->GetYaxis()->SetRangeUser(ymin,ymax);
					QCD->GetZaxis()->SetTitle(zlabel.c_str());
				}



				// Add background to event count
				string histoName = string(QCD->GetName());
				if((histoName.compare(plotNameToCount)==0) && (histoName.length() == plotNameToCount.length())){
					eventCounter->AddBackground("QCD",(TH1F*)QCD);	
				}

				canvas->SetName(QCD->GetName());
				QCD->SetFillColor(kGreen-5);
				if (errorh == NULL && QCD->Integral() > 0 ){
					errorh = (TH1*)QCD->Clone();
					if(basePlot == NULL){ basePlot = (TH1*)QCD->Clone(); }
				}else if( QCD->Integral() > 0 ){ errorh->Add(QCD); }
				if( (!isTH2F) && (QCD->Integral() > 0)){
					stack->Add(QCD); 
					canvasEmpty = false; 
				}

				if(isTH2F){
					objectSaver.SaveToFile(QCD, "TH2F", outputRoot, "stacker/histos/QCD");
				}else{
					objectSaver.SaveToFile(QCD, "TH1F", outputRoot, "stacker/histos/QCD");
				}
				legend->AddEntry(QCD, QCDLabel.c_str(), "f");
			}


		}

		if(errorh != NULL){
			if((newMaxY < errorh->GetMaximum()) && (ymin==0) && (ymax==0)){
				newMaxY = errorh->GetMaximum();
			}
		}



		if (doSignals && (!isTH2F)){

			// Loop over all signals
			for ( unsigned int signal = 0; signal < signals.size(); signal++){

				isTH2F = false;
				if((signals.at(signal).second)->at(p)->IsTH2F()){ isTH2F = true; }


				if((signals.at(signal).second)->at(p)->IsTH2F()){
					((signals.at(signal)).second)->at(p)->GetHisto()->GetXaxis()->SetTitle(((signals.at(signal)).second)->at(p)->GetXlabel().c_str());
					((signals.at(signal)).second)->at(p)->GetHisto()->GetYaxis()->SetTitle(((signals.at(signal)).second)->at(p)->GetYlabel().c_str());
				}
				if(outputFilename.length() < 1){ outputFilename = ((signals.at(signal)).second)->at(p)->GetOutputFilename(); }

				TH1* signalPlot = NULL;
				if(!isTH2F){ signalPlot = new TH1F(*((TH1F*)(((signals.at(signal)).second)->at(p)->GetHisto()))); }
				else{ signalPlot = new TH2F(*((TH2F*)(((signals.at(signal)).second)->at(p)->GetHisto()))); }

				HistoWrapper* signalWrapper = ((signals.at(signal)).second)->at(p);
				showText = signalWrapper->ShowText();
				doLogx = ((signals.at(signal)).second)->at(p)->DoLogx();
				doLogy = ((signals.at(signal)).second)->at(p)->DoLogy();
				if((signals.at(signal).second)->at(p)->IsTH2F()){ doLogz = (signals.at(signal).second)->at(p)->DoLogz(); }
				centerLabels = ((signals.at(signal)).second)->at(p)->CenterLabels();
				signalPlot->GetXaxis()->SetRangeUser(((signals.at(signal)).second)->at(p)->GetXminVis(),((signals.at(signal)).second)->at(p)->GetXmaxVis());
				signalPlot->GetXaxis()->SetTitle(((signals.at(signal)).second)->at(p)->GetXlabel().c_str());
				signalPlot->GetXaxis()->CenterLabels(centerLabels);
				signalPlot->GetYaxis()->SetTitle(((signals.at(signal)).second)->at(p)->GetYlabel().c_str());
				isTH2F = false;
				if((signals.at(signal).second)->at(p)->IsTH2F()){
					isTH2F = true;
					signalPlot->GetYaxis()->SetTitle(((signals.at(signal)).second)->at(p)->GetZlabel().c_str());
				}


				if(signalPlot==NULL){ continue; }
				if(!isTH2F){
					basePlot = (TH1F*)signalPlot->Clone();
				}else{
					basePlot = (TH2F*)signalPlot->Clone();
				}
				if(signalPlot->Integral() > 0){ canvasEmpty = false; }

				// Add signal to event count
				string histoName = string(signalPlot->GetName());
				if((histoName.compare(plotNameToCount)==0) && (histoName.length() == plotNameToCount.length())){
					eventCounter->AddSignal(signalLabels.at(signal),(TH1F*)signalPlot);	
				}

				// Prepare for canvas plotting
				canvas->SetName(signalPlot->GetName());
				signalPlot->SetFillStyle(0);
				signalPlot->SetLineWidth(3);
				signalPlot->SetLineColor(signalColor.at(signal));

				if((signalPlot != NULL) && (ymin==0) && (ymax==0)){
					if(newMaxY < signalPlot->GetMaximum()){
						newMaxY = signalPlot->GetMaximum();
					}
				}

				if(!isTH2F){
				objectSaver.SaveToFile(signalPlot, "TH1F", outputRoot, string("stacker/histos/" + signalLabels.at(signal)));
				}else{
				objectSaver.SaveToFile(signalPlot, "TH2F", outputRoot, string("stacker/histos/" + signalLabels.at(signal)));
				}
				legend->AddEntry(signalPlot, (signals.at(signal).first).c_str(), "l");
				signalHistos.push_back(signalPlot);

			}
		}

		// Extra plot info
		float xPlotInfo		= 1.00;
		float yPlotInfo		= 0.95;

		float dxPlotInfo	= 0.40; float dyPlotInfo	= 0.07; 
		TPaveText *plotInfo = new TPaveText(xPlotInfo-dxPlotInfo, yPlotInfo-dyPlotInfo, xPlotInfo, yPlotInfo, "brNDC");
		plotInfo->SetBorderSize(0);
		plotInfo->SetLineColor(0);
		plotInfo->SetFillColor(kWhite);
		stringstream lumiSS;
		lumiSS.str("");
		lumiSS << "#tau_{h}#tau_{h} channel " << endl;
		if( effectiveLumi>0 ){lumiSS << "(" << GetNiceLumi(effectiveLumi,units) << ")"; }
		plotInfo->AddText((lumiSS.str()).c_str());
		plotInfo->AddText("CMS Preliminary");
		plotInfo->SetFillStyle(0);


		if(stack != NULL){ 
			//stack->Draw(); 
		};
		if(collisions != NULL){ collisions->GetYaxis()->SetRangeUser(0.01,1.3*newMaxY); }
		if(stack != NULL){ 
			if(stack->GetHistogram() != NULL){
				stack->GetHistogram()->GetYaxis()->SetRangeUser(0.01,1.3*newMaxY); }
		}
		if(errorh != NULL){ errorh->GetYaxis()->SetRangeUser(0.01,1.3*newMaxY); }

		if(!isTH2F){
			// Error on backgrounds
			if (errorh != NULL){
				for(int bin = 0; bin <= errorh->GetNbinsX(); bin++){ errorh->SetBinError(bin,sqrt(errorh->GetBinContent(bin))); }
				errorh->SetLineColor(kGray+3);
				errorh->SetLineWidth(1);
				errorh->SetLineStyle(3);
				errorh->SetMarkerStyle(0);
				errorh->SetFillColor(kGray+3);
				errorh->SetFillStyle(3001);
			}

			// Draw stack of signal + backgrounds
			bool doCollisionsTemp = doCollisions;
			if(!canvasEmpty){
				float collMax = 0;
				float stackMax = 0;
				if(basePlot != NULL){ 
					basePlot->GetYaxis()->SetRangeUser(0.01,1.3*newMaxY); 
					basePlot->GetXaxis()->SetRangeUser(xmin,xmax);
					basePlot->GetYaxis()->SetTitle(ylabel.c_str());
					basePlot->GetXaxis()->SetTitle(xlabel.c_str());
				}
				basePlot->Draw("AXIS");
				if(doCollisions){	collMax	= collisions->GetMaximum() + sqrt(collisions->GetMaximum()); }
				if(doBackgrounds){	stackMax	= stack->GetMaximum() + sqrt( stack->GetMaximum()); }
				//if(doCollisions){ collisions->GetYaxis()->SetRangeUser(0.01,max((1.1*max(collMax,stackMax)),(double)ymax)); }
				//if(stackMax > 0 ){ stack->Draw("HIST"); stack->GetYaxis()->SetRangeUser(0.01,max((1.5*max(collMax,stackMax)), (double)ymax)); }
				if(doCollisions){ collisions->GetYaxis()->SetRangeUser(0.01,1.3*newMaxY); }
				if(stackMax > 0 ){ 
					//stack->GetYaxis()->SetRangeUser(0.01, 1.1*newMaxY); 
					//stack->Draw("HISTsame"); 
				}

				bool collisionsFirst = true;
				if ((doBackgrounds || doSignals)){
					if ((!doCollisions) || collMax < stackMax ){
						collisionsFirst = false;
					}else{
						collisionsFirst = true;
					}
				}else{
					collisionsFirst = true;
				}

				if (doCollisions && collisionsFirst){
					collisions->Draw("EP");
					if(doBackgrounds || doSignals){
						if(stackMax > 0 ){ 
							stack->Draw("sameHIST"); 
						} 
						if (errorh != NULL){ errorh->Draw("sameE2"); }
						collisions->Draw("sameEP");
						collisions->Draw("sameAXIS"); 
					}
				}else if((doBackgrounds || doSignals) && (!collisionsFirst)){
					if(doCollisions){ 
						collisions->Draw("EP"); 
						if(stackMax > 0 ){ 
							stack->Draw("sameHIST"); 
						}
						if (errorh != NULL){ errorh->Draw("sameE2"); } 
						collisions->Draw("sameEP"); 
						collisions->Draw("sameAXIS"); 
					}else{
						if(stackMax > 0 ){ 
							stack->Draw("sameHIST"); 
						}
						if (errorh != NULL){ errorh->Draw("sameE2"); }
						if(stackMax > 0 ){
							stack->GetHistogram()->Draw("sameAXIS");
							stack->GetXaxis()->CenterLabels(centerLabels);
							stack->GetXaxis()->SetTitle(xlabel.c_str());
							stack->GetYaxis()->SetTitle(ylabel.c_str());
							stack->GetXaxis()->SetRangeUser(xmin,xmax);
							//stack->GetYaxis()->SetRangeUser(0.01,max((1.5*stack->GetMaximum()), (double)ymax));
							stack->GetYaxis()->SetRangeUser(0.01, 1.3*newMaxY); 
						}
					}
				}
				if (doSignals){

					float signalYmax = 0;
					for ( unsigned int signalHisto = 0; signalHisto < signalHistos.size(); signalHisto++){
						float ymaxOverInt = 
							(signalHistos.at(signalHisto))->GetMaximum()/
							(signalHistos.at(signalHisto))->Integral();
						if(ymaxOverInt > signalYmax){ signalYmax = ymaxOverInt; }
					}

					for ( unsigned int signalHisto = 0; signalHisto < signalHistos.size(); signalHisto++){
						if((signalHistos.at(signalHisto)) != NULL){
							(signalHistos.at(signalHisto))->GetYaxis()->SetRangeUser(0.01,1.3*newMaxY);
						}
						if((doCollisions || doBackgrounds) || signalHisto != 0){
							//(signalHistos.at(signalHisto))->Scale(1/(signalHistos.at(signalHisto))->Integral());
							//(signalHistos.at(signalHisto))->GetYaxis()->SetRangeUser(0.001,1.2*signalYmax);
							(signalHistos.at(signalHisto))->Draw("HISTsame");
						}else{
							//(signalHistos.at(signalHisto))->Scale(1/(signalHistos.at(signalHisto))->Integral());
							//(signalHistos.at(signalHisto))->GetYaxis()->SetRangeUser(0.001,1.2*signalYmax);
							canvas = new TCanvas("test","test",800,800);
							canvas->cd();
							int nbins = (signalHistos.at(signalHisto))->GetNbinsX();
							float mint = (signalHistos.at(signalHisto))->GetBinLowEdge(1); 
							float maxt = (signalHistos.at(signalHisto))->GetBinLowEdge(nbins);
							TH1F* myhisto = new TH1F("ha","ha",nbins,mint,maxt);
							for(unsigned int b = 0; b<(signalHistos.at(signalHisto))->GetNbinsX(); b++){
								float binContent = (signalHistos.at(signalHisto))->GetBinContent(b);
								myhisto->SetBinContent(b,binContent);
							}
							myhisto->SetLineWidth(3);
							myhisto->SetFillStyle(0);
							myhisto->SetLineColor(4);
							myhisto->Draw("HIST");
							//((TH1F*)(signalHistos.at(signalHisto)))->Draw("HIST");

						}
					}
				}

				plotInfo->Draw();
				legend->Draw();

				// Save canvas
				SaveCanvas(canvas, outputDir, outputFilename);

				//Always do logy 
				doLogy = true;
				if(doLogx || doLogy || doLogz ){
					if(doLogx){ canvas->SetLogx(1); }
					if(doLogy){ canvas->SetLogy(1); }
					if(doLogz){ canvas->SetLogz(1); }
					SaveCanvas(canvas, outputDir, string(outputFilename+"_log"));
					canvas->SetLogx(0);
					canvas->SetLogy(0);
					canvas->SetLogz(0);
				}

				//stacksDir->cd();
				//canvas->Write();

			}
		}else{ // Draw and save now the TH2F
			if(doCollisions){

				canvas->Clear();
				TPad* pad = new TPad("mypad", "mypadtitle", 0.0,0.13,0.82,0.95);
				pad->Draw();
				pad->cd();
				collisions->GetZaxis()->SetLabelSize(0.04);
				collisions->GetZaxis()->SetRangeUser(0,1.1*collisions->GetMaximum());
				TH1* toPlot = collisions;
				toPlot->GetZaxis()->SetLabelSize(0.04);
				if(showText){
					toPlot->Draw("COLZTEXT");
				}else{
					toPlot->Draw("COLZ");
				}

				TPaveText *plotInfoObs = (TPaveText*)plotInfo->Clone();
				plotInfoObs->AddText("Observed events");
				//plotInfoObs->Draw();
				SaveCanvas(canvas, outputDir, (outputFilename+"_obs"));
				if(doLogx || doLogy || doLogz ){
					if(doLogx){ canvas->SetLogx(1); }
					if(doLogy){ canvas->SetLogy(1); }
					if(doLogz){ canvas->SetLogz(1); }
					SaveCanvas(canvas, outputDir, string(outputFilename+"_obs_log"));
					canvas->SetLogx(0);
					canvas->SetLogy(0);
					canvas->SetLogz(0);
				}
				gStyle->SetPadRightMargin(0.02);
			}
			if(doCollisions){

				canvas->Clear();
				TPad* pad = new TPad("mypad", "mypadtitle", 0.0,0.13,0.82,0.95);
				pad->Draw();
				pad->cd();
				collisions->GetZaxis()->SetLabelSize(0.04);
				collisions->GetZaxis()->SetRangeUser(0,1.1*collisions->GetMaximum());
				TH1* toPlot = collisions;
				toPlot->GetZaxis()->SetLabelSize(0.04);
				if(showText){
					toPlot->Draw("BOXTEXT");
				}else{
					toPlot->Draw("BOX");
				}

				TPaveText *plotInfoObs = (TPaveText*)plotInfo->Clone();
				plotInfoObs->AddText("Observed events");
				//plotInfoObs->Draw();
				SaveCanvas(canvas, outputDir, (outputFilename+"_obs_box"));
				if(doLogx || doLogy || doLogz ){
					if(doLogx){ canvas->SetLogx(1); }
					if(doLogy){ canvas->SetLogy(1); }
					if(doLogz){ canvas->SetLogz(1); }
					SaveCanvas(canvas, outputDir, string(outputFilename+"_obs_log_box"));
					canvas->SetLogx(0);
					canvas->SetLogy(0);
					canvas->SetLogz(0);
				}
				gStyle->SetPadRightMargin(0.02);
			}


			if(doBackgrounds){
				canvas->Clear();
				TPad* pad = new TPad("mypad", "mypadtitle", 0.0,0.13,0.82,0.95);
				pad->Draw();
				pad->cd();
				if(errorh==NULL){ continue; }
				TH1* toPlot = (TH1*)errorh->Clone();
				toPlot->GetYaxis()->SetRangeUser(ymin,ymax);
				toPlot->GetZaxis()->SetRangeUser(0,1.1*toPlot->GetMaximum());
				toPlot->GetZaxis()->SetLabelSize(0.04);
				if(showText){
					toPlot->Draw("COLZTEXT");
				}else{
					toPlot->Draw("COLZ");
				}
				TPaveText *plotInfoExp = (TPaveText*)plotInfo->Clone();
				plotInfoExp->AddText("Expected events");
				//plotInfoExp->Draw();
				SaveCanvas(canvas, outputDir, (outputFilename+"_exp"));
				if(doLogx || doLogy || doLogz ){
					if(doLogx){ canvas->SetLogx(1); }
					if(doLogy){ canvas->SetLogy(1); }
					if(doLogz){ canvas->SetLogz(1); }
					SaveCanvas(canvas, outputDir, string(outputFilename+"_exp_log"));
					canvas->SetLogx(0);
					canvas->SetLogy(0);
					canvas->SetLogz(0);
				}
				gStyle->SetPadRightMargin(0.02);
			}
			// Repeat above with BOX instead of COLZ
			if(doBackgrounds){
				canvas->Clear();
				TPad* pad = new TPad("mypad", "mypadtitle", 0.0,0.13,0.82,0.95);
				pad->Draw();
				pad->cd();
				if(errorh==NULL){ continue; }
				TH1* toPlot = (TH1*)errorh->Clone();
				toPlot->GetYaxis()->SetRangeUser(ymin,ymax);
				toPlot->GetZaxis()->SetRangeUser(0,1.1*toPlot->GetMaximum());
				toPlot->GetZaxis()->SetLabelSize(0.04);
				if(showText){
					toPlot->Draw("BOXTEXT");
				}else{
					toPlot->Draw("BOX");
				}
				TPaveText *plotInfoExp = (TPaveText*)plotInfo->Clone();
				plotInfoExp->AddText("Expected events");
				//plotInfoExp->Draw();
				SaveCanvas(canvas, outputDir, (outputFilename+"_exp_box"));
				if(doLogx || doLogy || doLogz ){
					if(doLogx){ canvas->SetLogx(1); }
					if(doLogy){ canvas->SetLogy(1); }
					if(doLogz){ canvas->SetLogz(1); }
					SaveCanvas(canvas, outputDir, string(outputFilename+"_exp_log_box"));
					canvas->SetLogx(0);
					canvas->SetLogy(0);
					canvas->SetLogz(0);
				}
				gStyle->SetPadRightMargin(0.02);
			}

			if(doCollisions && doBackgrounds){
				canvas->Clear();
				TPad* pad = new TPad("mypad", "mypadtitle", 0.0,0.13,0.82,0.95);
				pad->Draw();
				pad->cd();
				TH1* num = (TH1*)collisions->Clone();
				TH1* den = (TH1*)errorh->Clone();
				num->Divide(den);
				num->GetZaxis()->SetRangeUser(0,1.2*num->GetMaximum());
				num->GetZaxis()->SetLabelSize(0.04);
				((TH2F*)num)->GetZaxis()->SetRangeUser(0,1.7);
				num->Draw("COLZ");
				TPaveText *plotInfoObsOverExp = (TPaveText*)plotInfo->Clone();
				plotInfoObsOverExp->AddText("Obs. / Exp.");
				//plotInfoObsOverExp->Draw();
				SaveCanvas(canvas, outputDir, (outputFilename+"_obsOverExp"));
				if(doLogx || doLogy || doLogz ){
					if(doLogx){ canvas->SetLogx(1); }
					if(doLogy){ canvas->SetLogy(1); }
					if(doLogz){ canvas->SetLogz(1); }
					SaveCanvas(canvas, outputDir, string(outputFilename+"_obsOverExp_log"));
					canvas->SetLogx(0);
					canvas->SetLogy(0);
					canvas->SetLogz(0);
				}
				gStyle->SetPadRightMargin(0.02);
			}


			if(doSignals){
				for(unsigned int s = 0; s < signals.size(); s++){
					canvas->Clear();
					TPad* pad = new TPad("mypad", "mypadtitle", 0.0,0.13,0.82,0.95);
					pad->Draw();
					pad->cd();
					TH1* toPlot = (TH1*)(signals.at(s).second)->at(p)->GetHisto();
					toPlot->GetYaxis()->SetRangeUser(ymin,ymax);
					toPlot->GetZaxis()->SetRangeUser(0,1.1*toPlot->GetMaximum());
					toPlot->GetZaxis()->SetLabelSize(0.04);
					if(showText){
						toPlot->Draw("COLZTEXT");
					}else{
						toPlot->Draw("COLZ");
					}
					TPaveText *plotInfoExp = (TPaveText*)plotInfo->Clone();
					plotInfoExp->AddText("Expected events");
					//plotInfoExp->Draw();
					stringstream sigNumber; sigNumber.str("");
					sigNumber << s;
					SaveCanvas(canvas, outputDir, (outputFilename+"_sig"+sigNumber.str()));
					if(doLogx || doLogy || doLogz ){
						if(doLogx){ canvas->SetLogx(1); }
						if(doLogy){ canvas->SetLogy(1); }
						if(doLogz){ canvas->SetLogz(1); }
						SaveCanvas(canvas, outputDir, string(outputFilename+"_sig"+sigNumber.str()+"_log"));
						canvas->SetLogx(0);
						canvas->SetLogy(0);
						canvas->SetLogz(0);
					}
					gStyle->SetPadRightMargin(0.02);
				}
			}

		}//*/
	}

	cout << string(lastSavedPlotName.length(),'\b') << string(lastSavedPlotName.length(),' ') << endl; 

	return eventCounter;
}

// Save canvas
void PlotStacker::SaveCanvas(TCanvas* canvas, string dir, string filename){

	if(lastSavedPlotName.length()>0){
		cout << string(lastSavedPlotName.length(),'\b'); cout.flush(); 
		cout << string(lastSavedPlotName.length(),' '); cout.flush();
		cout << string(lastSavedPlotName.length(),'\b'); cout.flush(); 
	}
	cout << filename; cout.flush();

	// Create output dir if it doesn't exists already
	TString sysCommand = "if [ ! -d " + dir + " ]; then mkdir -p " + dir + "; fi";
	if(gSystem->Exec(sysCommand) > 0){ cout << ">>> ERROR: problem creating dir for plots " << dir << endl; exit(1); }// exit(0);

	// Loop over all file format extensions choosen and save canvas
	vector<string> extension; extension.push_back(".png");
	for( unsigned int ext = 0; ext < extension.size(); ext++){
		canvas->SaveAs( (dir + filename + extension.at(ext)).c_str() );
	}

	lastSavedPlotName = filename;

}

string PlotStacker::GetNiceLumi(float iLumi, string iUnits){
	string result;

	map<string,int> suf2mag; suf2mag["mb"] = 12; suf2mag["ub"] = 9; suf2mag["ub"] = 6; suf2mag["nb"] = 3; suf2mag["pb"] = 0; suf2mag["fb"] = -3; suf2mag["ab"] = -6;
	map<int,string> mag2suf; mag2suf[12] = "mb"; mag2suf[9] = "ub"; mag2suf[6] = "ub"; mag2suf[3] = "nb"; mag2suf[0] = "pb"; mag2suf[-3] = "fb"; mag2suf[-6] = "ab";

	int lumiMag = 3*((int)(((int)log10(iLumi)) / 3));
	int unitsMag = suf2mag[iUnits];
	int targetMag = unitsMag - lumiMag;

	float targetLumi = ((iLumi/(double)pow(10,lumiMag)) + 0.05)*10;
	targetLumi = ((int)targetLumi)/(double)10.0;

	stringstream ssout; ssout.str("");	
	if( ((int)(targetLumi*10)) == (((int)targetLumi)*10) ){ ssout << ((int)targetLumi) << ".0"; } 
	else{ ssout << targetLumi; }
	ssout << "/" << mag2suf[targetMag];
	result = ssout.str();

	return result;
}

bool PlotStacker::IsTH1F(TH1* iHisto){
	string className = string(iHisto->ClassName());
	return (className.compare("TH1F")==0);
}

bool PlotStacker::IsTH2F(TH1* iHisto){
	string className = string(iHisto->ClassName());
	return (className.compare("TH2F")==0);
}
