//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Jun  7 03:28:02 2011 by ROOT version 5.27/06b
// from TTree HMTTree/TauTauTree
// found on file: ditau_nTuple.root
//////////////////////////////////////////////////////////

#ifndef PlotStacker_h
#define PlotStacker_h

#include <TROOT.h>
#include <TSystem.h>
#include <TChain.h>
#include <TStyle.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <THStack.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TPaveText.h>
#include <TGraphAsymmErrors.h>
#include <TPaletteAxis.h>
#include <math.h>
#include <map>
#include <vector>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <utility>
#include <stdlib.h>
#include "HistoWrapper.h"
#include "EventCounter.h"
#include "TObjectSaver.h"
//#include "style-CMSTDR.h"



using namespace std;

class PlotStacker {
	public :

		// Backgrounds
		vector< pair< string, vector<HistoWrapper*>*> > backgrounds;
		vector<float>	backgroundOtherSF;
		vector<float>	backgroundCrossSection;
		vector<int> 	backgroundNumEvInDS;
		vector<int>		backgroundColor;
		vector<string>	backgroundLabels;


		// Signals
		vector< pair< string, vector<HistoWrapper*>*> > signals;
		vector<float>	signalOtherSF;
		vector<float>	signalCrossSection;
		vector<int> 	signalNumEvInDS;
		vector<int>		signalColor;
		vector<string>	signalLabels;

		// Collisions and QCD
		vector<HistoWrapper*>*	collisionHistos;
		string					collisionsLabel;
		float					collisionsSF;

		vector<HistoWrapper*>*	QCDHistos;
		float 					QCDSF;
		string					QCDLabel;

		vector< pair< string, vector<HistoWrapper*>*> > LSbackgrounds;
		vector<float>									LSbackgroundOtherSF;
		vector<float>									LSbackgroundCrossSection;
		vector<int>										LSbackgroundNumEvInDS;

		bool doBackgrounds;
		bool doSignals;
		bool doQCD;
		bool doCollisions;

		float allMCscaleFactor;

		string lastSavedPlotName;


		EventCounter* eventCounter;
		string plotNameToCount;
		vector<pair<string,string> > topologies;
		string outputDir;
		string outputRoot;
		unsigned int numberOfPlots;
		float lumi;
		float effectiveLumi;
		string units;
		string flags;
		bool chargeProductApplied;

		TDirectory* stacksDir;
		TDirectory* histosDir;

		TObjectSaver objectSaver;



		PlotStacker(string,string);
		virtual ~PlotStacker();
		virtual void AddBackground(vector<HistoWrapper*>*, string, string, int, float, float, int);
		virtual void AddLSBackground(vector<HistoWrapper*>*,string, float, float, int);
		virtual void AddQCD(vector<HistoWrapper*>*, string, float);
		virtual TH1* BuildQCD(int);
		virtual void AddSignal(vector<HistoWrapper*>*, string, string, int, float, float, int);
		virtual void AddCollisions(vector<HistoWrapper*>*, string, float);
		virtual EventCounter* DrawPlots();
		virtual void SaveCanvas(TCanvas*, string, string);
		virtual void SetLumi(float,string);
		virtual string GetNiceLumi(float,string);
		virtual void SetEffectiveLumi(float);
		virtual void SetAllMCscaleFactor(float);
		virtual void SetFlags(string);
		virtual bool IsFlagThere(string);
		virtual bool IsTH1F(TH1*);
		virtual bool IsTH2F(TH1*);
					

};

#endif










