/*
    Author:			Nil Valls <nil.valls@cern.ch>
    Date:			11 Jun 2011
    Description:	Plot stacking class.
    Notes:

*/

#include "PlotStamper.h"

#define PlotStamper_cxx
using namespace std;

#define AT __LINE__

// Default constructor
PlotStamper::PlotStamper(string iOutputRoot, string iOutputDir){

	// Draw horizontal error bars
	gStyle->SetErrorX(0.5);

	outputDir = iOutputDir;
	outputRoot = iOutputRoot;

	lumi	= 0;
	units	= "";

	doBackgrounds	= false;
	doSignals		= false;
	doQCD			= false;
	doCollisions	= false;

	topologies.clear();

	backgrounds.clear();
	backgroundColor.clear();
	backgroundLabels.clear();
	backgroundOtherSF.clear();
	backgroundCrossSection.clear();
	backgroundNumEvInDS.clear();

	signals.clear();
	signalColor.clear();
	signalLabels.clear();
	signalOtherSF.clear();
	signalCrossSection.clear();
	signalNumEvInDS.clear();

	LSbackgrounds.clear();
	LSbackgroundOtherSF.clear();
	LSbackgroundCrossSection.clear();
	LSbackgroundNumEvInDS.clear();

	allMCscaleFactor	= 1.0;
	collisionsSF		= 1.0;
	QCDSF				= 1.0;

	plotNameToCount = "InvariantMass_LSM_RightIntegrated";
	numberOfPlots	= 0;

	collisionHistos = NULL;
	QCDHistos		= NULL;

	bool chargeProductApplied = false;
	lastSavedPlotName="";

}

// Default destructor
PlotStamper::~PlotStamper(){
}

void PlotStamper::SetAllMCscaleFactor(float iFactor){
	allMCscaleFactor	= iFactor;
}

void PlotStamper::SetLumi(float iLumi, string iUnits){
	lumi	= iLumi;
	units	= iUnits;
}

// Set up analysis flags
void PlotStamper::SetFlags(string iFlags){
	flags = iFlags;
}

bool PlotStamper::IsFlagThere(string iFlag){
	size_t found = flags.find(iFlag);
	return ((0 <= found) && (found < flags.length()));
}

void PlotStamper::SetEffectiveLumi(float iLumi){
	effectiveLumi = iLumi;
}

// Add collision data plots
void PlotStamper::AddCollisions(vector<HistoWrapper*>* plots, string iLabel, float iScaleFactor){

	// Set of check consistent number of plots
	if (numberOfPlots == 0){
		numberOfPlots	= plots->size();
	}else{
		if ( plots->size() != numberOfPlots ){
			cout << "ERROR: imported vector of collision plots does not have the same amount of plots as the other topologies" << endl;
			exit(1);
		}
	}

	chargeProductApplied = plots->at(0)->ChargeProductApplied();
	collisionHistos		= plots;
	doCollisions		= true;
	collisionsSF		= iScaleFactor;
	collisionsLabel		= iLabel;
	topologies.push_back(pair<string,string>(iLabel,iLabel));
}

// Add signal plots
void PlotStamper::AddSignal(vector<HistoWrapper*>* plots, string iLabel, string iLabelForLegend, int iLineColor, float iCrossSection, float iOtherSF, int iNumEvInDS){

	// Set of check consistent number of plots
	if (numberOfPlots == 0){
		numberOfPlots	= plots->size();
	}else{
		if ( plots->size() != numberOfPlots ){
			cout << "ERROR: imported vector of signal plots does not have the same amount of plots as the other topologies" << endl;
			exit(1);
		}
	}

	chargeProductApplied = plots->at(0)->ChargeProductApplied();
	signals.push_back( pair< string, vector<HistoWrapper*>* >(iLabelForLegend, plots) );
	signalColor.push_back(iLineColor);
	signalOtherSF.push_back(iOtherSF);
	signalCrossSection.push_back(iCrossSection);
	signalNumEvInDS.push_back(iNumEvInDS);
	doSignals	= true;
	signalLabels.push_back(iLabel);
	topologies.push_back(pair<string,string>(iLabel,iLabelForLegend));

}

void PlotStamper::AddQCD( vector<HistoWrapper*>* plots, string iQCDLabel, float iScaleFactor){

	// Set of check consistent number of plots
	if (numberOfPlots == 0){
		numberOfPlots	= plots->size();
	}else{
		if ( plots->size() != numberOfPlots ){
			cout << "ERROR: imported vector of QCD plots does not have the same amount of plots as the other topologies" << endl;
			exit(1);
		}
	}

	QCDHistos		= plots;
	QCDLabel		= iQCDLabel;
	doQCD			= true;
	QCDSF			= iScaleFactor;
	topologies.push_back(pair<string,string>("QCD",iQCDLabel));
}

// Subtract LS backgrounds from LS collisions to obtain dd-QCD distros
TH1* PlotStamper::BuildQCD(int plotNum){
	
	TH1* result = NULL;
	int p = plotNum;

	if(!doQCD){ return NULL; }

	// Get LS collisions histo
	result = (TH1*)(QCDHistos->at(p)->GetHisto()->Clone());

	// Loop over all backgrounds
	for ( unsigned int bkg = 0; bkg < LSbackgrounds.size(); bkg++){
		HistoWrapper* backgroundWrapper = ((LSbackgrounds.at(bkg)).second)->at(p);
		TH1* backgroundPlot = (TH1*)(((LSbackgrounds.at(bkg)).second)->at(p)->GetHisto()->Clone()); 

		// Subtract this background from LS collisions
		result->Add(backgroundPlot,-1);
	}

	// Make sure no bins acquire negative values
	for(int bin=1; bin<result->GetNbinsX(); bin++){
		if(result->GetBinContent(bin) < 0){
			result->SetBinContent(bin, 0);
			result->SetBinError(bin, 0);
		}
	}//*/


	if( chargeProductApplied ){
		// Fix charge product plot
		if((string(result->GetName())).compare("ChargeProduct")==0){
			int osBin = -1;
			int lsBin = -1;
			for(int bin=1; bin <= result->GetNbinsX(); bin++){
				if(result->GetBinLowEdge(bin) == -1){ osBin = bin; }	
				if(result->GetBinLowEdge(bin) ==  1){ lsBin = bin; }	
			}
			result->SetBinContent(osBin, result->GetBinContent(lsBin)*QCDSF);
			result->SetBinContent(lsBin, 0);
		}else{
			result->Scale(QCDSF);
		}
	}else{
		// Fix charge product plot
		if((string(result->GetName())).compare("ChargeProduct")==0){
			int osBin = -1;
			int lsBin = -1;
			for(int bin=1; bin <= result->GetNbinsX(); bin++){
				if(result->GetBinLowEdge(bin) == -1){ osBin = bin; }	
				if(result->GetBinLowEdge(bin) ==  1){ lsBin = bin; }	
			}
			result->SetBinContent(osBin, result->GetBinContent(lsBin)*QCDSF);
		}else{
			result->Scale(1+QCDSF);
		}
	}


	return result;

}


// Add plots to subtract from QCD
void PlotStamper::AddLSBackground(vector<HistoWrapper*>* plots, string labelForLegend, float iCrossSection, float iOtherSF, int iNumEvInDS){


	// Set of check consistent number of plots
	if (numberOfPlots == 0){
		numberOfPlots	= plots->size();
	}else{
		if ( plots->size() != numberOfPlots ){
			cout << "ERROR: imported vector of background plots does not have the same amount of plots as the other topologies" << endl;
			exit(1);
		}
	}

	LSbackgrounds.push_back( pair< string, vector<HistoWrapper*>* >(labelForLegend, plots) );
	LSbackgroundOtherSF.push_back(iOtherSF);
	LSbackgroundCrossSection.push_back(iCrossSection);
	LSbackgroundNumEvInDS.push_back(iNumEvInDS);
}

// Add background plots
void PlotStamper::AddBackground(vector<HistoWrapper*>* plots, string iLabel, string iLabelForLegend, int iFillColor, float iCrossSection, float iOtherSF, int iNumEvInDS){

	// Set of check consistent number of plots
	if (numberOfPlots == 0){
		numberOfPlots	= plots->size();
	}else{
		if ( plots->size() != numberOfPlots ){
			cout << "ERROR: imported vector of background plots does not have the same amount of plots as the other topologies" << endl;
			exit(1);
		}
	}

	chargeProductApplied = plots->at(0)->ChargeProductApplied();
	backgrounds.push_back( pair< string, vector<HistoWrapper*>* >(iLabelForLegend, plots) );
	backgroundColor.push_back(iFillColor);
	backgroundOtherSF.push_back(iOtherSF);
	backgroundCrossSection.push_back(iCrossSection);
	backgroundNumEvInDS.push_back(iNumEvInDS);
	doBackgrounds	= true;
	backgroundLabels.push_back(iLabel);
	topologies.push_back(pair<string,string>(iLabel,iLabelForLegend));
}

// Perform the stacking and plotting here
EventCounter* PlotStamper::DrawPlots(){
	EventCounter* eventCounter = new EventCounter();


	// Check that there is at least something to plot
	if ( !(doCollisions || doSignals || doBackgrounds) ){
		cout << "ERROR: nothing to plot." << endl;
		return NULL;
	}

	// Declare canvas and legend
	TCanvas* canvas;
	TLegend *legend;

	// Loop over all plots
	for (unsigned int p = 0; p < numberOfPlots; p++){

		bool canvasEmpty = true;
		bool isTH2F	= false;
		bool doLogx = false;
		bool doLogy = false;
		bool doLogz = false;
		bool centerLabels = false;
		string outputFilename = "";
		vector<TH1*> signalHistos; signalHistos.clear();

		float xmin, xmax, ymin, ymax;
		string xlabel, ylabel, zlabel;

		// Set up canvas and legend
		float xLegend	= 0.92;
		float yLegend	= 0.88;

		float dxLegend	= 0.20; float dyLegend	= 0.30; 
		legend = new TLegend(xLegend-dxLegend, yLegend-dyLegend, xLegend, yLegend, NULL, "brNDC");
		legend->SetBorderSize(1);
		legend->SetFillColor(kWhite);
		legend->SetFillStyle(1001);
		string plotname = "test";
		canvas = new TCanvas(plotname.c_str(), plotname.c_str(), 800, 800);
		canvas->cd();


		TH1* collisions;
		if (doCollisions){
			if(outputFilename.length() < 1){ outputFilename = collisionHistos->at(p)->GetOutputFilename(); }
			collisions	= (TH1*)collisionHistos->at(p)->GetHisto()->Clone();

			// Add collisions to event count
			string histoName = string(collisions->GetName());
			if((histoName.compare(plotNameToCount)==0) && (histoName.length() == plotNameToCount.length())){
				TH1* toAdd = (TH1*)(collisions->Clone());
				eventCounter->AddCollisions(collisionsLabel,(TH1F*)toAdd);	
			}

			doLogx = collisionHistos->at(p)->DoLogx();
			doLogy = collisionHistos->at(p)->DoLogy();
			centerLabels = collisionHistos->at(p)->CenterLabels();

			TH1* collisionsn = (TH1*)(collisions->Clone());


			canvas->SetName(collisions->GetName());
			xmin = collisionHistos->at(p)->GetXminVis();
			xmax = collisionHistos->at(p)->GetXmaxVis();
			xlabel = collisionHistos->at(p)->GetXlabel().c_str();
			ylabel = collisionHistos->at(p)->GetYlabel().c_str();
			if(collisionHistos->at(p)->IsTH2F()){ zlabel = collisionHistos->at(p)->GetZlabel().c_str(); }
			collisions->GetXaxis()->SetRangeUser(xmin,xmax);
			collisions->GetXaxis()->CenterLabels(centerLabels);
			collisions->GetXaxis()->SetTitle(xlabel.c_str());
			collisions->GetYaxis()->SetTitle(ylabel.c_str());
			collisions->SetMarkerStyle(20);
			if(collisionHistos->at(p)->IsTH2F()){ 
				isTH2F = true;
				doLogz = collisionHistos->at(p)->DoLogz();
				ymin = collisionHistos->at(p)->GetYminVis();
				ymax = collisionHistos->at(p)->GetYmaxVis();
				collisions->GetYaxis()->SetRangeUser(ymin,ymax);
				collisions->GetZaxis()->SetTitle(zlabel.c_str());
				objectSaver.SaveToFile(collisions, "TH2F", outputRoot, "stamper/histos/collisions");
			}else{
				objectSaver.SaveToFile(collisions, "TH1F", outputRoot, "stamper/histos/collisions");
			}
			legend->AddEntry(collisions, (collisionsLabel).c_str(), "lep");
			if(collisions->Integral()!=0){ canvasEmpty = false; }


		}

		// Setup background
		TH1* errorh = NULL;
		if (doBackgrounds){


			// Loop over all backgrounds
			for ( unsigned int bkg = 0; bkg < backgrounds.size(); bkg++){
				if(outputFilename.length() < 1){ outputFilename = ((backgrounds.at(bkg)).second)->at(p)->GetOutputFilename(); }
				TH1* backgroundPlot = (TH1*)(((backgrounds.at(bkg)).second)->at(p)->GetHisto()->Clone()); 
				HistoWrapper* backgroundWrapper = ((backgrounds.at(bkg)).second)->at(p);


				doLogx = ((backgrounds.at(bkg)).second)->at(p)->DoLogx();
				doLogy = ((backgrounds.at(bkg)).second)->at(p)->DoLogy();
				centerLabels = ((backgrounds.at(bkg)).second)->at(p)->CenterLabels();
				backgroundPlot->GetXaxis()->SetRangeUser(((backgrounds.at(bkg)).second)->at(p)->GetXminVis(),((backgrounds.at(bkg)).second)->at(p)->GetXmaxVis());
				backgroundPlot->GetXaxis()->SetTitle(((backgrounds.at(bkg)).second)->at(p)->GetXlabel().c_str());
				backgroundPlot->GetXaxis()->CenterLabels(centerLabels);
				backgroundPlot->GetYaxis()->SetTitle(((backgrounds.at(bkg)).second)->at(p)->GetYlabel().c_str());
				if(((backgrounds.at(bkg)).second)->at(p)->IsTH2F()){
					isTH2F = true;
					doLogz = ((backgrounds.at(bkg)).second)->at(p)->DoLogz();
					backgroundPlot->GetZaxis()->SetTitle(((backgrounds.at(bkg)).second)->at(p)->GetZlabel().c_str());
					xmin = ((backgrounds.at(bkg)).second)->at(p)->GetXminVis();
					xmax = ((backgrounds.at(bkg)).second)->at(p)->GetXmaxVis();
					ymin = ((backgrounds.at(bkg)).second)->at(p)->GetYminVis();
					ymax = ((backgrounds.at(bkg)).second)->at(p)->GetYmaxVis();
					xlabel = ((backgrounds.at(bkg)).second)->at(p)->GetXlabel().c_str();
					ylabel = ((backgrounds.at(bkg)).second)->at(p)->GetYlabel().c_str();
					zlabel = ((backgrounds.at(bkg)).second)->at(p)->GetZlabel().c_str();
					backgroundPlot->GetYaxis()->SetRangeUser(ymin,ymax);
				}
				if(!doCollisions){
					xmin = ((backgrounds.at(bkg)).second)->at(p)->GetXminVis();
					xmax = ((backgrounds.at(bkg)).second)->at(p)->GetXmaxVis();
					xlabel = ((backgrounds.at(bkg)).second)->at(p)->GetXlabel().c_str();
					ylabel = ((backgrounds.at(bkg)).second)->at(p)->GetYlabel().c_str();
				}

				TH1* backgroundPlotn = (TH1*)(backgroundPlot->Clone());

				if(p==0){ 
					cout << "\tNormalization for " << backgroundLabels.at(bkg) << ":" 
						 << string(15-backgroundLabels.at(bkg).length(),' ') 
						 << setprecision(4) << backgroundWrapper->GetNormalization() << endl;
				}//*/
			
				// Add background to event count
				string histoName = string(backgroundPlot->GetName());
				if((histoName.compare(plotNameToCount)==0) && (histoName.length() == plotNameToCount.length())){
					eventCounter->AddBackground(backgroundLabels.at(bkg), (TH1F*)backgroundPlot);	
				}


				// Prepare for canvas plotting
				canvas->SetName(backgroundPlot->GetName());
				backgroundPlot->SetFillColor(backgroundColor.at(bkg));

				// Statistical errors on backgrounds
				if (errorh == NULL && (backgroundPlot->Integral() > 0) ){ errorh = (TH1*)backgroundPlot->Clone(); }
				else if( backgroundPlot->Integral() > 0 ){ errorh->Add(backgroundPlot); }


				if(isTH2F){
					objectSaver.SaveToFile(collisions, "TH2F", outputRoot, string("stamper/histos/"+backgroundLabels.at(bkg)));
				}else{
					objectSaver.SaveToFile(collisions, "TH1F", outputRoot, string("stamper/histos/"+backgroundLabels.at(bkg)));
				}
				legend->AddEntry(backgroundPlot, (backgrounds.at(bkg).first).c_str(), "f");


			}


			// QCD plot
			if (doQCD){
				TH1* QCD 		= BuildQCD(p);


				doLogx = QCDHistos->at(p)->DoLogx();
				doLogy = QCDHistos->at(p)->DoLogy();
				if(QCDHistos->at(p)->IsTH2F()){ doLogz = QCDHistos->at(p)->DoLogz(); }

				centerLabels = QCDHistos->at(p)->CenterLabels();

				xlabel = QCDHistos->at(p)->GetXlabel().c_str();
				ylabel = QCDHistos->at(p)->GetYlabel().c_str();
				if( QCDHistos->at(p)->IsTH2F()){ zlabel = QCDHistos->at(p)->GetZlabel().c_str(); }

				QCD->GetXaxis()->SetTitle(xlabel.c_str());
				QCD->GetXaxis()->CenterLabels(centerLabels);
				QCD->GetYaxis()->SetTitle(ylabel.c_str());
				if( QCDHistos->at(p)->IsTH2F()){ 
					isTH2F = true;
					QCD->GetZaxis()->SetTitle(zlabel.c_str());
					doLogz = QCDHistos->at(p)->DoLogz();
					ymin = QCDHistos->at(p)->GetYminVis();
					ymax = QCDHistos->at(p)->GetYmaxVis();
					QCD->GetYaxis()->SetRangeUser(ymin,ymax);
					QCD->GetZaxis()->SetTitle(zlabel.c_str());
				}



				// Add background to event count
				string histoName = string(QCD->GetName());
				if((histoName.compare(plotNameToCount)==0) && (histoName.length() == plotNameToCount.length())){
					eventCounter->AddBackground("QCD",(TH1F*)QCD);	
				}

				canvas->SetName(QCD->GetName());
				QCD->SetFillColor(kGreen-5);
				if (errorh == NULL && QCD->Integral() > 0 ){ errorh = (TH1*)QCD->Clone(); }
				else if( QCD->Integral() > 0 ){ errorh->Add(QCD); }
				if( (!isTH2F) && (QCD->Integral() > 0)){
					canvasEmpty = false; 
				}

				if(isTH2F){
					objectSaver.SaveToFile(collisions, "TH2F", outputRoot, "stamper/histos/QCD");
				}else{
					objectSaver.SaveToFile(collisions, "TH1F", outputRoot, "stamper/histos/QCD");
				}
				legend->AddEntry(QCD, QCDLabel.c_str(), "f");
			}


		}




		if (doSignals && (!isTH2F)){

			// Loop over all signals
			for ( unsigned int signal = 0; signal < signals.size(); signal++){
				if(outputFilename.length() < 1){ outputFilename = ((signals.at(signal)).second)->at(p)->GetOutputFilename(); }
				TH1F* signalPlot = new TH1F(*((TH1F*)(((signals.at(signal)).second)->at(p)->GetHisto()))); 
				HistoWrapper* signalWrapper = ((signals.at(signal)).second)->at(p);
				doLogx = ((signals.at(signal)).second)->at(p)->DoLogx();
				doLogy = ((signals.at(signal)).second)->at(p)->DoLogy();
				if((signals.at(signal).second)->at(p)->IsTH2F()){ doLogz = (signals.at(signal).second)->at(p)->DoLogz(); }
				centerLabels = ((signals.at(signal)).second)->at(p)->CenterLabels();
				signalPlot->GetXaxis()->SetRangeUser(((signals.at(signal)).second)->at(p)->GetXminVis(),((signals.at(signal)).second)->at(p)->GetXmaxVis());
				signalPlot->GetXaxis()->SetTitle(((signals.at(signal)).second)->at(p)->GetXlabel().c_str());
				signalPlot->GetXaxis()->CenterLabels(centerLabels);
				signalPlot->GetYaxis()->SetTitle(((signals.at(signal)).second)->at(p)->GetYlabel().c_str());
				if((signals.at(signal).second)->at(p)->IsTH2F()){
					signalPlot->GetYaxis()->SetTitle(((signals.at(signal)).second)->at(p)->GetZlabel().c_str());
				}


				TH1* signalPlotn = (TH1*)(signalPlot->Clone());
				if(signalPlot->Integral() > 0){ canvasEmpty = false; }

				// Add signal to event count
				string histoName = string(signalPlot->GetName());
				if((histoName.compare(plotNameToCount)==0) && (histoName.length() == plotNameToCount.length())){
					eventCounter->AddSignal(signalLabels.at(signal),(TH1F*)signalPlot);	
				}

				// Prepare for canvas plotting
				canvas->SetName(signalPlot->GetName());
				signalPlot->SetFillStyle(0);
				signalPlot->SetLineWidth(3);
				signalPlot->SetLineColor(signalColor.at(signal));

				objectSaver.SaveToFile(collisions, "TH1F", outputRoot, string("stamper/histos/" + signalLabels.at(signal)));
				legend->AddEntry(signalPlot, (signals.at(signal).first).c_str(), "l");
				signalHistos.push_back(signalPlot);
			}
		}

		// Extra plot info
		float xPlotInfo		= 1.00;
		float yPlotInfo		= 0.95;

		float dxPlotInfo	= 0.40; float dyPlotInfo	= 0.07; 
		TPaveText *plotInfo = new TPaveText(xPlotInfo-dxPlotInfo, yPlotInfo-dyPlotInfo, xPlotInfo, yPlotInfo, "brNDC");
		plotInfo->SetBorderSize(0);
		plotInfo->SetLineColor(0);
		plotInfo->SetFillColor(kWhite);
		stringstream lumiSS;
		lumiSS.str("");
		lumiSS << "#tau_{h}#tau_{h} channel " << endl;
		if( effectiveLumi>0 ){lumiSS << "(" << GetNiceLumi(effectiveLumi,units) << ")"; }
		plotInfo->AddText((lumiSS.str()).c_str());
		plotInfo->AddText("CMS Preliminary");
		plotInfo->SetFillStyle(0);

	}

	cout << string(lastSavedPlotName.length(),'\b') << string(lastSavedPlotName.length(),' ') << endl; 

	return eventCounter;
}

// Save canvas
void PlotStamper::SaveCanvas(TCanvas* canvas, string dir, string filename){

	if(lastSavedPlotName.length()>0){
		cout << string(lastSavedPlotName.length(),'\b'); cout.flush(); 
		cout << string(lastSavedPlotName.length(),' '); cout.flush();
		cout << string(lastSavedPlotName.length(),'\b'); cout.flush(); 
	}
	cout << filename; cout.flush();

	// Create output dir if it doesn't exists already
	TString sysCommand = "if [ ! -d " + dir + " ]; then mkdir -p " + dir + "; fi";
	if(gSystem->Exec(sysCommand) > 0){ cout << ">>> ERROR: problem creating dir for plots " << dir << endl; exit(1); }// exit(0);

	// Loop over all file format extensions choosen and save canvas
	vector<string> extension; extension.push_back(".png");
	for( unsigned int ext = 0; ext < extension.size(); ext++){
		canvas->SaveAs( (dir + filename + extension.at(ext)).c_str() );
	}

	lastSavedPlotName = filename;

}

string PlotStamper::GetNiceLumi(float iLumi, string iUnits){
	string result;

	map<string,int> suf2mag; suf2mag["mb"] = 12; suf2mag["ub"] = 9; suf2mag["ub"] = 6; suf2mag["nb"] = 3; suf2mag["pb"] = 0; suf2mag["fb"] = -3; suf2mag["ab"] = -6;
	map<int,string> mag2suf; mag2suf[12] = "mb"; mag2suf[9] = "ub"; mag2suf[6] = "ub"; mag2suf[3] = "nb"; mag2suf[0] = "pb"; mag2suf[-3] = "fb"; mag2suf[-6] = "ab";

	int lumiMag = 3*((int)(((int)log10(iLumi)) / 3));
	int unitsMag = suf2mag[iUnits];
	int targetMag = unitsMag - lumiMag;

	float targetLumi = ((iLumi/(double)pow(10,lumiMag)) + 0.05)*10;
	targetLumi = ((int)targetLumi)/(double)10.0;

	stringstream ssout; ssout.str("");	
	if( ((int)(targetLumi*10)) == (((int)targetLumi)*10) ){ ssout << ((int)targetLumi) << ".0"; } 
	else{ ssout << targetLumi; }
	ssout << "/" << mag2suf[targetMag];
	result = ssout.str();

	return result;
}

bool PlotStamper::IsTH1F(TH1* iHisto){
	string className = string(iHisto->ClassName());
	return (className.compare("TH1F")==0);
}

bool PlotStamper::IsTH2F(TH1* iHisto){
	string className = string(iHisto->ClassName());
	return (className.compare("TH2F")==0);
}
