
#define TObjectSaver_cxx
#include "TObjectSaver.h"

using namespace std;



// Default constructor
TObjectSaver::TObjectSaver(){

}

void TObjectSaver::SaveToFile(TH1* iHisto, string iClass, string iFile, string iDir){
	if(iDir.substr(iDir.length()).compare("/") == 0){
		iDir = iDir.substr(0,iDir.length()-1);
	}

	TFile* file = new TFile(iFile.c_str(),"UPDATE");
	string newdir;

	// Create new directory if it doesn't exist
	TDirectory* dir;
	size_t start	= 0;
	size_t end		= 0;
	string current	= "";
	while(end < iDir.length()){
		end = iDir.find("/", start);	
		newdir = iDir.substr(start, end-start);
		current = current + newdir;
		if(file->GetDirectory(current.c_str())){
			dir = file->GetDirectory(current.c_str());
		}else{
			if(current.find("/") < current.length()){
				dir = dir->mkdir(newdir.c_str());	
			}else{
				dir = file->mkdir(newdir.c_str());	
			}
		}
		dir->cd();
		current = current + "/";
		start = end+1;	
	}

	if(iClass.compare("TH1F")==0){
		TH1F* cloned = new TH1F(*((TH1F*)iHisto));
	}else if(iClass.compare("TH2F")==0){
		TH2F* cloned = new TH2F(*((TH2F*)iHisto));
	}else{
		return;
	}
	file->Write();
	file->Close();

}


void TObjectSaver::OpenFile(TFile* file, string iFile, string iDir){
	if(iDir.substr(iDir.length()).compare("/") == 0){
		iDir = iDir.substr(0,iDir.length()-1);
	}

	file = new TFile(iFile.c_str(),"UPDATE");
	string newdir;

	// Create new directory if it doesn't exist
	TDirectory* dir;
	size_t start	= 0;
	size_t end		= 0;

}

