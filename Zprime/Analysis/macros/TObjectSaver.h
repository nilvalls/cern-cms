
#ifndef TObjectSaver_h
#define TObjectSaver_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <math.h>
#include <map>
#include <vector>
#include <sstream>
#include <iostream>
#include <utility>


using namespace std;

class TObjectSaver {
	public :

		// Default constructor
		TObjectSaver();

		void SaveToFile(TH1*, string, string, string);
		void OpenFile(TFile*, string, string);
		void CloseFile(TFile*);

};

#endif
