//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Jun  7 03:28:02 2011 by ROOT version 5.27/06b
// from TTree HMTTree/TauTauTree
// found on file: ditau_nTuple.root
//////////////////////////////////////////////////////////

#ifndef TauTauAnalyzer_h
#define TauTauAnalyzer_h

#include <TROOT.h>
#include <TSystem.h>
#include <TLatex.h>
#include <TMath.h>
#include <TChain.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TPaveText.h>
#include <TGraphAsymmErrors.h>
#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <limits.h>
#include <map>
#include <vector>
#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <utility>
#include "configParser/config.h"
#include "TriggerEfficiency.h"
#include "PUcorrector.h"
#include "ETauMatcher.h"
#include "HistoWrapper.h"
#include "EfficiencyCounter.h"
#include "HistoIntegrator.h"
#include "CommonDefs.h"
#include "TRandom.h"


using namespace std;
using namespace Common;

class TauTauAnalyzer {
	public :
		TChain*          	fChain;   //!pointer to the analyzed TTree or TChain
		Int_t           	fCurrent; //!current Tree number in a TChain
		TriggerEfficiency*	tauTrigger;
		TriggerEfficiency*	metScaler;
		PUcorrector*		puCorrector;
		ETauMatcher*		etauMatcher;
		EfficiencyCounter*	effCounter;
		float				effectiveLumi;
		long long			numEvInPAT;
		long long			numEvInDS;
		double				numEvAnalyzed;
		double				numEvInNtuple;
		double				numEvPassing;
		double				numEvPassingWithTrigger;
		double				numEvPassingWithPURW;
		float				otherSF;
		float				allMCscaleFactor;
		float				OS2LS;
		float 				crossSection;
		float 				branchingRatio;
		int 				maxEvents;
		bool				correctPU;
		map<string, HistoWrapper*>*   histoCollection;
		HistoIntegrator*	histoIntegrator;
		bool				messages;
		Config*				histoConfig;
		bool				reverseChargeProduct;
		int					reportRate;
		map<string,int>		numberOfPairsPassing;
		map<string,double>	numberOfEventsPassing;
		map<string,double>	cutIncrease;
		vector<string>		cutsByOrder;
		vector<cutResult*>	cutsByOrderExtra;
		string				topology;
		string				passingEventsFile;
		string				flags;
		vector<string>		selectedEvents;
		string				cutsToApply;
		string				outputCuts;
		string				outputFlags;
		ofstream 			fevents;
		float 				ProWjetsMt;
		float 				ProWjetsPt;
		TPaveText*			listOfCutsPave;
		int					numberOfCuts;
		bool				isMC;
		bool				isSignal;
		bool				isQCD;
		float				normalization;
		float				calcNormalization;
		double				puCheck1;
		double				puCheck2;

		// Cut switches and thresholds;
		bool CutOn_RunNumber;					float Min_RunNumber;					float Max_RunNumber;
		bool CutOn_LumiSection;					float Min_LumiSection;					float Max_LumiSection;
		bool CutOn_EventNumber;					float Min_EventNumber;					float Max_EventNumber;
		bool CutOn_LL_MatchesGenHadronic;		float Min_LL_MatchesGenHadronic;		float Max_LL_MatchesGenHadronic;
		bool CutOn_SL_MatchesGenHadronic;		float Min_SL_MatchesGenHadronic;		float Max_SL_MatchesGenHadronic;
		bool CutOn_GenMatching;					float Min_GenMatching;					float Max_GenMatching;
		bool CutOn_InvariantMass;				float Min_InvariantMass;				float Max_InvariantMass;
		bool CutOn_VisibleMass;					float Min_VisibleMass;					float Max_VisibleMass;
		bool CutOn_LL_pT;						float Min_LL_pT;						float Max_LL_pT;
		bool CutOn_SL_pT;						float Min_SL_pT;						float Max_SL_pT;
		bool CutOn_LL_Eta;						float Min_LL_Eta;						float Max_LL_Eta;
		bool CutOn_SL_Eta;						float Min_SL_Eta;						float Max_SL_Eta;
		bool CutOn_LL_InCracks;					float Min_LL_InCracks;					float Max_LL_InCracks;
		bool CutOn_SL_InCracks;					float Min_SL_InCracks;					float Max_SL_InCracks;
		bool CutOn_DeltaR;						float Min_DeltaR;						float Max_DeltaR;
		bool CutOn_LL_LTpT;						float Min_LL_LTpT;						float Max_LL_LTpT;
		bool CutOn_SL_LTpT;						float Min_SL_LTpT;						float Max_SL_LTpT;
		bool CutOn_LL_LTHits;					float Min_LL_LTHits;					float Max_LL_LTHits;
		bool CutOn_SL_LTHits;					float Min_SL_LTHits;					float Max_SL_LTHits;
		bool CutOn_LL_H3x3overP;				float Min_LL_H3x3overP;					float Max_LL_H3x3overP;
		bool CutOn_SL_H3x3overP;				float Min_SL_H3x3overP;					float Max_SL_H3x3overP;
		bool CutOn_LL_AgainstTightElectron;		float Min_LL_AgainstTightElectron;		float Max_LL_AgainstTightElectron;
		bool CutOn_LL_AgainstMediumElectron;	float Min_LL_AgainstMediumElectron;		float Max_LL_AgainstMediumElectron;
		bool CutOn_LL_AgainstLooseElectron;		float Min_LL_AgainstLooseElectron;		float Max_LL_AgainstLooseElectron;
		bool CutOn_SL_AgainstTightElectron;		float Min_SL_AgainstTightElectron;		float Max_SL_AgainstTightElectron;
		bool CutOn_SL_AgainstMediumElectron;	float Min_SL_AgainstMediumElectron;		float Max_SL_AgainstMediumElectron;
		bool CutOn_SL_AgainstLooseElectron;		float Min_SL_AgainstLooseElectron;		float Max_SL_AgainstLooseElectron;
		bool CutOn_LL_AgainstTightMuon;			float Min_LL_AgainstTightMuon;			float Max_LL_AgainstTightMuon;
		bool CutOn_LL_AgainstMediumMuon;		float Min_LL_AgainstMediumMuon;			float Max_LL_AgainstMediumMuon;
		bool CutOn_LL_AgainstLooseMuon;			float Min_LL_AgainstLooseMuon;			float Max_LL_AgainstLooseMuon;
		bool CutOn_SL_AgainstMediumMuon;		float Min_SL_AgainstMediumMuon;			float Max_SL_AgainstMediumMuon;
		bool CutOn_SL_AgainstTightMuon;			float Min_SL_AgainstTightMuon;			float Max_SL_AgainstTightMuon;
		bool CutOn_SL_AgainstLooseMuon;			float Min_SL_AgainstLooseMuon;			float Max_SL_AgainstLooseMuon;
		bool CutOn_LL_TightIso;					float Min_LL_TightIso;					float Max_LL_TightIso;
		bool CutOn_LL_MediumIso;				float Min_LL_MediumIso;					float Max_LL_MediumIso;
		bool CutOn_LL_LooseIso;					float Min_LL_LooseIso;					float Max_LL_LooseIso;
		bool CutOn_LL_VLooseIso;				float Min_LL_VLooseIso;					float Max_LL_VLooseIso;
		bool CutOn_SL_TightIso;					float Min_SL_TightIso;					float Max_SL_TightIso;
		bool CutOn_SL_MediumIso;				float Min_SL_MediumIso;					float Max_SL_MediumIso;
		bool CutOn_SL_LooseIso;					float Min_SL_LooseIso;					float Max_SL_LooseIso;
		bool CutOn_SL_VLooseIso;				float Min_SL_VLooseIso;					float Max_SL_VLooseIso;
		bool CutOn_LL_DecayModeFinding;			float Min_LL_DecayModeFinding;			float Max_LL_DecayModeFinding;
		bool CutOn_SL_DecayModeFinding;			float Min_SL_DecayModeFinding;			float Max_SL_DecayModeFinding;
		bool CutOn_LL_DecayMode;				float Min_LL_DecayMode;					float Max_LL_DecayMode;
		bool CutOn_SL_DecayMode;				float Min_SL_DecayMode;					float Max_SL_DecayMode;
		bool CutOn_LL_NumProngs;				float Min_LL_NumProngs;					float Max_LL_NumProngs;
		bool CutOn_SL_NumProngs;				float Min_SL_NumProngs;					float Max_SL_NumProngs;
		bool CutOn_ChargeProduct;				float Min_ChargeProduct;				float Max_ChargeProduct;
		bool CutOn_CosDeltaPhi;					float Min_CosDeltaPhi;					float Max_CosDeltaPhi;
		bool CutOn_MET;							float Min_MET;							float Max_MET;
		bool CutOn_Zeta;						float Min_Zeta;							float Max_Zeta;
		bool CutOn_Btags;						float Min_Btags;						float Max_Btags;
		//NEWCUT


		// Declaration of leaf types
		Double_t        runNumber;
		Double_t        eventNumber;
		Double_t        lumiBlock;
		Int_t	numInteractionsBXm1;
		Int_t	numInteractionsBX0;
		Int_t	numInteractionsBXp1;
		vector<double>  *PDFWeights;
		Double_t        ISRGluonWeight;
		Double_t        ISRGammaWeight;
		Double_t        FSRWeight;

		vector<bool>*       Tau1_ParentTauMatched;
		vector<bool>*       Tau2_ParentTauMatched;
		vector<bool>*       Tau1_MatchesGenHadronic;
		vector<bool>*       Tau2_MatchesGenHadronic;
		vector<bool>*       Tau1_ZtauMatched;
		vector<bool>*       Tau2_ZtauMatched;
		vector<bool>*       Tau1_ZeMatched;
		vector<bool>*       Tau2_ZeMatched;

		vector<float>*	GenMET;
		vector<float>*	GenMETphi;
		vector<float>*	Tau1GenParentMass;
		vector<float>*	Tau2GenParentMass;
		vector<float>*	TauTauPlusMetGenMass;
		vector<float>*	Tau1LTIpVtZ;
		vector<float>*	Tau2LTIpVtZ;


		vector<float>   *TauTriggerEfficiency;
		vector<float>   *Tau1GenPt;
		vector<float>   *Tau2GenPt;
		vector<float>   *Tau1GenE;
		vector<float>   *Tau2GenE;
		vector<float>   *Tau1GenEta;
		vector<float>   *Tau2GenEta;
		vector<float>   *Tau1GenPhi;
		vector<float>   *Tau2GenPhi;
		vector<bool>    *Tau1Matched;
		vector<bool>    *Tau2Matched;
		vector<int>     *Tau1MotherId;
		vector<int>     *Tau2MotherId;
		vector<int>     *Tau1PdgId;
		vector<int>     *Tau2PdgId;
		vector<int>     *Tau1MotherPdgId;
		vector<int>     *Tau2MotherPdgId;
		vector<int>     *NumPV;
		vector<float>   *Tau1E;
		vector<double>  *Tau2E;
		vector<float>   *Tau1Et;
		vector<float>   *Tau2Et;
		vector<float>   *Tau1Pt;
		vector<float>   *Tau2Pt;
		vector<float>   *Tau1LTPt;
		vector<float>   *Tau2LTPt;
		vector<int>     *Tau1Charge;
		vector<int>     *Tau2Charge;
		vector<float>   *Tau1Eta;
		vector<float>   *Tau2Eta;
		vector<float>   *Tau1Phi;
		vector<float>   *Tau2Phi;
		vector<float>   *Tau1LTIpVtdxy;
		vector<float>   *Tau1LTIpVtdz;
		vector<float>   *Tau1LTIpVtxError;
		vector<int>     *Tau1LTValidHits;
		vector<float>   *Tau1LTNormChiSqrd;
		vector<float>   *Tau2LTIpVtdxy;
		vector<float>   *Tau2LTIpVtdz;
		vector<float>   *Tau2LTIpVtxError;
		vector<int>     *Tau2LTValidHits;
		vector<float>   *Tau2LTNormChiSqrd;
		vector<int>     *Tau1NProngs;
		vector<int>     *Tau2NProngs;
		vector<float>   *Tau1EmFraction;
		vector<float>   *Tau2EmFraction;
		vector<float>   *Tau1HcalTotOverPLead;
		vector<float>   *Tau2HcalTotOverPLead;
		vector<float>   *Tau1HCalMaxOverPLead;
		vector<float>   *Tau2HCalMaxOverPLead;
		vector<float>   *Tau1HCal3x3OverPLead;
		vector<float>   *Tau2HCal3x3OverPLead;
		vector<bool>    *Tau1DiscAgainstElectron;
		vector<bool>    *Tau2DiscAgainstElectron;
		vector<bool>    *Tau1DiscAgainstMuon;
		vector<bool>    *Tau2DiscAgainstMuon;
		vector<int>     *Tau1IsInTheCracks;
		vector<int>     *Tau2IsInTheCracks;
		vector<int>		*Tau1DecayMode;
		vector<int>		*Tau2DecayMode;
		vector<float>   *Tau1SumPtIsoTracks;
		vector<float>   *Tau2SumPtIsoTracks;
		vector<float>   *Tau1SumPtIsoGammas;
		vector<float>   *Tau2SumPtIsoGammas;
		vector<float>   *MET;
		vector<float>	*METphi;
		vector<float>	*METl1l2l3corr;
		vector<float>	*METl1l2l3corrPhi;
		vector<float>	*METsignificance;
		vector<float>   *TauTauVisibleMass;
		vector<float>   *TauTauVisPlusMetMass;
		vector<float>   *TauTauCollinearMetMass;
		vector<float>   *TauTauCosDPhi;
		vector<float>   *TauTauDeltaR;
		vector<float>   *TauTauPZeta;
		vector<float>   *TauTauPZetaVis;
		vector<float>   *Tau1MetCosDphi;
		vector<float>   *Tau2MetCosDphi;
		vector<double>  *Tau1MetMt;
		vector<double>  *Tau2MetMt;
		vector<float>   *nBtagsHiEffTrkCnt;
		vector<float>   *nBtagsHiPurityTrkCnt;
		vector<float>   *nBTagsHiEffSimpleSecVtx;
		vector<float>   *nBTagsHiPuritySimpleSecVtx;
		vector<float>   *nBTagsCombSecVtx;
		vector<float>   *jetSumEt;
		vector<float>   *jetMETSumEt;
		vector<int>     *nJets;
		vector<float>   *nExtraJets;
		
		vector<float>*      Tau1Tau2LeadingJetPtSum;
		vector<float>*      Tau1LeadingJetDeltaPhi;
		vector<float>*      Tau2LeadingJetDeltaPhi;

		vector<bool>    *Tau1hpsPFTauDiscriminationAgainstLooseElectron;
		vector<bool>    *Tau1hpsPFTauDiscriminationAgainstLooseMuon;
		vector<bool>    *Tau1hpsPFTauDiscriminationAgainstMediumElectron;
		vector<bool>    *Tau1hpsPFTauDiscriminationAgainstMediumMuon;
		vector<bool>    *Tau1hpsPFTauDiscriminationAgainstTightElectron;
		vector<bool>    *Tau1hpsPFTauDiscriminationAgainstTightMuon;
		vector<bool>    *Tau1hpsPFTauDiscriminationByDecayModeFinding;
		vector<bool>    *Tau1hpsPFTauDiscriminationByLooseIsolation;
		vector<bool>    *Tau1hpsPFTauDiscriminationByMediumIsolation;
		vector<bool>    *Tau1hpsPFTauDiscriminationByTightIsolation;
		vector<bool>    *Tau1hpsPFTauDiscriminationByVLooseIsolation;
		vector<bool>    *Tau2hpsPFTauDiscriminationAgainstLooseElectron;
		vector<bool>    *Tau2hpsPFTauDiscriminationAgainstLooseMuon;
		vector<bool>    *Tau2hpsPFTauDiscriminationAgainstMediumElectron;
		vector<bool>    *Tau2hpsPFTauDiscriminationAgainstMediumMuon;
		vector<bool>    *Tau2hpsPFTauDiscriminationAgainstTightElectron;
		vector<bool>    *Tau2hpsPFTauDiscriminationAgainstTightMuon;
		vector<bool>    *Tau2hpsPFTauDiscriminationByDecayModeFinding;
		vector<bool>    *Tau2hpsPFTauDiscriminationByLooseIsolation;
		vector<bool>    *Tau2hpsPFTauDiscriminationByMediumIsolation;
		vector<bool>    *Tau2hpsPFTauDiscriminationByTightIsolation;
		vector<bool>    *Tau2hpsPFTauDiscriminationByVLooseIsolation;

		vector<bool>	*Tau1hpsPFTauDiscriminationByVLooseIsolationDBSumPtCorr;
		vector<bool>	*Tau1hpsPFTauDiscriminationByLooseIsolationDBSumPtCorr;
		vector<bool>	*Tau1hpsPFTauDiscriminationByMediumIsolationDBSumPtCorr;
		vector<bool>	*Tau1hpsPFTauDiscriminationByTightIsolationDBSumPtCorr;
		vector<bool>	*Tau1hpsPFTauDiscriminationByVLooseCombinedIsolationDBSumPtCorr;
		vector<bool>	*Tau1hpsPFTauDiscriminationByLooseCombinedIsolationDBSumPtCorr;
		vector<bool>	*Tau1hpsPFTauDiscriminationByMediumCombinedIsolationDBSumPtCorr;
		vector<bool>	*Tau1hpsPFTauDiscriminationByTightCombinedIsolationDBSumPtCorr;
		vector<bool>	*Tau2hpsPFTauDiscriminationByVLooseIsolationDBSumPtCorr;
		vector<bool>	*Tau2hpsPFTauDiscriminationByLooseIsolationDBSumPtCorr;
		vector<bool>	*Tau2hpsPFTauDiscriminationByMediumIsolationDBSumPtCorr;
		vector<bool>	*Tau2hpsPFTauDiscriminationByTightIsolationDBSumPtCorr;
		vector<bool>	*Tau2hpsPFTauDiscriminationByVLooseCombinedIsolationDBSumPtCorr;
		vector<bool>	*Tau2hpsPFTauDiscriminationByLooseCombinedIsolationDBSumPtCorr;
		vector<bool>	*Tau2hpsPFTauDiscriminationByMediumCombinedIsolationDBSumPtCorr;
		vector<bool>	*Tau2hpsPFTauDiscriminationByTightCombinedIsolationDBSumPtCorr;

		vector<float>	*Tau1hpsPFTauDiscriminationByRawCombinedIsolationDBSumPtCorr;
		vector<float>	*Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorr;
		vector<float>	*Tau1hpsPFTauDiscriminationByRawGammaIsolationDBSumPtCorr;
		vector<float>	*Tau2hpsPFTauDiscriminationByRawCombinedIsolationDBSumPtCorr;
		vector<float>	*Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorr;
		vector<float>	*Tau2hpsPFTauDiscriminationByRawGammaIsolationDBSumPtCorr;

		vector<float>	*Tau1MElecPt;
		vector<float>	*Tau1MElecEta;
		vector<float>	*Tau1MElecPhi;
		vector<float>	*Tau1MElecDeltaR;
		vector<int>		*Tau1MElecCharge;
		vector<float>	*Tau1MElecTrkSumPt;
		vector<float>	*Tau1MElecEcalRecHitSumEt;
		vector<float>	*Tau2MElecPt;
		vector<float>	*Tau2MElecEta;
		vector<float>	*Tau2MElecPhi;
		vector<float>	*Tau2MElecDeltaR;
		vector<int>		*Tau2MElecCharge; 
		vector<float>	*Tau2MElecTrkSumPt;
		vector<float>	*Tau2MElecEcalRecHitSumEt;

		vector<float>*   ElectronPt;
		vector<float>*   ElectronEta;
		vector<float>*   ElectronPhi;
		vector<float>*   ElectronCharge;
		vector<float>*   ElectronTrkSumPt;
		vector<float>*   ElectronEcalRecHitSumEt;
		vector<float>*   ElectronTrackRedChi2;
		vector<float>*   ElectronTrackNumMissingHitsIn;
		vector<float>*   ElectronTrackNumMissingHitsOut;
		vector<float>*   ElectronTrackNumHits;
		vector<float>*   ElectronTrackQuality;



		//*/




		//*/

		// Histos
		TH1F* hMass;

		TauTauAnalyzer(int, string, string, string, float, Config*);
		virtual void SetMaxEvents(int);
		virtual ~TauTauAnalyzer();
		virtual Int_t					Cut(Long64_t entry);
		virtual Int_t					GetEntry(Long64_t entry);
		virtual Long64_t				LoadTree(Long64_t entry);
		virtual void					Init();
		virtual void					Loop();
		virtual Bool_t					Notify();
		virtual TChain*					GetTChain(string, string);
		virtual void					Show(Long64_t entry = -1);
		virtual void					SetTriggerEfficiency(string, string);
		virtual void					SetTriggerEfficiency(string);
		virtual void					SetMETSF(string, string);
		virtual void					SetFlags(string);
		virtual void					SetSelectEvents(string);
		virtual bool					IsFlagThere(string);
		virtual void					LoadPuFile(string);
		virtual void					LoadTriggerConfig(string);;
		virtual float 					GetEffectiveLumi(string, string, float);
		virtual float 					GetEffectiveLumi(float);
		virtual vector<HistoWrapper*>*	GetPlots(string, string, bool, bool);
		virtual vector<HistoWrapper*>*	GetOSPlots(string, string, bool, bool, long long, long long, float iOtherSF=1.0, float iAllMCscaleFactor=1.0);
		virtual vector<HistoWrapper*>*	GetQCDPlots(string, string, bool, bool, long long, long long, float iOtherSF=1.0, float iAllMCscaleFactor=1.0);
		virtual void					SetOS2LS(float iOS2LS=1.0);
		virtual void					BookHistos();
		virtual void					FillEvents(int);
		virtual void					FillHistos(int);
		virtual bool					PassesCuts(unsigned int);
		virtual pair<float,float>		ExtractCutThresholds(string);
		virtual vector<cutResult*>*		GetCutResults();
		virtual void					UpPairsForCut(string);
		virtual void					UpPairsForCut(string, double);
		virtual void					RegisterCut(string, bool iPrint=true);
		virtual void					SetCutThresholds(string);
		virtual pair<string,string>		GetThresholdStrings(string);
		virtual string					GetCutStringWithThresholds(string, bool iPadding=true);
		virtual bool					ApplyThisCut(string,string);
		virtual void					SetCutsToApply(string);
		virtual void					SetReportRate(int);
		virtual void					SetPassingEventsOutput(string);
		virtual void					SetTopology(string, float, float iBranchingRatio=1.0);
		virtual void					ResetCounters();
		virtual void 					MakeNewEfficiencyCounters();
		virtual bool					OutOfRange(float, float, float);
		virtual void					SpacePad(stringstream&, int);
		virtual void					AddCutToList(string, float, float);
		virtual void					MakeCutsPave();
		virtual void					MakeFlagsPave();
		virtual	vector<string>*			ParseString(const string&, char);
		virtual bool					FoundHisto(string);
		virtual HistoWrapper*			GetHisto(string);
		virtual bool 					FindStringInFile(string, string);
		virtual bool					ProcessThisEvent();
		virtual string					GetRunLSevent();
		virtual void					PrintEfficienciesHTML(string);
		virtual string					Replace(string, string, string);
		virtual bool					StringContains(string, string);
		virtual unsigned int			ParseCSV(const string&, char, float[]);


};

#endif

#ifdef TauTauAnalyzer_cxx

void TauTauAnalyzer::SpacePad(stringstream& iSS, int iNum){
	for (int i = 0; i < iNum; i++){ iSS << " "; }
}

void TauTauAnalyzer::Init(){
	// The Init() function is called when the selector needs to initialize
	// a new tree or chain. Typically here the branch addresses and branch
	// pointers of the tree will be set.
	// It is normally not necessary to make changes to the generated
	// code, but the routine can be extended by the user if needed.
	// Init() will be called many times when running on PROOF
	// (once per file to be processed).

	runNumber = 0;
	eventNumber = 0;
	lumiBlock = 0;
	numInteractionsBXm1 = 0;
	numInteractionsBX0 = 0;
	numInteractionsBXp1 = 0;

	// Set object pointer
	PDFWeights = 0;
	TauTriggerEfficiency = 0;
	Tau1_ParentTauMatched = 0;
	Tau2_ParentTauMatched = 0;
	Tau1_MatchesGenHadronic = 0;
	Tau2_MatchesGenHadronic = 0;
	Tau1_ZtauMatched = 0;
	Tau2_ZtauMatched = 0;
	Tau1_ZeMatched = 0;
	Tau2_ZeMatched = 0;

	GenMET = 0;
	GenMETphi = 0;
	Tau1GenParentMass = 0;
	Tau2GenParentMass = 0;
	TauTauPlusMetGenMass = 0;
	Tau1LTIpVtZ = 0;
	Tau2LTIpVtZ = 0;

	Tau1GenPt = 0;
	Tau2GenPt = 0;
	Tau1GenE = 0;
	Tau2GenE = 0;
	Tau1GenEta = 0;
	Tau2GenEta = 0;
	Tau1GenPhi = 0;
	Tau2GenPhi = 0;
	Tau1Matched = 0;
	Tau2Matched = 0;
	Tau1MotherId = 0;
	Tau2MotherId = 0;
	Tau1PdgId = 0;
	Tau2PdgId = 0;
	Tau1MotherPdgId = 0;
	Tau2MotherPdgId = 0;
	NumPV = 0;
	Tau1E = 0;
	Tau2E = 0;
	Tau1Et = 0;
	Tau2Et = 0;
	Tau1Pt = 0;
	Tau2Pt = 0;
	Tau1LTPt = 0;
	Tau2LTPt = 0;
	Tau1Charge = 0;
	Tau2Charge = 0;
	Tau1Eta = 0;
	Tau2Eta = 0;
	Tau1Phi = 0;
	Tau2Phi = 0;
	Tau1LTIpVtdxy = 0;
	Tau1LTIpVtdz = 0;
	Tau1LTIpVtxError = 0;
	Tau1LTValidHits = 0;
	Tau1LTNormChiSqrd = 0;
	Tau2LTIpVtdxy = 0;
	Tau2LTIpVtdz = 0;
	Tau2LTIpVtxError = 0;
	Tau2LTValidHits = 0;
	Tau2LTNormChiSqrd = 0;
	Tau1NProngs = 0;
	Tau2NProngs = 0;
	Tau1EmFraction = 0;
	Tau2EmFraction = 0;
	Tau1HcalTotOverPLead = 0;
	Tau2HcalTotOverPLead = 0;
	Tau1HCalMaxOverPLead = 0;
	Tau2HCalMaxOverPLead = 0;
	Tau1HCal3x3OverPLead = 0;
	Tau2HCal3x3OverPLead = 0;
	Tau1DiscAgainstElectron = 0;
	Tau2DiscAgainstElectron = 0;
	Tau1DiscAgainstMuon = 0;
	Tau2DiscAgainstMuon = 0;
	Tau1IsInTheCracks = 0;
	Tau2IsInTheCracks = 0;
	Tau1DecayMode = 0;
	Tau2DecayMode = 0;
	Tau1SumPtIsoTracks = 0;
	Tau2SumPtIsoTracks = 0;
	Tau1SumPtIsoGammas = 0;
	Tau2SumPtIsoGammas = 0;
	MET = 0;
	METphi = 0;
	METl1l2l3corr = 0;
	METl1l2l3corrPhi = 0;
	METsignificance = 0;
	TauTauVisibleMass = 0;
	TauTauVisPlusMetMass = 0;
	TauTauCollinearMetMass = 0;
	TauTauCosDPhi = 0;
	TauTauDeltaR = 0;
	TauTauPZeta = 0;
	TauTauPZetaVis = 0;
	Tau1MetCosDphi = 0;
	Tau2MetCosDphi = 0;
	Tau1MetMt = 0;
	Tau2MetMt = 0;
	nBtagsHiEffTrkCnt = 0;
	nBtagsHiPurityTrkCnt = 0;
	nBTagsHiEffSimpleSecVtx = 0;
	nBTagsHiPuritySimpleSecVtx = 0;
	nBTagsCombSecVtx = 0;
	jetSumEt = 0;
	jetMETSumEt = 0;
	nJets = 0;
	nExtraJets = 0;


	Tau1Tau2LeadingJetPtSum = 0;
	Tau1LeadingJetDeltaPhi = 0;
	Tau2LeadingJetDeltaPhi = 0;


	Tau1hpsPFTauDiscriminationAgainstLooseElectron = 0;
	Tau1hpsPFTauDiscriminationAgainstLooseMuon = 0;
	Tau1hpsPFTauDiscriminationAgainstMediumElectron = 0;
	Tau1hpsPFTauDiscriminationAgainstMediumMuon = 0;
	Tau1hpsPFTauDiscriminationAgainstTightElectron = 0;
	Tau1hpsPFTauDiscriminationAgainstTightMuon = 0;
	Tau1hpsPFTauDiscriminationByDecayModeFinding = 0;
	Tau1hpsPFTauDiscriminationByLooseIsolation = 0;
	Tau1hpsPFTauDiscriminationByMediumIsolation = 0;
	Tau1hpsPFTauDiscriminationByTightIsolation = 0;
	Tau1hpsPFTauDiscriminationByVLooseIsolation = 0;
	Tau2hpsPFTauDiscriminationAgainstLooseElectron = 0;
	Tau2hpsPFTauDiscriminationAgainstLooseMuon = 0;
	Tau2hpsPFTauDiscriminationAgainstMediumElectron = 0;
	Tau2hpsPFTauDiscriminationAgainstMediumMuon = 0;
	Tau2hpsPFTauDiscriminationAgainstTightElectron = 0;
	Tau2hpsPFTauDiscriminationAgainstTightMuon = 0;
	Tau2hpsPFTauDiscriminationByDecayModeFinding = 0;
	Tau2hpsPFTauDiscriminationByLooseIsolation = 0;
	Tau2hpsPFTauDiscriminationByMediumIsolation = 0;
	Tau2hpsPFTauDiscriminationByTightIsolation = 0;
	Tau2hpsPFTauDiscriminationByVLooseIsolation = 0;

	Tau1hpsPFTauDiscriminationByVLooseIsolationDBSumPtCorr = 0;
	Tau1hpsPFTauDiscriminationByLooseIsolationDBSumPtCorr = 0;
	Tau1hpsPFTauDiscriminationByMediumIsolationDBSumPtCorr = 0;
	Tau1hpsPFTauDiscriminationByTightIsolationDBSumPtCorr = 0;
	Tau1hpsPFTauDiscriminationByVLooseCombinedIsolationDBSumPtCorr = 0;
	Tau1hpsPFTauDiscriminationByLooseCombinedIsolationDBSumPtCorr = 0;
	Tau1hpsPFTauDiscriminationByMediumCombinedIsolationDBSumPtCorr = 0;
	Tau1hpsPFTauDiscriminationByTightCombinedIsolationDBSumPtCorr = 0;
	Tau2hpsPFTauDiscriminationByVLooseIsolationDBSumPtCorr = 0;
	Tau2hpsPFTauDiscriminationByLooseIsolationDBSumPtCorr = 0;
	Tau2hpsPFTauDiscriminationByMediumIsolationDBSumPtCorr = 0;
	Tau2hpsPFTauDiscriminationByTightIsolationDBSumPtCorr = 0;
	Tau2hpsPFTauDiscriminationByVLooseCombinedIsolationDBSumPtCorr = 0;
	Tau2hpsPFTauDiscriminationByLooseCombinedIsolationDBSumPtCorr = 0;
	Tau2hpsPFTauDiscriminationByMediumCombinedIsolationDBSumPtCorr = 0;
	Tau2hpsPFTauDiscriminationByTightCombinedIsolationDBSumPtCorr = 0;
	Tau1hpsPFTauDiscriminationByRawCombinedIsolationDBSumPtCorr = 0;
	Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorr = 0;
	Tau1hpsPFTauDiscriminationByRawGammaIsolationDBSumPtCorr = 0;
	Tau2hpsPFTauDiscriminationByRawCombinedIsolationDBSumPtCorr = 0;
	Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorr = 0;
	Tau2hpsPFTauDiscriminationByRawGammaIsolationDBSumPtCorr = 0;


	Tau1MElecPt = 0;
	Tau1MElecEta = 0;
	Tau1MElecPhi = 0;
	Tau1MElecDeltaR = 0;
	Tau1MElecCharge = 0;
	Tau1MElecTrkSumPt = 0;
	Tau1MElecEcalRecHitSumEt = 0;
	Tau2MElecPt = 0;
	Tau2MElecEta = 0;
	Tau2MElecPhi = 0;
	Tau2MElecDeltaR = 0;
	Tau2MElecCharge = 0;
	Tau2MElecTrkSumPt = 0;
	Tau2MElecEcalRecHitSumEt = 0;

	ElectronPt = 0;
	ElectronEta = 0;
	ElectronPhi = 0;
	ElectronCharge = 0;
	ElectronTrkSumPt = 0;
	ElectronEcalRecHitSumEt = 0;
	ElectronTrackRedChi2 = 0;
	ElectronTrackNumMissingHitsIn = 0;
	ElectronTrackNumMissingHitsOut = 0;
	ElectronTrackNumHits = 0;
	ElectronTrackQuality = 0;


	//*/

	// Set branch addresses and branch pointers
	if (!fChain) return;
	//fChain->Add(tree);
	fCurrent = -1; 
	fChain->SetMakeClass(1);

	fChain->SetBranchAddress("runNumber", &runNumber);
	fChain->SetBranchAddress("eventNumber", &eventNumber);
	fChain->SetBranchAddress("lumiBlock", &lumiBlock);
	fChain->SetBranchAddress("numInteractionsBXm1", &numInteractionsBXm1);
	fChain->SetBranchAddress("numInteractionsBX0", &numInteractionsBX0);
	fChain->SetBranchAddress("numInteractionsBXp1", &numInteractionsBXp1);
	fChain->SetBranchAddress("PDFWeights", &PDFWeights);
	fChain->SetBranchAddress("ISRGluonWeight", &ISRGluonWeight);
	fChain->SetBranchAddress("ISRGammaWeight", &ISRGammaWeight);
	fChain->SetBranchAddress("FSRWeight", &FSRWeight);
	fChain->SetBranchAddress("Tau1_ParentTauMatched", &Tau1_ParentTauMatched);
	fChain->SetBranchAddress("Tau2_ParentTauMatched", &Tau2_ParentTauMatched);
	fChain->SetBranchAddress("Tau1_MatchesGenHadronic", &Tau1_MatchesGenHadronic);
	fChain->SetBranchAddress("Tau2_MatchesGenHadronic", &Tau2_MatchesGenHadronic);
	fChain->SetBranchAddress("Tau1_ZtauMatched", &Tau1_ZtauMatched);
	fChain->SetBranchAddress("Tau2_ZtauMatched", &Tau2_ZtauMatched);
	fChain->SetBranchAddress("Tau1_ZeMatched", &Tau1_ZeMatched);
	fChain->SetBranchAddress("Tau2_ZeMatched", &Tau2_ZeMatched);//*/

	fChain->SetBranchAddress("TauTriggerEfficiency", &TauTriggerEfficiency);
	fChain->SetBranchAddress("Tau1GenPt", &Tau1GenPt);
	fChain->SetBranchAddress("Tau2GenPt", &Tau2GenPt);
	fChain->SetBranchAddress("Tau1GenE", &Tau1GenE);
	fChain->SetBranchAddress("Tau2GenE", &Tau2GenE);
	fChain->SetBranchAddress("Tau1GenEta", &Tau1GenEta);
	fChain->SetBranchAddress("Tau2GenEta", &Tau2GenEta);
	fChain->SetBranchAddress("Tau1GenPhi", &Tau1GenPhi);
	fChain->SetBranchAddress("Tau2GenPhi", &Tau2GenPhi);
	fChain->SetBranchAddress("Tau1Matched", &Tau1Matched);
	fChain->SetBranchAddress("Tau2Matched", &Tau2Matched);
	fChain->SetBranchAddress("Tau1MotherId", &Tau1MotherId);
	fChain->SetBranchAddress("Tau2MotherId", &Tau2MotherId);
	fChain->SetBranchAddress("Tau1PdgId", &Tau1PdgId);
	fChain->SetBranchAddress("Tau2PdgId", &Tau2PdgId);
	fChain->SetBranchAddress("Tau1MotherPdgId", &Tau1MotherPdgId);
	fChain->SetBranchAddress("Tau2MotherPdgId", &Tau2MotherPdgId);
	fChain->SetBranchAddress("NumPV", &NumPV);
	fChain->SetBranchAddress("Tau1MetMt", &Tau1MetMt);
	fChain->SetBranchAddress("Tau2MetMt", &Tau2MetMt);
	fChain->SetBranchAddress("Tau1E", &Tau1E);
	fChain->SetBranchAddress("Tau2E", &Tau2E);
	fChain->SetBranchAddress("Tau1Et", &Tau1Et);
	fChain->SetBranchAddress("Tau2Et", &Tau2Et);
	fChain->SetBranchAddress("Tau1Pt", &Tau1Pt);
	fChain->SetBranchAddress("Tau2Pt", &Tau2Pt);
	fChain->SetBranchAddress("Tau1LTPt", &Tau1LTPt);
	fChain->SetBranchAddress("Tau2LTPt", &Tau2LTPt);
	fChain->SetBranchAddress("Tau1Charge", &Tau1Charge);
	fChain->SetBranchAddress("Tau2Charge", &Tau2Charge);
	fChain->SetBranchAddress("Tau1Eta", &Tau1Eta);
	fChain->SetBranchAddress("Tau2Eta", &Tau2Eta);
	fChain->SetBranchAddress("Tau1Phi", &Tau1Phi);
	fChain->SetBranchAddress("Tau2Phi", &Tau2Phi);
	fChain->SetBranchAddress("Tau1LTIpVtdxy", &Tau1LTIpVtdxy);
	fChain->SetBranchAddress("Tau1LTIpVtdz", &Tau1LTIpVtdz);
	fChain->SetBranchAddress("Tau1LTIpVtxError", &Tau1LTIpVtxError);
	fChain->SetBranchAddress("Tau1LTValidHits", &Tau1LTValidHits);
	fChain->SetBranchAddress("Tau1LTNormChiSqrd", &Tau1LTNormChiSqrd);
	fChain->SetBranchAddress("Tau2LTIpVtdxy", &Tau2LTIpVtdxy);
	fChain->SetBranchAddress("Tau2LTIpVtdz", &Tau2LTIpVtdz);
	fChain->SetBranchAddress("Tau2LTIpVtxError", &Tau2LTIpVtxError);
	fChain->SetBranchAddress("Tau2LTValidHits", &Tau2LTValidHits);
	fChain->SetBranchAddress("Tau2LTNormChiSqrd", &Tau2LTNormChiSqrd);
	fChain->SetBranchAddress("Tau1NProngs", &Tau1NProngs);
	fChain->SetBranchAddress("Tau2NProngs", &Tau2NProngs);
	fChain->SetBranchAddress("Tau1EmFraction", &Tau1EmFraction);
	fChain->SetBranchAddress("Tau2EmFraction", &Tau2EmFraction);
	fChain->SetBranchAddress("Tau1HcalTotOverPLead", &Tau1HcalTotOverPLead);
	fChain->SetBranchAddress("Tau2HcalTotOverPLead", &Tau2HcalTotOverPLead);
	fChain->SetBranchAddress("Tau1HCalMaxOverPLead", &Tau1HCalMaxOverPLead);
	fChain->SetBranchAddress("Tau2HCalMaxOverPLead", &Tau2HCalMaxOverPLead);
	fChain->SetBranchAddress("Tau1HCal3x3OverPLead", &Tau1HCal3x3OverPLead);
	fChain->SetBranchAddress("Tau2HCal3x3OverPLead", &Tau2HCal3x3OverPLead);
	fChain->SetBranchAddress("Tau1DiscAgainstElectron", &Tau1DiscAgainstElectron);
	fChain->SetBranchAddress("Tau2DiscAgainstElectron", &Tau2DiscAgainstElectron);
	fChain->SetBranchAddress("Tau1DiscAgainstMuon", &Tau1DiscAgainstMuon);
	fChain->SetBranchAddress("Tau2DiscAgainstMuon", &Tau2DiscAgainstMuon);
	fChain->SetBranchAddress("Tau1IsInTheCracks", &Tau1IsInTheCracks);
	fChain->SetBranchAddress("Tau2IsInTheCracks", &Tau2IsInTheCracks);
	fChain->SetBranchAddress("Tau1DecayMode", &Tau1DecayMode);
	fChain->SetBranchAddress("Tau2DecayMode", &Tau2DecayMode);
	fChain->SetBranchAddress("MET", &MET);
/*	fChain->SetBranchAddress("Tau1SumPtIsoTracks", &Tau1SumPtIsoTracks);
	fChain->SetBranchAddress("Tau2SumPtIsoTracks", &Tau2SumPtIsoTracks);
	fChain->SetBranchAddress("Tau1SumPtIsoGammas", &Tau1SumPtIsoGammas);
	fChain->SetBranchAddress("Tau2SumPtIsoGammas", &Tau2SumPtIsoGammas); //*/
	fChain->SetBranchAddress("TauTauVisibleMass", &TauTauVisibleMass);
	fChain->SetBranchAddress("TauTauVisPlusMetMass", &TauTauVisPlusMetMass);
	fChain->SetBranchAddress("TauTauCollinearMetMass", &TauTauCollinearMetMass);
	fChain->SetBranchAddress("TauTauCosDPhi", &TauTauCosDPhi);
	fChain->SetBranchAddress("TauTauDeltaR", &TauTauDeltaR);
	fChain->SetBranchAddress("TauTauPZeta", &TauTauPZeta);
	fChain->SetBranchAddress("TauTauPZetaVis", &TauTauPZetaVis);
	fChain->SetBranchAddress("Tau1MetCosDphi", &Tau1MetCosDphi);
	fChain->SetBranchAddress("Tau2MetCosDphi", &Tau2MetCosDphi);
	fChain->SetBranchAddress("nBtagsHiEffTrkCnt", &nBtagsHiEffTrkCnt);
	fChain->SetBranchAddress("nBtagsHiPurityTrkCnt", &nBtagsHiPurityTrkCnt);
	fChain->SetBranchAddress("nBTagsHiEffSimpleSecVtx", &nBTagsHiEffSimpleSecVtx);
	fChain->SetBranchAddress("nBTagsHiPuritySimpleSecVtx", &nBTagsHiPuritySimpleSecVtx);
	fChain->SetBranchAddress("nBTagsCombSecVtx", &nBTagsCombSecVtx);
	fChain->SetBranchAddress("jetSumEt", &jetSumEt);
	fChain->SetBranchAddress("jetMETSumEt", &jetMETSumEt);
	fChain->SetBranchAddress("nJets", &nJets);
	fChain->SetBranchAddress("nExtraJets", &nExtraJets);

//*/
	fChain->SetBranchAddress("Tau1hpsPFTauDiscriminationAgainstLooseElectron", &Tau1hpsPFTauDiscriminationAgainstLooseElectron);
	fChain->SetBranchAddress("Tau1hpsPFTauDiscriminationAgainstLooseMuon", &Tau1hpsPFTauDiscriminationAgainstLooseMuon);
	fChain->SetBranchAddress("Tau1hpsPFTauDiscriminationAgainstMediumElectron", &Tau1hpsPFTauDiscriminationAgainstMediumElectron);
	fChain->SetBranchAddress("Tau1hpsPFTauDiscriminationAgainstTightElectron", &Tau1hpsPFTauDiscriminationAgainstTightElectron);
	fChain->SetBranchAddress("Tau1hpsPFTauDiscriminationAgainstTightMuon", &Tau1hpsPFTauDiscriminationAgainstTightMuon);
	fChain->SetBranchAddress("Tau1hpsPFTauDiscriminationByDecayModeFinding", &Tau1hpsPFTauDiscriminationByDecayModeFinding);
	fChain->SetBranchAddress("Tau1hpsPFTauDiscriminationByLooseIsolation", &Tau1hpsPFTauDiscriminationByLooseIsolation);
	fChain->SetBranchAddress("Tau1hpsPFTauDiscriminationByMediumIsolation", &Tau1hpsPFTauDiscriminationByMediumIsolation);
	fChain->SetBranchAddress("Tau1hpsPFTauDiscriminationByTightIsolation", &Tau1hpsPFTauDiscriminationByTightIsolation);
	fChain->SetBranchAddress("Tau1hpsPFTauDiscriminationByVLooseIsolation", &Tau1hpsPFTauDiscriminationByVLooseIsolation);
	fChain->SetBranchAddress("Tau2hpsPFTauDiscriminationAgainstLooseElectron", &Tau2hpsPFTauDiscriminationAgainstLooseElectron);
	fChain->SetBranchAddress("Tau2hpsPFTauDiscriminationAgainstLooseMuon", &Tau2hpsPFTauDiscriminationAgainstLooseMuon);
	fChain->SetBranchAddress("Tau2hpsPFTauDiscriminationAgainstMediumElectron", &Tau2hpsPFTauDiscriminationAgainstMediumElectron);
	fChain->SetBranchAddress("Tau2hpsPFTauDiscriminationAgainstTightMuon", &Tau2hpsPFTauDiscriminationAgainstTightMuon);
	fChain->SetBranchAddress("Tau2hpsPFTauDiscriminationAgainstTightElectron", &Tau2hpsPFTauDiscriminationAgainstTightElectron);
	fChain->SetBranchAddress("Tau2hpsPFTauDiscriminationByDecayModeFinding", &Tau2hpsPFTauDiscriminationByDecayModeFinding);
	fChain->SetBranchAddress("Tau2hpsPFTauDiscriminationByLooseIsolation", &Tau2hpsPFTauDiscriminationByLooseIsolation);
	fChain->SetBranchAddress("Tau2hpsPFTauDiscriminationByMediumIsolation", &Tau2hpsPFTauDiscriminationByMediumIsolation);
	fChain->SetBranchAddress("Tau2hpsPFTauDiscriminationByTightIsolation", &Tau2hpsPFTauDiscriminationByTightIsolation);
	fChain->SetBranchAddress("Tau2hpsPFTauDiscriminationByVLooseIsolation", &Tau2hpsPFTauDiscriminationByVLooseIsolation);


	fChain->SetBranchAddress("GenMET", &GenMET);
	fChain->SetBranchAddress("GenMETphi", &GenMETphi);
	fChain->SetBranchAddress("Tau1GenParentMass", &Tau1GenParentMass);
	fChain->SetBranchAddress("Tau2GenParentMass", &Tau2GenParentMass);
	fChain->SetBranchAddress("TauTauPlusMetGenMass", &TauTauPlusMetGenMass);
	fChain->SetBranchAddress("Tau1LTIpVtZ", &Tau1LTIpVtZ);
	fChain->SetBranchAddress("Tau2LTIpVtZ", &Tau2LTIpVtZ);
	fChain->SetBranchAddress("METphi", &METphi);
	fChain->SetBranchAddress("METl1l2l3corr", &METl1l2l3corr);
	fChain->SetBranchAddress("METl1l2l3corrPhi", &METl1l2l3corrPhi);
	fChain->SetBranchAddress("METsignificance", &METsignificance);
	fChain->SetBranchAddress("Tau1Tau2LeadingJetPtSum", &Tau1Tau2LeadingJetPtSum);
	fChain->SetBranchAddress("Tau1LeadingJetDeltaPhi", &Tau1LeadingJetDeltaPhi);
	fChain->SetBranchAddress("Tau2LeadingJetDeltaPhi", &Tau2LeadingJetDeltaPhi);
	fChain->SetBranchAddress("Tau1hpsPFTauDiscriminationAgainstMediumMuon", &Tau1hpsPFTauDiscriminationAgainstMediumMuon);
	fChain->SetBranchAddress("Tau2hpsPFTauDiscriminationAgainstMediumMuon", &Tau2hpsPFTauDiscriminationAgainstMediumMuon);
	fChain->SetBranchAddress("Tau1hpsPFTauDiscriminationByVLooseIsolationDBSumPtCorr", &Tau1hpsPFTauDiscriminationByVLooseIsolationDBSumPtCorr);
	fChain->SetBranchAddress("Tau1hpsPFTauDiscriminationByLooseIsolationDBSumPtCorr", &Tau1hpsPFTauDiscriminationByLooseIsolationDBSumPtCorr);
	fChain->SetBranchAddress("Tau1hpsPFTauDiscriminationByMediumIsolationDBSumPtCorr", &Tau1hpsPFTauDiscriminationByMediumIsolationDBSumPtCorr);
	fChain->SetBranchAddress("Tau1hpsPFTauDiscriminationByTightIsolationDBSumPtCorr", &Tau1hpsPFTauDiscriminationByTightIsolationDBSumPtCorr);
	fChain->SetBranchAddress("Tau1hpsPFTauDiscriminationByVLooseCombinedIsolationDBSumPtCorr", &Tau1hpsPFTauDiscriminationByVLooseCombinedIsolationDBSumPtCorr);
	fChain->SetBranchAddress("Tau1hpsPFTauDiscriminationByLooseCombinedIsolationDBSumPtCorr", &Tau1hpsPFTauDiscriminationByLooseCombinedIsolationDBSumPtCorr);
	fChain->SetBranchAddress("Tau1hpsPFTauDiscriminationByMediumCombinedIsolationDBSumPtCorr", &Tau1hpsPFTauDiscriminationByMediumCombinedIsolationDBSumPtCorr);
	fChain->SetBranchAddress("Tau1hpsPFTauDiscriminationByTightCombinedIsolationDBSumPtCorr", &Tau1hpsPFTauDiscriminationByTightCombinedIsolationDBSumPtCorr);
	fChain->SetBranchAddress("Tau2hpsPFTauDiscriminationByVLooseIsolationDBSumPtCorr", &Tau2hpsPFTauDiscriminationByVLooseIsolationDBSumPtCorr);
	fChain->SetBranchAddress("Tau2hpsPFTauDiscriminationByLooseIsolationDBSumPtCorr", &Tau2hpsPFTauDiscriminationByLooseIsolationDBSumPtCorr);
	fChain->SetBranchAddress("Tau2hpsPFTauDiscriminationByMediumIsolationDBSumPtCorr", &Tau2hpsPFTauDiscriminationByMediumIsolationDBSumPtCorr);
	fChain->SetBranchAddress("Tau2hpsPFTauDiscriminationByTightIsolationDBSumPtCorr", &Tau2hpsPFTauDiscriminationByTightIsolationDBSumPtCorr);
	fChain->SetBranchAddress("Tau2hpsPFTauDiscriminationByVLooseCombinedIsolationDBSumPtCorr", &Tau2hpsPFTauDiscriminationByVLooseCombinedIsolationDBSumPtCorr);
	fChain->SetBranchAddress("Tau2hpsPFTauDiscriminationByLooseCombinedIsolationDBSumPtCorr", &Tau2hpsPFTauDiscriminationByLooseCombinedIsolationDBSumPtCorr);
	fChain->SetBranchAddress("Tau2hpsPFTauDiscriminationByMediumCombinedIsolationDBSumPtCorr", &Tau2hpsPFTauDiscriminationByMediumCombinedIsolationDBSumPtCorr);
	fChain->SetBranchAddress("Tau2hpsPFTauDiscriminationByTightCombinedIsolationDBSumPtCorr", &Tau2hpsPFTauDiscriminationByTightCombinedIsolationDBSumPtCorr);
	fChain->SetBranchAddress("Tau1hpsPFTauDiscriminationByRawCombinedIsolationDBSumPtCorr", &Tau1hpsPFTauDiscriminationByRawCombinedIsolationDBSumPtCorr);
	fChain->SetBranchAddress("Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorr", &Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorr);
	fChain->SetBranchAddress("Tau1hpsPFTauDiscriminationByRawGammaIsolationDBSumPtCorr", &Tau1hpsPFTauDiscriminationByRawGammaIsolationDBSumPtCorr);
	fChain->SetBranchAddress("Tau2hpsPFTauDiscriminationByRawCombinedIsolationDBSumPtCorr", &Tau2hpsPFTauDiscriminationByRawCombinedIsolationDBSumPtCorr);
	fChain->SetBranchAddress("Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorr", &Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorr);
	fChain->SetBranchAddress("Tau2hpsPFTauDiscriminationByRawGammaIsolationDBSumPtCorr", &Tau2hpsPFTauDiscriminationByRawGammaIsolationDBSumPtCorr);






	Notify();
}

void TauTauAnalyzer::BookHistos(){

	HistoWrapper* prepWrapper;
	histoCollection = new map<string, HistoWrapper*>();
	histoCollection->clear();

	map<string, Config*> histo = histoConfig->getGroups();
	string histoPrefix;

	// First get all TH1F
	histoPrefix = "th1f_";
	for (map<string, Config*>::iterator i = histo.begin(); i != histo.end(); ++i) {
		string groupName = i->first;
		Config* group = i->second;
		if (groupName.substr(0, histoPrefix.length()) == histoPrefix) {

			string th1Name		= groupName.substr(histoPrefix.length());
			string title		= group->pString("title");
			int numBins			= group->pInt("numBins");
			string xbinsString	= group->pString("xbins");
			double xMin			= group->pDouble("xMin");
			double xMax			= group->pDouble("xMax");
			double xMinVis		= group->pDouble("xMinVis");
			double xMaxVis		= group->pDouble("xMaxVis");

			double yMinVis		= group->pDouble("yMinVis");
			double yMaxVis		= group->pDouble("yMaxVis");

			string xLabel		= group->pString("xLabel");
			string yLabel		= group->pString("yLabel");
			bool logx			= group->pBool("logx");
			bool logy			= group->pBool("logy");
			bool centerLabels	= group->pBool("centerLabels");
			bool showOF			= group->pBool("showOF");

			if(xbinsString.length() > 0){
				float *xbins = 0;
				xbins = new float [10000];
				int dim = ParseCSV(xbinsString,',',xbins);
				prepWrapper = new HistoWrapper(new TH1F(th1Name.c_str(), title.c_str(), dim, xbins));
				prepWrapper->SetRangeUser(xbins[0], xbins[dim], yMinVis, yMaxVis);
				prepWrapper->SetHasVariableWidthBins(true);
			}else{
				prepWrapper = new HistoWrapper(new TH1F(th1Name.c_str(), title.c_str(), numBins, xMin, xMax));
				prepWrapper->SetRangeUser(xMinVis, xMaxVis, yMinVis, yMaxVis);
			}
			prepWrapper->SetXlabel(xLabel);
			prepWrapper->SetYlabel(yLabel);
			prepWrapper->SetLog(logx,logy);
			prepWrapper->SetShowOF(showOF);
			prepWrapper->SetCenterLabels(centerLabels);
			prepWrapper->SetOutputFilename(th1Name);
			histoCollection->insert(pair<string,HistoWrapper*>(th1Name,prepWrapper));
		}
	}

	// Then get all TH2F
	histoPrefix = "th2f_";
	for (map<string, Config*>::iterator i = histo.begin(); i != histo.end(); ++i) {
		string groupName = i->first;
		Config* group = i->second;
		if (groupName.substr(0, histoPrefix.length()) == histoPrefix) {

			string th1Name		= groupName.substr(histoPrefix.length());
			string title		= group->pString("title");
			int numBinsX		= group->pInt("numBinsX");
			int numBinsY		= group->pInt("numBinsY");
			double xMin			= group->pDouble("xMin");
			double xMax			= group->pDouble("xMax");
			double yMin			= group->pDouble("yMin");
			double yMax			= group->pDouble("yMax");
			double xMinVis		= group->pDouble("xMinVis");
			double xMaxVis		= group->pDouble("xMaxVis");
			double yMinVis		= group->pDouble("yMinVis");
			double yMaxVis		= group->pDouble("yMaxVis");
			string xLabel		= group->pString("xLabel");
			string yLabel		= group->pString("yLabel");
			string zLabel		= group->pString("zLabel");
			bool logx			= group->pBool("logx");
			bool logy			= group->pBool("logy");
			bool logz			= group->pBool("logz");
			bool centerLabels	= group->pBool("centerLabels");
			bool showText		= group->pBool("showText");


			prepWrapper = new HistoWrapper(new TH2F(th1Name.c_str(), title.c_str(), numBinsX, xMin, xMax, numBinsY, yMin, yMax));
			prepWrapper->SetRangeUser(xMinVis,xMaxVis,yMinVis,yMaxVis);
			prepWrapper->SetXlabel(xLabel);
			prepWrapper->SetYlabel(yLabel);
			prepWrapper->SetZlabel(zLabel);
			prepWrapper->SetShowText(showText);
			prepWrapper->SetLog(logx,logy,logz);
			prepWrapper->SetCenterLabels(centerLabels);
			prepWrapper->SetOutputFilename(th1Name);
			histoCollection->insert(pair<string,HistoWrapper*>(th1Name,prepWrapper));
		}
	}

}

Bool_t TauTauAnalyzer::Notify(){
	// The Notify() function is called when a new file is opened. This
	// can be either for a new TTree in a TChain or when when a new TTree
	// is started when using PROOF. It is normally not necessary to make changes
	// to the generated code, but the routine can be extended by the
	// user if needed. The return value is currently not used.

	return kTRUE;
}

void TauTauAnalyzer::Show(Long64_t entry){
	// Print contents of entry.
	// If entry is not specified, print current entry
	if (!fChain) return;
	fChain->Show(entry);
}


Int_t TauTauAnalyzer::Cut(Long64_t entry){
	// This function may be called from Loop.
	// returns  1 if entry is accepted.
	// returns -1 otherwise.
	return 1;
}


#endif
