#!/bin/bash

# MaxEvents
if [ -z "$1" ]; then
	maxEvents=10000
else
	maxEvents=$1
fi

# Template
baseConfig="template_Nminus1.cfg"

# Clean
rm -f Nminus1*.cfg

# Decleare all cuts here
cutsStr="TauPt TauEta DeltaR LTpT LTHits H3x3overP TrackIso GammaIso NProngs ChargeProduct CosDPhi MinMET"

# Parse cut string into array
cuts=($(echo $cutsStr | sed 's/\(\ \)/\1 /g'))

# Loop over all cuts
for cut in "${cuts[@]}"; do

	# Subtract cut in question
	newList="$( echo $cutsStr | sed 's/'$cut'//g' )"

	# Set up output filename
	outputF="Nminus1_"$cut".cfg"

	# Copy from template
	cp $baseConfig $outputF

	# Apply changes to template
	sed -i 's/cutsToApply.*/cutsToApply\ =\ '"$newList"'/g' $outputF
	sed -i 's/AllCuts/Nminus1_'$cut'/g' $outputF
	sed -i 's/\(maxEvents.*=\).*/\1\ '$maxEvents'/g' $outputF

done
