#!/bin/bash

template="cfg/template.cfg"
tmp=".tmpCAC.cfg"

toDo="ChargeProduct TauPt TauEta DeltaR LTpT aLooseElectron aLooseMuon VLooseIso CosDPhi MET Zeta"

cuts=("ChargeProduct:-1:-1" "TauPt:20:" "TauEta::2.1" "DeltaR:0.7:" "LTpT:5:" "aLooseElectron" "aLooseMuon" "VLooseIso" "CosDPhi:-1:-0.95" "MET:20:" "Zeta:-7:")

list=""
label="CAC"
for cut in ${cuts[@]}; do


	/bin/cp -f $template $tmp

	list="$list $cut"
	label=$label"_"${cut%%:*}

	sed -i "s/\(cutsToApply.*=\).*/\1\ ${list}/g" $tmp
	sed -i "s/\(analysisTag.*=\).*/\1\ ${label}/g" $tmp

	rlabel=${cut%%:*}
	if [[ $toDo == *$rlabel* ]]; then
		./runPlotter.sh $tmp
	fi

	rm -f "$tmp"
done

