#!/bin/bash

template="cfg/template_Nminus1_v2.cfg"
tmp=".tmpNm1.cfg"




#toDo="ChargeProduct TauPt TauEta DeltaR LTpT CosDPhi MET Zeta"
toDo="TauPt TauEta DeltaR LTpT ChargeProduct MediumIso aTightElectron"

#cuts=("ChargeProduct:-1:-1" "TauPt:20:" "TauEta::2.1" "DeltaR:0.7:" "LTpT:5:" "aLooseElectron" "aLooseMuon" "VLooseIso" "CosDPhi:-1:-0.95" "MET:20:" "Zeta:-7:")
cuts=("TauPt:35:" "TauEta::2.1" "DeltaR:0.7:" "LTpT:5:" "ChargeProduct:-1:-1" "MediumIso" "aTightElectron:0:0")

list=""
for cut in ${cuts[@]}; do
	label="Nminus1"
	list="$list $cut"
done


for cut in ${cuts[@]}; do

	rlabel=${cut%%:*}

	if [[ $toDo == *$rlabel* ]]; then
		/bin/cp -f $template $tmp


		nlabel="$(echo "$label" | sed 's/_'$rlabel'//g')"

		rcut="$( echo "$cut" | sed 's/\./\\\./g')"
		nlist="$(echo "$list" | sed 's/\ '$rcut'//g')"

		sed -i "s/\(cutsToApply.*=\).*/\1\ ${nlist}/g" $tmp
		sed -i "s/\(analysisTag.*=\).*/\1\ Nminus1_${rlabel}/g" $tmp

		./runPlotter.sh $tmp

		rm -f "$tmp"
	fi
done

