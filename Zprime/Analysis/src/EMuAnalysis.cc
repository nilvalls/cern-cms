#include "HighMassAnalysis/Analysis/interface/EMuAnalysis.h"
#include "SHarper/HEEPAnalyzer/interface/HEEPEle.h"
#include "SHarper/HEEPAnalyzer/interface/HEEPCutCodes.h"

using namespace edm;
using namespace reco;
using namespace std;

EMuAnalysis::EMuAnalysis(const edm::ParameterSet& iCongif): HiMassTauAnalysis(iCongif){} 
EMuAnalysis::~EMuAnalysis(){}
  
void EMuAnalysis::fillNtuple(){
  // store vector of PDF weights
  _PDFWeights = pdfWeightVector;

  // store ISR weights
  _ISRGluonWeight = isrgluon_weight;
  _ISRGammaWeight = isrgamma_weight;

  // store FSR weights
  _FSRWeight = fsr_weight;

  // fill jet information used for jet based cuts (e.g. jet veto)
  _jetBtagHiDiscByTrkCntHiEff = -99.;
  _jetBtagHiDiscByTrkCntHiPurity = -99.;
  _jetBtagHiDiscBySimpleSecVtxHiEff = -10.;
  _jetBtagHiDiscBySimpleSecVtxHiPurity = -10.;
  _jetBtagHiDiscByCombSecVtx = -10.;
  for(pat::JetCollection::const_iterator patJet = _patJets->begin(); patJet != _patJets->end(); ++patJet ){
    if(patJet->et() < _RecoJetPtCut)continue;
    if(fabs(patJet->eta()) > _RecoJetEtaMaxCut) continue;
    _jetPt->push_back((float)patJet->pt());
    _jetEt->push_back(patJet->et());
    _jetE->push_back(patJet->energy());
    _jetEta->push_back(patJet->eta()); 
    _jetPhi->push_back(patJet->phi());
    //_jetEmFraction->push_back(patJet->emEnergyFraction());
   
    
    if(patJet->bDiscriminator("trackCountingHighEffBJetTags") > _jetBtagHiDiscByTrkCntHiEff)_jetBtagHiDiscByTrkCntHiEff = patJet->bDiscriminator("trackCountingHighEffBJetTags");
    if(patJet->bDiscriminator("trackCountingHighPurBJetTags") > _jetBtagHiDiscByTrkCntHiPurity)_jetBtagHiDiscByTrkCntHiPurity = patJet->bDiscriminator("trackCountingHighPurBJetTags");
    if(patJet->bDiscriminator("simpleSecondaryVertexHighEffBJetTags") > _jetBtagHiDiscBySimpleSecVtxHiEff)_jetBtagHiDiscBySimpleSecVtxHiEff = patJet->bDiscriminator("simpleSecondaryVertexHighEffBJetTags");
    if(patJet->bDiscriminator("simpleSecondaryVertexHighPurBJetTags") > _jetBtagHiDiscBySimpleSecVtxHiPurity) _jetBtagHiDiscBySimpleSecVtxHiPurity = patJet->bDiscriminator("simpleSecondaryVertexHighPurBJetTags");
    if(patJet->bDiscriminator("combinedSecondaryVertexBJetTags") > _jetBtagHiDiscByCombSecVtx)_jetBtagHiDiscByCombSecVtx = patJet->bDiscriminator("combinedSecondaryVertexBJetTags");
  }
  
  // loop over electrons and muons to create e-mu pairs
  for(pat::ElectronCollection::const_iterator patElectron = _patElectrons->begin(); patElectron != _patElectrons->end(); ++patElectron) {
    if(patElectron->pt() < _RecoElectronPtMinCut || fabs(patElectron->eta()) > _RecoElectronEtaCut) continue;
    for(pat::MuonCollection::const_iterator patMuon = _patMuons->begin(); patMuon != _patMuons->end(); ++patMuon) {
      if(patMuon->pt() < _RecoMuonPtMinCut || fabs(patMuon->eta()) > _RecoMuonEtaCut)continue;
      if(deltaR(patMuon->p4(), patElectron->p4()) < _DiTauDeltaRCut) continue;
      // determine whether ntuple information will be filled based on systematics (smearing of resolution and scale for taus or electrons)      

      // determine whether ntuple information will be filled based on systematics (smearing of resolution and scale for Muons or electrons) 

      // Recalculate MET for cases when electron or taus are smeared (using EDAnalyzer method and configuration parameters to recalculate the MET)
      // Recalculating MET for cases when electron is smeared ----> set SmearTheTau to False, SmearTheElectron to True, and specify correct smearing factors
      // Recalculating MET for cases when tau is smeared ----> set SmearTheTau to True, SmearTheElectron to False, and specify correct smearing factors
      // Recalculating MET for cases when jets are smeared ----> set SmearTheTau to False, SmearTheElectron to False, SmearTheJet to True, and specify correct smearing factors
      // SMEARING FACTORS: smear ONLY electron pt, tau pt, and jet energy scale factor
      double met_e = TMath::Sqrt(((*(_patMETs->begin())).px() + deltaForMEx)*((*(_patMETs->begin())).px() + deltaForMEx) + ((*(_patMETs->begin())).py() + deltaForMEy)*((*(_patMETs->begin())).py() + deltaForMEy));
      double met_px = ((*(_patMETs->begin())).px() + deltaForMEx);
      double met_py = ((*(_patMETs->begin())).py() + deltaForMEy);
      double met_pz = (*(_patMETs->begin())).pz();

      reco::Candidate::LorentzVector LorentzVectorMET(met_px,  met_py,  met_pz, met_e);
      MET = LorentzVectorMET;
      
      const reco::Vertex& thePrimaryEventVertex = (*(_primaryEventVertexCollection)->begin());
      
      if(!_AnalyzeData){      
      	_muongenPt->push_back(matchToGen(*patMuon, 13).second.pt());
      	_muongenE->push_back(matchToGen(*patMuon, 13).second.energy());
      	_muongenEta->push_back(matchToGen(*patMuon, 13).second.eta());
      	_muongenPhi->push_back(matchToGen(*patMuon, 13).second.phi());
      
      	const GenParticleRef theGenmuonRef = patMuon->genParticleRef();
      	_muonMatched->push_back(int(matchToGen(*patMuon, 13).first));							 
      	if(theGenmuonRef.isNonnull())_muonMotherId->push_back(abs(theGenmuonRef->mother()->mother()->pdgId())); 			 
      	else _muonMotherId->push_back(0);												 
        
	// get Electron Gen Info
      	_egenPt->push_back(matchToGen(*patElectron, 11).second.pt());
      	_egenE->push_back(matchToGen(*patElectron, 11).second.energy());
      	_egenEta->push_back(matchToGen(*patElectron, 11).second.eta());
      	_egenPhi->push_back(matchToGen(*patElectron, 11).second.phi());
        _eMatched->push_back(int(matchToGen(*patElectron, 11).first));
        _ePdgId->push_back(getMatchedPdgId(*patElectron).first);
        _eMotherPdgId->push_back(getMatchedPdgId(*patElectron).second);
      }
      
      _muonE->push_back(patMuon->energy()); 											       
      _muonEt->push_back(patMuon->et());												       
      _muonPt->push_back(patMuon->pt());												       
      _muonCharge->push_back(patMuon->charge());											       
      _muonEta->push_back(patMuon->eta());  											       
      _muonPhi->push_back(patMuon->phi());
      if(patMuon->globalTrack().isNonnull()){
        _muonIpVtx->push_back(patMuon->globalTrack()->dxy(thePrimaryEventVertex.position()));
        _muonIpVtxError->push_back(patMuon->globalTrack()->d0Error());
        _muonValidHits->push_back(patMuon->globalTrack()->numberOfValidHits());
        _muonNormChiSqrd ->push_back(patMuon->globalTrack()->normalizedChi2());
     }
      else{
        _muonIpVtx->push_back(1000);
        _muonIpVtxError->push_back(1000);
        _muonValidHits->push_back(0);
        _muonNormChiSqrd ->push_back(1000);
      }
      _muonEcalIsoDr03->push_back(patMuon->ecalIso());
      _muonTrkIsoDr03->push_back(patMuon->trackIso());
      _muonCaloIsoDr03->push_back(patMuon->caloIso());
      
      _muonEcalIsoDr04->push_back(patMuon->ecalIsoDeposit()->depositAndCountWithin(0.4, reco::IsoDeposit::Vetos(),_RecoMuonEcalIsoRecHitThreshold).first);
      _muonTrkIsoDr04->push_back(patMuon->trackIsoDeposit()->depositAndCountWithin(0.4, reco::IsoDeposit::Vetos(),_RecoMuonTrackIsoTrkThreshold).first);
      
      _muonEcalIsoDr05->push_back(patMuon->isolationR05().emEt);
      _muonHadIsoDr05->push_back(patMuon->isolationR05().hadEt);
      _muonTrkIsoDr05->push_back(patMuon->isolationR05().sumPt);
      _muonCaloIsoDr05->push_back(patMuon->isolationR05().hadEt + patMuon->isolationR05().emEt);
      _muonCaloComp->push_back(patMuon->caloCompatibility());
      _muonIsGlobalMuon->push_back((int)patMuon->isGlobalMuon());
      _muonisTrackerMuon->push_back((int)patMuon->isTrackerMuon());
      _muonIsGlobalPromptTight->push_back((int)patMuon->muonID("GlobalMuonPromptTight"));
      _muonIsTMLastStationLoose->push_back((int)patMuon->muonID("TMLastStationLoose"));
      _muonIsTMLastStationTight->push_back((int)patMuon->muonID("TMLastStationTight"));
      _muonIsTM2DCompatibilityLoose->push_back((int)patMuon->muonID("TM2DCompatibilityLoose"));
      _muonIsTM2DCompatibilityTight->push_back((int)patMuon->muonID("TM2DCompatibilityTight"));
      _muonPionVeto->push_back((_RecoMuonCaloCompCoefficient * muon::caloCompatibility(*patMuon)) + (_RecoMuonSegmCompCoefficient * muon::segmentCompatibility(*patMuon)));

      
      //----eEcalDriven? eTrkDriven?
      _eEcalDrivenv->push_back(patElectron->ecalDrivenSeed());
      _eTrkDrivenv->push_back(patElectron->trackerDrivenSeed());
      heep::Ele theHeepElec(*patElectron);  // defines HEEP electron
      _isEEv->push_back(patElectron->isEE());
      _isEBv->push_back(patElectron->isEB());
      _isEBEEGap   ->push_back(patElectron->isEBEEGap()) ;
      _isEBEtaGap  ->push_back(patElectron->isEBEtaGap());
      _isEBPhiGap  ->push_back(patElectron->isEBPhiGap());
      _isEEDeeGap  ->push_back(patElectron->isEEDeeGap());
      _isEERingGap ->push_back(patElectron->isEERingGap());
      _eE->push_back(Electron.energy());			       
      _ePt->push_back(Electron.pt());										       
      _eEt->push_back(theHeepElec.et());       
      _eCharge->push_back(patElectron->charge());	       
      _eEta->push_back(Electron.eta());										       
      _ePhi->push_back(Electron.phi());									       
      //_eSigmaEtaEta->push_back(patElectron->scSigmaEtaEta());							       
      _eSigmaEtaEta->push_back(theHeepElec.sigmaEtaEta());							       
      //_eSigmaIEtaIEta->push_back(patElectron->scSigmaIEtaIEta());		       
      _eSigmaIEtaIEta->push_back(theHeepElec.sigmaIEtaIEta());		       
      //_eEOverP->push_back(patElectron->eSuperClusterOverP());					       
      _eEOverP->push_back(theHeepElec.epIn());					       
      //_eHOverEm->push_back(patElectron->hadronicOverEm());		       
      _eHOverEm->push_back(theHeepElec.hOverE());		       
      //_eDeltaPhiIn->push_back(patElectron->deltaEtaSuperClusterTrackAtVtx());	       
      _eDeltaPhiIn->push_back(theHeepElec.dPhiIn());	       
      //_eDeltaEtaIn->push_back(patElectron->deltaPhiSuperClusterTrackAtVtx());
      _eDeltaEtaIn->push_back(theHeepElec.dEtaIn());

      _eEcalIsoPat->push_back(patElectron->ecalIso());
      _eHcalIsoPat->push_back(patElectron->hcalIso());										       
      _eTrkIsoPat->push_back(patElectron->trackIso());
      
      _heepTrkIso->push_back(theHeepElec.isolPtTrks());
      _heepEcalIso->push_back(theHeepElec.isolEm());
      _heepHcalIso->push_back(theHeepElec.isolHad());
      _heepHcalIsoDepth1->push_back(theHeepElec.isolHadDepth1());										       
      _heepHcalIsoDepth2->push_back(theHeepElec.isolHadDepth2());										       
      _heepEcalHcalIsoDepth1->push_back(theHeepElec.isolEmHadDepth1());										       
     
      _eUserEcalIso->push_back(patElectron->userIsolation(pat::EcalIso));
      _eUserHcalIso->push_back(patElectron->userIsolation(pat::HcalIso));										     
      _eUserTrkIso->push_back(patElectron->userIsolation(pat::TrackIso));
      _eIsoPat->push_back(patElectron->caloIso());

      //_eSCE1x5->push_back(patElectron->scE1x5());								       
      _eSCE1x5->push_back(theHeepElec.scE1x5());								       
      //_eSCE2x5->push_back(patElectron->scE2x5Max());		       
      _eSCE2x5->push_back(theHeepElec.scE2x5Max());		       
     // _eSCE5x5->push_back(patElectron->scE5x5()); 
       _eSCE5x5->push_back(theHeepElec.scE5x5()); 
     // fill the electrons impact parameter and its error
      if(patElectron->gsfTrack().isNonnull()) {
        _eIp->push_back(patElectron->gsfTrack()->dxy(thePrimaryEventVertex.position()));
        _eIpError->push_back(patElectron->gsfTrack()->d0Error()); 
      }
      else{
	_eIp->push_back(-100.);
	_eIpError->push_back(-100.);
      } 
      // get impact paramenter from of the CTF tracks
      if(patElectron->closestCtfTrackRef().isNonnull()){
	_eIp_ctf->push_back(patElectron->closestCtfTrackRef()->dxy(thePrimaryEventVertex.position()));
	_eIpError_ctf->push_back(patElectron->closestCtfTrackRef()->d0Error());
      } 
      else{ 
        _eIpError_ctf->push_back(-100.);
        _eIp_ctf->push_back(-100.); 
      }

      _eClass->push_back(patElectron->classification());
      if(patElectron->isElectronIDAvailable("eidRobustTight")) _eIdRobustTight->push_back(patElectron->electronID("eidRobustTight"));
      else _eIdRobustTight->push_back(-1.);
      if(patElectron->isElectronIDAvailable("eidRobustLoose")) _eIdRobustLoose->push_back(patElectron->electronID("eidRobustLoose"));
      else _eIdRobustLoose->push_back(-1.);
      if(patElectron->isElectronIDAvailable("loose")) _eIdLoose->push_back(patElectron->electronID("loose"));
      else _eIdLoose->push_back(-1.);
      if(patElectron->isElectronIDAvailable("tight")) _eIdTight->push_back(patElectron->electronID("tight"));
      else _eIdTight->push_back(-1.);
      if(patElectron->isElectronIDAvailable("eidRobustHighEnergy")) _eIdHighEnergy->push_back(patElectron->electronID("eidRobustHighEnergy"));
      else _eIdHighEnergy->push_back(-1.);
      
      const Track* elTrack = (const reco::Track*)(patElectron->gsfTrack().get());
      // number of expected hits before the first track hit
      const HitPattern& pInner = elTrack->trackerExpectedHitsInner(); 
      _eMissingHits->push_back(pInner.numberOfHits());
      
      // HEEP selection cuts
      int cutResult = patElectron->userInt("HEEPId");
      _heepPassedEt->push_back(heep::CutCodes::passCuts(cutResult, "et"));
      _heepPassedPt->push_back(heep::CutCodes::passCuts(cutResult, "pt"));
      _heepPassedDetEta->push_back(heep::CutCodes::passCuts(cutResult, "detEta"));
      _heepPassedCrack->push_back(heep::CutCodes::passCuts(cutResult, "crack"));
      _heepPassedDEtaIn->push_back(heep::CutCodes::passCuts(cutResult, "dEtaIn"));
      _heepPassedDPhiIn->push_back(heep::CutCodes::passCuts(cutResult, "dPhiIn"));
      _heepPassedHadem->push_back(heep::CutCodes::passCuts(cutResult, "hadem"));
      _heepPassedSigmaIEtaIEta->push_back(heep::CutCodes::passCuts(cutResult, "sigmaIEtaIEta"));
      _heepPassed2by5Over5By5->push_back(heep::CutCodes::passCuts(cutResult, "e2x5Over5x5"));
      _heepPassedEcalHad1Iso->push_back(heep::CutCodes::passCuts(cutResult, "isolEmHadDepth1"));
      _heepPassedHad2Iso->push_back(heep::CutCodes::passCuts(cutResult, "isolHadDepth2"));
      _heepPassedTrkIso->push_back(heep::CutCodes::passCuts(cutResult, "isolPtTrks"));
      _heepPassedEcalDriven->push_back(heep::CutCodes::passCuts(cutResult, "ecalDriven"));
      //_heepPassedAllCuts->push_back(heep::CutCodes::passCuts(cutResult);
      _heepPassedAllCuts->push_back(heep::CutCodes::passCuts(cutResult, ~heep::CutCodes::DETAIN));
      
      _mEt->push_back(MET.pt());
      _eMuCosDPhi->push_back(cos(TMath::Abs(normalizedPhi(patElectron->phi() - patMuon->phi()))));	
      _eMuDelatR->push_back(reco::deltaR(*patElectron, *patMuon));
      // vis mass
      _eMuMass->push_back(GetVisMass(*patElectron,*patMuon));
      // vis + met mass
      _eMuMetMass->push_back(GetVisPlusMETMass(*patElectron,*patMuon));      
      _eMEtMt->push_back(CalculateLeptonMetMt(*patElectron));
      _muMEtMt->push_back(CalculateLeptonMetMt(*patMuon));
      // Collinear mass approximation
      _eMuCollMass->push_back(GetCollinearApproxMass(*patElectron,*patMuon));

      _eMuPZeta->push_back(CalculatePZeta(*patElectron,*patMuon));
      _eMuPZetaVis->push_back(CalculatePZetaVis(*patElectron,*patMuon));
      // get cosDphi between highest Pt Lepton and MET
      _leptonMetCosDphi->push_back((patMuon->pt() > patElectron->pt()) ? cos(TMath::Abs(normalizedPhi(patMuon->phi() - MET.phi()))) :
      									 cos(TMath::Abs(normalizedPhi(patElectron->phi() - MET.phi())))); 
      
      // loop over track collection and sum the pt of extra tracks in event above 1GeV 
      float minDr = _JetMuonMatchingDeltaR;
      unsigned int nBtagsHiEffTrkCnt = 0;
      unsigned int nBTagsHiEffSimpleSecVtx = 0;
      unsigned int nBtagsHiPurityTrkCnt = 0;
      unsigned int nBTagsHiPuritySimpleSecVtx = 0;
      unsigned int nBTagsCombSecVtx = 0;
      unsigned int nMuonsInJet = 0;
      float jetSumEt = 0;
      unsigned int nJets = 0;
      bool muonInJet = false;
      // loop over jet collection excluding jets in the direction of my di-tau cands
      for(pat::JetCollection::const_iterator patJet = _patJets->begin(); patJet != _patJets->end(); ++patJet){
        if(deltaR(patJet->eta(), patJet->phi(), patElectron->eta(), patElectron->phi()) < minDr) continue;
	if(deltaR(patJet->eta(), patJet->phi(), patMuon->eta(), patMuon->phi()) < minDr){
	  muonInJet = true;
	  nMuonsInJet++;
	  continue;
	}
	if(patJet->et() < _RecoJetPtCut) continue;
	if(fabs(patJet->eta()) >_RecoJetEtaMaxCut) continue;
	nJets++;
      	
	float trkCntHighEffBtag =  patJet->bDiscriminator("trackCountingHighEffBJetTags");
	float trkCntHighPurityBtag =  patJet->bDiscriminator("trackCountingHighPurBJetTags");
	float simpleSecVtxHighEffBTag = patJet->bDiscriminator("simpleSecondaryVertexHighEffBJetTags");
	float simpleSecVtxHighPurityBTag = patJet->bDiscriminator("simpleSecondaryVertexHighPurBJetTags");
	float combSecVtxBTag = patJet->bDiscriminator("combinedSecondaryVertexBJetTags");
	//use medium wp 
      	if(trkCntHighEffBtag > -90 && fabs(trkCntHighEffBtag) > 3.3) nBtagsHiEffTrkCnt++; 	     	   
      	if(trkCntHighPurityBtag > -90 && fabs(trkCntHighPurityBtag) > 1.93) nBtagsHiPurityTrkCnt++; 	     	   
      	if(simpleSecVtxHighEffBTag > 3.05) nBTagsHiEffSimpleSecVtx++;	     	   
      	if(simpleSecVtxHighPurityBTag > 2.0) nBTagsHiPuritySimpleSecVtx++;	     	   
      	if(combSecVtxBTag > 0.5) nBTagsCombSecVtx++; 
	jetSumEt += patJet->et();	   
      }
      _muonInJet->push_back(muonInJet);
      _nBtagsHiEffTrkCnt->push_back(nBtagsHiEffTrkCnt);
      _nBtagsHiPurityTrkCnt->push_back(nBtagsHiPurityTrkCnt);
      _nBTagsHiEffSimpleSecVtx->push_back(nBTagsHiEffSimpleSecVtx);
      _nBTagsHiPuritySimpleSecVtx->push_back(nBTagsHiPuritySimpleSecVtx);
      _nBTagsCombSecVtx->push_back(nBTagsCombSecVtx);
      _jetSumEt->push_back(jetSumEt);
      _jetMETSumEt->push_back(jetSumEt + _patMETs->begin()->et());
      _nJets->push_back(nJets);

    }										  
  }
  _EMuTree->Fill();
}

void EMuAnalysis::setupBranches(){
  _EMuTree = new TTree(_NtupleTreeName.c_str(), "EMuTree");
  
  //Event info
  _EMuTree->Branch("runNumber",&_runNumber);
  _EMuTree->Branch("eventNumber",&_eventNumber);
  _EMuTree->Branch("passedHLTMu9",&_passedHLTMu9);
  _EMuTree->Branch("passedHLTMu11",&_passedHLTMu11);
  _EMuTree->Branch("passedHLTMu15",&_passedHLTMu15);
  _EMuTree->Branch("passedHLTIsoMu9",&_passedHLTIsoMu9);
  _EMuTree->Branch("passedHLTIsoMu11",&_passedHLTIsoMu11);
  _EMuTree->Branch("passedHLTIsoMu13",&_passedHLTIsoMu13);
  _EMuTree->Branch("passedHLTIsoMu15",&_passedHLTIsoMu15);
  _EMuTree->Branch("passedHLTMu5Elec9",&_passedHLTMu5Elec9);
  _EMuTree->Branch("passedHLTMu8Elec8",&_passedHLTMu8Elec8);
  _EMuTree->Branch("passedHLTMu11Elec8",&_passedHLTMu11Elec8);

  _EMuTree->Branch("PDFWweights", &_PDFWeights);
  _EMuTree->Branch("ISRGluonWeight", &_ISRGluonWeight);
  _EMuTree->Branch("ISRGammaWeight", &_ISRGammaWeight);
  _EMuTree->Branch("FSRWeight", &_FSRWeight);
  
   // For the muons
  _EMuTree->Branch("muonMatched",&_muonMatched);
  _EMuTree->Branch("muonMotherId",&_muonMotherId);
  _EMuTree->Branch("muonE",&_muonE);
  _EMuTree->Branch("muonEt",&_muonEt);
  _EMuTree->Branch("muonPt",&_muonPt);
  _EMuTree->Branch("muonCharge",&_muonCharge);
  _EMuTree->Branch("muonEta",&_muonEta);
  _EMuTree->Branch("muonPhi",&_muonPhi);
  
  _EMuTree->Branch("muonIpVtx",&_muonIpVtx);
  _EMuTree->Branch("muonIpVtxError",&_muonIpVtxError);  
  _EMuTree->Branch("muonValidHits",&_muonValidHits);
  _EMuTree->Branch("muonNormChiSqrd",&_muonNormChiSqrd);
  _EMuTree->Branch("muonEcalIso",&_muonEcalIsoDr03);
  _EMuTree->Branch("muonTrkIso",&_muonTrkIsoDr03);
  _EMuTree->Branch("muonCaloIso",&_muonCaloIsoDr03);
  _EMuTree->Branch("muonEcalIsoDr04",&_muonEcalIsoDr04);
  _EMuTree->Branch("muonTrkIsoDr04",&_muonTrkIsoDr04);
  _EMuTree->Branch("muonEcalIsoDr05",&_muonEcalIsoDr05);
  _EMuTree->Branch("muonHadIsoDr05",&_muonHadIsoDr05);
  _EMuTree->Branch("muonTrkIsoDr05",&_muonTrkIsoDr05);
  _EMuTree->Branch("muonCaloIsoDr05",&_muonCaloIsoDr05);
  _EMuTree->Branch("muonCaloComp",&_muonCaloComp);
  _EMuTree->Branch("muonIsGlobalMuon",&_muonIsGlobalMuon);
  _EMuTree->Branch("muonisTrackerMuon",&_muonisTrackerMuon);
  _EMuTree->Branch("muonIsGlobalPromptTight",&_muonIsGlobalPromptTight);
  _EMuTree->Branch("muonIsTMLastStationLoose",&_muonIsTMLastStationLoose);
  _EMuTree->Branch("muonIsTMLastStationTight",&_muonIsTMLastStationTight);
  _EMuTree->Branch("muonIsTM2DCompatibilityLoose",&_muonIsTM2DCompatibilityLoose);
  _EMuTree->Branch("muonIsTM2DCompatibilityTight",&_muonIsTM2DCompatibilityTight);
  _EMuTree->Branch("muonPionVeto",&_muonPionVeto);
  _EMuTree->Branch("muonInJet",&_muonInJet);

  // For the electrons
  _EMuTree->Branch("eMatched",&_eMatched);
  _EMuTree->Branch("ePdgId",&_ePdgId);
  _EMuTree->Branch("eMotherPdgId",&_eMotherPdgId);
  _EMuTree->Branch("eE",&_eE);
  _EMuTree->Branch("eEt",&_eEt);
  _EMuTree->Branch("ePt",&_ePt);
  _EMuTree->Branch("eCharge",&_eCharge);
  _EMuTree->Branch("eEta",&_eEta);
  _EMuTree->Branch("ePhi",&_ePhi);
  _EMuTree->Branch("eSigmaEtaEta",&_eSigmaEtaEta);
  _EMuTree->Branch("eSigmaIEtaIEta",&_eSigmaIEtaIEta);
  _EMuTree->Branch("eEOverP",&_eEOverP);
  _EMuTree->Branch("eHOverEm",&_eHOverEm);
  _EMuTree->Branch("eDeltaPhiIn", &_eDeltaPhiIn);
  _EMuTree->Branch("eDeltaEtaIn", &_eDeltaEtaIn);
  _EMuTree->Branch("eEcalIsoPat", &_eEcalIsoPat);
  _EMuTree->Branch("eHcalIsoPat", &_eHcalIsoPat);
  _EMuTree->Branch("eTrkIsoPat", &_eTrkIsoPat);
  _EMuTree->Branch("eIsoPat", &_eIsoPat);
  _EMuTree->Branch("heepTrkIso", &_heepTrkIso);
  _EMuTree->Branch("heepEcalIso", &_heepEcalIso);
  _EMuTree->Branch("heepHcalIso", &_heepHcalIso);
  _EMuTree->Branch("heepHcalIsoDepth1", &_heepHcalIsoDepth1);
  _EMuTree->Branch("heepHcalIsoDepth2", &_heepHcalIsoDepth2);
  _EMuTree->Branch("heepEcalHcalIsoDepth1", &_heepEcalHcalIsoDepth1);
  _EMuTree->Branch("eUserEcalIso", &_eUserEcalIso);
  _EMuTree->Branch("eUserHcalIso", &_eUserHcalIso);
  _EMuTree->Branch("eUserTrkIso", &_eUserTrkIso);
  _EMuTree->Branch("eSCE1x5", &_eSCE1x5);
  _EMuTree->Branch("eSCE2x5", &_eSCE2x5);
  _EMuTree->Branch("eSCE5x5", &_eSCE5x5);
  _EMuTree->Branch("eIp", &_eIp);
  _EMuTree->Branch("eIpError", &_eIpError);
  _EMuTree->Branch("eIp_ctf", &_eIp_ctf);
  _EMuTree->Branch("eIpError_ctf",&_eIpError_ctf);
  _EMuTree->Branch("eClass", &_eClass);
  _EMuTree->Branch("eMissingHits", &_eMissingHits);
  _EMuTree->Branch("eIdRobustTight", &_eIdRobustTight);
  _EMuTree->Branch("eIdRobustLoose", &_eIdRobustLoose);
  _EMuTree->Branch("eIdTight", &_eIdTight);
  _EMuTree->Branch("eIdLoose", &_eIdLoose);
  _EMuTree->Branch("eIdHighEnergy", &_eIdHighEnergy);
  _EMuTree->Branch("heepPassedEt", &_heepPassedEt);
  _EMuTree->Branch("heepPassedPt", &_heepPassedPt);
  _EMuTree->Branch("heepPassedDetEta", &_heepPassedDetEta);
  _EMuTree->Branch("heepPassedCrack", &_heepPassedCrack);
  _EMuTree->Branch("heepPassedDEtaIn", &_heepPassedDEtaIn);
  _EMuTree->Branch("heepPassedDPhiIn", &_heepPassedDPhiIn);
  _EMuTree->Branch("heepPassedHadem", &_heepPassedHadem);
  _EMuTree->Branch("heepPassedSigmaIEtaIEta", &_heepPassedSigmaIEtaIEta);
  _EMuTree->Branch("heepPassed2by5Over5By5", &_heepPassed2by5Over5By5);
  _EMuTree->Branch("heepPassedEcalHad1Iso", &_heepPassedEcalHad1Iso);
  _EMuTree->Branch("heepPassedHad2Iso", &_heepPassedHad2Iso);
  _EMuTree->Branch("heepPassedTrkIso", &_heepPassedTrkIso);
  _EMuTree->Branch("heepPassedEcalDriven", &_heepPassedEcalDriven);
  _EMuTree->Branch("heepPassedAllCuts", &_heepPassedAllCuts);
  _EMuTree->Branch("isEE", &_isEEv);
  _EMuTree->Branch("isEB", &_isEBv);
  _EMuTree->Branch("isEBEEGap",&_isEBEEGap) ;  
  _EMuTree->Branch("isEBEtaGap",&_isEBEtaGap); 
  _EMuTree->Branch("isEBPhiGap",&_isEBPhiGap) ; 
  _EMuTree->Branch("isEEDeeGap",&_isEEDeeGap) ; 
  _EMuTree->Branch("isEERingGap",&_isEERingGap) ;
  
  _EMuTree->Branch("mEt", &_mEt);
  _EMuTree->Branch("eMuMass",&_eMuMass);
  _EMuTree->Branch("eMuCosDPhi",&_eMuCosDPhi);
  _EMuTree->Branch("eMuDeltaR",&_eMuDelatR);
  _EMuTree->Branch("eMuMetMass",&_eMuMetMass);
  _EMuTree->Branch("eMuCollMass",&_eMuCollMass);
  _EMuTree->Branch("eMEtMass",&_eMEtMass);
  _EMuTree->Branch("muMEtMass",&_muMEtMass);
  _EMuTree->Branch("eMEtMt",&_eMEtMt);
  _EMuTree->Branch("muMEtMt",&_muMEtMt);
  
  _EMuTree->Branch("eMuPZetaVis",&_eMuPZetaVis);
  _EMuTree->Branch("eMuPZeta",&_eMuPZeta);
  
  //-------Jet Info
  _EMuTree->Branch("jetPt",  &_jetPt);
  _EMuTree->Branch("jetEt",  &_jetEt);
  _EMuTree->Branch("jetE",   &_jetE);
  _EMuTree->Branch("jetEta", &_jetEta); 
  _EMuTree->Branch("jetPhi", &_jetPhi);
  _EMuTree->Branch("jetEmFraction", &_jetEmFraction);
  _EMuTree->Branch("jetSumEt",&_jetSumEt);
  _EMuTree->Branch("jetMETSumEt",&_jetMETSumEt);
 
  //-----Gen resolution
  _EMuTree->Branch("egenPt",  &_egenPt);
  _EMuTree->Branch("egenE",   &_egenE);
  _EMuTree->Branch("egenPhi", &_egenPhi);
  _EMuTree->Branch("egenEta", &_egenEta);

  _EMuTree->Branch("muongenPt",  &_muongenPt);
  _EMuTree->Branch("muongenE",   &_muongenE);
  _EMuTree->Branch("muongenPhi", &_muongenPhi);
  _EMuTree->Branch("muongenEta", &_muongenEta);

  _EMuTree->Branch("eEcalDriven",        &_eEcalDrivenv);
  _EMuTree->Branch("eTrkDriven",         &_eTrkDrivenv);
  
  _EMuTree->Branch("nBtagsHiEffTrkCnt",&_nBtagsHiEffTrkCnt);
  _EMuTree->Branch("nBtagsHiPurityTrkCnt",&_nBtagsHiPurityTrkCnt);
  _EMuTree->Branch("nBTagsHiEffSimpleSecVtx",&_nBTagsHiEffSimpleSecVtx);
  _EMuTree->Branch("nBTagsHiPuritySimpleSecVtx",&_nBTagsHiPuritySimpleSecVtx);
  _EMuTree->Branch("nBTagsCombSecVtx",&_nBTagsCombSecVtx);
  
  _EMuTree->Branch("extraTrkPtSum",&_extraTrkPtSum);
  _EMuTree->Branch("leptonMetCosDphi",&_leptonMetCosDphi);
  _EMuTree->Branch("nJets",&_nJets);
  
  _EMuTree->Branch("jetBtagHiDiscByTrkCntHiEff",&_jetBtagHiDiscByTrkCntHiEff);
  _EMuTree->Branch("jetBtagHiDiscByTrkCntHiPurity",&_jetBtagHiDiscByTrkCntHiPurity);
  _EMuTree->Branch("jetBtagHiDiscBySimpleSecVtxHiEff",&_jetBtagHiDiscBySimpleSecVtxHiEff);
  _EMuTree->Branch("jetBtagHiDiscBySimpleSecVtxHiPurity",&_jetBtagHiDiscBySimpleSecVtxHiPurity);
  _EMuTree->Branch("jetBtagHiDiscByCombSecVtx",&_jetBtagHiDiscByCombSecVtx);
}

void EMuAnalysis::initializeVectors(){
  _muonMatched = NULL;
  _muonMotherId = NULL;
  _muonE = NULL;
  _muonEt = NULL; 
  _muonPt = NULL; 
  _muonCharge = NULL;
  _muonEta = NULL;
  _muonPhi = NULL;
  
  _muonIpVtx = NULL;
  _muonIpVtxError = NULL;
  
  _muonValidHits = NULL;
  _muonNormChiSqrd = NULL;
  _muonEcalIsoDr03 = NULL;
  _muonTrkIsoDr03 = NULL;
  _muonCaloIsoDr03 = NULL;
  _muonEcalIsoDr04 = NULL;
  _muonTrkIsoDr04 = NULL;
  _muonEcalIsoDr05 = NULL;
  _muonHadIsoDr05 = NULL; 
  _muonTrkIsoDr05 = NULL; 
  _muonCaloIsoDr05 = NULL;
  _muonCaloComp = NULL;  
  _muonIsGlobalMuon = NULL;
  _muonisTrackerMuon = NULL;
  _muonIsGlobalPromptTight = NULL;
  _muonIsTMLastStationLoose = NULL;
  _muonIsTMLastStationTight = NULL;
  _muonIsTM2DCompatibilityLoose = NULL;
  _muonIsTM2DCompatibilityTight = NULL;
  _muonPionVeto = NULL;
  _muonInJet = NULL;

  _eMatched = NULL;
  _ePdgId = NULL;
  _eMotherPdgId = NULL;
  _eE = NULL;
  _eEt = NULL;
  _ePt = NULL;
  _eCharge = NULL;
  _eEta = NULL;
  _ePhi = NULL;
  _eSigmaEtaEta = NULL;
  _eSigmaIEtaIEta = NULL;
  _eEOverP = NULL;
  _eHOverEm = NULL;
  _eDeltaPhiIn = NULL;
  _eDeltaEtaIn = NULL;
  _eEcalIsoPat = NULL;
  _eHcalIsoPat = NULL;
  _eTrkIsoPat = NULL;
  _eIsoPat = NULL;
  _heepTrkIso = NULL;
  _heepEcalIso = NULL;
  _heepHcalIso = NULL;
  _heepHcalIsoDepth1 = NULL;	 
  _heepHcalIsoDepth2 = NULL;	 
  _heepEcalHcalIsoDepth1 = NULL;
  _eUserEcalIso = NULL;
  _eUserTrkIso = NULL;
  _eUserHcalIso = NULL;  
  _eSCE1x5 = NULL;
  _eSCE2x5 = NULL;
  _eSCE5x5 = NULL;
  _eIp = NULL;
  _eIpError = NULL;
  _eIp_ctf = NULL; 
  _eIpError_ctf = NULL;
  _eClass = NULL;
  _eMissingHits = NULL;
  _eIdRobustTight = NULL;
  _eIdRobustLoose = NULL;
  _eIdTight = NULL;
  _eIdLoose = NULL;
  _eIdHighEnergy = NULL;
  _heepPassedEt = NULL;
  _heepPassedPt = NULL;
  _heepPassedDetEta = NULL;
  _heepPassedCrack = NULL;
  _heepPassedDEtaIn = NULL;
  _heepPassedDPhiIn = NULL;
  _heepPassedHadem = NULL;
  _heepPassedSigmaIEtaIEta = NULL;
  _heepPassed2by5Over5By5 = NULL;
  _heepPassedEcalHad1Iso = NULL;
  _heepPassedHad2Iso = NULL;
  _heepPassedTrkIso = NULL;
  _heepPassedEcalDriven = NULL;
  _heepPassedAllCuts = NULL;
  
  _isEEv = NULL;
  _isEBv = NULL;
  _isEBEEGap = NULL;  
  _isEBEtaGap = NULL; 
  _isEBPhiGap = NULL; 
  _isEEDeeGap = NULL; 
  _isEERingGap = NULL;

  _mEt = NULL;
  _eMuMass = NULL;
  _eMuCosDPhi = NULL;
  _eMuDelatR = NULL;
  _eMuMetMass = NULL;
  _eMuCollMass = NULL;
  _eMEtMass = NULL;
  _muMEtMass = NULL;
  _eMEtMt = NULL;
  _muMEtMt = NULL;
 
  
  _eMuPZetaVis = NULL;
  _eMuPZeta = NULL; 
  //------Jet info
  _jetPt = NULL;
  _jetEt = NULL;
  _jetE = NULL;
  _jetPhi = NULL;
  _jetEta = NULL;
  _jetEmFraction = NULL;

  //-----Gen resolution
  _egenPt = NULL;
  _egenE = NULL;
  _egenEta = NULL;
  _egenPhi = NULL;

  _muongenPt = NULL;
  _muongenE = NULL;
  _muongenEta = NULL;
  _muongenPhi = NULL;

  _eEcalDrivenv = NULL;
  _eTrkDrivenv = NULL;

  _extraTrkPtSum = NULL;  
  _leptonMetCosDphi = NULL;
  
  _nBtagsHiEffTrkCnt = NULL;		     	 
  _nBtagsHiPurityTrkCnt = NULL;	     	 
  _nBTagsHiEffSimpleSecVtx = NULL;      	 
  _nBTagsHiPuritySimpleSecVtx = NULL;	 
  _nBTagsCombSecVtx = NULL;		     	 
  _jetSumEt = NULL;				     	 
  _jetMETSumEt = NULL;	 
  _nJets = NULL;    	 
}

void EMuAnalysis::clearVectors(){
  _muonMotherId->clear();
  _muonMatched->clear();
  _muonE->clear();
  _muonEt->clear(); 
  _muonPt->clear(); 
  _muonCharge->clear();
  _muonEta->clear();
  _muonPhi->clear();
  
  _muonIpVtx->clear();
  _muonIpVtxError->clear();
  
  _muonValidHits->clear();
  _muonNormChiSqrd->clear();
  _muonEcalIsoDr03->clear();
  _muonTrkIsoDr03->clear();
  _muonCaloIsoDr03->clear();
  
  _muonEcalIsoDr04->clear();
  _muonTrkIsoDr04->clear();
  
  _muonEcalIsoDr05->clear();
  _muonHadIsoDr05->clear(); 
  _muonTrkIsoDr05->clear(); 
  _muonCaloIsoDr05->clear();
  _muonCaloComp->clear();  
  _muonIsGlobalMuon->clear();
  _muonisTrackerMuon->clear();
  _muonIsGlobalPromptTight->clear();
  _muonIsTMLastStationLoose->clear();
  _muonIsTMLastStationTight->clear();
  _muonIsTM2DCompatibilityLoose->clear();
  _muonIsTM2DCompatibilityTight->clear();
  _muonPionVeto->clear();
  _muonInJet->clear();

  _eMatched->clear();
  _ePdgId->clear();
  _eMotherPdgId->clear();
  _eE->clear();
  _eEt->clear();
  _ePt->clear();
  _eCharge->clear();
  _eEta->clear();
  _ePhi->clear();
  _eSigmaEtaEta->clear();
  _eSigmaIEtaIEta->clear();
  _eEOverP->clear();
  _eHOverEm->clear();
  _eDeltaPhiIn->clear();
  _eDeltaEtaIn->clear();
  _eEcalIsoPat->clear();
  _eHcalIsoPat->clear();
  _eTrkIsoPat->clear();
  _eIsoPat->clear();
  _heepTrkIso->clear();
  _heepEcalIso->clear();
  _heepHcalIso->clear();
  _heepHcalIsoDepth1->clear();	 
  _heepHcalIsoDepth2->clear();	 
  _heepEcalHcalIsoDepth1->clear();
  _eUserEcalIso->clear();
  _eUserTrkIso->clear();
  _eUserHcalIso->clear();
 
  _eSCE1x5->clear();
  _eSCE2x5->clear();
  _eSCE5x5->clear();
  _eIp->clear();
  _eIpError->clear();
  _eIp_ctf->clear();
  _eIpError_ctf->clear();
  _eClass->clear();
  _eMissingHits->clear();
  _eIdRobustTight->clear();
  _eIdRobustLoose->clear();
  _eIdTight->clear();
  _eIdLoose->clear();
  _eIdHighEnergy->clear();
  _heepPassedEt->clear();
  _heepPassedPt->clear();
  _heepPassedDetEta->clear();
  _heepPassedCrack->clear();
  _heepPassedDEtaIn->clear();
  _heepPassedDPhiIn->clear();
  _heepPassedHadem->clear();
  _heepPassedSigmaIEtaIEta->clear();
  _heepPassed2by5Over5By5->clear();
  _heepPassedEcalHad1Iso->clear();
  _heepPassedHad2Iso->clear();
  _heepPassedTrkIso->clear();
  _heepPassedEcalDriven->clear();
  _heepPassedAllCuts->clear();

  _isEEv->clear();
  _isEBv->clear();
  _isEBEEGap->clear();
  _isEBEtaGap->clear();
  _isEBPhiGap->clear();
  _isEEDeeGap->clear();
  _isEERingGap->clear();
  _mEt->clear();
    
  _eMuMass->clear();
  _eMuCosDPhi->clear();
  _eMuDelatR->clear();
  _eMuMetMass->clear();
  _eMuCollMass->clear();
  _eMuPZetaVis->clear();
  _eMuPZeta->clear();
  _eMEtMass->clear();
  _muMEtMass->clear();
  _eMEtMt->clear();
  _muMEtMt->clear();
  

    //------Jet info
  _jetPt->clear();
  _jetEt->clear();
  _jetE->clear();
  _jetPhi->clear();
  _jetEta->clear();
  _jetEmFraction->clear();

  //-----Gen resolution
  _egenPt->clear();
  _egenE->clear();
  _egenEta->clear();
  _egenPhi->clear();

  _muongenPt->clear();
  _muongenE->clear();
  _muongenEta->clear();
  _muongenPhi->clear();

  _eEcalDrivenv->clear();
  _eTrkDrivenv ->clear();

  _extraTrkPtSum->clear();
  _leptonMetCosDphi->clear();
  _nBtagsHiEffTrkCnt->clear();		     	 
  _nBtagsHiPurityTrkCnt->clear();	     	 
  _nBTagsHiEffSimpleSecVtx->clear();      	 
  _nBTagsHiPuritySimpleSecVtx->clear();	 
  _nBTagsCombSecVtx->clear();		     	 
  _jetSumEt->clear();				     	 
  _jetMETSumEt->clear();
  _nJets->clear();	     	 
}
  
//define this as a plug-in
DEFINE_FWK_MODULE(EMuAnalysis);
