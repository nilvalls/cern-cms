#include "HighMassAnalysis/Analysis/interface/ElecTauTauAnalysis.h"
#include "SHarper/HEEPAnalyzer/interface/HEEPEle.h"
#include "SHarper/HEEPAnalyzer/interface/HEEPCutCodes.h"

using namespace edm;
using namespace reco;
using namespace std;

ElecTauTauAnalysis::ElecTauTauAnalysis(const edm::ParameterSet& iCongif): HiMassTauAnalysis(iCongif){} 
ElecTauTauAnalysis::~ElecTauTauAnalysis(){}
  
void ElecTauTauAnalysis::fillNtuple(){
  // store vector of PDF weights
  _PDFWeights = pdfWeightVector;

  // store ISR weights
  _ISRGluonWeight = isrgluon_weight;
  _ISRGammaWeight = isrgamma_weight;

  // store FSR weights
  _FSRWeight = fsr_weight;
  
  _genLevelElecPt = -5.;
  _genLevelElecEta = -10.;
  _genLevelElecPhi = -10.;
  _genLevelTau1Pt = -5.;
  _genLevelTau1Eta = -10.;
  _genLevelTau1Phi = -10.;
  _genLevelTau2Pt = -5.;
  _genLevelTau2Eta = -10.;
  _genLevelTau2Phi = -10.;
  
  if(_AnalyzeSignal){
    //get the genParticles
    pair<bool, reco::Candidate*> theGenElecPair = GetGenCandidate(11, 24, 3);
    if(!theGenElecPair.first) theGenElecPair = GetGenCandidate(-11, 24, 3);
    if(theGenElecPair.first){
      _genLevelElecPt = theGenElecPair.second->pt();
      _genLevelElecEta = theGenElecPair.second->eta();
      _genLevelElecPhi = theGenElecPair.second->phi();
    }
  
    pair<bool, reco::Candidate*> theGenTau1Pair = GetGenCandidate(15, 25, 3);
    if(theGenTau1Pair.first){
      _genLevelTau1Pt = theGenTau1Pair.second->pt();
      _genLevelTau1Eta = theGenTau1Pair.second->eta();
      _genLevelTau1Phi = theGenTau1Pair.second->phi();
    }
  
    pair<bool, reco::Candidate*> theGenTau2Pair = GetGenCandidate(-15, 25, 3);
    if(theGenTau2Pair.first){
      _genLevelTau2Pt = theGenTau2Pair.second->pt();
      _genLevelTau2Eta = theGenTau2Pair.second->eta();
      _genLevelTau2Phi = theGenTau2Pair.second->phi();
    }

  }
  // get the primary vertex
  const reco::Vertex& thePrimaryEventVertex = (*(_primaryEventVertexCollection)->begin());
  const reco::BeamSpot& theBeamSpot = *_beamSpot;
  _nVtx = _primaryEventVertexCollection->size();
  //count good vertices 
  _nGoodVtx = 0;
  for( reco::VertexCollection::const_iterator vtxIt = _primaryEventVertexCollection->begin(); vtxIt != _primaryEventVertexCollection->end(); ++vtxIt){    
    if(vtxIt->ndof() < 7.) continue;
    if(abs(vtxIt->z()) > 24.) continue;
   // if(passRecoVertexCuts(*vtxIt))
    double vtxdxy = sqrt((vtxIt->x()*vtxIt->x()) + (vtxIt->y()*vtxIt->y()));
    if(vtxdxy > 2.0) continue; 
    _nGoodVtx++;
  }
  
  //get MET
  const pat::MET theMET = *(_patMETs->begin());
  _MEt = theMET.pt();
  _zEvent = foundZMuMuOrZEE().first;
  _zMassDiff = foundZMuMuOrZEE().second.first;
  _zPtAsymm = foundZMuMuOrZEE().second.second;
  
  //get number of btagged jets passing quality criteria
  _nBtagsHiEffTrkCnt = 0;
  _nBTagsHiEffSimpleSecVtx = 0;
  _nBtagsHiPurityTrkCnt = 0;
  _nBTagsHiPuritySimpleSecVtx = 0;
  _nBTagsCombSecVtx = 0;
  // loop over jet collection excluding jets in the direction of my di-tau cands
  for(pat::JetCollection::const_iterator patJet = _patJets->begin(); patJet != _patJets->end(); ++patJet){
    if(patJet->et() < _RecoJetPtCut) continue;
    if(fabs(patJet->eta()) >_RecoJetEtaMaxCut) continue;
    if(patJet->numberOfDaughters() <= 1) continue;
    if(patJet->neutralHadronEnergyFraction() > 0.99) continue;
    if(patJet->neutralEmEnergyFraction() > 0.99) continue;
    
    float trkCntHighEffBtag =  patJet->bDiscriminator("trackCountingHighEffBJetTags");
    float trkCntHighPurityBtag =  patJet->bDiscriminator("trackCountingHighPurBJetTags");
    float simpleSecVtxHighEffBTag = patJet->bDiscriminator("simpleSecondaryVertexHighEffBJetTags");
    float simpleSecVtxHighPurityBTag = patJet->bDiscriminator("simpleSecondaryVertexHighPurBJetTags");
    float combSecVtxBTag = patJet->bDiscriminator("combinedSecondaryVertexBJetTags");
    //use medium wp 
    if(trkCntHighEffBtag > 3.3) _nBtagsHiEffTrkCnt++;		       
    if(trkCntHighPurityBtag > 1.93) _nBtagsHiPurityTrkCnt++;		       
    if(simpleSecVtxHighEffBTag > 3.05) _nBTagsHiEffSimpleSecVtx++;	       
    if(simpleSecVtxHighPurityBTag > 2.0) _nBTagsHiPuritySimpleSecVtx++;  	       
    if(combSecVtxBTag > 0.5) _nBTagsCombSecVtx++; 
  }
  
  double _minLeadTauPt = _RecoTauPtMinCut;
  double _minSubLeadTauPt = _RecoTau2PtMinCut;
  double _maxTauEta = _RecoTauEtaCut;
  _nTauPairCounter = 0;
  for(pat::ElectronCollection::const_iterator patElec = _patElectrons->begin(); patElec != _patElectrons->end(); ++patElec){
    //check the stored ids
    if(_AnalyzeSignal && !matchToGen(*patElec, 11).first) continue;		       
    if(patElec->pt() < _RecoElectronPtMinCut) continue; 
    if(fabs(patElec->eta()) > _RecoElectronEtaCut) continue;
    for(CompositeCandidateCollection::const_iterator theCand = _patDiTaus->begin(); theCand != _patDiTaus->end(); ++theCand){	       
      pair<const pat::Tau*, const pat::Tau*> theCandDaughters = getPATComponents(*theCand);					       
      const pat::Tau* theLeadTau = theCandDaughters.first;									       
      const pat::Tau* theSubLeadTau = theCandDaughters.second;									       

      if(theCandDaughters.first->pt() < theCandDaughters.second->pt()){ 							       
  	theLeadTau = theCandDaughters.second;											       
  	theSubLeadTau = theCandDaughters.first; 										       
      } 															       
      if(_AnalyzeSignal && !matchToGen(*theLeadTau).first) continue;
      if(theLeadTau->pt() < _minLeadTauPt) continue;
      if(_AnalyzeSignal && !matchToGen(*theSubLeadTau).first) continue;
      if(theSubLeadTau->pt() < _minSubLeadTauPt) continue;
      if(abs(theLeadTau->eta()) > _maxTauEta || abs(theSubLeadTau->eta()) > _maxTauEta) continue;								    	
      if(reco::deltaR(*theLeadTau, *theSubLeadTau) < _DiTauDeltaRCut) continue;							 			    	
       _nTauPairCounter++;
      //LeadTau Info																		      
      _leadTauMatched->push_back(!_AnalyzeData ? (unsigned int)matchToGen(*theLeadTau).first : 0);								      
      _leadTauMotherId->push_back(!_AnalyzeData && theLeadTau->genParticleRef().isNonnull() ? abs(theLeadTau->genParticleRef()->mother()->mother()->pdgId()) : 0);    
      _leadTauGenPt->push_back(!_AnalyzeData ? matchToGen(*theLeadTau).second.pt() : 0);									      
      _leadTauGenE->push_back(!_AnalyzeData ? matchToGen(*theLeadTau).second.energy() : 0);									      
      _leadTauGenEta->push_back(!_AnalyzeData ? matchToGen(*theLeadTau).second.eta() : -10);									      
      _leadTauGenPhi->push_back(!_AnalyzeData ? matchToGen(*theLeadTau).second.phi() : -10);									      
    
      _leadTauPt->push_back(theLeadTau->pt());  														      
      _leadTauEnergy->push_back(theLeadTau->energy());  													      
      _leadTauEta->push_back(theLeadTau->eta());														      
      _leadTauPhi->push_back(theLeadTau->phi());														      
      _leadTauCharge->push_back(theLeadTau->charge());  													      
      const reco::PFCandidateRef theLeadTauLeadChargedCand = theLeadTau->leadPFChargedHadrCand();								      
      _leadTauLeadChargedCandPt->push_back(theLeadTauLeadChargedCand.isNonnull() ? theLeadTauLeadChargedCand->pt() : -1.);					      
      _leadTauLeadChargedCandCharge->push_back(theLeadTauLeadChargedCand.isNonnull() ? theLeadTauLeadChargedCand->charge() : -2);				      
      //_leadTauLeadChargedCandRecHitsSize->push_back(theLeadTauLeadChargedCand->trackRef() ? theLeadTauLeadChargedCand->trackRef()->recHitsSize(): 0.);	      
      _leadTauLeadChargedCandDxyVtx->push_back(theLeadTauLeadChargedCand.isNonnull() && theLeadTauLeadChargedCand->trackRef().isNonnull() ? theLeadTauLeadChargedCand->trackRef()->dxy(thePrimaryEventVertex.position()) : -100);
      _leadTauLeadChargedCandDxyBS->push_back(theLeadTauLeadChargedCand.isNonnull() && theLeadTauLeadChargedCand->trackRef().isNonnull() ? theLeadTauLeadChargedCand->trackRef()->dxy(theBeamSpot) : -100);
      _leadTauLeadChargedCandDxyError->push_back(theLeadTauLeadChargedCand.isNonnull() && theLeadTauLeadChargedCand->trackRef().isNonnull() ? theLeadTauLeadChargedCand->trackRef()->d0Error() : -10000.);
      _leadTauLeadChargedCandDzVtx->push_back(theLeadTauLeadChargedCand.isNonnull() && theLeadTauLeadChargedCand->trackRef().isNonnull() ? theLeadTauLeadChargedCand->trackRef()->dz(thePrimaryEventVertex.position()) : -100);
      _leadTauLeadChargedCandDzBS->push_back(theLeadTauLeadChargedCand.isNonnull() && theLeadTauLeadChargedCand->trackRef().isNonnull() ? theLeadTauLeadChargedCand->trackRef()->dz(theBeamSpot.position()) : -100);
      _leadTauLeadChargedCandDzError->push_back(theLeadTauLeadChargedCand.isNonnull() && theLeadTauLeadChargedCand->trackRef().isNonnull() ? theLeadTauLeadChargedCand->trackRef()->dzError() : -10000.);
      _leadTauLeadChargedCandVx->push_back(theLeadTauLeadChargedCand.isNonnull() && theLeadTauLeadChargedCand->trackRef().isNonnull() ? theLeadTauLeadChargedCand->trackRef()->vx() : -100.);
      _leadTauLeadChargedCandVy->push_back(theLeadTauLeadChargedCand.isNonnull() && theLeadTauLeadChargedCand->trackRef().isNonnull() ? theLeadTauLeadChargedCand->trackRef()->vy() : -100.);
      _leadTauLeadChargedCandVz->push_back(theLeadTauLeadChargedCand.isNonnull() && theLeadTauLeadChargedCand->trackRef().isNonnull() ? theLeadTauLeadChargedCand->trackRef()->vz() : -100.);
      _leadTauDeltaZVtx->push_back(theLeadTau->vz() - thePrimaryEventVertex.z());
      _leadTauNProngs->push_back(theLeadTau->signalPFChargedHadrCands().size());
      _leadTauEmFraction->push_back(theLeadTau->emFraction());
      _leadTauHcalTotOverPLead->push_back(theLeadTau->hcalTotOverPLead());
      _leadTauHcalMaxOverPLead->push_back(theLeadTau->hcalMaxOverPLead());
      _leadTauHcal3x3OverPLead->push_back(theLeadTau->hcal3x3OverPLead());
      _leadTauElectronPreId->push_back(theLeadTau->electronPreIDDecision());
      _leadTauModifiedEOverP->push_back(theLeadTau->ecalStripSumEOverPLead());
      _leadTauBremsRecoveryEOverPLead->push_back(theLeadTau->bremsRecoveryEOverPLead());
      _leadTauLTSignedIp->push_back(theLeadTau->leadPFChargedHadrCandsignedSipt());
      _leadTauTrkIsoSumPt->push_back(theLeadTau->isolationPFChargedHadrCandsPtSum());
      _leadTauECALIsoSumEt->push_back(theLeadTau->isolationPFGammaCandsEtSum());      
      _leadTauIdByDecayModeFinding->push_back((unsigned int)theLeadTau->tauID("decayModeFinding"));
      _leadTauIdByVLooseIsolation->push_back((unsigned int)theLeadTau->tauID("byVLooseIsolation"));
      _leadTauIdByLooseIsolation->push_back((unsigned int)theLeadTau->tauID("byLooseIsolation"));
      _leadTauIdByMediumIsolation->push_back((unsigned int)theLeadTau->tauID("byMediumIsolation"));
      _leadTauIdByTightIsolation->push_back((unsigned int)theLeadTau->tauID("byTightIsolation"));
      _leadTauIdByVLooseCombinedIsolationDBSumPtCorr->push_back((unsigned int)theLeadTau->tauID("byVLooseCombinedIsolationDeltaBetaCorr"));
      _leadTauIdByLooseCombinedIsolationDBSumPtCorr->push_back((unsigned int)theLeadTau->tauID("byVLooseIsolationDeltaBetaCorr"));
      _leadTauIdByMediumCombinedIsolationDBSumPtCorr->push_back((unsigned int)theLeadTau->tauID("byMediumIsolationDeltaBetaCorr"));
      _leadTauIdByTightCombinedIsolationDBSumPtCorr->push_back((unsigned int)theLeadTau->tauID("byTightCombinedIsolationDeltaBetaCorr"));
      _leadTauIdByLooseElectronRejection->push_back((unsigned int)theLeadTau->tauID("againstElectronLoose"));
      _leadTauIdByMediumElectronRejection->push_back((unsigned int)theLeadTau->tauID("againstElectronMedium"));
      _leadTauIdByTightElectronRejection->push_back((unsigned int)theLeadTau->tauID("againstElectronTight"));
      _leadTauIdByLooseElecRejection->push_back((unsigned int)theLeadTau->tauID("againstElectronLoose"));
      _leadTauIdByTightElecRejection->push_back((unsigned int)theLeadTau->tauID("againstElectronTight"));
      _leadTauMEtMt->push_back(CalculateLeptonMetMt(*theLeadTau, theMET));
    
      //SubLeadTauInfo
      _subLeadTauMatched->push_back(!_AnalyzeData ? (unsigned int)matchToGen(*theSubLeadTau).first : 0);						
      _subLeadTauMotherId->push_back(!_AnalyzeData && theSubLeadTau->genParticleRef().isNonnull() ? abs(theSubLeadTau->genParticleRef()->mother()->mother()->pdgId()) : 0); 
      _subLeadTauGenPt->push_back(!_AnalyzeData ? matchToGen(*theSubLeadTau).second.pt() : 0);  					
      _subLeadTauGenE->push_back(!_AnalyzeData ? matchToGen(*theSubLeadTau).second.energy() : 0);						
      _subLeadTauGenEta->push_back(!_AnalyzeData ? matchToGen(*theSubLeadTau).second.eta() : -10);						
      _subLeadTauGenPhi->push_back(!_AnalyzeData ? matchToGen(*theSubLeadTau).second.phi() : -10);						
    
      _subLeadTauPt->push_back(theSubLeadTau->pt());
      _subLeadTauEnergy->push_back(theSubLeadTau->energy());
      _subLeadTauEta->push_back(theSubLeadTau->eta());
      _subLeadTauPhi->push_back(theSubLeadTau->phi());
      _subLeadTauCharge->push_back(theSubLeadTau->charge());
      const reco::PFCandidateRef theSubLeadTauLeadChargedCand = theSubLeadTau->leadPFChargedHadrCand();
      _subLeadTauLeadChargedCandPt->push_back(theSubLeadTauLeadChargedCand.isNonnull() ? theSubLeadTauLeadChargedCand->pt() : -1.);
      _subLeadTauLeadChargedCandCharge->push_back(theSubLeadTauLeadChargedCand.isNonnull() ? theSubLeadTauLeadChargedCand->charge() : -2);
      //_subLeadTauLeadChargedCandRecHitsSize->push_back(theSubLeadTau->leadTrack().isNonnull() ? theSubLeadTau->leadTrack()->recHitsSize(): 0.);
      _subLeadTauLeadChargedCandDxyVtx->push_back(theSubLeadTauLeadChargedCand.isNonnull() && theSubLeadTauLeadChargedCand->trackRef().isNonnull() ? theSubLeadTauLeadChargedCand->trackRef()->dxy(thePrimaryEventVertex.position()) : -100);
      _subLeadTauLeadChargedCandDxyBS->push_back(theSubLeadTauLeadChargedCand.isNonnull() && theSubLeadTauLeadChargedCand->trackRef().isNonnull() ? theSubLeadTauLeadChargedCand->trackRef()->dxy(theBeamSpot) : -100);
      _subLeadTauLeadChargedCandDxyError->push_back(theSubLeadTauLeadChargedCand.isNonnull() && theSubLeadTauLeadChargedCand->trackRef().isNonnull() ? theSubLeadTauLeadChargedCand->trackRef()->d0Error() : -10000.);
      _subLeadTauLeadChargedCandDzVtx->push_back(theSubLeadTauLeadChargedCand.isNonnull() && theSubLeadTauLeadChargedCand->trackRef().isNonnull() ? theSubLeadTauLeadChargedCand->trackRef()->dz(thePrimaryEventVertex.position()) : -100);
      _subLeadTauLeadChargedCandDzBS->push_back(theSubLeadTauLeadChargedCand.isNonnull() && theSubLeadTauLeadChargedCand->trackRef().isNonnull() ? theSubLeadTauLeadChargedCand->trackRef()->dz(theBeamSpot.position()) : -100);
      _subLeadTauLeadChargedCandDzError->push_back(theSubLeadTauLeadChargedCand.isNonnull() && theSubLeadTauLeadChargedCand->trackRef().isNonnull() ? theSubLeadTauLeadChargedCand->trackRef()->dzError() : -10000.);
      _subLeadTauLeadChargedCandVx->push_back(theSubLeadTauLeadChargedCand.isNonnull() && theSubLeadTauLeadChargedCand->trackRef().isNonnull() ? theSubLeadTauLeadChargedCand->trackRef()->vx() : -100.);
      _subLeadTauLeadChargedCandVy->push_back(theSubLeadTauLeadChargedCand.isNonnull() && theSubLeadTauLeadChargedCand->trackRef().isNonnull() ? theSubLeadTauLeadChargedCand->trackRef()->vy() : -100.);
      _subLeadTauLeadChargedCandVz->push_back(theSubLeadTauLeadChargedCand.isNonnull() && theSubLeadTauLeadChargedCand->trackRef().isNonnull() ? theSubLeadTauLeadChargedCand->trackRef()->vz() : -100.);
      _subLeadTauDeltaZVtx->push_back(theSubLeadTau->vz() - thePrimaryEventVertex.z());
      _subLeadTauNProngs->push_back(theSubLeadTau->signalPFChargedHadrCands().size());
      _subLeadTauEmFraction->push_back(theSubLeadTau->emFraction());										     
      _subLeadTauHcalTotOverPLead->push_back(theSubLeadTau->hcalTotOverPLead());							     
      _subLeadTauHcalMaxOverPLead->push_back(theSubLeadTau->hcalMaxOverPLead());							     
      _subLeadTauHcal3x3OverPLead->push_back(theSubLeadTau->hcal3x3OverPLead());							     
      _subLeadTauElectronPreId->push_back(theSubLeadTau->electronPreIDDecision());								     
      _subLeadTauModifiedEOverP->push_back(theSubLeadTau->ecalStripSumEOverPLead());								     
      _subLeadTauBremsRecoveryEOverPLead->push_back(theSubLeadTau->bremsRecoveryEOverPLead());  					     
      _subLeadTauLTSignedIp->push_back(theSubLeadTau->leadPFChargedHadrCandsignedSipt());
      _subLeadTauTrkIsoSumPt->push_back(theSubLeadTau->isolationPFChargedHadrCandsPtSum());
      _subLeadTauECALIsoSumEt->push_back(theSubLeadTau->isolationPFGammaCandsEtSum());
      _subLeadTauIdByDecayModeFinding->push_back((unsigned int)theSubLeadTau->tauID("decayModeFinding"));
      _subLeadTauIdByVLooseIsolation->push_back((unsigned int)theSubLeadTau->tauID("byVLooseIsolation"));
      _subLeadTauIdByLooseIsolation->push_back((unsigned int)theSubLeadTau->tauID("byLooseIsolation"));
      _subLeadTauIdByMediumIsolation->push_back((unsigned int)theSubLeadTau->tauID("byMediumIsolation"));
      _subLeadTauIdByTightIsolation->push_back((unsigned int)theSubLeadTau->tauID("byTightIsolation"));
      _subLeadTauIdByVLooseCombinedIsolationDBSumPtCorr->push_back((unsigned int)theSubLeadTau->tauID("byVLooseCombinedIsolationDeltaBetaCorr"));
      _subLeadTauIdByLooseCombinedIsolationDBSumPtCorr->push_back((unsigned int)theSubLeadTau->tauID("byVLooseIsolationDeltaBetaCorr"));
      _subLeadTauIdByMediumCombinedIsolationDBSumPtCorr->push_back((unsigned int)theSubLeadTau->tauID("byMediumIsolationDeltaBetaCorr"));
      _subLeadTauIdByTightCombinedIsolationDBSumPtCorr->push_back((unsigned int)theSubLeadTau->tauID("byTightCombinedIsolationDeltaBetaCorr"));
      _subLeadTauIdByLooseElectronRejection->push_back((unsigned int)theSubLeadTau->tauID("againstElectronLoose"));
      _subLeadTauIdByMediumElectronRejection->push_back((unsigned int)theSubLeadTau->tauID("againstElectronMedium"));
      _subLeadTauIdByTightElectronRejection->push_back((unsigned int)theSubLeadTau->tauID("againstElectronTight"));
      _subLeadTauIdByLooseElecRejection->push_back((unsigned int)theSubLeadTau->tauID("againstElectronLoose"));
      _subLeadTauIdByTightElecRejection->push_back((unsigned int)theSubLeadTau->tauID("againstElectronTight"));
      _subLeadTauMEtMt->push_back(CalculateLeptonMetMt(*theSubLeadTau, theMET));
    
      //DiTauInfo
      _diHadTauCosDPhi->push_back(cos(TMath::Abs(normalizedPhi(theLeadTau->phi() - theSubLeadTau->phi()))));	  
      _diHadTauDelatR->push_back(reco::deltaR(*theLeadTau, *theSubLeadTau));
      _diHadTauPZeta->push_back(CalculatePZeta(*theLeadTau,*theSubLeadTau, theMET));
      _diHadTauPZetaVis->push_back(CalculatePZetaVis(*theLeadTau,*theSubLeadTau));

      _diHadTauMass->push_back(GetVisMass(*theLeadTau,*theSubLeadTau));
      _diHadTauMetMass->push_back(GetVisPlusMETMass(*theLeadTau,*theSubLeadTau, theMET));    
      _diHadTauCollMass->push_back(GetCollinearApproxMass(*theLeadTau,*theSubLeadTau, theMET));
      
      _diHadCandPt->push_back(theCand->pt());
      _diHadCandEt->push_back(theCand->et());
      _diHadCandEta->push_back(theCand->eta());
      _diHadCandPhi->push_back(theCand->phi());
      _diHadCandMass->push_back(theCand->mass());
      _diHadCandCharge->push_back(theCand->charge());
    
      //get Elec info and pair them with the taus
      _elecMatched->push_back(!_AnalyzeData ? (unsigned int)matchToGen(*patElec, 11).first : 0);							
      _elecMotherId->push_back(!_AnalyzeData && patElec->genParticleRef().isNonnull() ? abs(patElec->genParticleRef()->mother()->mother()->pdgId()) : 0);			 
      _elecGenPt->push_back(!_AnalyzeData ? matchToGen(*patElec, 11).second.pt() : 0);  					
      _elecGenE->push_back(!_AnalyzeData ? matchToGen(*patElec, 11).second.energy() : 0);						
      _elecGenEta->push_back(!_AnalyzeData ? matchToGen(*patElec, 11).second.eta() : -10);						
      _elecGenPhi->push_back(!_AnalyzeData ? matchToGen(*patElec, 11).second.phi() : -10);						
     
      _elecPt->push_back(patElec->pt());												       
      _elecE->push_back(patElec->energy());											       
      _elecEta->push_back(patElec->eta());											       
      _elecPhi->push_back(patElec->phi());
      _elecCharge->push_back(patElec->charge());											       
      _elecDxyVtx->push_back(patElec->gsfTrack().isNonnull() ? patElec->gsfTrack()->dxy(thePrimaryEventVertex.position()) : -100.);
      _elecDxyBS->push_back(patElec->gsfTrack().isNonnull() ? patElec->gsfTrack()->dxy(theBeamSpot) : -100.);
      _elecDxyError->push_back(patElec->gsfTrack().isNonnull() ? patElec->gsfTrack()->d0Error() : -10000.);
      _elecDzVtx->push_back(patElec->gsfTrack().isNonnull() ? patElec->gsfTrack()->dz(thePrimaryEventVertex.position()) : -100.);
      _elecDzBS->push_back(patElec->gsfTrack().isNonnull() ? patElec->gsfTrack()->dz(theBeamSpot.position()) : -100.);
      _elecDzError->push_back(patElec->gsfTrack().isNonnull() ? patElec->gsfTrack()->dzError() : -10000.);
      _elecVx->push_back(patElec->gsfTrack().isNonnull() ? patElec->gsfTrack()->vx() : -100.);
      _elecVy->push_back(patElec->gsfTrack().isNonnull() ? patElec->gsfTrack()->vy() : -100.);
      _elecVz->push_back(patElec->gsfTrack().isNonnull() ? patElec->gsfTrack()->vz() : -100.);
      _elecEcalIsoDr03->push_back(patElec->ecalIso());
      _elecTrkIsoDr03->push_back(patElec->trackIso());
      _elecCaloIsoDr03->push_back(patElec->caloIso());
      //_ElecEcalIsoDr04->push_back(patElec->ecalIsoDeposit()->depositAndCountWithin(0.4, reco::IsoDeposit::Vetos(),_RecoElecEcalIsoRecHitThreshold).first);
      //_ElecTrkIsoDr04->push_back(patElec->trackIsoDeposit()->depositAndCountWithin(0.4, reco::IsoDeposit::Vetos(),_RecoElecTrackIsoTrkThreshold).first);
/*
      _ElecEcalIsoDr05->push_back(patElec->isolationR05().emEt);
      _ElecHadIsoDr05->push_back(patElec->isolationR05().hadEt);
      _ElecTrkIsoDr05->push_back(patElec->isolationR05().sumPt);
      _ElecCaloIsoDr05->push_back(patElec->isolationR05().hadEt + patElec->isolationR05().emEt);
      _ElecPFIsoDR03SumChargedHadronPt->push_back(patElec->pfIsolationR03().sumChargedHadronPt);
      _ElecPFIsoDR03SumChargedParticlePt->push_back(patElec->pfIsolationR03().sumChargedParticlePt);
      _ElecPFIsoDR03SumNeutralHadronPt->push_back(patElec->pfIsolationR03().sumNeutralHadronEt);
      _ElecPFIsoDR03SumPhotonHadronPt->push_back(patElec->pfIsolationR03().sumPhotonEt);
      _ElecPFIsoDR03SumPUPt->push_back(patElec->pfIsolationR03().sumPUPt);
      _ElecPFIsoDR04SumChargedHadronPt->push_back(patElec->pfIsolationR04().sumChargedHadronPt);
      _ElecPFIsoDR04SumChargedParticlePt->push_back(patElec->pfIsolationR04().sumChargedParticlePt);
      _ElecPFIsoDR04SumNeutralHadronPt->push_back(patElec->pfIsolationR04().sumNeutralHadronEt);
      _ElecPFIsoDR04SumPhotonHadronPt->push_back(patElec->pfIsolationR04().sumPhotonEt);
      _ElecPFIsoDR04SumPUPt->push_back(patElec->pfIsolationR04().sumPUPt);
*/      
      _elecClass->push_back(patElec->classification());
      _elecIdLoose->push_back(patElec->isElectronIDAvailable("eidLoose") ? patElec->electronID("eidLoose") : -1);
      _elecIdTight->push_back(patElec->isElectronIDAvailable("eidTight") ? patElec->electronID("eidTight") : -1);
      _elecIdRobustHighEnergy->push_back(patElec->isElectronIDAvailable("eidRobustHighEnergy") ? patElec->electronID("eidRobustHighEnergy") : -1);
      _elecIdRobustLoose->push_back(patElec->isElectronIDAvailable("eidRobustLoose") ? patElec->electronID("eidRobustLoose") : -1);
      _elecIdRobustTight->push_back(patElec->isElectronIDAvailable("eidRobustTight") ? patElec->electronID("eidRobustTight") : -1);
      
      const Track* elTrack = (const reco::Track*)(patElec->gsfTrack().get());
      // number of expected hits before the first track hit
      const HitPattern& pInner = elTrack->trackerExpectedHitsInner(); 
      _elecMissingHits->push_back(pInner.numberOfHits());
      
      // HEEP selection cuts
      int cutResult = patElec->userInt("HEEPId");
      _heepPassedEt->push_back(heep::CutCodes::passCuts(cutResult, "et"));
      _heepPassedPt->push_back(heep::CutCodes::passCuts(cutResult, "pt"));
      _heepPassedDetEta->push_back(heep::CutCodes::passCuts(cutResult, "detEta"));
      _heepPassedCrack->push_back(heep::CutCodes::passCuts(cutResult, "crack"));
      _heepPassedDEtaIn->push_back(heep::CutCodes::passCuts(cutResult, "dEtaIn"));
      _heepPassedDPhiIn->push_back(heep::CutCodes::passCuts(cutResult, "dPhiIn"));
      _heepPassedHadem->push_back(heep::CutCodes::passCuts(cutResult, "hadem"));
      _heepPassedSigmaIEtaIEta->push_back(heep::CutCodes::passCuts(cutResult, "sigmaIEtaIEta"));
      _heepPassed2by5Over5By5->push_back(heep::CutCodes::passCuts(cutResult, "e2x5Over5x5"));
      _heepPassedEcalHad1Iso->push_back(heep::CutCodes::passCuts(cutResult, "isolEmHadDepth1"));
      _heepPassedHad2Iso->push_back(heep::CutCodes::passCuts(cutResult, "isolHadDepth2"));
      _heepPassedTrkIso->push_back(heep::CutCodes::passCuts(cutResult, "isolPtTrks"));
      _heepPassedEcalDriven->push_back(heep::CutCodes::passCuts(cutResult, "ecalDriven"));
      _heepPassedAllCuts->push_back(heep::CutCodes::passCuts(cutResult, ~heep::CutCodes::DETAIN));
      
      _elecMEtMt->push_back(CalculateLeptonMetMt(*patElec, theMET));
      
      //get mu-LeadTau combinations
      _elecLeadTauCosDPhi->push_back(cos(TMath::Abs(normalizedPhi(theLeadTau->phi() - patElec->phi()))));
      _elecLeadTauDelatR->push_back(reco::deltaR(*theLeadTau, *patElec));
      _elecLeadTauPZeta->push_back(CalculatePZeta(*theLeadTau,*patElec, theMET));
      _elecLeadTauPZetaVis->push_back(CalculatePZetaVis(*theLeadTau,*patElec));
      _elecLeadTauMass->push_back(GetVisMass(*theLeadTau,*patElec));
      _elecLeadTauMetMass->push_back(GetVisPlusMETMass(*theLeadTau,*patElec, theMET));	
      _elecLeadTauCollMass->push_back(GetCollinearApproxMass(*theLeadTau,*patElec, theMET));
      
      //get mu-SubLeadTau combinations
      _elecSubLeadTauCosDPhi->push_back(cos(TMath::Abs(normalizedPhi(theSubLeadTau->phi() - patElec->phi()))));
      _elecSubLeadTauDelatR->push_back(reco::deltaR(*theSubLeadTau, *patElec));
      _elecSubLeadTauPZeta->push_back(CalculatePZeta(*theSubLeadTau,*patElec, theMET));
      _elecSubLeadTauPZetaVis->push_back(CalculatePZetaVis(*theSubLeadTau,*patElec));
      _elecSubLeadTauMass->push_back(GetVisMass(*theSubLeadTau,*patElec));
      _elecSubLeadTauMetMass->push_back(GetVisPlusMETMass(*theSubLeadTau,*patElec, theMET));    
      _elecSubLeadTauCollMass->push_back(GetCollinearApproxMass(*theSubLeadTau,*patElec, theMET));
      
      // get mu-DiTau candidate correlations
      _elecDiHadCandCosDPhi->push_back(cos(TMath::Abs(normalizedPhi(theCand->phi() - patElec->phi()))));
      _elecDiHadCandMass->push_back(GetVisMass(*theCand,*patElec));
      
      //get met correlations
      _elecMetCosDPhi->push_back(cos(TMath::Abs(normalizedPhi(patElec->phi() - theMET.phi()))));
      _diHadTauCandMetCosDPhi->push_back(cos(TMath::Abs(normalizedPhi(theCand->phi() - theMET.phi()))));
    }
  } 
  _MuTauTauTree->Fill();
}

void ElecTauTauAnalysis::setupBranches(){
  _MuTauTauTree = new TTree(_NtupleTreeName.c_str(), "MuTauTauTree");
  
  //Event info
  _MuTauTauTree->Branch("runNumber",&runNum);
  _MuTauTauTree->Branch("eventNumber",&eventNum);
 
  _MuTauTauTree->Branch("InTimePU",&_inTimePUNumInteractions);
  _MuTauTauTree->Branch("OutTimePlusPU",&_outTimePlusPUNumInteractions);
  _MuTauTauTree->Branch("OutTimeMinusPU",&_outTimeMinusPUNumInteractions);
  
  _MuTauTauTree->Branch("nVtx",&_nVtx);
  _MuTauTauTree->Branch("nGoodVtx",&_nGoodVtx);
  
  _MuTauTauTree->Branch("MEt",&_MEt);
  _MuTauTauTree->Branch("zEvent", &_zEvent);
  _MuTauTauTree->Branch("zMassDiff", &_zMassDiff);
  _MuTauTauTree->Branch("zPtAsymm", &_zPtAsymm);
  
  //_MuTauTauTree->Branch("passedHLTMu5Elec17",&_passedHLTMu5Elec17);
  //_MuTauTauTree->Branch("passedHLTMu11Elec8",&_passedHLTMu11Elec8);
  
  //_MuTauTauTree->Branch("passedHLTMu8Elec17",&_passedHLTMu8Elec17);
  //_MuTauTauTree->Branch("passedHLTMu17Elec8",&_passedHLTMu17Elec8);
  
  //_MuTauTauTree->Branch("passedHLTIsoMu24",&_passedHLTIsoMu24);

  _MuTauTauTree->Branch("PDFWweights", &_PDFWeights);
  _MuTauTauTree->Branch("ISRGluonWeight", &_ISRGluonWeight);
  _MuTauTauTree->Branch("ISRGammaWeight", &_ISRGammaWeight);
  _MuTauTauTree->Branch("FSRWeight", &_FSRWeight);
  
  //GenLevel Info (not dependent on reco)
  _MuTauTauTree->Branch("genLevelElecPt", &_genLevelElecPt);
  _MuTauTauTree->Branch("genLevelElecEta", &_genLevelElecEta);
  _MuTauTauTree->Branch("genLevelElecPhi", &_genLevelElecPhi);
  _MuTauTauTree->Branch("genLevelTau1Pt", &_genLevelTau1Pt);
  _MuTauTauTree->Branch("genLevelTau1Eta", &_genLevelTau1Eta);
  _MuTauTauTree->Branch("genLevelTau1Phi", &_genLevelTau1Phi);
  _MuTauTauTree->Branch("genLevelTau2Pt", &_genLevelTau2Pt);
  _MuTauTauTree->Branch("genLevelTau2Eta", &_genLevelTau2Eta);
  _MuTauTauTree->Branch("genLevelTau2Phi", &_genLevelTau2Phi);
  
  //BTag Counting
  _MuTauTauTree->Branch("nBtagsHiEffTrkCnt",&_nBtagsHiEffTrkCnt);
  _MuTauTauTree->Branch("nBtagsHiPurityTrkCnt",&_nBtagsHiPurityTrkCnt);
  _MuTauTauTree->Branch("nBTagsHiEffSimpleSecVtx",&_nBTagsHiEffSimpleSecVtx);
  _MuTauTauTree->Branch("nBTagsHiPuritySimpleSecVtx",&_nBTagsHiPuritySimpleSecVtx);
  _MuTauTauTree->Branch("nBTagsCombSecVtx",&_nBTagsCombSecVtx);
  
  // For the Lead Tau
  _MuTauTauTree->Branch("leadTauMatched", &_leadTauMatched);
  _MuTauTauTree->Branch("leadTauMotherId", &_leadTauMotherId);
  _MuTauTauTree->Branch("leadTauGenPt", &_leadTauGenPt);
  _MuTauTauTree->Branch("leadTauGenE", &_leadTauGenE);
  _MuTauTauTree->Branch("leadTauGenEta", &_leadTauGenEta);
  _MuTauTauTree->Branch("leadTauGenPhi", &_leadTauGenPhi);
  
  _MuTauTauTree->Branch("leadTauPt",&_leadTauPt);
  _MuTauTauTree->Branch("leadTauEnergy",&_leadTauEnergy);
  _MuTauTauTree->Branch("leadTauEta",&_leadTauEta);
  _MuTauTauTree->Branch("leadTauPhi",&_leadTauPhi);
  _MuTauTauTree->Branch("leadTauCharge",&_leadTauCharge);
  _MuTauTauTree->Branch("leadTauLeadChargedCandPt",&_leadTauLeadChargedCandPt);
  _MuTauTauTree->Branch("leadTauLeadChargedCandCharge",&_leadTauLeadChargedCandCharge);
  //_MuTauTauTree->Branch("leadTauLeadChargedCandRecHitsSize",&_leadTauLeadChargedCandRecHitsSize);
  _MuTauTauTree->Branch("leadTauLeadChargedCandDxyVtx",&_leadTauLeadChargedCandDxyVtx);
  _MuTauTauTree->Branch("leadTauLeadChargedCandDxyBS",&_leadTauLeadChargedCandDxyBS);
  _MuTauTauTree->Branch("leadTauLeadChargedCandDxyError",&_leadTauLeadChargedCandDxyError);
  _MuTauTauTree->Branch("leadTauLeadChargedCandDzVtx",&_leadTauLeadChargedCandDzVtx);
  _MuTauTauTree->Branch("leadTauLeadChargedCandDzBS",&_leadTauLeadChargedCandDzBS);
  _MuTauTauTree->Branch("leadTauLeadChargedCandDzError",&_leadTauLeadChargedCandDzError);
  _MuTauTauTree->Branch("leadTauLeadChargedCandVx",&_leadTauLeadChargedCandVx);
  _MuTauTauTree->Branch("leadTauLeadChargedCandVy",&_leadTauLeadChargedCandVy);
  _MuTauTauTree->Branch("leadTauLeadChargedCandVz",&_leadTauLeadChargedCandVz);
  _MuTauTauTree->Branch("leadTauDeltaZVtx",&_leadTauDeltaZVtx);
  _MuTauTauTree->Branch("leadTauNProngs",&_leadTauNProngs);
  _MuTauTauTree->Branch("leadTauEmFraction",&_leadTauEmFraction);
  _MuTauTauTree->Branch("leadTauHcalTotOverPLead",&_leadTauHcalTotOverPLead);
  _MuTauTauTree->Branch("leadTauHcalMaxOverPLead",&_leadTauHcalMaxOverPLead);
  _MuTauTauTree->Branch("leadTauHcal3x3OverPLead",&_leadTauHcal3x3OverPLead);
  _MuTauTauTree->Branch("leadTauElectronPreId",&_leadTauElectronPreId);
  _MuTauTauTree->Branch("leadTauModifiedEOverP",&_leadTauModifiedEOverP);
  _MuTauTauTree->Branch("leadTauBremsRecoveryEOverPLead",&_leadTauBremsRecoveryEOverPLead);
  _MuTauTauTree->Branch("leadTauLTSignedIp",&_leadTauLTSignedIp);
  _MuTauTauTree->Branch("leadTauTrkIsoSumPt",&_leadTauTrkIsoSumPt);
  _MuTauTauTree->Branch("leadTauECALIsoSumEt",&_leadTauECALIsoSumEt);
  _MuTauTauTree->Branch("leadTauIdByDecayModeFinding",&_leadTauIdByDecayModeFinding);
  _MuTauTauTree->Branch("leadTauIdByVLooseIsolation",&_leadTauIdByVLooseIsolation);
  _MuTauTauTree->Branch("leadTauIdByLooseIsolation",&_leadTauIdByLooseIsolation);
  _MuTauTauTree->Branch("leadTauIdByMediumIsolation",&_leadTauIdByMediumIsolation);
  _MuTauTauTree->Branch("leadTauIdByTightIsolation",&_leadTauIdByTightIsolation);
  _MuTauTauTree->Branch("leadTauIdByVLooseCombinedIsolationDBSumPtCorr",&_leadTauIdByVLooseCombinedIsolationDBSumPtCorr);
  _MuTauTauTree->Branch("leadTauIdByLooseCombinedIsolationDBSumPtCorr",&_leadTauIdByLooseCombinedIsolationDBSumPtCorr);
  _MuTauTauTree->Branch("leadTauIdByMediumCombinedIsolationDBSumPtCorr",&_leadTauIdByMediumCombinedIsolationDBSumPtCorr);
  _MuTauTauTree->Branch("leadTauIdByTightCombinedIsolationDBSumPtCorr",&_leadTauIdByTightCombinedIsolationDBSumPtCorr);
  _MuTauTauTree->Branch("leadTauIdByLooseElectronRejection",&_leadTauIdByLooseElectronRejection);
  _MuTauTauTree->Branch("leadTauIdByMediumElectronRejection",&_leadTauIdByMediumElectronRejection);
  _MuTauTauTree->Branch("leadTauIdByTightElectronRejection",&_leadTauIdByTightElectronRejection);
  _MuTauTauTree->Branch("leadTauIdByLooseElecRejection",&_leadTauIdByLooseElecRejection);
  _MuTauTauTree->Branch("leadTauIdByTightElecRejection",&_leadTauIdByTightElecRejection);
  _MuTauTauTree->Branch("leadTauMEtMt", &_leadTauMEtMt);
   
   // For the Sub Lead Tau
  _MuTauTauTree->Branch("subLeadTauMatched", &_subLeadTauMatched);
  _MuTauTauTree->Branch("subLeadTauMotherId", &_subLeadTauMotherId);
  _MuTauTauTree->Branch("subLeadTauGenPt", &_subLeadTauGenPt);
  _MuTauTauTree->Branch("subLeadTauGenE", &_subLeadTauGenE);
  _MuTauTauTree->Branch("subLeadTauGenEta", &_subLeadTauGenEta);
  _MuTauTauTree->Branch("subLeadTauGenPhi", &_subLeadTauGenPhi);
  
  _MuTauTauTree->Branch("subLeadTauPt",&_subLeadTauPt);
  _MuTauTauTree->Branch("subLeadTauEnergy",&_subLeadTauEnergy);
  _MuTauTauTree->Branch("subLeadTauEta",&_subLeadTauEta);
  _MuTauTauTree->Branch("subLeadTauPhi",&_subLeadTauPhi);
  _MuTauTauTree->Branch("subLeadTauCharge",&_subLeadTauCharge);
  _MuTauTauTree->Branch("subLeadTauLeadChargedCandPt",&_subLeadTauLeadChargedCandPt);
  _MuTauTauTree->Branch("subLeadTauLeadChargedCandCharge",&_subLeadTauLeadChargedCandCharge);
  //_MuTauTauTree->Branch("subLeadTauLeadChargedCandRecHitsSize",&_subLeadTauLeadChargedCandRecHitsSize);
  _MuTauTauTree->Branch("subleadTauLeadChargedCandDxyVtx",&_subLeadTauLeadChargedCandDxyVtx);
  _MuTauTauTree->Branch("subleadTauLeadChargedCandDxyBS",&_subLeadTauLeadChargedCandDxyBS);
  _MuTauTauTree->Branch("subleadTauLeadChargedCandDxyError",&_subLeadTauLeadChargedCandDxyError);
  _MuTauTauTree->Branch("subleadTauLeadChargedCandDzVtx",&_subLeadTauLeadChargedCandDzVtx);
  _MuTauTauTree->Branch("subleadTauLeadChargedCandDzBS",&_subLeadTauLeadChargedCandDzBS);
  _MuTauTauTree->Branch("subLeadTauLeadChargedCandVx",&_subLeadTauLeadChargedCandVx);
  _MuTauTauTree->Branch("subLeadTauLeadChargedCandVy",&_subLeadTauLeadChargedCandVy);
  _MuTauTauTree->Branch("subLeadTauLeadChargedCandVz",&_subLeadTauLeadChargedCandVz);
  _MuTauTauTree->Branch("subleadTauLeadChargedCandDzError",&_subLeadTauLeadChargedCandDzError);
  _MuTauTauTree->Branch("subLeadTauDeltaZVtx",&_subLeadTauDeltaZVtx);
  _MuTauTauTree->Branch("subLeadTauNProngs",&_subLeadTauNProngs);
  _MuTauTauTree->Branch("subLeadTauEmFraction",&_subLeadTauEmFraction);
  _MuTauTauTree->Branch("subLeadTauHcalTotOverPLead",&_subLeadTauHcalTotOverPLead);
  _MuTauTauTree->Branch("subLeadTauHcalMaxOverPLead",&_subLeadTauHcalMaxOverPLead);
  _MuTauTauTree->Branch("subLeadTauHcal3x3OverPLead",&_subLeadTauHcal3x3OverPLead);
  _MuTauTauTree->Branch("subLeadTauElectronPreId",&_subLeadTauElectronPreId);
  _MuTauTauTree->Branch("subLeadTauModifiedEOverP",&_subLeadTauModifiedEOverP);
  _MuTauTauTree->Branch("subLeadTauBremsRecoveryEOverPLead",&_subLeadTauBremsRecoveryEOverPLead);
  _MuTauTauTree->Branch("subLeadTauLTSignedIp",&_subLeadTauLTSignedIp);
  _MuTauTauTree->Branch("subLeadTauTrkIsoSumPt",&_subLeadTauTrkIsoSumPt);
  _MuTauTauTree->Branch("subLeadTauECALIsoSumEt",&_subLeadTauECALIsoSumEt);
  _MuTauTauTree->Branch("subLeadTauIdByDecayModeFinding",&_subLeadTauIdByDecayModeFinding);
  _MuTauTauTree->Branch("subLeadTauIdByVLooseIsolation",&_subLeadTauIdByVLooseIsolation);
  _MuTauTauTree->Branch("subLeadTauIdByLooseIsolation",&_subLeadTauIdByLooseIsolation);
  _MuTauTauTree->Branch("subLeadTauIdByMediumIsolation",&_subLeadTauIdByMediumIsolation);
  _MuTauTauTree->Branch("subLeadTauIdByTightIsolation",&_subLeadTauIdByTightIsolation);
  _MuTauTauTree->Branch("subLeadTauIdByVLooseCombinedIsolationDBSumPtCorr",&_subLeadTauIdByVLooseCombinedIsolationDBSumPtCorr);
  _MuTauTauTree->Branch("subLeadTauIdByLooseCombinedIsolationDBSumPtCorr",&_subLeadTauIdByLooseCombinedIsolationDBSumPtCorr);
  _MuTauTauTree->Branch("subLeadTauIdByMediumCombinedIsolationDBSumPtCorr",&_subLeadTauIdByMediumCombinedIsolationDBSumPtCorr);
  _MuTauTauTree->Branch("subLeadTauIdByTightCombinedIsolationDBSumPtCorr",&_subLeadTauIdByTightCombinedIsolationDBSumPtCorr);
  _MuTauTauTree->Branch("subLeadTauIdByLooseElectronRejection",&_subLeadTauIdByLooseElectronRejection);
  _MuTauTauTree->Branch("subLeadTauIdByMediumElectronRejection",&_subLeadTauIdByMediumElectronRejection);
  _MuTauTauTree->Branch("subLeadTauIdByTightElectronRejection",&_subLeadTauIdByTightElectronRejection);
  _MuTauTauTree->Branch("subLeadTauIdByLooseElecRejection",&_subLeadTauIdByLooseElecRejection);
  _MuTauTauTree->Branch("subLeadTauIdByTightElecRejection",&_subLeadTauIdByTightElecRejection);
  _MuTauTauTree->Branch("subLeadTauMEtMt", &_subLeadTauMEtMt);
  
  _MuTauTauTree->Branch("diHadTauCosDPhi", &_diHadTauCosDPhi);
  _MuTauTauTree->Branch("diHadTauDelatR", &_diHadTauDelatR);
  _MuTauTauTree->Branch("diHadTauPZeta", &_diHadTauPZeta);
  _MuTauTauTree->Branch("diHadTauPZetaVis", &_diHadTauPZetaVis);
  _MuTauTauTree->Branch("diHadTauMass", &_diHadTauMass);
  _MuTauTauTree->Branch("diHadTauMetMass", &_diHadTauMetMass);
  _MuTauTauTree->Branch("diHadTauCollMass", &_diHadTauCollMass);
  
  _MuTauTauTree->Branch("diHadCandPt", &_diHadCandPt);
  _MuTauTauTree->Branch("diHadCandEt", &_diHadCandEt);
  _MuTauTauTree->Branch("diHadCandPhi", &_diHadCandPhi);
  _MuTauTauTree->Branch("diHadCandEta", &_diHadCandEta);
  _MuTauTauTree->Branch("diHadCandMass", &_diHadCandMass);
  _MuTauTauTree->Branch("diHadCandCharge", &_diHadCandCharge);
  
  
  //Elec info
  _MuTauTauTree->Branch("elecMatched", &_elecMatched);
  _MuTauTauTree->Branch("elecMotherId", &_elecMotherId);
  _MuTauTauTree->Branch("elecGenPt", &_elecGenPt);
  _MuTauTauTree->Branch("elecGenE", &_elecGenE);
  _MuTauTauTree->Branch("elecGenEta", &_elecGenEta);
  _MuTauTauTree->Branch("elecGenPhi", &_elecGenPhi);
  
  _MuTauTauTree->Branch("elecPt", &_elecPt);
  _MuTauTauTree->Branch("elecE", &_elecE);
  _MuTauTauTree->Branch("elecEta", &_elecEta);
  _MuTauTauTree->Branch("elecPhi", &_elecPhi);
  _MuTauTauTree->Branch("elecCharge", &_elecCharge);
  _MuTauTauTree->Branch("elecDxyVtx", &_elecDxyVtx);
  _MuTauTauTree->Branch("elecDxyBS", &_elecDxyBS);
  _MuTauTauTree->Branch("elecDxyError", &_elecDxyError);
  _MuTauTauTree->Branch("elecDzVtx", &_elecDzVtx);
  _MuTauTauTree->Branch("elecDzBS", &_elecDzBS);
  _MuTauTauTree->Branch("elecDzError", &_elecDzError);
  _MuTauTauTree->Branch("elecVx", &_elecVx);
  _MuTauTauTree->Branch("elecVy", &_elecVy);
  _MuTauTauTree->Branch("elecVz", &_elecVz);
  _MuTauTauTree->Branch("elecEcalIsoDr03", &_elecEcalIsoDr03);
  _MuTauTauTree->Branch("elecTrkIsoDr03", &_elecTrkIsoDr03);
  _MuTauTauTree->Branch("elecCaloIsoDr03", &_elecCaloIsoDr03);
  _MuTauTauTree->Branch("elecEcalIsoDr04", &_elecEcalIsoDr04);
  _MuTauTauTree->Branch("elecTrkIsoDr04", &_elecTrkIsoDr04);
  _MuTauTauTree->Branch("elecEcalIsoDr05", &_elecEcalIsoDr05);
  _MuTauTauTree->Branch("elecHadIsoDr05", &_elecHadIsoDr05);
  _MuTauTauTree->Branch("elecTrkIsoDr05", &_elecTrkIsoDr05);
  _MuTauTauTree->Branch("elecCaloIsoDr05", &_elecCaloIsoDr05);
  _MuTauTauTree->Branch("elecPFIsoDR03SumChargedHadronPt", &_elecPFIsoDR03SumChargedHadronPt);
  _MuTauTauTree->Branch("elecPFIsoDR03SumChargedParticlePt", &_elecPFIsoDR03SumChargedParticlePt);
  _MuTauTauTree->Branch("elecPFIsoDR03SumNeutralHadronPt", &_elecPFIsoDR03SumNeutralHadronPt);
  _MuTauTauTree->Branch("elecPFIsoDR03SumPhotonHadronPt", &_elecPFIsoDR03SumPhotonHadronPt);
  _MuTauTauTree->Branch("elecPFIsoDR03SumPUPt", &_elecPFIsoDR03SumPUPt);
  _MuTauTauTree->Branch("elecPFIsoDR04SumChargedHadronPt", &_elecPFIsoDR04SumChargedHadronPt);
  _MuTauTauTree->Branch("elecPFIsoDR04SumChargedParticlePt", &_elecPFIsoDR04SumChargedParticlePt);
  _MuTauTauTree->Branch("elecPFIsoDR04SumNeutralHadronPt", &_elecPFIsoDR04SumNeutralHadronPt);
  _MuTauTauTree->Branch("elecPFIsoDR04SumPhotonHadronPt", &_elecPFIsoDR04SumPhotonHadronPt);
  _MuTauTauTree->Branch("elecPFIsoDR04SumPUPt", &_elecPFIsoDR04SumPUPt);
  
  _MuTauTauTree->Branch("elecClass", &_elecClass);
  _MuTauTauTree->Branch("elecIdLoose", &_elecIdLoose);
  _MuTauTauTree->Branch("elecIdTight", &_elecIdTight);
  _MuTauTauTree->Branch("elecIdRobustHighEnergy", &_elecIdRobustHighEnergy);
  _MuTauTauTree->Branch("elecIdRobustLoose", &_elecIdRobustLoose);
  _MuTauTauTree->Branch("elecIdRobustTight", &_elecIdRobustTight);
  _MuTauTauTree->Branch("elecMissingHits", &_elecMissingHits);
  _MuTauTauTree->Branch("heepPassedEt", &_heepPassedEt);
  _MuTauTauTree->Branch("heepPassedPt", &_heepPassedPt);
  _MuTauTauTree->Branch("heepPassedDetEta", &_heepPassedDetEta);
  _MuTauTauTree->Branch("heepPassedCrack", &_heepPassedCrack);
  _MuTauTauTree->Branch("heepPassedDEtaIn", &_heepPassedDEtaIn);
  _MuTauTauTree->Branch("heepPassedDPhiIn", &_heepPassedDPhiIn);
  _MuTauTauTree->Branch("heepPassedHadem", &_heepPassedHadem);
  _MuTauTauTree->Branch("heepPassedSigmaIEtaIEta", &_heepPassedSigmaIEtaIEta);
  _MuTauTauTree->Branch("heepPassed2by5Over5By5", &_heepPassed2by5Over5By5);
  _MuTauTauTree->Branch("heepPassedEcalHad1Iso", &_heepPassedEcalHad1Iso);
  _MuTauTauTree->Branch("heepPassedHad2Iso", &_heepPassedHad2Iso);
  _MuTauTauTree->Branch("heepPassedTrkIso", &_heepPassedTrkIso);
  _MuTauTauTree->Branch("heepPassedEcalDriven", &_heepPassedEcalDriven);
  _MuTauTauTree->Branch("heepPassedAllCuts", &_heepPassedAllCuts);
  
  _MuTauTauTree->Branch("elecMEtMt", &_elecMEtMt);

  //mu-LeadTau Info
  _MuTauTauTree->Branch("elecLeadTauCosDPhi", &_elecLeadTauCosDPhi);
  _MuTauTauTree->Branch("elecLeadTauDelatR", &_elecLeadTauDelatR);
  _MuTauTauTree->Branch("elecLeadTauMass", &_elecLeadTauMass);
  _MuTauTauTree->Branch("elecLeadTauMetMass", &_elecLeadTauMetMass);
  _MuTauTauTree->Branch("elecLeadTauCollMass", &_elecLeadTauCollMass);
  _MuTauTauTree->Branch("elecLeadTauPZeta", &_elecLeadTauPZeta);
  _MuTauTauTree->Branch("elecLeadTauPZetaVis", &_elecLeadTauPZetaVis);
  
  //mu-SubLeadTau Info
  _MuTauTauTree->Branch("elecSubLeadTauCosDPhi", &_elecSubLeadTauCosDPhi);
  _MuTauTauTree->Branch("elecSubLeadTauDelatR", &_elecSubLeadTauDelatR);
  _MuTauTauTree->Branch("elecSubLeadTauMass", &_elecSubLeadTauMass);
  _MuTauTauTree->Branch("elecSubLeadTauMetMass", &_elecSubLeadTauMetMass);
  _MuTauTauTree->Branch("elecSubLeadTauCollMass", &_elecSubLeadTauCollMass);
  _MuTauTauTree->Branch("elecSubLeadTauPZeta", &_elecSubLeadTauPZeta);
  _MuTauTauTree->Branch("elecSubLeadTauPZetaVis", &_elecSubLeadTauPZetaVis);
  
  _MuTauTauTree->Branch("elecDiHadCandCosDPhi", &_elecDiHadCandCosDPhi);
  _MuTauTauTree->Branch("elecDiHadCandMass", &_elecDiHadCandMass);
  
  _MuTauTauTree->Branch("elecMetCosDPhi", &_elecMetCosDPhi);
  _MuTauTauTree->Branch("diHadTauCandMetCosDPhi", &_diHadTauCandMetCosDPhi);
}

void ElecTauTauAnalysis::initializeVectors(){
  //Lead Tau Info
  _leadTauMatched = NULL;
  _leadTauMotherId = NULL;
  _leadTauGenPt = NULL;
  _leadTauGenE = NULL;
  _leadTauGenEta = NULL;
  _leadTauGenPhi = NULL;
  
  _leadTauPt = NULL;
  _leadTauEnergy = NULL;
  _leadTauEta = NULL;
  _leadTauPhi = NULL;
  _leadTauCharge = NULL;
  _leadTauLeadChargedCandPt = NULL;
  _leadTauLeadChargedCandCharge = NULL;
  //_leadTauLeadChargedCandRecHitsSize = NULL;
  _leadTauLeadChargedCandDxyVtx = NULL;
  _leadTauLeadChargedCandDxyBS = NULL;
  _leadTauLeadChargedCandDxyError = NULL;
  _leadTauLeadChargedCandDzVtx = NULL;
  _leadTauLeadChargedCandDzBS = NULL;
  _leadTauLeadChargedCandDzError = NULL;
  _leadTauLeadChargedCandVx = NULL;
  _leadTauLeadChargedCandVy = NULL;
  _leadTauLeadChargedCandVz = NULL;
  _leadTauDeltaZVtx = NULL;
  _leadTauNProngs = NULL;
  _leadTauEmFraction = NULL;
  _leadTauHcalTotOverPLead = NULL;
  _leadTauHcalMaxOverPLead = NULL;
  _leadTauHcal3x3OverPLead = NULL;
  _leadTauElectronPreId = NULL;
  _leadTauModifiedEOverP = NULL;
  _leadTauBremsRecoveryEOverPLead = NULL;
  _leadTauLTSignedIp = NULL;
  _leadTauTrkIsoSumPt = NULL;
  _leadTauECALIsoSumEt = NULL;
  _leadTauIdByDecayModeFinding = NULL;
  _leadTauIdByVLooseIsolation = NULL;
  _leadTauIdByLooseIsolation = NULL;
  _leadTauIdByMediumIsolation = NULL;
  _leadTauIdByTightIsolation = NULL;
  _leadTauIdByVLooseCombinedIsolationDBSumPtCorr = NULL;
  _leadTauIdByLooseCombinedIsolationDBSumPtCorr = NULL;
  _leadTauIdByMediumCombinedIsolationDBSumPtCorr = NULL;
  _leadTauIdByTightCombinedIsolationDBSumPtCorr = NULL;
  _leadTauIdByLooseElectronRejection = NULL;
  _leadTauIdByMediumElectronRejection = NULL;
  _leadTauIdByTightElectronRejection = NULL;
  _leadTauIdByLooseElecRejection = NULL;
  _leadTauIdByTightElecRejection = NULL;
  _leadTauMEtMt = NULL;
 
  //Sub Lead Tau Info
  _subLeadTauMatched = NULL;
  _subLeadTauMotherId = NULL;
  _subLeadTauGenPt = NULL;
  _subLeadTauGenE = NULL;
  _subLeadTauGenEta = NULL;
  _subLeadTauGenPhi = NULL;
  
  _subLeadTauPt = NULL;
  _subLeadTauEnergy = NULL;
  _subLeadTauEta = NULL;
  _subLeadTauPhi = NULL;
  _subLeadTauCharge = NULL;
  _subLeadTauLeadChargedCandPt = NULL;
  _subLeadTauLeadChargedCandCharge = NULL;
  //_subLeadTauLeadChargedCandRecHitsSize = NULL;
  _subLeadTauLeadChargedCandDxyVtx = NULL;
  _subLeadTauLeadChargedCandDxyBS = NULL;
  _subLeadTauLeadChargedCandDxyError = NULL;
  _subLeadTauLeadChargedCandDzVtx = NULL;
  _subLeadTauLeadChargedCandDzBS = NULL;
  _subLeadTauLeadChargedCandDzError = NULL;
  _subLeadTauLeadChargedCandVx = NULL;
  _subLeadTauLeadChargedCandVy = NULL;
  _subLeadTauLeadChargedCandVz = NULL;
  _subLeadTauNProngs = NULL;
  _subLeadTauDeltaZVtx = NULL;
  _subLeadTauEmFraction = NULL;
  _subLeadTauHcalTotOverPLead = NULL;
  _subLeadTauHcalMaxOverPLead = NULL;
  _subLeadTauHcal3x3OverPLead = NULL;
  _subLeadTauElectronPreId = NULL;
  _subLeadTauModifiedEOverP = NULL;
  _subLeadTauBremsRecoveryEOverPLead = NULL;
  _subLeadTauLTSignedIp = NULL;
  _subLeadTauTrkIsoSumPt = NULL;
  _subLeadTauECALIsoSumEt = NULL;
  _subLeadTauIdByDecayModeFinding = NULL;
  _subLeadTauIdByVLooseIsolation = NULL;
  _subLeadTauIdByLooseIsolation = NULL;
  _subLeadTauIdByMediumIsolation = NULL;
  _subLeadTauIdByTightIsolation = NULL;
  _subLeadTauIdByVLooseCombinedIsolationDBSumPtCorr = NULL;
  _subLeadTauIdByLooseCombinedIsolationDBSumPtCorr = NULL;
  _subLeadTauIdByMediumCombinedIsolationDBSumPtCorr = NULL;
  _subLeadTauIdByTightCombinedIsolationDBSumPtCorr = NULL;
  _subLeadTauIdByLooseElectronRejection = NULL;
  _subLeadTauIdByMediumElectronRejection = NULL;
  _subLeadTauIdByTightElectronRejection = NULL;
  _subLeadTauIdByLooseElecRejection = NULL;
  _subLeadTauIdByTightElecRejection = NULL;
  _subLeadTauMEtMt = NULL;
  
  //diTau info
  _diHadTauCosDPhi = NULL;
  _diHadTauDelatR = NULL;
  _diHadTauPZeta = NULL;
  _diHadTauPZetaVis = NULL;
  _diHadTauMass = NULL;
  _diHadTauMetMass = NULL;
  _diHadTauCollMass = NULL;
  
  _diHadCandPt = NULL;
  _diHadCandEt = NULL;
  _diHadCandEta = NULL;
  _diHadCandPhi = NULL;
  _diHadCandMass = NULL;  
  _diHadCandCharge = NULL;  
  
  //Elec Info
  _elecMatched = NULL;
  _elecMotherId = NULL;
  _elecGenPt = NULL;
  _elecGenE = NULL;
  _elecGenEta = NULL;
  _elecGenPhi = NULL;
  
  _elecPt = NULL;
  _elecE = NULL;
  _elecEta = NULL;
  _elecPhi = NULL;
  _elecCharge = NULL;
  _elecDxyVtx = NULL;
  _elecDxyBS = NULL;
  _elecDxyError = NULL;
  _elecDzVtx = NULL;
  _elecDzBS = NULL;
  _elecDzError = NULL;
  _elecVx = NULL;
  _elecVy = NULL;
  _elecVz = NULL;
  _elecEcalIsoDr03 = NULL;
  _elecTrkIsoDr03 = NULL;
  _elecCaloIsoDr03 = NULL;
  _elecEcalIsoDr04 = NULL;
  _elecTrkIsoDr04 = NULL;
  _elecEcalIsoDr05 = NULL;
  _elecHadIsoDr05 = NULL;
  _elecTrkIsoDr05 = NULL;
  _elecCaloIsoDr05 = NULL;
  _elecPFIsoDR03SumChargedHadronPt = NULL;
  _elecPFIsoDR03SumChargedParticlePt = NULL;
  _elecPFIsoDR03SumNeutralHadronPt = NULL;
  _elecPFIsoDR03SumPhotonHadronPt = NULL;
  _elecPFIsoDR03SumPUPt = NULL;
  _elecPFIsoDR04SumChargedHadronPt = NULL;
  _elecPFIsoDR04SumChargedParticlePt = NULL;
  _elecPFIsoDR04SumNeutralHadronPt = NULL;
  _elecPFIsoDR04SumPhotonHadronPt = NULL;
  _elecPFIsoDR04SumPUPt = NULL;
  
  _elecClass = NULL;
  _elecIdLoose = NULL;
  _elecIdTight = NULL;
  _elecIdRobustHighEnergy = NULL;
  _elecIdRobustLoose = NULL;
  _elecIdRobustTight = NULL;
  _elecMissingHits = NULL;
  _heepPassedEt = NULL;
  _heepPassedPt = NULL;
  _heepPassedDetEta = NULL;
  _heepPassedCrack = NULL;
  _heepPassedDEtaIn = NULL;
  _heepPassedDPhiIn = NULL;
  _heepPassedHadem = NULL;
  _heepPassedSigmaIEtaIEta = NULL;
  _heepPassed2by5Over5By5 = NULL;
  _heepPassedEcalHad1Iso = NULL;
  _heepPassedHad2Iso = NULL;
  _heepPassedTrkIso = NULL;
  _heepPassedEcalDriven = NULL;
  _heepPassedAllCuts = NULL;
 
  _elecMEtMt = NULL;
      
  //mu-LeadTau Info
  _elecLeadTauCosDPhi = NULL;
  _elecLeadTauDelatR = NULL;
  _elecLeadTauPZeta = NULL;
  _elecLeadTauPZetaVis = NULL;
  _elecLeadTauMass = NULL;
  _elecLeadTauMetMass = NULL;    
  _elecLeadTauCollMass = NULL;
  
  //mu-SubLeadTau Info
  _elecSubLeadTauCosDPhi = NULL;
  _elecSubLeadTauDelatR = NULL;
  _elecSubLeadTauPZeta = NULL;
  _elecSubLeadTauPZetaVis = NULL;
  _elecSubLeadTauMass = NULL;
  _elecSubLeadTauMetMass = NULL;	
  _elecSubLeadTauCollMass = NULL;
  
  _elecDiHadCandCosDPhi = NULL;
  _elecDiHadCandMass = NULL;
  
  _elecMetCosDPhi = NULL;
  _diHadTauCandMetCosDPhi = NULL;

}

void ElecTauTauAnalysis::clearVectors(){
  //Lead Tau Info
  _leadTauMatched->clear();
  _leadTauMotherId->clear();
  _leadTauGenPt->clear();
  _leadTauGenE->clear();
  _leadTauGenEta->clear();
  _leadTauGenPhi->clear();
  
  _leadTauPt->clear();
  _leadTauEnergy->clear();
  _leadTauEta->clear();
  _leadTauPhi->clear();
  _leadTauCharge->clear();
  _leadTauLeadChargedCandPt->clear();
  _leadTauLeadChargedCandCharge->clear();
  //_leadTauLeadChargedCandRecHitsSize->clear();
  _leadTauLeadChargedCandDxyVtx->clear();
  _leadTauLeadChargedCandDxyBS->clear();
  _leadTauLeadChargedCandDxyError->clear();
  _leadTauLeadChargedCandDzVtx->clear();
  _leadTauLeadChargedCandDzBS->clear();
  _leadTauLeadChargedCandDzError->clear();
  _leadTauLeadChargedCandVx->clear();
  _leadTauLeadChargedCandVy->clear();
  _leadTauLeadChargedCandVz->clear();
  _leadTauDeltaZVtx->clear();
  _leadTauNProngs->clear();
  _leadTauEmFraction->clear();
  _leadTauHcalTotOverPLead->clear();
  _leadTauHcalMaxOverPLead->clear();
  _leadTauHcal3x3OverPLead->clear();
  _leadTauElectronPreId->clear();
  _leadTauModifiedEOverP->clear();
  _leadTauBremsRecoveryEOverPLead->clear();
  _leadTauLTSignedIp->clear();
  _leadTauTrkIsoSumPt->clear();
  _leadTauECALIsoSumEt->clear();
  _leadTauIdByDecayModeFinding->clear();
  _leadTauIdByVLooseIsolation->clear();
  _leadTauIdByLooseIsolation->clear();
  _leadTauIdByMediumIsolation->clear();
  _leadTauIdByTightIsolation->clear();
  _leadTauIdByVLooseCombinedIsolationDBSumPtCorr->clear();
  _leadTauIdByLooseCombinedIsolationDBSumPtCorr->clear();
  _leadTauIdByMediumCombinedIsolationDBSumPtCorr->clear();
  _leadTauIdByTightCombinedIsolationDBSumPtCorr->clear();
  _leadTauIdByLooseElectronRejection->clear();
  _leadTauIdByMediumElectronRejection->clear();
  _leadTauIdByTightElectronRejection->clear();
  _leadTauIdByLooseElecRejection->clear();
  _leadTauIdByTightElecRejection->clear();
  _leadTauMEtMt->clear();
  
  //SubLead Tau Info
  _subLeadTauMatched->clear();
  _subLeadTauMotherId->clear();
  _subLeadTauGenPt->clear();
  _subLeadTauGenE->clear();
  _subLeadTauGenEta->clear();
  _subLeadTauGenPhi->clear();
  
  _subLeadTauPt->clear();
  _subLeadTauEnergy->clear();
  _subLeadTauEta->clear();
  _subLeadTauPhi->clear();
  _subLeadTauCharge->clear();
  _subLeadTauLeadChargedCandPt->clear();
  _subLeadTauLeadChargedCandCharge->clear();
  //_subLeadTauLeadChargedCandRecHitsSize->clear();
  _subLeadTauLeadChargedCandDxyVtx->clear();
  _subLeadTauLeadChargedCandDxyBS->clear();
  _subLeadTauLeadChargedCandDxyError->clear();
  _subLeadTauLeadChargedCandDzVtx->clear();
  _subLeadTauLeadChargedCandDzBS->clear();
  _subLeadTauLeadChargedCandDzError->clear();
  _subLeadTauLeadChargedCandVx->clear();
  _subLeadTauLeadChargedCandVy->clear();
  _subLeadTauLeadChargedCandVz->clear();
  _subLeadTauDeltaZVtx->clear();
  _subLeadTauNProngs->clear();
  _subLeadTauEmFraction->clear();
  _subLeadTauHcalTotOverPLead->clear();
  _subLeadTauHcalMaxOverPLead->clear();
  _subLeadTauHcal3x3OverPLead->clear();
  _subLeadTauElectronPreId->clear();
  _subLeadTauModifiedEOverP->clear();
  _subLeadTauBremsRecoveryEOverPLead->clear();
  _subLeadTauLTSignedIp->clear();
  _subLeadTauTrkIsoSumPt->clear();
  _subLeadTauECALIsoSumEt->clear();
  _subLeadTauIdByDecayModeFinding->clear();
  _subLeadTauIdByVLooseIsolation->clear();
  _subLeadTauIdByLooseIsolation->clear();
  _subLeadTauIdByMediumIsolation->clear();
  _subLeadTauIdByTightIsolation->clear();
  _subLeadTauIdByVLooseCombinedIsolationDBSumPtCorr->clear();
  _subLeadTauIdByLooseCombinedIsolationDBSumPtCorr->clear();
  _subLeadTauIdByMediumCombinedIsolationDBSumPtCorr->clear();
  _subLeadTauIdByTightCombinedIsolationDBSumPtCorr->clear();
  _subLeadTauIdByLooseElectronRejection->clear();
  _subLeadTauIdByMediumElectronRejection->clear();
  _subLeadTauIdByTightElectronRejection->clear();
  _subLeadTauIdByLooseElecRejection->clear();
  _subLeadTauIdByTightElecRejection->clear();
  _subLeadTauMEtMt->clear();
  
  //diTau info
  _diHadTauCosDPhi->clear();
  _diHadTauDelatR->clear();
  _diHadTauPZeta->clear();
  _diHadTauPZetaVis->clear();
  _diHadTauMass->clear();
  _diHadTauMetMass->clear();
  _diHadTauCollMass->clear();
  
  _diHadCandPt->clear();
  _diHadCandEt->clear();
  _diHadCandEta->clear();
  _diHadCandPhi->clear();
  _diHadCandMass->clear();
  _diHadCandCharge->clear();
  
  
  //Elec Info
  _elecMatched->clear();
  _elecMotherId->clear();
  _elecGenPt->clear();
  _elecGenE->clear();
  _elecGenEta->clear();
  _elecGenPhi->clear();
  
  _elecPt->clear();
  _elecE->clear();
  _elecEta->clear();
  _elecPhi->clear();
  _elecCharge->clear();
  _elecDxyVtx->clear();
  _elecDxyBS->clear();
  _elecDxyError->clear();
  _elecDzVtx->clear();
  _elecDzBS->clear();
  _elecDzError->clear();
  _elecVx->clear();
  _elecVy->clear();
  _elecVz->clear();
  _elecEcalIsoDr03->clear();
  _elecTrkIsoDr03->clear();
  _elecCaloIsoDr03->clear();
  _elecEcalIsoDr04->clear();
  _elecTrkIsoDr04->clear();
  _elecEcalIsoDr05->clear();
  _elecHadIsoDr05->clear();
  _elecTrkIsoDr05->clear();
  _elecCaloIsoDr05->clear();
  _elecPFIsoDR03SumChargedHadronPt->clear();
  _elecPFIsoDR03SumChargedParticlePt->clear();
  _elecPFIsoDR03SumNeutralHadronPt->clear();
  _elecPFIsoDR03SumPhotonHadronPt->clear();
  _elecPFIsoDR03SumPUPt->clear();
  _elecPFIsoDR04SumChargedHadronPt->clear();
  _elecPFIsoDR04SumChargedParticlePt->clear();
  _elecPFIsoDR04SumNeutralHadronPt->clear();
  _elecPFIsoDR04SumPhotonHadronPt->clear();
  _elecPFIsoDR04SumPUPt->clear();
 
  _elecClass->clear();
  _elecIdTight->clear();
  _elecIdLoose->clear();
  _elecIdRobustHighEnergy->clear();
  _elecIdRobustLoose->clear();
  _elecIdRobustTight->clear();
  _elecMissingHits->clear();
  _heepPassedEt->clear();
  _heepPassedPt->clear();
  _heepPassedDetEta->clear();
  _heepPassedCrack->clear();
  _heepPassedDEtaIn->clear();
  _heepPassedDPhiIn->clear();
  _heepPassedHadem->clear();
  _heepPassedSigmaIEtaIEta->clear();
  _heepPassed2by5Over5By5->clear();
  _heepPassedEcalHad1Iso->clear();
  _heepPassedHad2Iso->clear();
  _heepPassedTrkIso->clear();
  _heepPassedEcalDriven->clear();
  _heepPassedAllCuts->clear();
  
  _elecMEtMt->clear();

  //mu-LeadTau Info
  _elecLeadTauCosDPhi->clear();
  _elecLeadTauDelatR->clear();
  _elecLeadTauPZeta->clear();
  _elecLeadTauPZetaVis->clear();
  _elecLeadTauMass->clear();
  _elecLeadTauMetMass->clear();    
  _elecLeadTauCollMass->clear();
  
  //mu-SubLeadTau Info
  _elecSubLeadTauCosDPhi->clear();
  _elecSubLeadTauDelatR->clear();
  _elecSubLeadTauPZeta->clear();
  _elecSubLeadTauPZetaVis->clear();
  _elecSubLeadTauMass->clear();
  _elecSubLeadTauMetMass->clear();	
  _elecSubLeadTauCollMass->clear();
  
  _elecDiHadCandCosDPhi->clear();
  _elecDiHadCandMass->clear();
  
  _elecMetCosDPhi->clear();
  _diHadTauCandMetCosDPhi->clear();
  
}
  
//define this as a plug-in
DEFINE_FWK_MODULE(ElecTauTauAnalysis);
