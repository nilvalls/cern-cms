#include "HighMassAnalysis/Analysis/interface/MuTauTauAnalysis.h"

using namespace edm;
using namespace reco;
using namespace std;

MuTauTauAnalysis::MuTauTauAnalysis(const edm::ParameterSet& iCongif): HiMassTauAnalysis(iCongif){} 
MuTauTauAnalysis::~MuTauTauAnalysis(){}
  
void MuTauTauAnalysis::fillNtuple(){
  // store vector of PDF weights
  _PDFWeights = pdfWeightVector;

  // store ISR weights
  _ISRGluonWeight = isrgluon_weight;
  _ISRGammaWeight = isrgamma_weight;

  // store FSR weights
  _FSRWeight = fsr_weight;
  
  _genLevelMuonPt = -5.;
  _genLevelMuonEta = -10.;
  _genLevelMuonPhi = -10.;
  _genLevelTau1Pt = -5.;
  _genLevelTau1Eta = -10.;
  _genLevelTau1Phi = -10.;
  _genLevelTau2Pt = -5.;
  _genLevelTau2Eta = -10.;
  _genLevelTau2Phi = -10.;
  
  if(_AnalyzeSignal){
    //get the genParticles
    pair<bool, reco::Candidate*> theGenMuonPair = GetGenCandidate(13, 24, 3);
    if(!theGenMuonPair.first) theGenMuonPair = GetGenCandidate(-13, 24, 3);
    if(theGenMuonPair.first){
      _genLevelMuonPt = theGenMuonPair.second->pt();
      _genLevelMuonEta = theGenMuonPair.second->eta();
      _genLevelMuonPhi = theGenMuonPair.second->phi();
    }
  
    pair<bool, reco::Candidate*> theGenTau1Pair = GetGenCandidate(15, 25, 3);
    if(theGenTau1Pair.first){
      _genLevelTau1Pt = theGenTau1Pair.second->pt();
      _genLevelTau1Eta = theGenTau1Pair.second->eta();
      _genLevelTau1Phi = theGenTau1Pair.second->phi();
    }
  
    pair<bool, reco::Candidate*> theGenTau2Pair = GetGenCandidate(-15, 25, 3);
    if(theGenTau2Pair.first){
      _genLevelTau2Pt = theGenTau2Pair.second->pt();
      _genLevelTau2Eta = theGenTau2Pair.second->eta();
      _genLevelTau2Phi = theGenTau2Pair.second->phi();
    }

  }
  // get the primary vertex
  const reco::Vertex& thePrimaryEventVertex = (*(_primaryEventVertexCollection)->begin());
  const reco::BeamSpot& theBeamSpot = *_beamSpot;
  _nVtx = _primaryEventVertexCollection->size();
  //count good vertices 
  _nGoodVtx = 0;
  for( reco::VertexCollection::const_iterator vtxIt = _primaryEventVertexCollection->begin(); vtxIt != _primaryEventVertexCollection->end(); ++vtxIt){    
    if(vtxIt->ndof() < 7.) continue;
    if(abs(vtxIt->z()) > 24.) continue;
   // if(passRecoVertexCuts(*vtxIt))
    double vtxdxy = sqrt((vtxIt->x()*vtxIt->x()) + (vtxIt->y()*vtxIt->y()));
    if(vtxdxy > 2.0) continue; 
    _nGoodVtx++;
  }
  
  //get MET
  const pat::MET theMET = *(_patMETs->begin());
  _MEt = theMET.pt();
  _zEvent = foundZMuMuOrZEE().first;
  _zMassDiff = foundZMuMuOrZEE().second.first;
  _zPtAsymm = foundZMuMuOrZEE().second.second;
  
  //get number of btagged jets passing quality criteria
  _nBtagsHiEffTrkCnt = 0;
  _nBTagsHiEffSimpleSecVtx = 0;
  _nBtagsHiPurityTrkCnt = 0;
  _nBTagsHiPuritySimpleSecVtx = 0;
  _nBTagsCombSecVtx = 0;
  // loop over jet collection excluding jets in the direction of my di-tau cands
  for(pat::JetCollection::const_iterator patJet = _patJets->begin(); patJet != _patJets->end(); ++patJet){
    if(patJet->et() < _RecoJetPtCut) continue;
    if(fabs(patJet->eta()) >_RecoJetEtaMaxCut) continue;
    if(patJet->numberOfDaughters() <= 1) continue;
    if(patJet->neutralHadronEnergyFraction() > 0.99) continue;
    if(patJet->neutralEmEnergyFraction() > 0.99) continue;
    
    float trkCntHighEffBtag =  patJet->bDiscriminator("trackCountingHighEffBJetTags");
    float trkCntHighPurityBtag =  patJet->bDiscriminator("trackCountingHighPurBJetTags");
    float simpleSecVtxHighEffBTag = patJet->bDiscriminator("simpleSecondaryVertexHighEffBJetTags");
    float simpleSecVtxHighPurityBTag = patJet->bDiscriminator("simpleSecondaryVertexHighPurBJetTags");
    float combSecVtxBTag = patJet->bDiscriminator("combinedSecondaryVertexBJetTags");
    //use medium wp 
    if(trkCntHighEffBtag > 3.3) _nBtagsHiEffTrkCnt++;		       
    if(trkCntHighPurityBtag > 1.93) _nBtagsHiPurityTrkCnt++;		       
    if(simpleSecVtxHighEffBTag > 3.05) _nBTagsHiEffSimpleSecVtx++;	       
    if(simpleSecVtxHighPurityBTag > 2.0) _nBTagsHiPuritySimpleSecVtx++;  	       
    if(combSecVtxBTag > 0.5) _nBTagsCombSecVtx++; 
  }
  
  double _minLeadTauPt = _RecoTauPtMinCut;
  double _minSubLeadTauPt = _RecoTau2PtMinCut;
  double _maxTauEta = _RecoTauEtaCut;
  _nMuonCounter = 0;
  _nTauPairCounter = 0;
  for(pat::MuonCollection::const_iterator patMuon = _patMuons->begin(); patMuon != _patMuons->end(); ++patMuon){
    _nMuonCounter++;		       
    if(_AnalyzeSignal && !matchToGen(*patMuon, 13).first) continue;		       
    if(patMuon->pt() < _RecoMuonPtMinCut) continue; 
    if(fabs(patMuon->eta()) > _RecoMuonEtaCut) continue;
    for(CompositeCandidateCollection::const_iterator theCand = _patDiTaus->begin(); theCand != _patDiTaus->end(); ++theCand){	       
      pair<const pat::Tau*, const pat::Tau*> theCandDaughters = getPATComponents(*theCand);					       
      const pat::Tau* theLeadTau = theCandDaughters.first;									       
      const pat::Tau* theSubLeadTau = theCandDaughters.second;									       

      if(theCandDaughters.first->pt() < theCandDaughters.second->pt()){ 							       
  	theLeadTau = theCandDaughters.second;											       
  	theSubLeadTau = theCandDaughters.first; 										       
      } 															       
      if(_AnalyzeSignal && !matchToGen(*theLeadTau).first) continue;
      if(theLeadTau->pt() < _minLeadTauPt) continue;
      if(_AnalyzeSignal && !matchToGen(*theSubLeadTau).first) continue;
      if(theSubLeadTau->pt() < _minSubLeadTauPt) continue;
      if(abs(theLeadTau->eta()) > _maxTauEta || abs(theSubLeadTau->eta()) > _maxTauEta) continue;								    	
      if(reco::deltaR(*theLeadTau, *theSubLeadTau) < _DiTauDeltaRCut) continue;							 			    	
       _nTauPairCounter++;
      //LeadTau Info																		      
      _leadTauMatched->push_back(!_AnalyzeData ? (unsigned int)matchToGen(*theLeadTau).first : 0);								      
      _leadTauMotherId->push_back(!_AnalyzeData && theLeadTau->genParticleRef().isNonnull() ? abs(theLeadTau->genParticleRef()->mother()->mother()->pdgId()) : 0);    
      _leadTauGenPt->push_back(!_AnalyzeData ? matchToGen(*theLeadTau).second.pt() : 0);									      
      _leadTauGenE->push_back(!_AnalyzeData ? matchToGen(*theLeadTau).second.energy() : 0);									      
      _leadTauGenEta->push_back(!_AnalyzeData ? matchToGen(*theLeadTau).second.eta() : -10);									      
      _leadTauGenPhi->push_back(!_AnalyzeData ? matchToGen(*theLeadTau).second.phi() : -10);									      
    
      _leadTauPt->push_back(theLeadTau->pt());  														      
      _leadTauEnergy->push_back(theLeadTau->energy());  													      
      _leadTauEta->push_back(theLeadTau->eta());														      
      _leadTauPhi->push_back(theLeadTau->phi());														      
      _leadTauCharge->push_back(theLeadTau->charge());  													      
      const reco::PFCandidateRef theLeadTauLeadChargedCand = theLeadTau->leadPFChargedHadrCand();								      
      _leadTauLeadChargedCandPt->push_back(theLeadTauLeadChargedCand.isNonnull() ? theLeadTauLeadChargedCand->pt() : -1.);					      
      _leadTauLeadChargedCandCharge->push_back(theLeadTauLeadChargedCand.isNonnull() ? theLeadTauLeadChargedCand->charge() : -2);				      
      //_leadTauLeadChargedCandRecHitsSize->push_back(theLeadTauLeadChargedCand->trackRef() ? theLeadTauLeadChargedCand->trackRef()->recHitsSize(): 0.);	      
      _leadTauLeadChargedCandDxyVtx->push_back(theLeadTauLeadChargedCand.isNonnull() && theLeadTauLeadChargedCand->trackRef().isNonnull() ? theLeadTauLeadChargedCand->trackRef()->dxy(thePrimaryEventVertex.position()) : -100);
      _leadTauLeadChargedCandDxyBS->push_back(theLeadTauLeadChargedCand.isNonnull() && theLeadTauLeadChargedCand->trackRef().isNonnull() ? theLeadTauLeadChargedCand->trackRef()->dxy(theBeamSpot) : -100);
      _leadTauLeadChargedCandDxyError->push_back(theLeadTauLeadChargedCand.isNonnull() && theLeadTauLeadChargedCand->trackRef().isNonnull() ? theLeadTauLeadChargedCand->trackRef()->d0Error() : -10000.);
      _leadTauLeadChargedCandDzVtx->push_back(theLeadTauLeadChargedCand.isNonnull() && theLeadTauLeadChargedCand->trackRef().isNonnull() ? theLeadTauLeadChargedCand->trackRef()->dz(thePrimaryEventVertex.position()) : -100);
      _leadTauLeadChargedCandDzBS->push_back(theLeadTauLeadChargedCand.isNonnull() && theLeadTauLeadChargedCand->trackRef().isNonnull() ? theLeadTauLeadChargedCand->trackRef()->dz(theBeamSpot.position()) : -100);
      _leadTauLeadChargedCandDzError->push_back(theLeadTauLeadChargedCand.isNonnull() && theLeadTauLeadChargedCand->trackRef().isNonnull() ? theLeadTauLeadChargedCand->trackRef()->dzError() : -10000.);
      _leadTauLeadChargedCandVx->push_back(theLeadTauLeadChargedCand.isNonnull() && theLeadTauLeadChargedCand->trackRef().isNonnull() ? theLeadTauLeadChargedCand->trackRef()->vx() : -100.);
      _leadTauLeadChargedCandVy->push_back(theLeadTauLeadChargedCand.isNonnull() && theLeadTauLeadChargedCand->trackRef().isNonnull() ? theLeadTauLeadChargedCand->trackRef()->vy() : -100.);
      _leadTauLeadChargedCandVz->push_back(theLeadTauLeadChargedCand.isNonnull() && theLeadTauLeadChargedCand->trackRef().isNonnull() ? theLeadTauLeadChargedCand->trackRef()->vz() : -100.);
      _leadTauDeltaZVtx->push_back(theLeadTau->vz() - thePrimaryEventVertex.z());
      _leadTauNProngs->push_back(theLeadTau->signalPFChargedHadrCands().size());
      _leadTauEmFraction->push_back(theLeadTau->emFraction());
      _leadTauHcalTotOverPLead->push_back(theLeadTau->hcalTotOverPLead());
      _leadTauHcalMaxOverPLead->push_back(theLeadTau->hcalMaxOverPLead());
      _leadTauHcal3x3OverPLead->push_back(theLeadTau->hcal3x3OverPLead());
      _leadTauElectronPreId->push_back(theLeadTau->electronPreIDDecision());
      _leadTauModifiedEOverP->push_back(theLeadTau->ecalStripSumEOverPLead());
      _leadTauBremsRecoveryEOverPLead->push_back(theLeadTau->bremsRecoveryEOverPLead());
      _leadTauLTSignedIp->push_back(theLeadTau->leadPFChargedHadrCandsignedSipt());
      _leadTauTrkIsoSumPt->push_back(theLeadTau->isolationPFChargedHadrCandsPtSum());
      _leadTauECALIsoSumEt->push_back(theLeadTau->isolationPFGammaCandsEtSum());      
      _leadTauIdByDecayModeFinding->push_back((unsigned int)theLeadTau->tauID("decayModeFinding"));
      _leadTauIdByVLooseIsolation->push_back((unsigned int)theLeadTau->tauID("byVLooseIsolation"));
      _leadTauIdByLooseIsolation->push_back((unsigned int)theLeadTau->tauID("byLooseIsolation"));
      _leadTauIdByMediumIsolation->push_back((unsigned int)theLeadTau->tauID("byMediumIsolation"));
      _leadTauIdByTightIsolation->push_back((unsigned int)theLeadTau->tauID("byTightIsolation"));
      _leadTauIdByVLooseCombinedIsolationDBSumPtCorr->push_back((unsigned int)theLeadTau->tauID("byVLooseCombinedIsolationDeltaBetaCorr"));
      _leadTauIdByLooseCombinedIsolationDBSumPtCorr->push_back((unsigned int)theLeadTau->tauID("byVLooseIsolationDeltaBetaCorr"));
      _leadTauIdByMediumCombinedIsolationDBSumPtCorr->push_back((unsigned int)theLeadTau->tauID("byMediumIsolationDeltaBetaCorr"));
      _leadTauIdByTightCombinedIsolationDBSumPtCorr->push_back((unsigned int)theLeadTau->tauID("byTightCombinedIsolationDeltaBetaCorr"));
      _leadTauIdByLooseElectronRejection->push_back((unsigned int)theLeadTau->tauID("againstElectronLoose"));
      _leadTauIdByMediumElectronRejection->push_back((unsigned int)theLeadTau->tauID("againstElectronMedium"));
      _leadTauIdByTightElectronRejection->push_back((unsigned int)theLeadTau->tauID("againstElectronTight"));
      _leadTauIdByLooseMuonRejection->push_back((unsigned int)theLeadTau->tauID("againstMuonLoose"));
      _leadTauIdByTightMuonRejection->push_back((unsigned int)theLeadTau->tauID("againstMuonTight"));
      _leadTauMEtMt->push_back(CalculateLeptonMetMt(*theLeadTau, theMET));
    
      //SubLeadTauInfo
      _subLeadTauMatched->push_back(!_AnalyzeData ? (unsigned int)matchToGen(*theSubLeadTau).first : 0);						
      _subLeadTauMotherId->push_back(!_AnalyzeData && theSubLeadTau->genParticleRef().isNonnull() ? abs(theSubLeadTau->genParticleRef()->mother()->mother()->pdgId()) : 0); 
      _subLeadTauGenPt->push_back(!_AnalyzeData ? matchToGen(*theSubLeadTau).second.pt() : 0);  					
      _subLeadTauGenE->push_back(!_AnalyzeData ? matchToGen(*theSubLeadTau).second.energy() : 0);						
      _subLeadTauGenEta->push_back(!_AnalyzeData ? matchToGen(*theSubLeadTau).second.eta() : -10);						
      _subLeadTauGenPhi->push_back(!_AnalyzeData ? matchToGen(*theSubLeadTau).second.phi() : -10);						
    
      _subLeadTauPt->push_back(theSubLeadTau->pt());
      _subLeadTauEnergy->push_back(theSubLeadTau->energy());
      _subLeadTauEta->push_back(theSubLeadTau->eta());
      _subLeadTauPhi->push_back(theSubLeadTau->phi());
      _subLeadTauCharge->push_back(theSubLeadTau->charge());
      const reco::PFCandidateRef theSubLeadTauLeadChargedCand = theSubLeadTau->leadPFChargedHadrCand();
      _subLeadTauLeadChargedCandPt->push_back(theSubLeadTauLeadChargedCand.isNonnull() ? theSubLeadTauLeadChargedCand->pt() : -1.);
      _subLeadTauLeadChargedCandCharge->push_back(theSubLeadTauLeadChargedCand.isNonnull() ? theSubLeadTauLeadChargedCand->charge() : -2);
      //_subLeadTauLeadChargedCandRecHitsSize->push_back(theSubLeadTau->leadTrack().isNonnull() ? theSubLeadTau->leadTrack()->recHitsSize(): 0.);
      _subLeadTauLeadChargedCandDxyVtx->push_back(theSubLeadTauLeadChargedCand.isNonnull() && theSubLeadTauLeadChargedCand->trackRef().isNonnull() ? theSubLeadTauLeadChargedCand->trackRef()->dxy(thePrimaryEventVertex.position()) : -100);
      _subLeadTauLeadChargedCandDxyBS->push_back(theSubLeadTauLeadChargedCand.isNonnull() && theSubLeadTauLeadChargedCand->trackRef().isNonnull() ? theSubLeadTauLeadChargedCand->trackRef()->dxy(theBeamSpot) : -100);
      _subLeadTauLeadChargedCandDxyError->push_back(theSubLeadTauLeadChargedCand.isNonnull() && theSubLeadTauLeadChargedCand->trackRef().isNonnull() ? theSubLeadTauLeadChargedCand->trackRef()->d0Error() : -10000.);
      _subLeadTauLeadChargedCandDzVtx->push_back(theSubLeadTauLeadChargedCand.isNonnull() && theSubLeadTauLeadChargedCand->trackRef().isNonnull() ? theSubLeadTauLeadChargedCand->trackRef()->dz(thePrimaryEventVertex.position()) : -100);
      _subLeadTauLeadChargedCandDzBS->push_back(theSubLeadTauLeadChargedCand.isNonnull() && theSubLeadTauLeadChargedCand->trackRef().isNonnull() ? theSubLeadTauLeadChargedCand->trackRef()->dz(theBeamSpot.position()) : -100);
      _subLeadTauLeadChargedCandDzError->push_back(theSubLeadTauLeadChargedCand.isNonnull() && theSubLeadTauLeadChargedCand->trackRef().isNonnull() ? theSubLeadTauLeadChargedCand->trackRef()->dzError() : -10000.);
      _subLeadTauLeadChargedCandVx->push_back(theSubLeadTauLeadChargedCand.isNonnull() && theSubLeadTauLeadChargedCand->trackRef().isNonnull() ? theSubLeadTauLeadChargedCand->trackRef()->vx() : -100.);
      _subLeadTauLeadChargedCandVy->push_back(theSubLeadTauLeadChargedCand.isNonnull() && theSubLeadTauLeadChargedCand->trackRef().isNonnull() ? theSubLeadTauLeadChargedCand->trackRef()->vy() : -100.);
      _subLeadTauLeadChargedCandVz->push_back(theSubLeadTauLeadChargedCand.isNonnull() && theSubLeadTauLeadChargedCand->trackRef().isNonnull() ? theSubLeadTauLeadChargedCand->trackRef()->vz() : -100.);
      _subLeadTauDeltaZVtx->push_back(theSubLeadTau->vz() - thePrimaryEventVertex.z());
      _subLeadTauNProngs->push_back(theSubLeadTau->signalPFChargedHadrCands().size());
      _subLeadTauEmFraction->push_back(theSubLeadTau->emFraction());										     
      _subLeadTauHcalTotOverPLead->push_back(theSubLeadTau->hcalTotOverPLead());							     
      _subLeadTauHcalMaxOverPLead->push_back(theSubLeadTau->hcalMaxOverPLead());							     
      _subLeadTauHcal3x3OverPLead->push_back(theSubLeadTau->hcal3x3OverPLead());							     
      _subLeadTauElectronPreId->push_back(theSubLeadTau->electronPreIDDecision());								     
      _subLeadTauModifiedEOverP->push_back(theSubLeadTau->ecalStripSumEOverPLead());								     
      _subLeadTauBremsRecoveryEOverPLead->push_back(theSubLeadTau->bremsRecoveryEOverPLead());  					     
      _subLeadTauLTSignedIp->push_back(theSubLeadTau->leadPFChargedHadrCandsignedSipt());
      _subLeadTauTrkIsoSumPt->push_back(theSubLeadTau->isolationPFChargedHadrCandsPtSum());
      _subLeadTauECALIsoSumEt->push_back(theSubLeadTau->isolationPFGammaCandsEtSum());
      _subLeadTauIdByDecayModeFinding->push_back((unsigned int)theSubLeadTau->tauID("decayModeFinding"));
      _subLeadTauIdByVLooseIsolation->push_back((unsigned int)theSubLeadTau->tauID("byVLooseIsolation"));
      _subLeadTauIdByLooseIsolation->push_back((unsigned int)theSubLeadTau->tauID("byLooseIsolation"));
      _subLeadTauIdByMediumIsolation->push_back((unsigned int)theSubLeadTau->tauID("byMediumIsolation"));
      _subLeadTauIdByTightIsolation->push_back((unsigned int)theSubLeadTau->tauID("byTightIsolation"));
      _subLeadTauIdByVLooseCombinedIsolationDBSumPtCorr->push_back((unsigned int)theSubLeadTau->tauID("byVLooseCombinedIsolationDeltaBetaCorr"));
      _subLeadTauIdByLooseCombinedIsolationDBSumPtCorr->push_back((unsigned int)theSubLeadTau->tauID("byVLooseIsolationDeltaBetaCorr"));
      _subLeadTauIdByMediumCombinedIsolationDBSumPtCorr->push_back((unsigned int)theSubLeadTau->tauID("byMediumIsolationDeltaBetaCorr"));
      _subLeadTauIdByTightCombinedIsolationDBSumPtCorr->push_back((unsigned int)theSubLeadTau->tauID("byTightCombinedIsolationDeltaBetaCorr"));
      _subLeadTauIdByLooseElectronRejection->push_back((unsigned int)theSubLeadTau->tauID("againstElectronLoose"));
      _subLeadTauIdByMediumElectronRejection->push_back((unsigned int)theSubLeadTau->tauID("againstElectronMedium"));
      _subLeadTauIdByTightElectronRejection->push_back((unsigned int)theSubLeadTau->tauID("againstElectronTight"));
      _subLeadTauIdByLooseMuonRejection->push_back((unsigned int)theSubLeadTau->tauID("againstMuonLoose"));
      _subLeadTauIdByTightMuonRejection->push_back((unsigned int)theSubLeadTau->tauID("againstMuonTight"));
      _subLeadTauMEtMt->push_back(CalculateLeptonMetMt(*theSubLeadTau, theMET));
    
      //DiTauInfo
      _diHadTauCosDPhi->push_back(cos(TMath::Abs(normalizedPhi(theLeadTau->phi() - theSubLeadTau->phi()))));	  
      _diHadTauDelatR->push_back(reco::deltaR(*theLeadTau, *theSubLeadTau));
      _diHadTauPZeta->push_back(CalculatePZeta(*theLeadTau,*theSubLeadTau, theMET));
      _diHadTauPZetaVis->push_back(CalculatePZetaVis(*theLeadTau,*theSubLeadTau));

      _diHadTauMass->push_back(GetVisMass(*theLeadTau,*theSubLeadTau));
      _diHadTauMetMass->push_back(GetVisPlusMETMass(*theLeadTau,*theSubLeadTau, theMET));    
      _diHadTauCollMass->push_back(GetCollinearApproxMass(*theLeadTau,*theSubLeadTau, theMET));
      
      _diHadCandPt->push_back(theCand->pt());
      _diHadCandEt->push_back(theCand->et());
      _diHadCandEta->push_back(theCand->eta());
      _diHadCandPhi->push_back(theCand->phi());
      _diHadCandMass->push_back(theCand->mass());
      _diHadCandCharge->push_back(theCand->charge());
    
      //get muon info and pair them with the taus
      _muonMatched->push_back(!_AnalyzeData ? (unsigned int)matchToGen(*patMuon, 13).first : 0);							
      _muonMotherId->push_back(!_AnalyzeData && patMuon->genParticleRef().isNonnull() ? abs(patMuon->genParticleRef()->mother()->mother()->pdgId()) : 0);			 
      _muonGenPt->push_back(!_AnalyzeData ? matchToGen(*patMuon, 13).second.pt() : 0);  					
      _muonGenE->push_back(!_AnalyzeData ? matchToGen(*patMuon, 13).second.energy() : 0);						
      _muonGenEta->push_back(!_AnalyzeData ? matchToGen(*patMuon, 13).second.eta() : -10);						
      _muonGenPhi->push_back(!_AnalyzeData ? matchToGen(*patMuon, 13).second.phi() : -10);						
     
      _muonPt->push_back(patMuon->pt());												       
      _muonE->push_back(patMuon->energy());											       
      _muonEta->push_back(patMuon->eta());											       
      _muonPhi->push_back(patMuon->phi());
      _muonCharge->push_back(patMuon->charge());											       
      _muonDxyVtx->push_back(patMuon->innerTrack().isNonnull() ? patMuon->innerTrack()->dxy(thePrimaryEventVertex.position()) : -100.);
      _muonDxyBS->push_back(patMuon->innerTrack().isNonnull() ? patMuon->innerTrack()->dxy(theBeamSpot) : -100.);
      _muonDxyError->push_back(patMuon->innerTrack().isNonnull() ? patMuon->innerTrack()->d0Error() : -10000.);
      _muonDzVtx->push_back(patMuon->innerTrack().isNonnull() ? patMuon->innerTrack()->dz(thePrimaryEventVertex.position()) : -100.);
      _muonDzBS->push_back(patMuon->innerTrack().isNonnull() ? patMuon->innerTrack()->dz(theBeamSpot.position()) : -100.);
      _muonDzError->push_back(patMuon->innerTrack().isNonnull() ? patMuon->innerTrack()->dzError() : -10000.);
      _muonVx->push_back(patMuon->innerTrack().isNonnull() ? patMuon->innerTrack()->vx() : -100.);
      _muonVy->push_back(patMuon->innerTrack().isNonnull() ? patMuon->innerTrack()->vy() : -100.);
      _muonVz->push_back(patMuon->innerTrack().isNonnull() ? patMuon->innerTrack()->vz() : -100.);
      _muonValidHits->push_back(patMuon->globalTrack().isNonnull() ? patMuon->globalTrack()->numberOfValidHits() : 0.);
      _muonNormChiSqrd->push_back(patMuon->globalTrack().isNonnull() ? patMuon->globalTrack()->normalizedChi2(): 1000.);
      _muonChambersValidHits->push_back(patMuon->globalTrack().isNonnull() ? patMuon->globalTrack()->hitPattern().numberOfValidMuonHits() : 0);
      _muonMatchedStations->push_back(patMuon->numberOfMatchedStations());
      _muonPixelHits->push_back(patMuon->innerTrack().isNonnull() ? patMuon->innerTrack()->hitPattern().numberOfValidPixelHits() : 0);
      _muonTrkLayersWithHits->push_back(patMuon->track().isNonnull() ? patMuon->track()->hitPattern().trackerLayersWithMeasurement() : 0);
      _muonEcalIsoDr03->push_back(patMuon->ecalIso());
      _muonTrkIsoDr03->push_back(patMuon->trackIso());
      _muonCaloIsoDr03->push_back(patMuon->caloIso());
      //_muonEcalIsoDr04->push_back(patMuon->ecalIsoDeposit()->depositAndCountWithin(0.4, reco::IsoDeposit::Vetos(),_RecoMuonEcalIsoRecHitThreshold).first);
      //_muonTrkIsoDr04->push_back(patMuon->trackIsoDeposit()->depositAndCountWithin(0.4, reco::IsoDeposit::Vetos(),_RecoMuonTrackIsoTrkThreshold).first);
      _muonEcalIsoDr05->push_back(patMuon->isolationR05().emEt);
      _muonHadIsoDr05->push_back(patMuon->isolationR05().hadEt);
      _muonTrkIsoDr05->push_back(patMuon->isolationR05().sumPt);
      _muonCaloIsoDr05->push_back(patMuon->isolationR05().hadEt + patMuon->isolationR05().emEt);
      _muonPFIsoDR03SumChargedHadronPt->push_back(patMuon->pfIsolationR03().sumChargedHadronPt);
      _muonPFIsoDR03SumChargedParticlePt->push_back(patMuon->pfIsolationR03().sumChargedParticlePt);
      _muonPFIsoDR03SumNeutralHadronPt->push_back(patMuon->pfIsolationR03().sumNeutralHadronEt);
      _muonPFIsoDR03SumPhotonHadronPt->push_back(patMuon->pfIsolationR03().sumPhotonEt);
      _muonPFIsoDR03SumPUPt->push_back(patMuon->pfIsolationR03().sumPUPt);
      _muonPFIsoDR04SumChargedHadronPt->push_back(patMuon->pfIsolationR04().sumChargedHadronPt);
      _muonPFIsoDR04SumChargedParticlePt->push_back(patMuon->pfIsolationR04().sumChargedParticlePt);
      _muonPFIsoDR04SumNeutralHadronPt->push_back(patMuon->pfIsolationR04().sumNeutralHadronEt);
      _muonPFIsoDR04SumPhotonHadronPt->push_back(patMuon->pfIsolationR04().sumPhotonEt);
      _muonPFIsoDR04SumPUPt->push_back(patMuon->pfIsolationR04().sumPUPt);
      _muonCaloComp->push_back(patMuon->caloCompatibility());
      _muonIsGlobalMuon->push_back((int)patMuon->isGlobalMuon());
      _muonIsTrackerMuon->push_back((int)patMuon->isTrackerMuon());
      _muonIsGlobalPromptTight->push_back((int)patMuon->muonID("GlobalMuonPromptTight"));
      _muonIsTMLastStationLoose->push_back((int)patMuon->muonID("TMLastStationLoose"));
      _muonIsTMLastStationTight->push_back((int)patMuon->muonID("TMLastStationTight"));
      _muonIsTM2DCompatibilityLoose->push_back((int)patMuon->muonID("TM2DCompatibilityLoose"));
      _muonIsTM2DCompatibilityTight->push_back((int)patMuon->muonID("TM2DCompatibilityTight"));
      _muonPionVeto->push_back((_RecoMuonCaloCompCoefficient * muon::caloCompatibility(*patMuon)) + (_RecoMuonSegmCompCoefficient * muon::segmentCompatibility(*patMuon)));
      
      _muonMEtMt->push_back(CalculateLeptonMetMt(*patMuon, theMET));
      
      //get mu-LeadTau combinations
      _muonLeadTauCosDPhi->push_back(cos(TMath::Abs(normalizedPhi(theLeadTau->phi() - patMuon->phi()))));
      _muonLeadTauDelatR->push_back(reco::deltaR(*theLeadTau, *patMuon));
      _muonLeadTauPZeta->push_back(CalculatePZeta(*theLeadTau,*patMuon, theMET));
      _muonLeadTauPZetaVis->push_back(CalculatePZetaVis(*theLeadTau,*patMuon));
      _muonLeadTauMass->push_back(GetVisMass(*theLeadTau,*patMuon));
      _muonLeadTauMetMass->push_back(GetVisPlusMETMass(*theLeadTau,*patMuon, theMET));	
      _muonLeadTauCollMass->push_back(GetCollinearApproxMass(*theLeadTau,*patMuon, theMET));
      
      //get mu-SubLeadTau combinations
      _muonSubLeadTauCosDPhi->push_back(cos(TMath::Abs(normalizedPhi(theSubLeadTau->phi() - patMuon->phi()))));
      _muonSubLeadTauDelatR->push_back(reco::deltaR(*theSubLeadTau, *patMuon));
      _muonSubLeadTauPZeta->push_back(CalculatePZeta(*theSubLeadTau,*patMuon, theMET));
      _muonSubLeadTauPZetaVis->push_back(CalculatePZetaVis(*theSubLeadTau,*patMuon));
      _muonSubLeadTauMass->push_back(GetVisMass(*theSubLeadTau,*patMuon));
      _muonSubLeadTauMetMass->push_back(GetVisPlusMETMass(*theSubLeadTau,*patMuon, theMET));    
      _muonSubLeadTauCollMass->push_back(GetCollinearApproxMass(*theSubLeadTau,*patMuon, theMET));
      
      // get mu-DiTau candidate correlations
      _muonDiHadCandCosDPhi->push_back(cos(TMath::Abs(normalizedPhi(theCand->phi() - patMuon->phi()))));
      _muonDiHadCandMass->push_back(GetVisMass(*theCand,*patMuon));
      
      //get met correlations
      _muonMetCosDPhi->push_back(cos(TMath::Abs(normalizedPhi(patMuon->phi() - theMET.phi()))));
      _diHadTauCandMetCosDPhi->push_back(cos(TMath::Abs(normalizedPhi(theCand->phi() - theMET.phi()))));
    }
  }
  if(_nMuonCounter > 0) _nMuons++;
  if(_nTauPairCounter > 0) _nTauPairs++;
 
  _MuTauTauTree->Fill();
}

void MuTauTauAnalysis::setupBranches(){
  _MuTauTauTree = new TTree(_NtupleTreeName.c_str(), "MuTauTauTree");
  
  //Event info
  _MuTauTauTree->Branch("runNumber",&runNum);
  _MuTauTauTree->Branch("eventNumber",&eventNum);
 
  _MuTauTauTree->Branch("InTimePU",&_inTimePUNumInteractions);
  _MuTauTauTree->Branch("OutTimePlusPU",&_outTimePlusPUNumInteractions);
  _MuTauTauTree->Branch("OutTimeMinusPU",&_outTimeMinusPUNumInteractions);
  
  _MuTauTauTree->Branch("nVtx",&_nVtx);
  _MuTauTauTree->Branch("nGoodVtx",&_nGoodVtx);
  
  _MuTauTauTree->Branch("MEt",&_MEt);
  _MuTauTauTree->Branch("zEvent", &_zEvent);
  _MuTauTauTree->Branch("zMassDiff", &_zMassDiff);
  _MuTauTauTree->Branch("zPtAsymm", &_zPtAsymm);
  
  //_MuTauTauTree->Branch("passedHLTMu5Elec17",&_passedHLTMu5Elec17);
  //_MuTauTauTree->Branch("passedHLTMu11Elec8",&_passedHLTMu11Elec8);
  
  //_MuTauTauTree->Branch("passedHLTMu8Elec17",&_passedHLTMu8Elec17);
  //_MuTauTauTree->Branch("passedHLTMu17Elec8",&_passedHLTMu17Elec8);
  
  //_MuTauTauTree->Branch("passedHLTIsoMu24",&_passedHLTIsoMu24);

  _MuTauTauTree->Branch("PDFWweights", &_PDFWeights);
  _MuTauTauTree->Branch("ISRGluonWeight", &_ISRGluonWeight);
  _MuTauTauTree->Branch("ISRGammaWeight", &_ISRGammaWeight);
  _MuTauTauTree->Branch("FSRWeight", &_FSRWeight);
  
  //GenLevel Info (not dependent on reco)
  _MuTauTauTree->Branch("genLevelMuonPt", &_genLevelMuonPt);
  _MuTauTauTree->Branch("genLevelMuonEta", &_genLevelMuonEta);
  _MuTauTauTree->Branch("genLevelMuonPhi", &_genLevelMuonPhi);
  _MuTauTauTree->Branch("genLevelTau1Pt", &_genLevelTau1Pt);
  _MuTauTauTree->Branch("genLevelTau1Eta", &_genLevelTau1Eta);
  _MuTauTauTree->Branch("genLevelTau1Phi", &_genLevelTau1Phi);
  _MuTauTauTree->Branch("genLevelTau2Pt", &_genLevelTau2Pt);
  _MuTauTauTree->Branch("genLevelTau2Eta", &_genLevelTau2Eta);
  _MuTauTauTree->Branch("genLevelTau2Phi", &_genLevelTau2Phi);
  
  //BTag Counting
  _MuTauTauTree->Branch("nBtagsHiEffTrkCnt",&_nBtagsHiEffTrkCnt);
  _MuTauTauTree->Branch("nBtagsHiPurityTrkCnt",&_nBtagsHiPurityTrkCnt);
  _MuTauTauTree->Branch("nBTagsHiEffSimpleSecVtx",&_nBTagsHiEffSimpleSecVtx);
  _MuTauTauTree->Branch("nBTagsHiPuritySimpleSecVtx",&_nBTagsHiPuritySimpleSecVtx);
  _MuTauTauTree->Branch("nBTagsCombSecVtx",&_nBTagsCombSecVtx);
  
  // For the Lead Tau
  _MuTauTauTree->Branch("leadTauMatched", &_leadTauMatched);
  _MuTauTauTree->Branch("leadTauMotherId", &_leadTauMotherId);
  _MuTauTauTree->Branch("leadTauGenPt", &_leadTauGenPt);
  _MuTauTauTree->Branch("leadTauGenE", &_leadTauGenE);
  _MuTauTauTree->Branch("leadTauGenEta", &_leadTauGenEta);
  _MuTauTauTree->Branch("leadTauGenPhi", &_leadTauGenPhi);
  
  _MuTauTauTree->Branch("leadTauPt",&_leadTauPt);
  _MuTauTauTree->Branch("leadTauEnergy",&_leadTauEnergy);
  _MuTauTauTree->Branch("leadTauEta",&_leadTauEta);
  _MuTauTauTree->Branch("leadTauPhi",&_leadTauPhi);
  _MuTauTauTree->Branch("leadTauCharge",&_leadTauCharge);
  _MuTauTauTree->Branch("leadTauLeadChargedCandPt",&_leadTauLeadChargedCandPt);
  _MuTauTauTree->Branch("leadTauLeadChargedCandCharge",&_leadTauLeadChargedCandCharge);
  //_MuTauTauTree->Branch("leadTauLeadChargedCandRecHitsSize",&_leadTauLeadChargedCandRecHitsSize);
  _MuTauTauTree->Branch("leadTauLeadChargedCandDxyVtx",&_leadTauLeadChargedCandDxyVtx);
  _MuTauTauTree->Branch("leadTauLeadChargedCandDxyBS",&_leadTauLeadChargedCandDxyBS);
  _MuTauTauTree->Branch("leadTauLeadChargedCandDxyError",&_leadTauLeadChargedCandDxyError);
  _MuTauTauTree->Branch("leadTauLeadChargedCandDzVtx",&_leadTauLeadChargedCandDzVtx);
  _MuTauTauTree->Branch("leadTauLeadChargedCandDzBS",&_leadTauLeadChargedCandDzBS);
  _MuTauTauTree->Branch("leadTauLeadChargedCandDzError",&_leadTauLeadChargedCandDzError);
  _MuTauTauTree->Branch("leadTauLeadChargedCandVx",&_leadTauLeadChargedCandVx);
  _MuTauTauTree->Branch("leadTauLeadChargedCandVy",&_leadTauLeadChargedCandVy);
  _MuTauTauTree->Branch("leadTauLeadChargedCandVz",&_leadTauLeadChargedCandVz);
  _MuTauTauTree->Branch("leadTauDeltaZVtx",&_leadTauDeltaZVtx);
  _MuTauTauTree->Branch("leadTauNProngs",&_leadTauNProngs);
  _MuTauTauTree->Branch("leadTauEmFraction",&_leadTauEmFraction);
  _MuTauTauTree->Branch("leadTauHcalTotOverPLead",&_leadTauHcalTotOverPLead);
  _MuTauTauTree->Branch("leadTauHcalMaxOverPLead",&_leadTauHcalMaxOverPLead);
  _MuTauTauTree->Branch("leadTauHcal3x3OverPLead",&_leadTauHcal3x3OverPLead);
  _MuTauTauTree->Branch("leadTauElectronPreId",&_leadTauElectronPreId);
  _MuTauTauTree->Branch("leadTauModifiedEOverP",&_leadTauModifiedEOverP);
  _MuTauTauTree->Branch("leadTauBremsRecoveryEOverPLead",&_leadTauBremsRecoveryEOverPLead);
  _MuTauTauTree->Branch("leadTauLTSignedIp",&_leadTauLTSignedIp);
  _MuTauTauTree->Branch("leadTauTrkIsoSumPt",&_leadTauTrkIsoSumPt);
  _MuTauTauTree->Branch("leadTauECALIsoSumEt",&_leadTauECALIsoSumEt);
  _MuTauTauTree->Branch("leadTauIdByDecayModeFinding",&_leadTauIdByDecayModeFinding);
  _MuTauTauTree->Branch("leadTauIdByVLooseIsolation",&_leadTauIdByVLooseIsolation);
  _MuTauTauTree->Branch("leadTauIdByLooseIsolation",&_leadTauIdByLooseIsolation);
  _MuTauTauTree->Branch("leadTauIdByMediumIsolation",&_leadTauIdByMediumIsolation);
  _MuTauTauTree->Branch("leadTauIdByTightIsolation",&_leadTauIdByTightIsolation);
  _MuTauTauTree->Branch("leadTauIdByVLooseCombinedIsolationDBSumPtCorr",&_leadTauIdByVLooseCombinedIsolationDBSumPtCorr);
  _MuTauTauTree->Branch("leadTauIdByLooseCombinedIsolationDBSumPtCorr",&_leadTauIdByLooseCombinedIsolationDBSumPtCorr);
  _MuTauTauTree->Branch("leadTauIdByMediumCombinedIsolationDBSumPtCorr",&_leadTauIdByMediumCombinedIsolationDBSumPtCorr);
  _MuTauTauTree->Branch("leadTauIdByTightCombinedIsolationDBSumPtCorr",&_leadTauIdByTightCombinedIsolationDBSumPtCorr);
  _MuTauTauTree->Branch("leadTauIdByLooseElectronRejection",&_leadTauIdByLooseElectronRejection);
  _MuTauTauTree->Branch("leadTauIdByMediumElectronRejection",&_leadTauIdByMediumElectronRejection);
  _MuTauTauTree->Branch("leadTauIdByTightElectronRejection",&_leadTauIdByTightElectronRejection);
  _MuTauTauTree->Branch("leadTauIdByLooseMuonRejection",&_leadTauIdByLooseMuonRejection);
  _MuTauTauTree->Branch("leadTauIdByTightMuonRejection",&_leadTauIdByTightMuonRejection);
  _MuTauTauTree->Branch("leadTauMEtMt", &_leadTauMEtMt);
   
   // For the Sub Lead Tau
  _MuTauTauTree->Branch("subLeadTauMatched", &_subLeadTauMatched);
  _MuTauTauTree->Branch("subLeadTauMotherId", &_subLeadTauMotherId);
  _MuTauTauTree->Branch("subLeadTauGenPt", &_subLeadTauGenPt);
  _MuTauTauTree->Branch("subLeadTauGenE", &_subLeadTauGenE);
  _MuTauTauTree->Branch("subLeadTauGenEta", &_subLeadTauGenEta);
  _MuTauTauTree->Branch("subLeadTauGenPhi", &_subLeadTauGenPhi);
  
  _MuTauTauTree->Branch("subLeadTauPt",&_subLeadTauPt);
  _MuTauTauTree->Branch("subLeadTauEnergy",&_subLeadTauEnergy);
  _MuTauTauTree->Branch("subLeadTauEta",&_subLeadTauEta);
  _MuTauTauTree->Branch("subLeadTauPhi",&_subLeadTauPhi);
  _MuTauTauTree->Branch("subLeadTauCharge",&_subLeadTauCharge);
  _MuTauTauTree->Branch("subLeadTauLeadChargedCandPt",&_subLeadTauLeadChargedCandPt);
  _MuTauTauTree->Branch("subLeadTauLeadChargedCandCharge",&_subLeadTauLeadChargedCandCharge);
  //_MuTauTauTree->Branch("subLeadTauLeadChargedCandRecHitsSize",&_subLeadTauLeadChargedCandRecHitsSize);
  _MuTauTauTree->Branch("subleadTauLeadChargedCandDxyVtx",&_subLeadTauLeadChargedCandDxyVtx);
  _MuTauTauTree->Branch("subleadTauLeadChargedCandDxyBS",&_subLeadTauLeadChargedCandDxyBS);
  _MuTauTauTree->Branch("subleadTauLeadChargedCandDxyError",&_subLeadTauLeadChargedCandDxyError);
  _MuTauTauTree->Branch("subleadTauLeadChargedCandDzVtx",&_subLeadTauLeadChargedCandDzVtx);
  _MuTauTauTree->Branch("subleadTauLeadChargedCandDzBS",&_subLeadTauLeadChargedCandDzBS);
  _MuTauTauTree->Branch("subLeadTauLeadChargedCandVx",&_subLeadTauLeadChargedCandVx);
  _MuTauTauTree->Branch("subLeadTauLeadChargedCandVy",&_subLeadTauLeadChargedCandVy);
  _MuTauTauTree->Branch("subLeadTauLeadChargedCandVz",&_subLeadTauLeadChargedCandVz);
  _MuTauTauTree->Branch("subleadTauLeadChargedCandDzError",&_subLeadTauLeadChargedCandDzError);
  _MuTauTauTree->Branch("subLeadTauDeltaZVtx",&_subLeadTauDeltaZVtx);
  _MuTauTauTree->Branch("subLeadTauNProngs",&_subLeadTauNProngs);
  _MuTauTauTree->Branch("subLeadTauEmFraction",&_subLeadTauEmFraction);
  _MuTauTauTree->Branch("subLeadTauHcalTotOverPLead",&_subLeadTauHcalTotOverPLead);
  _MuTauTauTree->Branch("subLeadTauHcalMaxOverPLead",&_subLeadTauHcalMaxOverPLead);
  _MuTauTauTree->Branch("subLeadTauHcal3x3OverPLead",&_subLeadTauHcal3x3OverPLead);
  _MuTauTauTree->Branch("subLeadTauElectronPreId",&_subLeadTauElectronPreId);
  _MuTauTauTree->Branch("subLeadTauModifiedEOverP",&_subLeadTauModifiedEOverP);
  _MuTauTauTree->Branch("subLeadTauBremsRecoveryEOverPLead",&_subLeadTauBremsRecoveryEOverPLead);
  _MuTauTauTree->Branch("subLeadTauLTSignedIp",&_subLeadTauLTSignedIp);
  _MuTauTauTree->Branch("subLeadTauTrkIsoSumPt",&_subLeadTauTrkIsoSumPt);
  _MuTauTauTree->Branch("subLeadTauECALIsoSumEt",&_subLeadTauECALIsoSumEt);
  _MuTauTauTree->Branch("subLeadTauIdByDecayModeFinding",&_subLeadTauIdByDecayModeFinding);
  _MuTauTauTree->Branch("subLeadTauIdByVLooseIsolation",&_subLeadTauIdByVLooseIsolation);
  _MuTauTauTree->Branch("subLeadTauIdByLooseIsolation",&_subLeadTauIdByLooseIsolation);
  _MuTauTauTree->Branch("subLeadTauIdByMediumIsolation",&_subLeadTauIdByMediumIsolation);
  _MuTauTauTree->Branch("subLeadTauIdByTightIsolation",&_subLeadTauIdByTightIsolation);
  _MuTauTauTree->Branch("subLeadTauIdByVLooseCombinedIsolationDBSumPtCorr",&_subLeadTauIdByVLooseCombinedIsolationDBSumPtCorr);
  _MuTauTauTree->Branch("subLeadTauIdByLooseCombinedIsolationDBSumPtCorr",&_subLeadTauIdByLooseCombinedIsolationDBSumPtCorr);
  _MuTauTauTree->Branch("subLeadTauIdByMediumCombinedIsolationDBSumPtCorr",&_subLeadTauIdByMediumCombinedIsolationDBSumPtCorr);
  _MuTauTauTree->Branch("subLeadTauIdByTightCombinedIsolationDBSumPtCorr",&_subLeadTauIdByTightCombinedIsolationDBSumPtCorr);
  _MuTauTauTree->Branch("subLeadTauIdByLooseElectronRejection",&_subLeadTauIdByLooseElectronRejection);
  _MuTauTauTree->Branch("subLeadTauIdByMediumElectronRejection",&_subLeadTauIdByMediumElectronRejection);
  _MuTauTauTree->Branch("subLeadTauIdByTightElectronRejection",&_subLeadTauIdByTightElectronRejection);
  _MuTauTauTree->Branch("subLeadTauIdByLooseMuonRejection",&_subLeadTauIdByLooseMuonRejection);
  _MuTauTauTree->Branch("subLeadTauIdByTightMuonRejection",&_subLeadTauIdByTightMuonRejection);
  _MuTauTauTree->Branch("subLeadTauMEtMt", &_subLeadTauMEtMt);
  
  _MuTauTauTree->Branch("diHadTauCosDPhi", &_diHadTauCosDPhi);
  _MuTauTauTree->Branch("diHadTauDelatR", &_diHadTauDelatR);
  _MuTauTauTree->Branch("diHadTauPZeta", &_diHadTauPZeta);
  _MuTauTauTree->Branch("diHadTauPZetaVis", &_diHadTauPZetaVis);
  _MuTauTauTree->Branch("diHadTauMass", &_diHadTauMass);
  _MuTauTauTree->Branch("diHadTauMetMass", &_diHadTauMetMass);
  _MuTauTauTree->Branch("diHadTauCollMass", &_diHadTauCollMass);
  
  _MuTauTauTree->Branch("diHadCandPt", &_diHadCandPt);
  _MuTauTauTree->Branch("diHadCandEt", &_diHadCandEt);
  _MuTauTauTree->Branch("diHadCandPhi", &_diHadCandPhi);
  _MuTauTauTree->Branch("diHadCandEta", &_diHadCandEta);
  _MuTauTauTree->Branch("diHadCandMass", &_diHadCandMass);
  _MuTauTauTree->Branch("diHadCandCharge", &_diHadCandCharge);
  
  
  //muon info
  _MuTauTauTree->Branch("muonMatched", &_muonMatched);
  _MuTauTauTree->Branch("muonMotherId", &_muonMotherId);
  _MuTauTauTree->Branch("muonGenPt", &_muonGenPt);
  _MuTauTauTree->Branch("muonGenE", &_muonGenE);
  _MuTauTauTree->Branch("muonGenEta", &_muonGenEta);
  _MuTauTauTree->Branch("muonGenPhi", &_muonGenPhi);
  
  _MuTauTauTree->Branch("muonPt", &_muonPt);
  _MuTauTauTree->Branch("muonE", &_muonE);
  _MuTauTauTree->Branch("muonEta", &_muonEta);
  _MuTauTauTree->Branch("muonPhi", &_muonPhi);
  _MuTauTauTree->Branch("muonCharge", &_muonCharge);
  _MuTauTauTree->Branch("muonDxyVtx", &_muonDxyVtx);
  _MuTauTauTree->Branch("muonDxyBS", &_muonDxyBS);
  _MuTauTauTree->Branch("muonDxyError", &_muonDxyError);
  _MuTauTauTree->Branch("muonDzVtx", &_muonDzVtx);
  _MuTauTauTree->Branch("muonDzBS", &_muonDzBS);
  _MuTauTauTree->Branch("muonDzError", &_muonDzError);
  _MuTauTauTree->Branch("muonVx", &_muonVx);
  _MuTauTauTree->Branch("muonVy", &_muonVy);
  _MuTauTauTree->Branch("muonVz", &_muonVz);
  _MuTauTauTree->Branch("muonValidHits", &_muonValidHits);
  _MuTauTauTree->Branch("muonNormChiSqrd", &_muonNormChiSqrd);
  _MuTauTauTree->Branch("muonChambersValidHits", &_muonChambersValidHits);
  _MuTauTauTree->Branch("muonMatchedStations", &_muonMatchedStations);
  _MuTauTauTree->Branch("muonPixelHits", &_muonPixelHits);
  _MuTauTauTree->Branch("muonTrkLayersWithHits", &_muonTrkLayersWithHits);
  _MuTauTauTree->Branch("muonEcalIsoDr03", &_muonEcalIsoDr03);
  _MuTauTauTree->Branch("muonTrkIsoDr03", &_muonTrkIsoDr03);
  _MuTauTauTree->Branch("muonCaloIsoDr03", &_muonCaloIsoDr03);
  _MuTauTauTree->Branch("muonEcalIsoDr04", &_muonEcalIsoDr04);
  _MuTauTauTree->Branch("muonTrkIsoDr04", &_muonTrkIsoDr04);
  _MuTauTauTree->Branch("muonEcalIsoDr05", &_muonEcalIsoDr05);
  _MuTauTauTree->Branch("muonHadIsoDr05", &_muonHadIsoDr05);
  _MuTauTauTree->Branch("muonTrkIsoDr05", &_muonTrkIsoDr05);
  _MuTauTauTree->Branch("muonCaloIsoDr05", &_muonCaloIsoDr05);
  _MuTauTauTree->Branch("muonPFIsoDR03SumChargedHadronPt", &_muonPFIsoDR03SumChargedHadronPt);
  _MuTauTauTree->Branch("muonPFIsoDR03SumChargedParticlePt", &_muonPFIsoDR03SumChargedParticlePt);
  _MuTauTauTree->Branch("muonPFIsoDR03SumNeutralHadronPt", &_muonPFIsoDR03SumNeutralHadronPt);
  _MuTauTauTree->Branch("muonPFIsoDR03SumPhotonHadronPt", &_muonPFIsoDR03SumPhotonHadronPt);
  _MuTauTauTree->Branch("muonPFIsoDR03SumPUPt", &_muonPFIsoDR03SumPUPt);
  _MuTauTauTree->Branch("muonPFIsoDR04SumChargedHadronPt", &_muonPFIsoDR04SumChargedHadronPt);
  _MuTauTauTree->Branch("muonPFIsoDR04SumChargedParticlePt", &_muonPFIsoDR04SumChargedParticlePt);
  _MuTauTauTree->Branch("muonPFIsoDR04SumNeutralHadronPt", &_muonPFIsoDR04SumNeutralHadronPt);
  _MuTauTauTree->Branch("muonPFIsoDR04SumPhotonHadronPt", &_muonPFIsoDR04SumPhotonHadronPt);
  _MuTauTauTree->Branch("muonPFIsoDR04SumPUPt", &_muonPFIsoDR04SumPUPt);
  _MuTauTauTree->Branch("muonCaloComp", &_muonCaloComp);
  _MuTauTauTree->Branch("muonIsGlobalMuon", &_muonIsGlobalMuon);
  _MuTauTauTree->Branch("muonisTrackerMuon", &_muonIsTrackerMuon);
  _MuTauTauTree->Branch("muonIsGlobalPromptTight", &_muonIsGlobalPromptTight);
  _MuTauTauTree->Branch("muonIsTMLastStationLoose", &_muonIsTMLastStationLoose);
  _MuTauTauTree->Branch("muonIsTMLastStationTight", &_muonIsTMLastStationTight);
  _MuTauTauTree->Branch("muonIsTM2DCompatibilityLoose", &_muonIsTM2DCompatibilityLoose);
  _MuTauTauTree->Branch("muonIsTM2DCompatibilityTight", &_muonIsTM2DCompatibilityTight);
  _MuTauTauTree->Branch("muonPionVeto", &_muonPionVeto);
  _MuTauTauTree->Branch("muonMEtMt", &_muonMEtMt);

  //mu-LeadTau Info
  _MuTauTauTree->Branch("muonLeadTauCosDPhi", &_muonLeadTauCosDPhi);
  _MuTauTauTree->Branch("muonLeadTauDelatR", &_muonLeadTauDelatR);
  _MuTauTauTree->Branch("muonLeadTauMass", &_muonLeadTauMass);
  _MuTauTauTree->Branch("muonLeadTauMetMass", &_muonLeadTauMetMass);
  _MuTauTauTree->Branch("muonLeadTauCollMass", &_muonLeadTauCollMass);
  _MuTauTauTree->Branch("muonLeadTauPZeta", &_muonLeadTauPZeta);
  _MuTauTauTree->Branch("muonLeadTauPZetaVis", &_muonLeadTauPZetaVis);
  
  //mu-SubLeadTau Info
  _MuTauTauTree->Branch("muonSubLeadTauCosDPhi", &_muonSubLeadTauCosDPhi);
  _MuTauTauTree->Branch("muonSubLeadTauDelatR", &_muonSubLeadTauDelatR);
  _MuTauTauTree->Branch("muonSubLeadTauMass", &_muonSubLeadTauMass);
  _MuTauTauTree->Branch("muonSubLeadTauMetMass", &_muonSubLeadTauMetMass);
  _MuTauTauTree->Branch("muonSubLeadTauCollMass", &_muonSubLeadTauCollMass);
  _MuTauTauTree->Branch("muonSubLeadTauPZeta", &_muonSubLeadTauPZeta);
  _MuTauTauTree->Branch("muonSubLeadTauPZetaVis", &_muonSubLeadTauPZetaVis);
  
  _MuTauTauTree->Branch("muonDiHadCandCosDPhi", &_muonDiHadCandCosDPhi);
  _MuTauTauTree->Branch("muonDiHadCandMass", &_muonDiHadCandMass);
  
  _MuTauTauTree->Branch("muonMetCosDPhi", &_muonMetCosDPhi);
  _MuTauTauTree->Branch("diHadTauCandMetCosDPhi", &_diHadTauCandMetCosDPhi);
}

void MuTauTauAnalysis::initializeVectors(){
  //Lead Tau Info
  _leadTauMatched = NULL;
  _leadTauMotherId = NULL;
  _leadTauGenPt = NULL;
  _leadTauGenE = NULL;
  _leadTauGenEta = NULL;
  _leadTauGenPhi = NULL;
  
  _leadTauPt = NULL;
  _leadTauEnergy = NULL;
  _leadTauEta = NULL;
  _leadTauPhi = NULL;
  _leadTauCharge = NULL;
  _leadTauLeadChargedCandPt = NULL;
  _leadTauLeadChargedCandCharge = NULL;
  //_leadTauLeadChargedCandRecHitsSize = NULL;
  _leadTauLeadChargedCandDxyVtx = NULL;
  _leadTauLeadChargedCandDxyBS = NULL;
  _leadTauLeadChargedCandDxyError = NULL;
  _leadTauLeadChargedCandDzVtx = NULL;
  _leadTauLeadChargedCandDzBS = NULL;
  _leadTauLeadChargedCandDzError = NULL;
  _leadTauLeadChargedCandVx = NULL;
  _leadTauLeadChargedCandVy = NULL;
  _leadTauLeadChargedCandVz = NULL;
  _leadTauDeltaZVtx = NULL;
  _leadTauNProngs = NULL;
  _leadTauEmFraction = NULL;
  _leadTauHcalTotOverPLead = NULL;
  _leadTauHcalMaxOverPLead = NULL;
  _leadTauHcal3x3OverPLead = NULL;
  _leadTauElectronPreId = NULL;
  _leadTauModifiedEOverP = NULL;
  _leadTauBremsRecoveryEOverPLead = NULL;
  _leadTauLTSignedIp = NULL;
  _leadTauTrkIsoSumPt = NULL;
  _leadTauECALIsoSumEt = NULL;
  _leadTauIdByDecayModeFinding = NULL;
  _leadTauIdByVLooseIsolation = NULL;
  _leadTauIdByLooseIsolation = NULL;
  _leadTauIdByMediumIsolation = NULL;
  _leadTauIdByTightIsolation = NULL;
  _leadTauIdByVLooseCombinedIsolationDBSumPtCorr = NULL;
  _leadTauIdByLooseCombinedIsolationDBSumPtCorr = NULL;
  _leadTauIdByMediumCombinedIsolationDBSumPtCorr = NULL;
  _leadTauIdByTightCombinedIsolationDBSumPtCorr = NULL;
  _leadTauIdByLooseElectronRejection = NULL;
  _leadTauIdByMediumElectronRejection = NULL;
  _leadTauIdByTightElectronRejection = NULL;
  _leadTauIdByLooseMuonRejection = NULL;
  _leadTauIdByTightMuonRejection = NULL;
  _leadTauMEtMt = NULL;
 
  //Sub Lead Tau Info
  _subLeadTauMatched = NULL;
  _subLeadTauMotherId = NULL;
  _subLeadTauGenPt = NULL;
  _subLeadTauGenE = NULL;
  _subLeadTauGenEta = NULL;
  _subLeadTauGenPhi = NULL;
  
  _subLeadTauPt = NULL;
  _subLeadTauEnergy = NULL;
  _subLeadTauEta = NULL;
  _subLeadTauPhi = NULL;
  _subLeadTauCharge = NULL;
  _subLeadTauLeadChargedCandPt = NULL;
  _subLeadTauLeadChargedCandCharge = NULL;
  //_subLeadTauLeadChargedCandRecHitsSize = NULL;
  _subLeadTauLeadChargedCandDxyVtx = NULL;
  _subLeadTauLeadChargedCandDxyBS = NULL;
  _subLeadTauLeadChargedCandDxyError = NULL;
  _subLeadTauLeadChargedCandDzVtx = NULL;
  _subLeadTauLeadChargedCandDzBS = NULL;
  _subLeadTauLeadChargedCandDzError = NULL;
  _subLeadTauLeadChargedCandVx = NULL;
  _subLeadTauLeadChargedCandVy = NULL;
  _subLeadTauLeadChargedCandVz = NULL;
  _subLeadTauNProngs = NULL;
  _subLeadTauDeltaZVtx = NULL;
  _subLeadTauEmFraction = NULL;
  _subLeadTauHcalTotOverPLead = NULL;
  _subLeadTauHcalMaxOverPLead = NULL;
  _subLeadTauHcal3x3OverPLead = NULL;
  _subLeadTauElectronPreId = NULL;
  _subLeadTauModifiedEOverP = NULL;
  _subLeadTauBremsRecoveryEOverPLead = NULL;
  _subLeadTauLTSignedIp = NULL;
  _subLeadTauTrkIsoSumPt = NULL;
  _subLeadTauECALIsoSumEt = NULL;
  _subLeadTauIdByDecayModeFinding = NULL;
  _subLeadTauIdByVLooseIsolation = NULL;
  _subLeadTauIdByLooseIsolation = NULL;
  _subLeadTauIdByMediumIsolation = NULL;
  _subLeadTauIdByTightIsolation = NULL;
  _subLeadTauIdByVLooseCombinedIsolationDBSumPtCorr = NULL;
  _subLeadTauIdByLooseCombinedIsolationDBSumPtCorr = NULL;
  _subLeadTauIdByMediumCombinedIsolationDBSumPtCorr = NULL;
  _subLeadTauIdByTightCombinedIsolationDBSumPtCorr = NULL;
  _subLeadTauIdByLooseElectronRejection = NULL;
  _subLeadTauIdByMediumElectronRejection = NULL;
  _subLeadTauIdByTightElectronRejection = NULL;
  _subLeadTauIdByLooseMuonRejection = NULL;
  _subLeadTauIdByTightMuonRejection = NULL;
  _subLeadTauMEtMt = NULL;
  
  //diTau info
  _diHadTauCosDPhi = NULL;
  _diHadTauDelatR = NULL;
  _diHadTauPZeta = NULL;
  _diHadTauPZetaVis = NULL;
  _diHadTauMass = NULL;
  _diHadTauMetMass = NULL;
  _diHadTauCollMass = NULL;
  
  _diHadCandPt = NULL;
  _diHadCandEt = NULL;
  _diHadCandEta = NULL;
  _diHadCandPhi = NULL;
  _diHadCandMass = NULL;  
  _diHadCandCharge = NULL;  
  
  //muon Info
  _muonMatched = NULL;
  _muonMotherId = NULL;
  _muonGenPt = NULL;
  _muonGenE = NULL;
  _muonGenEta = NULL;
  _muonGenPhi = NULL;
  
  _muonPt = NULL;
  _muonE = NULL;
  _muonEta = NULL;
  _muonPhi = NULL;
  _muonCharge = NULL;
  _muonDxyVtx = NULL;
  _muonDxyBS = NULL;
  _muonDxyError = NULL;
  _muonDzVtx = NULL;
  _muonDzBS = NULL;
  _muonDzError = NULL;
  _muonVx = NULL;
  _muonVy = NULL;
  _muonVz = NULL;
  _muonValidHits = NULL;
  _muonNormChiSqrd = NULL;
  _muonChambersValidHits = NULL;
  _muonMatchedStations = NULL;
  _muonPixelHits = NULL;
  _muonTrkLayersWithHits = NULL;
  _muonEcalIsoDr03 = NULL;
  _muonTrkIsoDr03 = NULL;
  _muonCaloIsoDr03 = NULL;
  _muonEcalIsoDr04 = NULL;
  _muonTrkIsoDr04 = NULL;
  _muonEcalIsoDr05 = NULL;
  _muonHadIsoDr05 = NULL;
  _muonTrkIsoDr05 = NULL;
  _muonCaloIsoDr05 = NULL;
  _muonPFIsoDR03SumChargedHadronPt = NULL;
  _muonPFIsoDR03SumChargedParticlePt = NULL;
  _muonPFIsoDR03SumNeutralHadronPt = NULL;
  _muonPFIsoDR03SumPhotonHadronPt = NULL;
  _muonPFIsoDR03SumPUPt = NULL;
  _muonPFIsoDR04SumChargedHadronPt = NULL;
  _muonPFIsoDR04SumChargedParticlePt = NULL;
  _muonPFIsoDR04SumNeutralHadronPt = NULL;
  _muonPFIsoDR04SumPhotonHadronPt = NULL;
  _muonPFIsoDR04SumPUPt = NULL;
  _muonCaloComp = NULL;
  _muonIsGlobalMuon = NULL;
  _muonIsTrackerMuon = NULL;
  _muonIsGlobalPromptTight = NULL;
  _muonIsTMLastStationLoose = NULL;
  _muonIsTMLastStationTight = NULL;
  _muonIsTM2DCompatibilityLoose = NULL;
  _muonIsTM2DCompatibilityTight = NULL;
  _muonPionVeto = NULL;
  _muonMEtMt = NULL;
      
  //mu-LeadTau Info
  _muonLeadTauCosDPhi = NULL;
  _muonLeadTauDelatR = NULL;
  _muonLeadTauPZeta = NULL;
  _muonLeadTauPZetaVis = NULL;
  _muonLeadTauMass = NULL;
  _muonLeadTauMetMass = NULL;    
  _muonLeadTauCollMass = NULL;
  
  //mu-SubLeadTau Info
  _muonSubLeadTauCosDPhi = NULL;
  _muonSubLeadTauDelatR = NULL;
  _muonSubLeadTauPZeta = NULL;
  _muonSubLeadTauPZetaVis = NULL;
  _muonSubLeadTauMass = NULL;
  _muonSubLeadTauMetMass = NULL;	
  _muonSubLeadTauCollMass = NULL;
  
  _muonDiHadCandCosDPhi = NULL;
  _muonDiHadCandMass = NULL;
  
  _muonMetCosDPhi = NULL;
  _diHadTauCandMetCosDPhi = NULL;

}

void MuTauTauAnalysis::clearVectors(){
  //Lead Tau Info
  _leadTauMatched->clear();
  _leadTauMotherId->clear();
  _leadTauGenPt->clear();
  _leadTauGenE->clear();
  _leadTauGenEta->clear();
  _leadTauGenPhi->clear();
  
  _leadTauPt->clear();
  _leadTauEnergy->clear();
  _leadTauEta->clear();
  _leadTauPhi->clear();
  _leadTauCharge->clear();
  _leadTauLeadChargedCandPt->clear();
  _leadTauLeadChargedCandCharge->clear();
  //_leadTauLeadChargedCandRecHitsSize->clear();
  _leadTauLeadChargedCandDxyVtx->clear();
  _leadTauLeadChargedCandDxyBS->clear();
  _leadTauLeadChargedCandDxyError->clear();
  _leadTauLeadChargedCandDzVtx->clear();
  _leadTauLeadChargedCandDzBS->clear();
  _leadTauLeadChargedCandDzError->clear();
  _leadTauLeadChargedCandVx->clear();
  _leadTauLeadChargedCandVy->clear();
  _leadTauLeadChargedCandVz->clear();
  _leadTauDeltaZVtx->clear();
  _leadTauNProngs->clear();
  _leadTauEmFraction->clear();
  _leadTauHcalTotOverPLead->clear();
  _leadTauHcalMaxOverPLead->clear();
  _leadTauHcal3x3OverPLead->clear();
  _leadTauElectronPreId->clear();
  _leadTauModifiedEOverP->clear();
  _leadTauBremsRecoveryEOverPLead->clear();
  _leadTauLTSignedIp->clear();
  _leadTauTrkIsoSumPt->clear();
  _leadTauECALIsoSumEt->clear();
  _leadTauIdByDecayModeFinding->clear();
  _leadTauIdByVLooseIsolation->clear();
  _leadTauIdByLooseIsolation->clear();
  _leadTauIdByMediumIsolation->clear();
  _leadTauIdByTightIsolation->clear();
  _leadTauIdByVLooseCombinedIsolationDBSumPtCorr->clear();
  _leadTauIdByLooseCombinedIsolationDBSumPtCorr->clear();
  _leadTauIdByMediumCombinedIsolationDBSumPtCorr->clear();
  _leadTauIdByTightCombinedIsolationDBSumPtCorr->clear();
  _leadTauIdByLooseElectronRejection->clear();
  _leadTauIdByMediumElectronRejection->clear();
  _leadTauIdByTightElectronRejection->clear();
  _leadTauIdByLooseMuonRejection->clear();
  _leadTauIdByTightMuonRejection->clear();
  _leadTauMEtMt->clear();
  
  //SubLead Tau Info
  _subLeadTauMatched->clear();
  _subLeadTauMotherId->clear();
  _subLeadTauGenPt->clear();
  _subLeadTauGenE->clear();
  _subLeadTauGenEta->clear();
  _subLeadTauGenPhi->clear();
  
  _subLeadTauPt->clear();
  _subLeadTauEnergy->clear();
  _subLeadTauEta->clear();
  _subLeadTauPhi->clear();
  _subLeadTauCharge->clear();
  _subLeadTauLeadChargedCandPt->clear();
  _subLeadTauLeadChargedCandCharge->clear();
  //_subLeadTauLeadChargedCandRecHitsSize->clear();
  _subLeadTauLeadChargedCandDxyVtx->clear();
  _subLeadTauLeadChargedCandDxyBS->clear();
  _subLeadTauLeadChargedCandDxyError->clear();
  _subLeadTauLeadChargedCandDzVtx->clear();
  _subLeadTauLeadChargedCandDzBS->clear();
  _subLeadTauLeadChargedCandDzError->clear();
  _subLeadTauLeadChargedCandVx->clear();
  _subLeadTauLeadChargedCandVy->clear();
  _subLeadTauLeadChargedCandVz->clear();
  _subLeadTauDeltaZVtx->clear();
  _subLeadTauNProngs->clear();
  _subLeadTauEmFraction->clear();
  _subLeadTauHcalTotOverPLead->clear();
  _subLeadTauHcalMaxOverPLead->clear();
  _subLeadTauHcal3x3OverPLead->clear();
  _subLeadTauElectronPreId->clear();
  _subLeadTauModifiedEOverP->clear();
  _subLeadTauBremsRecoveryEOverPLead->clear();
  _subLeadTauLTSignedIp->clear();
  _subLeadTauTrkIsoSumPt->clear();
  _subLeadTauECALIsoSumEt->clear();
  _subLeadTauIdByDecayModeFinding->clear();
  _subLeadTauIdByVLooseIsolation->clear();
  _subLeadTauIdByLooseIsolation->clear();
  _subLeadTauIdByMediumIsolation->clear();
  _subLeadTauIdByTightIsolation->clear();
  _subLeadTauIdByVLooseCombinedIsolationDBSumPtCorr->clear();
  _subLeadTauIdByLooseCombinedIsolationDBSumPtCorr->clear();
  _subLeadTauIdByMediumCombinedIsolationDBSumPtCorr->clear();
  _subLeadTauIdByTightCombinedIsolationDBSumPtCorr->clear();
  _subLeadTauIdByLooseElectronRejection->clear();
  _subLeadTauIdByMediumElectronRejection->clear();
  _subLeadTauIdByTightElectronRejection->clear();
  _subLeadTauIdByLooseMuonRejection->clear();
  _subLeadTauIdByTightMuonRejection->clear();
  _subLeadTauMEtMt->clear();
  
  //diTau info
  _diHadTauCosDPhi->clear();
  _diHadTauDelatR->clear();
  _diHadTauPZeta->clear();
  _diHadTauPZetaVis->clear();
  _diHadTauMass->clear();
  _diHadTauMetMass->clear();
  _diHadTauCollMass->clear();
  
  _diHadCandPt->clear();
  _diHadCandEt->clear();
  _diHadCandEta->clear();
  _diHadCandPhi->clear();
  _diHadCandMass->clear();
  _diHadCandCharge->clear();
  
  
  //muon Info
  _muonMatched->clear();
  _muonMotherId->clear();
  _muonGenPt->clear();
  _muonGenE->clear();
  _muonGenEta->clear();
  _muonGenPhi->clear();
  
  _muonPt->clear();
  _muonE->clear();
  _muonEta->clear();
  _muonPhi->clear();
  _muonCharge->clear();
  _muonDxyVtx->clear();
  _muonDxyBS->clear();
  _muonDxyError->clear();
  _muonDzVtx->clear();
  _muonDzBS->clear();
  _muonDzError->clear();
  _muonVx->clear();
  _muonVy->clear();
  _muonVz->clear();
  _muonValidHits->clear();
  _muonNormChiSqrd->clear();
  _muonChambersValidHits->clear();
  _muonMatchedStations->clear();
  _muonPixelHits->clear();
  _muonTrkLayersWithHits->clear();
  _muonEcalIsoDr03->clear();
  _muonTrkIsoDr03->clear();
  _muonCaloIsoDr03->clear();
  _muonEcalIsoDr04->clear();
  _muonTrkIsoDr04->clear();
  _muonEcalIsoDr05->clear();
  _muonHadIsoDr05->clear();
  _muonTrkIsoDr05->clear();
  _muonCaloIsoDr05->clear();
  _muonPFIsoDR03SumChargedHadronPt->clear();
  _muonPFIsoDR03SumChargedParticlePt->clear();
  _muonPFIsoDR03SumNeutralHadronPt->clear();
  _muonPFIsoDR03SumPhotonHadronPt->clear();
  _muonPFIsoDR03SumPUPt->clear();
  _muonPFIsoDR04SumChargedHadronPt->clear();
  _muonPFIsoDR04SumChargedParticlePt->clear();
  _muonPFIsoDR04SumNeutralHadronPt->clear();
  _muonPFIsoDR04SumPhotonHadronPt->clear();
  _muonPFIsoDR04SumPUPt->clear();
  _muonCaloComp->clear();
  _muonIsGlobalMuon->clear();
  _muonIsTrackerMuon->clear();
  _muonIsGlobalPromptTight->clear();
  _muonIsTMLastStationLoose->clear();
  _muonIsTMLastStationTight->clear();
  _muonIsTM2DCompatibilityLoose->clear();
  _muonIsTM2DCompatibilityTight->clear();
  _muonPionVeto->clear();
  _muonMEtMt->clear();

  //mu-LeadTau Info
  _muonLeadTauCosDPhi->clear();
  _muonLeadTauDelatR->clear();
  _muonLeadTauPZeta->clear();
  _muonLeadTauPZetaVis->clear();
  _muonLeadTauMass->clear();
  _muonLeadTauMetMass->clear();    
  _muonLeadTauCollMass->clear();
  
  //mu-SubLeadTau Info
  _muonSubLeadTauCosDPhi->clear();
  _muonSubLeadTauDelatR->clear();
  _muonSubLeadTauPZeta->clear();
  _muonSubLeadTauPZetaVis->clear();
  _muonSubLeadTauMass->clear();
  _muonSubLeadTauMetMass->clear();	
  _muonSubLeadTauCollMass->clear();
  
  _muonDiHadCandCosDPhi->clear();
  _muonDiHadCandMass->clear();
  
  _muonMetCosDPhi->clear();
  _diHadTauCandMetCosDPhi->clear();
  
}
  
//define this as a plug-in
DEFINE_FWK_MODULE(MuTauTauAnalysis);
