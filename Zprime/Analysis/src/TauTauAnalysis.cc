#include "HighMassAnalysis/Analysis/interface/TauTauAnalysis.h"
#include "SHarper/HEEPAnalyzer/interface/HEEPEle.h"
#include "SHarper/HEEPAnalyzer/interface/HEEPCutCodes.h"

using namespace edm;
using namespace reco;
using namespace std;

TauTauAnalysis::TauTauAnalysis(const edm::ParameterSet& iConfig): HiMassTauAnalysis(iConfig){
	cout << "Initializing" << endl;
} 

TauTauAnalysis::~TauTauAnalysis(){}
  
void TauTauAnalysis::fillNtuple(){
	EventsInPATuple->Fill(0);

	// Recalculate MET for cases when electron or taus are smeared (using EDAnalyzer method and configuration parameters to recalculate the MET)
	// Recalculating MET for cases when electron is smeared ----> set SmearTheTau to False, SmearTheElectron to True, and specify correct smearing factors
	// Recalculating MET for cases when tau is smeared ----> set SmearTheTau to True, SmearTheElectron to False, and specify correct smearing factors
	// Recalculating MET for cases when jets are smeared ----> set SmearTheTau to False, SmearTheElectron to False, SmearTheJet to True, and specify correct smearing factors
	// SMEARING FACTORS: smear ONLY electron pt, tau pt, and jet energy scale factor
	/*double met_e = TMath::Sqrt(
								((*(_patMETs->begin())).px() + deltaForMEx) * 
								((*(_patMETs->begin())).px() + deltaForMEx) + ((*(_patMETs->begin())).py() + deltaForMEy) *
								((*(_patMETs->begin())).py() + deltaForMEy)
							  );
	double met_px = ((*(_patMETs->begin())).px() + deltaForMEx);
	double met_py = ((*(_patMETs->begin())).py() + deltaForMEy);
	double met_pz = (*(_patMETs->begin())).pz();
	reco::Candidate::LorentzVector MET(met_px, met_py, met_pz, met_e);//*/
	
	// Get primary vertex
	const reco::Vertex& thePrimaryEventVertex = (*(_primaryEventVertexCollection)->begin());

	// Tau loops: Tau1 is always leads in pT
	int theNumberOfTaus1 = 0;
	int theNumberOfTaus2 = 0;

	//cout << "NEW EVENT" << endl;
	if(_AnalysisType.compare("coll") != 0){
		_NumberOfHadronicGenTaus = NumberOfHadronicGenTaus();
	}else{
		_NumberOfHadronicGenTaus = -1;
	}

	if(_NumberOfHadronicGenTaus == 2){
		Triplet genHadronicTaus = GetGenHadronicTaus();
		TLorentzVector genTau1_p4 = genHadronicTaus.tau1;
		TLorentzVector genTau2_p4 = genHadronicTaus.tau2;
		_GenTau1P					= genTau1_p4.P();
		_GenTau2P					= genTau2_p4.P();
		_GenTau1Pt					= genTau1_p4.Pt();
		_GenTau2Pt					= genTau2_p4.Pt();
		_GenTau1Phi					= genTau1_p4.Vect().Phi();
		_GenTau2Phi					= genTau2_p4.Vect().Phi();
		_GenTau1Theta				= genTau1_p4.Vect().Theta();
		_GenTau2Theta				= genTau2_p4.Vect().Theta();
		_GenTausOmega				= genTau1_p4.Vect().Angle(genTau2_p4.Vect());


		TVector3 genBoost = -(genTau1_p4 + genTau2_p4).BoostVector();
		TLorentzVector boosted_genTau1_p4 = BoostAndRotateToRestFrame(genTau1_p4, genBoost);
		TLorentzVector boosted_genTau2_p4 = BoostAndRotateToRestFrame(genTau2_p4, genBoost);
		_BoostedGenTau1P			= boosted_genTau1_p4.P();
		_BoostedGenTau2P			= boosted_genTau2_p4.P();
		_BoostedGenTau1Pt			= boosted_genTau1_p4.Pt();
		_BoostedGenTau2Pt			= boosted_genTau2_p4.Pt();
		_BoostedGenTau1Phi			= boosted_genTau1_p4.Vect().Phi();
		_BoostedGenTau2Phi			= boosted_genTau2_p4.Vect().Phi();
		_BoostedGenTau1Theta		= boosted_genTau1_p4.Vect().Theta();
		_BoostedGenTau2Theta		= boosted_genTau2_p4.Vect().Theta();
		_BoostedGenTausOmega		= boosted_genTau1_p4.Vect().Angle(boosted_genTau2_p4.Vect());
		
	}

	// Start loop over patTaus so select two (if applicable) that form a good (and heaviest) pair
	theNumberOfTaus1 = 0;
	for ( pat::TauCollection::const_iterator patTau1 = _patTaus->begin(); patTau1 != _patTaus->end(); ++patTau1 ) {
		theNumberOfTaus1++;
		if (patTau1->pt() < _RecoTauPtMinCut){ continue; }

		theNumberOfTaus2 = theNumberOfTaus1 + 1;
		for ( pat::TauCollection::const_iterator patTau2 = (patTau1 + 1); patTau2 != _patTaus->end(); ++patTau2 ) {
			theNumberOfTaus2++;
			if (patTau2->pt() < _RecoTau2PtMinCut){ continue; }

			if( theNumberOfTaus2 <= theNumberOfTaus1 ) continue; // Make sure we don't double-count: only compare pairs in which the tau2 iterator is larger than the tau 1 iterator, else skip combo

			// pat::TauCollection should be sorted by pT in descending order, but let's make sure, and if not, flip them so patTau1 has the largest pT 
			if (patTau1->pt() < patTau2->pt()){
				pat::TauCollection::const_iterator patTauTemp = patTau1;
				patTau1 = patTau2;
				patTau2 = patTauTemp;
			}	

			if(patTau1->pt() < _RecoTauPtMinCut){ continue; }
			if(patTau2->pt() < _RecoTauPtMinCut){ continue; }
			if(fabs(patTau1->eta()) > _RecoTauEtaCut ){ continue; }
			if(fabs(patTau2->eta()) > _RecoTauEtaCut ){ continue; }

			// =========   NO VECTOR FILLING BEFORE THIS POINT   ========= //
			
			// Run and event info
			_runNumber		=	runNum;
			_eventNumber	=	eventNum;	
			_lumiBlock		=	lumiBlock;

			// Pileup info
			_numInteractionsBXm1	= -1;
			_numInteractionsBX0		= -1;
			_numInteractionsBXp1	= -1;

			_comboNumber->push_back(_comboNumber->size());





			if(_AnalysisType.compare("coll") != 0){
				std::vector<PileupSummaryInfo>::const_iterator PVI;
				for(PVI = PupInfo->begin(); PVI != PupInfo->end(); ++PVI) {
					int bunchCrossing = PVI->getBunchCrossing();
					int numInteractions = PVI->getPU_NumInteractions();

						 if( bunchCrossing == -1 ){ _numInteractionsBXm1    = numInteractions; }
					else if( bunchCrossing ==  0 ){ _numInteractionsBX0     = numInteractions; }
					else if( bunchCrossing ==  1 ){ _numInteractionsBXp1    = numInteractions; }
				}
				
				_Tau1_MatchesGenHadronic-> push_back(MatchesGenHadronicTau((*patTau1), 0.25).first);
				_Tau2_MatchesGenHadronic-> push_back(MatchesGenHadronicTau((*patTau2), 0.25).first);
				_Tau1_ZeMatched			-> push_back(matchToGen((*patTau1), 0.25, 11, 23, 0, false).first);
				_Tau1_ZtauMatched		-> push_back(matchToGen((*patTau1), 0.25, 15, 23, 0, true).first);
				_Tau2_ZeMatched			-> push_back(matchToGen((*patTau2), 0.25, 11, 23, 0, false).first);
				_Tau2_ZtauMatched		-> push_back(matchToGen((*patTau2), 0.25, 15, 23, 0, true).first);
				_Tau1_ParentTauMatched	-> push_back(matchToGen((*patTau1), 0.25, 15, _LeptonMotherId, 0, true).first);
				_Tau2_ParentTauMatched	-> push_back(matchToGen((*patTau2), 0.25, 15, _LeptonMotherId, 0, true).first);

			}else{
				_Tau1_MatchesGenHadronic-> push_back(false);
				_Tau2_MatchesGenHadronic-> push_back(false);
				_Tau1_ParentTauMatched	-> push_back(false);
				_Tau2_ParentTauMatched	-> push_back(false);
				_Tau1_ZtauMatched		-> push_back(false);
				_Tau2_ZtauMatched		-> push_back(false);
				_Tau1_ZeMatched			-> push_back(false);
				_Tau2_ZeMatched			-> push_back(false);
			}


			// Generator variables
			if(_MatchTauToGen){       
				_GenMET					->	push_back(GetGenME().pt());
				_GenMETphi				->	push_back(GetGenME().phi());
				_Tau1GenPt				->	push_back(MatchesGenHadronicTau(*patTau1, 0.25).second.pt()); 
				_Tau2GenPt				->	push_back(MatchesGenHadronicTau(*patTau2, 0.25).second.pt()); 
				_Tau1GenE				->	push_back(MatchesGenHadronicTau(*patTau1, 0.25).second.energy()); 
				_Tau2GenE				->	push_back(MatchesGenHadronicTau(*patTau2, 0.25).second.energy()); 
				_Tau1GenEta				->	push_back(MatchesGenHadronicTau(*patTau1, 0.25).second.eta()); 
				_Tau2GenEta				->	push_back(MatchesGenHadronicTau(*patTau2, 0.25).second.eta()); 
				_Tau1GenPhi				->	push_back(MatchesGenHadronicTau(*patTau1, 0.25).second.phi()); 
				_Tau2GenPhi				->	push_back(MatchesGenHadronicTau(*patTau2, 0.25).second.phi()); 
				_Tau1GenParentMass	 	->	push_back(matchToGenParentMass(*patTau1));
				_Tau2GenParentMass	 	->	push_back(matchToGenParentMass(*patTau2));
				_TauTauPlusMetGenMass	->	push_back(GetVisPlusMETMass( MatchesGenHadronicTau((*patTau1), 0.25).second, MatchesGenHadronicTau((*patTau2), 0.25).second, GetGenME()));
				_Tau1Matched			->	push_back(int(MatchesGenHadronicTau(*patTau1, 0.25).first));                             
				_Tau2Matched			->	push_back(int(MatchesGenHadronicTau(*patTau2, 0.25).first));                             

				const GenParticleRef theGenTau1Ref = patTau1->genParticleRef(); 
				if ( theGenTau1Ref.isNonnull() ){ _Tau1MotherId->push_back(abs(theGenTau1Ref->mother()->mother()->pdgId())); }               
				else{ _Tau1MotherId->push_back(0); }                                                 
				const GenParticleRef theGenTau2Ref = patTau2->genParticleRef(); 
				if ( theGenTau2Ref.isNonnull() ){ _Tau2MotherId->push_back(abs(theGenTau2Ref->mother()->mother()->pdgId())); }               
				else{ _Tau2MotherId->push_back(0); }                                                 

				_Tau1PdgId		->	push_back(getMatchedPdgId(*patTau1).second);
				_Tau2PdgId		->	push_back(getMatchedPdgId(*patTau2).second);
				_Tau1MotherPdgId	->	push_back(getMatchedPdgId(*patTau1).second);
				_Tau2MotherPdgId	->	push_back(getMatchedPdgId(*patTau2).second);

			}else{
				_GenMET			->	push_back(-1);
				_GenMETphi		->	push_back(-1);
				_Tau1GenPt		->	push_back(-1);
				_Tau2GenPt		->	push_back(-1);
				_Tau1GenE		->	push_back(-1);
				_Tau2GenE		->	push_back(-1);
				_Tau1GenEta		->	push_back(999);
				_Tau2GenEta		->	push_back(999);
				_Tau1GenPhi		->	push_back(-1);
				_Tau2GenPhi		->	push_back(-1);
				_Tau1GenParentMass->	push_back(-1);
				_Tau2GenParentMass->	push_back(-1);
				_TauTauPlusMetGenMass->	push_back(-1);
				_Tau1Matched	->	push_back(0);
				_Tau2Matched	->	push_back(0);
				_Tau1MotherId	->	push_back(0);                                                 
				_Tau2MotherId	->	push_back(0);                                                 
				_Tau1PdgId		->	push_back(0);
				_Tau2PdgId		->	push_back(0);
				_Tau1MotherPdgId	->	push_back(0);
				_Tau2MotherPdgId	->	push_back(0);
			}


			// Number of primary vertices
			_NumPV						-> push_back( (_primaryEventVertexCollection)->size() );

			// Reco Tau variables
			_DeltaR_recoTau1_genTau1->push_back(reco::deltaR(patTau1->eta(), patTau1->phi(), -log(tan(_GenTau1Theta/2.0)), _GenTau1Phi));
			_DeltaR_recoTau2_genTau1->push_back(reco::deltaR(patTau2->eta(), patTau2->phi(), -log(tan(_GenTau1Theta/2.0)), _GenTau1Phi));
			_DeltaR_recoTau1_genTau2->push_back(reco::deltaR(patTau1->eta(), patTau1->phi(), -log(tan(_GenTau2Theta/2.0)), _GenTau2Phi));
			_DeltaR_recoTau2_genTau2->push_back(reco::deltaR(patTau2->eta(), patTau2->phi(), -log(tan(_GenTau2Theta/2.0)), _GenTau2Phi));
			_Tau1E						->	push_back( patTau1->energy() );
			_Tau2E						->	push_back( patTau2->energy() );
			_Tau1Et						->	push_back( patTau1->et() );
			_Tau2Et						->	push_back( patTau2->et() );
			_Tau1Pt						->	push_back( patTau1->pt() );
			_Tau2Pt						->	push_back( patTau2->pt() );
			_Tau1Eta					->	push_back( patTau1->eta() );
			_Tau2Eta					->	push_back( patTau2->eta() );
			_Tau1Phi					->	push_back( patTau1->phi() );
			_Tau2Phi					->	push_back( patTau2->phi() );
			_Tau1NProngs				->	push_back( patTau1->signalPFChargedHadrCands().size() );
			_Tau2NProngs				->	push_back( patTau2->signalPFChargedHadrCands().size() );
			_Tau1NSignalGammas			->	push_back( patTau1->signalPFGammaCands().size());
			_Tau2NSignalGammas			->	push_back( patTau2->signalPFGammaCands().size());
			_Tau1NSignalNeutrals		->	push_back( patTau1->signalPFNeutrHadrCands().size());
			_Tau2NSignalNeutrals		->	push_back( patTau2->signalPFNeutrHadrCands().size());
			//_Tau1NSignalPiZeros			->	push_back( patTau1->signalPiZeroCandidates().size());
			//_Tau2NSignalPiZeros			->	push_back( patTau2->signalPiZeroCandidates().size());
			_Tau1NSignalPiZeros			->	push_back(-1);
			_Tau2NSignalPiZeros			->	push_back(-1);
			_Tau1DecayMode				->	push_back( patTau1->decayMode() );
			_Tau2DecayMode				->	push_back( patTau2->decayMode() );
			_Tau1EmFraction				->	push_back( patTau1->emFraction() );
			_Tau2EmFraction				->	push_back( patTau2->emFraction() );
			_Tau1IsInTheCracks			->	push_back( isInTheCracks(patTau1->eta()) );
			_Tau2IsInTheCracks			->	push_back( isInTheCracks(patTau2->eta()) );

			float minDeltaR = 999;
			reco::PFTau matchedPFTau;
			for ( reco::PFTauCollection::const_iterator hpsTau = _hpsTau->begin(); hpsTau != _hpsTau->end(); ++hpsTau ) {
				float thisDeltaR = reco::deltaR(hpsTau->p4(),patTau1->p4());
				if(thisDeltaR < minDeltaR){ minDeltaR = thisDeltaR; matchedPFTau = (*hpsTau); }
			}

			// HPS discriminants

			if ((_RecoTauSource.label()).find("HPS") < (_RecoTauSource.label()).length()){ 
				_Tau1hpsPFTauDiscriminationAgainstLooseElectron		->	push_back( (patTau1->tauID("againstLooseElectron") 	> 0.5) );
				_Tau1hpsPFTauDiscriminationAgainstLooseMuon			->	push_back( (patTau1->tauID("againstLooseMuon") 		> 0.5) );
				_Tau1hpsPFTauDiscriminationAgainstMediumElectron	->	push_back( (patTau1->tauID("againstMediumElectron")	> 0.5) );
				//_Tau1hpsPFTauDiscriminationAgainstMediumMuon		->	push_back( (patTau1->tauID("againstMediumMuon")	> 0.5) );
				_Tau1hpsPFTauDiscriminationAgainstMediumMuon		->	push_back( false );
				_Tau1hpsPFTauDiscriminationAgainstTightElectron		->	push_back( (patTau1->tauID("againstTightElectron")	> 0.5) );
				_Tau1hpsPFTauDiscriminationAgainstTightMuon			->	push_back( (patTau1->tauID("againstTightMuon")		> 0.5) );
				_Tau1hpsPFTauDiscriminationByDecayModeFinding		->	push_back( (patTau1->tauID("byDecayModeFinding")	> 0.5) );
				_Tau1hpsPFTauDiscriminationByLooseIsolation			->	push_back( (patTau1->tauID("byLooseIsolation")		> 0.5) );
				_Tau1hpsPFTauDiscriminationByMediumIsolation		->	push_back( (patTau1->tauID("byMediumIsolation")		> 0.5) );
				_Tau1hpsPFTauDiscriminationByTightIsolation			->	push_back( (patTau1->tauID("byTightIsolation")		> 0.5) );
				_Tau1hpsPFTauDiscriminationByVLooseIsolation		->	push_back( (patTau1->tauID("byVLooseIsolation")		> 0.5) );
				_Tau2hpsPFTauDiscriminationAgainstLooseElectron		->	push_back( (patTau2->tauID("againstLooseElectron") 	> 0.5) );
				_Tau2hpsPFTauDiscriminationAgainstLooseMuon			->	push_back( (patTau2->tauID("againstLooseMuon") 		> 0.5) );
				_Tau2hpsPFTauDiscriminationAgainstMediumElectron	->	push_back( (patTau2->tauID("againstMediumElectron")	> 0.5) );
				//_Tau2hpsPFTauDiscriminationAgainstMediumMuon		->	push_back( (patTau2->tauID("againstMediumMuon")	> 0.5) );
				_Tau2hpsPFTauDiscriminationAgainstMediumMuon		->	push_back( false );
				_Tau2hpsPFTauDiscriminationAgainstTightElectron		->	push_back( (patTau2->tauID("againstTightElectron")	> 0.5) );
				_Tau2hpsPFTauDiscriminationAgainstTightMuon			->	push_back( (patTau2->tauID("againstTightMuon")		> 0.5) );
				_Tau2hpsPFTauDiscriminationByDecayModeFinding		->	push_back( (patTau2->tauID("byDecayModeFinding")	> 0.5) );
				_Tau2hpsPFTauDiscriminationByLooseIsolation			->	push_back( (patTau2->tauID("byLooseIsolation")		> 0.5) );
				_Tau2hpsPFTauDiscriminationByMediumIsolation		->	push_back( (patTau2->tauID("byMediumIsolation")		> 0.5) );
				_Tau2hpsPFTauDiscriminationByTightIsolation			->	push_back( (patTau2->tauID("byTightIsolation")		> 0.5) );
				_Tau2hpsPFTauDiscriminationByVLooseIsolation		->	push_back( (patTau2->tauID("byVLooseIsolation")		> 0.5) );

				_Tau1hpsPFTauDiscriminationByVLooseIsolationDBSumPtCorr				->	push_back( (patTau1->tauID("byVLooseIsolationDeltaBetaCorr")			> 0.5) );
				_Tau1hpsPFTauDiscriminationByLooseIsolationDBSumPtCorr				->	push_back( (patTau1->tauID("byLooseIsolationDeltaBetaCorr")				> 0.5) );
				_Tau1hpsPFTauDiscriminationByMediumIsolationDBSumPtCorr				->	push_back( (patTau1->tauID("byMediumIsolationDeltaBetaCorr")			> 0.5) );
				_Tau1hpsPFTauDiscriminationByTightIsolationDBSumPtCorr				->	push_back( (patTau1->tauID("byTightIsolationDeltaBetaCorr")				> 0.5) );
				_Tau1hpsPFTauDiscriminationByVLooseCombinedIsolationDBSumPtCorr		->	push_back( (patTau1->tauID("byVLooseCombinedIsolationDeltaBetaCorr")	> 0.5) );
				_Tau1hpsPFTauDiscriminationByLooseCombinedIsolationDBSumPtCorr		->	push_back( (patTau1->tauID("byLooseCombinedIsolationDeltaBetaCorr")		> 0.5) );
				_Tau1hpsPFTauDiscriminationByMediumCombinedIsolationDBSumPtCorr		->	push_back( (patTau1->tauID("byMediumCombinedIsolationDeltaBetaCorr")	> 0.5) );
				_Tau1hpsPFTauDiscriminationByTightCombinedIsolationDBSumPtCorr		->	push_back( (patTau1->tauID("byTightCombinedIsolationDeltaBetaCorr")		> 0.5) );

				_Tau2hpsPFTauDiscriminationByVLooseIsolationDBSumPtCorr				->	push_back( (patTau2->tauID("byVLooseIsolationDeltaBetaCorr")			> 0.5) );
				_Tau2hpsPFTauDiscriminationByLooseIsolationDBSumPtCorr				->	push_back( (patTau2->tauID("byLooseIsolationDeltaBetaCorr")				> 0.5) );
				_Tau2hpsPFTauDiscriminationByMediumIsolationDBSumPtCorr				->	push_back( (patTau2->tauID("byMediumIsolationDeltaBetaCorr")			> 0.5) );
				_Tau2hpsPFTauDiscriminationByTightIsolationDBSumPtCorr				->	push_back( (patTau2->tauID("byTightIsolationDeltaBetaCorr")				> 0.5) );
				_Tau2hpsPFTauDiscriminationByVLooseCombinedIsolationDBSumPtCorr		->	push_back( (patTau2->tauID("byVLooseCombinedIsolationDeltaBetaCorr")	> 0.5) );
				_Tau2hpsPFTauDiscriminationByLooseCombinedIsolationDBSumPtCorr		->	push_back( (patTau2->tauID("byLooseCombinedIsolationDeltaBetaCorr")		> 0.5) );
				_Tau2hpsPFTauDiscriminationByMediumCombinedIsolationDBSumPtCorr		->	push_back( (patTau2->tauID("byMediumCombinedIsolationDeltaBetaCorr")	> 0.5) );
				_Tau2hpsPFTauDiscriminationByTightCombinedIsolationDBSumPtCorr		->	push_back( (patTau2->tauID("byTightCombinedIsolationDeltaBetaCorr")		> 0.5) );

				_Tau1hpsPFTauDiscriminationByRawCombinedIsolationDBSumPtCorr		->	push_back( patTau1->tauID("byRawCombinedIsolationDBSumPtCorr") );
				_Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorr			->	push_back( patTau1->tauID("byRawChargedIsolationDBSumPtCorr")  );
				_Tau1hpsPFTauDiscriminationByRawGammaIsolationDBSumPtCorr			->	push_back( patTau1->tauID("byRawGammaIsolationDBSumPtCorr")    );
				_Tau2hpsPFTauDiscriminationByRawCombinedIsolationDBSumPtCorr		->	push_back( patTau2->tauID("byRawCombinedIsolationDBSumPtCorr") );
				_Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorr			->	push_back( patTau2->tauID("byRawChargedIsolationDBSumPtCorr")  );
				_Tau2hpsPFTauDiscriminationByRawGammaIsolationDBSumPtCorr			->	push_back( patTau2->tauID("byRawGammaIsolationDBSumPtCorr")    ); 

/*				_Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p0	->	push_back( patTau1->tauID("byRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p0")  );
				_Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p1	->	push_back( patTau1->tauID("byRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p1")  );
				_Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p2	->	push_back( patTau1->tauID("byRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p2")  );
				_Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p3	->	push_back( patTau1->tauID("byRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p3")  );
				_Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p4	->	push_back( patTau1->tauID("byRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p4")  );
				_Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p0	->	push_back( patTau2->tauID("byRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p0")  );
				_Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p1	->	push_back( patTau2->tauID("byRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p1")  );
				_Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p2	->	push_back( patTau2->tauID("byRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p2")  );
				_Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p3	->	push_back( patTau2->tauID("byRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p3")  );
				_Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p4	->	push_back( patTau2->tauID("byRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p4")  );
				//*/

			}

			// Leading track variables
			if(patTau1->leadPFChargedHadrCand().isNonnull()){
				_Tau1LTPt		->	push_back(patTau1->leadPFChargedHadrCand()->pt());
				_Tau1Charge 	->	push_back(patTau1->leadPFChargedHadrCand()->charge());

				if(patTau1->leadPFChargedHadrCand()->trackRef().isNonnull()){
					_Tau1LTvalid		->	push_back(true);
					_Tau1LTIpVtdxy		->	push_back(patTau1->leadPFChargedHadrCand()->trackRef()->dxy(thePrimaryEventVertex.position()));
					_Tau1LTIpVtdz		->	push_back(patTau1->leadPFChargedHadrCand()->trackRef()->dz(thePrimaryEventVertex.position()));
					_Tau1LTIpVtdxyError	->	push_back(patTau1->leadPFChargedHadrCand()->trackRef()->dxyError());
					_Tau1LTIpVtdzError	->	push_back(patTau1->leadPFChargedHadrCand()->trackRef()->dzError());
					_Tau1LTvx			->	push_back(patTau1->leadPFChargedHadrCand()->trackRef()->vx());
					_Tau1LTvy			->	push_back(patTau1->leadPFChargedHadrCand()->trackRef()->vy());
					_Tau1LTvz			->	push_back(patTau1->leadPFChargedHadrCand()->trackRef()->vz());
					_Tau1LTValidHits	->	push_back(patTau1->leadPFChargedHadrCand()->trackRef()->numberOfValidHits());
					_Tau1LTNormChiSqrd	->	push_back(patTau1->leadPFChargedHadrCand()->trackRef()->normalizedChi2());
				}else{ 
					_Tau1LTvalid		->	push_back(false);
					_Tau1LTIpVtdxy		->	push_back(-1);
					_Tau1LTIpVtdz		->	push_back(-1);
					_Tau1LTIpVtdxyError	->	push_back(-1);
					_Tau1LTIpVtdzError	->	push_back(-1);
					_Tau1LTvx			->	push_back(-1);
					_Tau1LTvy			->	push_back(-1);
					_Tau1LTvz			->	push_back(-1);
					_Tau1LTValidHits	->	push_back(-1);
					_Tau1LTNormChiSqrd	->	push_back(-1);
				}

			}else{
				_Tau1LTvalid		->	push_back(false);
				_Tau1LTPt			->	push_back(-1);
				_Tau1Charge 		->	push_back(0);
				_Tau1LTIpVtdxy		->	push_back(-1);
				_Tau1LTIpVtdz		->	push_back(-1);
				_Tau1LTIpVtdxyError	->	push_back(-1);
				_Tau1LTIpVtdzError	->	push_back(-1);
				_Tau1LTvx			->	push_back(-1);
				_Tau1LTvy			->	push_back(-1);
				_Tau1LTvz			->	push_back(-1);
				_Tau1LTValidHits	->	push_back(-1);
				_Tau1LTNormChiSqrd	->	push_back(-1);
			}

			if(patTau2->leadPFChargedHadrCand().isNonnull()){

				_Tau2LTPt		->	push_back(patTau2->leadPFChargedHadrCand()->pt());
				_Tau2Charge 	->	push_back(patTau2->leadPFChargedHadrCand()->charge());

				if(patTau2->leadPFChargedHadrCand()->trackRef().isNonnull()){
					_Tau2LTvalid		->	push_back(true);
					_Tau2LTIpVtdxy		->	push_back(patTau2->leadPFChargedHadrCand()->trackRef()->dxy(thePrimaryEventVertex.position()));
					_Tau2LTIpVtdz		->	push_back(patTau2->leadPFChargedHadrCand()->trackRef()->dz(thePrimaryEventVertex.position()));
					_Tau2LTIpVtdxyError	->	push_back(patTau2->leadPFChargedHadrCand()->trackRef()->dxyError());
					_Tau2LTIpVtdzError	->	push_back(patTau2->leadPFChargedHadrCand()->trackRef()->dzError());
					_Tau2LTvx			->	push_back(patTau2->leadPFChargedHadrCand()->trackRef()->vx());
					_Tau2LTvy			->	push_back(patTau2->leadPFChargedHadrCand()->trackRef()->vy());
					_Tau2LTvz			->	push_back(patTau2->leadPFChargedHadrCand()->trackRef()->vz());
					_Tau2LTValidHits	->	push_back(patTau2->leadPFChargedHadrCand()->trackRef()->numberOfValidHits());
					_Tau2LTNormChiSqrd	->	push_back(patTau2->leadPFChargedHadrCand()->trackRef()->normalizedChi2());
				}else{ 
					_Tau2LTvalid		->	push_back(false);
					_Tau2LTIpVtdxy		->	push_back(-1);
					_Tau2LTIpVtdz		->	push_back(-1);
					_Tau2LTIpVtdxyError	->	push_back(-1);
					_Tau2LTIpVtdzError	->	push_back(-1);
					_Tau2LTvx			->	push_back(-1);
					_Tau2LTvy			->	push_back(-1);
					_Tau2LTvz			->	push_back(-1);
					_Tau2LTValidHits	->	push_back(-1);
					_Tau2LTNormChiSqrd	->	push_back(-1);
				}

			}else{
				_Tau2LTvalid		->	push_back(false);
				_Tau2LTPt			->	push_back(-1);
				_Tau2Charge 		->	push_back(0);
				_Tau2LTIpVtdxy		->	push_back(-1);
				_Tau2LTIpVtdz		->	push_back(-1);
				_Tau2LTIpVtdxyError	->	push_back(-1);
				_Tau2LTIpVtdzError	->	push_back(-1);
				_Tau2LTvx			->	push_back(-1);
				_Tau2LTvy			->	push_back(-1);
				_Tau2LTvz			->	push_back(-1);
				_Tau2LTValidHits	->	push_back(-1);
				_Tau2LTNormChiSqrd	->	push_back(-1);
			}


			// MET variables
			_MET				-> push_back((_pfMEThandle->front()).pt());		
			_METphi				-> push_back((_pfMEThandle->front()).phi());		
			_METl1l2l3corr		-> push_back(theMETVector.pt());
			_METl1l2l3corrPhi	-> push_back(theMETVector.phi());
			_METsignificance	-> push_back((_pfMEThandle->front()).significance());		


			_TauTauVisibleMass			->	push_back(GetVisMass(*patTau1, *patTau2));
			_TauTauVisPlusMetMass		->	push_back(GetVisPlusMETMass(*patTau1, *patTau2));
			_TauTauCosDPhi				->	push_back(cos(TMath::Abs(normalizedPhi(patTau1->phi() - patTau2->phi()))));
			_TauTauDeltaR				->	push_back(reco::deltaR(patTau1->p4(), patTau2->p4()));
			_TauTauPZeta				->	push_back(CalculatePZeta(*patTau1, *patTau2));
			_TauTauPZetaVis				->	push_back(CalculatePZetaVis(*patTau1, *patTau2));
			_Tau1MetCosDphi				->	push_back(cos(TMath::Abs(normalizedPhi(patTau1->phi() - theMETVector.phi()))));
			_Tau2MetCosDphi				->	push_back(cos(TMath::Abs(normalizedPhi(patTau2->phi() - theMETVector.phi()))));
			_Tau1MetMt					->	push_back(CalculateLeptonMetMt(*patTau1));
			_Tau2MetMt					->	push_back(CalculateLeptonMetMt(*patTau2));


			// Btagging variables
			unsigned int nBtagsHiEffTrkCnt			= 0;
			unsigned int nBTagsHiEffSimpleSecVtx	= 0;
			unsigned int nBtagsHiPurityTrkCnt		= 0;
			unsigned int nBTagsHiPuritySimpleSecVtx	= 0;
			unsigned int nBTagsCombSecVtx			= 0;
			float 		 jetSumEt					= 0;
			unsigned int nJets						= 0;

			pat::JetCollection::const_iterator leadingJet;
			float leadingJetPt = 0;

			for(pat::JetCollection::const_iterator patJet = _patJets->begin(); patJet != _patJets->end(); ++patJet){
				if(deltaR(patJet->eta(), patJet->phi(), patTau1->eta(), patTau1->phi()) < _JetTauMatchingDeltaR) continue;
				if(deltaR(patJet->eta(), patJet->phi(), patTau2->eta(), patTau2->phi()) < _JetTauMatchingDeltaR) continue;
				if(patJet->et() < _RecoJetPtCut) continue;
				if(fabs(patJet->eta()) >_RecoJetEtaMaxCut) continue;
				nJets++;
				if(patJet->pt()>leadingJetPt){
					leadingJet = patJet;
					leadingJetPt = patJet->pt();
				}


				float trkCntHighEffBtag				=	patJet->bDiscriminator("trackCountingHighEffBJetTags");
				float trkCntHighPurityBtag			=	patJet->bDiscriminator("trackCountingHighPurBJetTags");
				float simpleSecVtxHighEffBTag		=	patJet->bDiscriminator("simpleSecondaryVertexHighEffBJetTags");
				float simpleSecVtxHighPurityBTag	=	patJet->bDiscriminator("simpleSecondaryVertexHighPurBJetTags");
				float combSecVtxBTag				=	patJet->bDiscriminator("combinedSecondaryVertexBJetTags");
				//use medium wp  
				if(trkCntHighEffBtag > 3.3) nBtagsHiEffTrkCnt++;    
				//if(trkCntHighEffBtag > -90 && (trkCntHighEffBtag) > 3.3) nBtagsHiEffTrkCnt++;    
				if(trkCntHighPurityBtag > -90 && (trkCntHighPurityBtag) > 1.93) nBtagsHiPurityTrkCnt++;                    
				if(simpleSecVtxHighEffBTag > 3.05) nBTagsHiEffSimpleSecVtx++;                  
				if(simpleSecVtxHighPurityBTag > 2.0) nBTagsHiPuritySimpleSecVtx++;                 
				if(combSecVtxBTag > 0.5) nBTagsCombSecVtx++; 
				jetSumEt += patJet->et();          
			}   

			if(leadingJetPt != 0){
				_Tau1Tau2LeadingJetPtSum->push_back((patTau1->p4()+patTau2->p4()+leadingJet->p4()).pt());

				float deltaPhiTau1LeadJet = fabs(patTau1->phi()-leadingJet->phi());
				float deltaPhiTau2LeadJet = fabs(patTau2->phi()-leadingJet->phi());
				while(deltaPhiTau1LeadJet > TMath::Pi()/2.0){ deltaPhiTau1LeadJet -= TMath::Pi()/2.0; } 
				while(deltaPhiTau2LeadJet > TMath::Pi()/2.0){ deltaPhiTau2LeadJet -= TMath::Pi()/2.0; } 
				_Tau1LeadingJetDeltaPhi->push_back(deltaPhiTau1LeadJet);
				_Tau2LeadingJetDeltaPhi->push_back(deltaPhiTau2LeadJet);
			}else{
				_Tau1Tau2LeadingJetPtSum->push_back(0);
				_Tau1LeadingJetDeltaPhi->push_back(0);
				_Tau2LeadingJetDeltaPhi->push_back(0);
			}


			_nBtagsHiEffTrkCnt->push_back(nBtagsHiEffTrkCnt);
			_nBtagsHiPurityTrkCnt->push_back(nBtagsHiPurityTrkCnt);
			_nBTagsHiEffSimpleSecVtx->push_back(nBTagsHiEffSimpleSecVtx);
			_nBTagsHiPuritySimpleSecVtx->push_back(nBTagsHiPuritySimpleSecVtx);
			_nBTagsCombSecVtx->push_back(nBTagsCombSecVtx);
			_jetSumEt->push_back(jetSumEt);
			_jetMETSumEt->push_back(jetSumEt + _patMETs->begin()->et());
			_nJets->push_back(nJets);
			//_nExtraJets					->	push_back();





			// SVFit
			std::vector<NSVfitStandalone::MeasuredTauLepton> measuredTauLeptons;
			measuredTauLeptons.push_back(NSVfitStandalone::MeasuredTauLepton(NSVfitStandalone::kHadDecay,patTau1->p4()));
			measuredTauLeptons.push_back(NSVfitStandalone::MeasuredTauLepton(NSVfitStandalone::kHadDecay,patTau2->p4()));

			NSVfitStandalone::Vector measuredMET(	(_pfMEThandle->front()).pt() * TMath::Sin((_pfMEThandle->front()).phi()),
													(_pfMEThandle->front()).pt() * TMath::Cos((_pfMEThandle->front()).phi()),
													0
												);

			TMatrixD covMET = (_pfMEThandle->front()).getSignificanceMatrix();
			unsigned int verbosity = 0;
			NSVfitStandaloneAlgorithm::NSVfitStandaloneAlgorithm algo =  NSVfitStandaloneAlgorithm::NSVfitStandaloneAlgorithm(measuredTauLeptons, measuredMET, covMET, verbosity);

			algo.maxObjFunctionCalls(50000);
			algo.fit();

			_NSVFitStatus->push_back(algo.fitStatus());
			_NSVFitMass->push_back(algo.mass());
			_NSVFitMassUncert->push_back(algo.massUncert());
			_NSVFitMET->push_back(sqrt(pow(algo.fittedMET().X(),2) + pow(algo.fittedMET().Y(),2)));
			_NSVFitMETphi->push_back(algo.fittedMET().phi());
			_NSVFitSystemPt->push_back(algo.fittedDiTauSystem().pt());
			_NSVFitSystemMag->push_back(sqrt(algo.fittedDiTauSystem().Vect().Mag2()));
			_NSVFitSystemPhi->push_back(algo.fittedDiTauSystem().phi());
			_NSVFitSystemEta->push_back(algo.fittedDiTauSystem().eta());

			TLorentzVector tau1_p4(
									algo.fittedTauLeptons().at(0).px(),
									algo.fittedTauLeptons().at(0).py(),
									algo.fittedTauLeptons().at(0).pz(),
									algo.fittedTauLeptons().at(0).E()
								);

			TLorentzVector tau2_p4(
									algo.fittedTauLeptons().at(1).px(),
									algo.fittedTauLeptons().at(1).py(),
									algo.fittedTauLeptons().at(1).pz(),
									algo.fittedTauLeptons().at(1).E()
								);

			TLorentzVector system_p4(algo.fittedDiTauSystem().px(), algo.fittedDiTauSystem().py(), algo.fittedDiTauSystem().pz(), algo.fittedDiTauSystem().E());
			TVector3 boost = -system_p4.BoostVector();
			TLorentzVector boosted_tau1_p4 = BoostAndRotateToRestFrame(tau1_p4, boost);
			TLorentzVector boosted_tau2_p4 = BoostAndRotateToRestFrame(tau2_p4, boost);

			_SVFitTau1P->push_back(tau1_p4.P());
			_SVFitTau2P->push_back(tau2_p4.P());
			_SVFitTau1Pt->push_back(tau1_p4.Pt());
			_SVFitTau2Pt->push_back(tau2_p4.Pt());
			_SVFitTau1Phi->push_back(tau1_p4.Vect().Phi());
			_SVFitTau2Phi->push_back(tau2_p4.Vect().Phi());
			_SVFitTau1Theta->push_back(tau1_p4.Vect().Theta());
			_SVFitTau2Theta->push_back(tau2_p4.Vect().Theta());
			_SVFitTausOmega->push_back(tau1_p4.Vect().Angle(tau2_p4.Vect()));

			_DeltaR_recoTau1_SVFitTau1->push_back(reco::deltaR(patTau1->eta(), patTau1->phi(), tau1_p4.Vect().Eta(), tau1_p4.Vect().Phi()));
			_DeltaR_recoTau2_SVFitTau1->push_back(reco::deltaR(patTau2->eta(), patTau2->phi(), tau1_p4.Vect().Eta(), tau1_p4.Vect().Phi()));
			_DeltaR_recoTau1_SVFitTau2->push_back(reco::deltaR(patTau1->eta(), patTau1->phi(), tau2_p4.Vect().Eta(), tau2_p4.Vect().Phi()));
			_DeltaR_recoTau2_SVFitTau2->push_back(reco::deltaR(patTau2->eta(), patTau2->phi(), tau2_p4.Vect().Eta(), tau2_p4.Vect().Phi()));

			_BoostedSVFitTau1P->push_back(boosted_tau1_p4.P());
			_BoostedSVFitTau2P->push_back(boosted_tau2_p4.P());
			_BoostedSVFitTau1Pt->push_back(boosted_tau1_p4.Pt());
			_BoostedSVFitTau2Pt->push_back(boosted_tau2_p4.Pt());
			_BoostedSVFitTau1Phi->push_back(boosted_tau1_p4.Vect().Phi());
			_BoostedSVFitTau2Phi->push_back(boosted_tau2_p4.Vect().Phi());
			_BoostedSVFitTau1Theta->push_back(boosted_tau1_p4.Vect().Theta());
			_BoostedSVFitTau2Theta->push_back(boosted_tau2_p4.Vect().Theta());
			_BoostedSVFitTausOmega->push_back(boosted_tau1_p4.Vect().Angle(boosted_tau2_p4.Vect()));
			//*/


			
		} // end of tau2 loop
	} // end of tau1 loop    

	// Fill the ntuple
	if(_Tau1Pt->size()>0){ _TauTauTree->Fill(); }

}

unsigned int TauTauAnalysis::NumberOfHadronicGenTaus(){
	unsigned int count = 0;
	for(GenParticleCollection::const_iterator genParticle = _genParticles->begin(); genParticle != _genParticles->end(); ++genParticle){

		// Match id of this gen particle with wanted id
		if((abs(genParticle->pdgId()) == 15) && (genParticle->status() != 3)) {
			// Examine number of neutrinos
			bool foundElectronOrMuon = false;
			for( unsigned int ii=0; ii < (genParticle->numberOfDaughters()); ii++) {
				daughterCand = genParticle->daughter(ii);
				if( (abs(daughterCand->pdgId()) == 11) || (abs(daughterCand->pdgId()) == 13) ){
					foundElectronOrMuon = true;
					break;
				}
			}
			if(!foundElectronOrMuon){ count++; }
		}
	}
	return count;

}

Triplet TauTauAnalysis::GetGenHadronicTaus(){
		Triplet result;
		result.tau1 = TLorentzVector(0,0,0,0);
		result.tau2 = TLorentzVector(0,0,0,0);
		result.parent = TLorentzVector(0,0,0,0);

		bool haveOneHadronic = false;
		for(GenParticleCollection::const_iterator genParticle = _genParticles->begin(); genParticle != _genParticles->end(); ++genParticle){

			// Match id of this gen particle with wanted id
			if((abs(genParticle->pdgId()) == 15) && (genParticle->status() != 3)) {

				// Examine number of neutrinos
				bool foundElectronOrMuon = false;
				for( unsigned int ii=0; ii < (genParticle->numberOfDaughters()); ii++) {

					daughterCand = genParticle->daughter(ii);
					if( (abs(daughterCand->pdgId()) == 11) || (abs(daughterCand->pdgId()) == 13) ){
						foundElectronOrMuon = true;
						break;
					}
				}

				// It it's hadronic...
				if(!foundElectronOrMuon){

						if(!haveOneHadronic){
							result.tau1 = TLorentzVector(genParticle->px(), genParticle->py(), genParticle->pz(), genParticle->p4().E());
							const reco::Candidate * parent = genParticle->mother(0)->mother(0);
							result.parent = TLorentzVector(parent->px(), parent->py(), parent->pz(), parent->p4().E());
							haveOneHadronic = true;
						}else{
							result.tau2 = TLorentzVector(genParticle->px(), genParticle->py(), genParticle->pz(), genParticle->p4().E());
							return result;
						}

				}

			}
		}
		return result;

}

TLorentzVector TauTauAnalysis::GetClosestGenMatch(TLorentzVector const & iTau){
	float minDeltaR = 999;
	reco::Candidate::LorentzVector theGenObject(0,0,0,0);
	for(GenParticleCollection::const_iterator genParticle = _genParticles->begin(); genParticle != _genParticles->end(); ++genParticle){

		// Match id of this gen particle with wanted id
		if((abs(genParticle->pdgId()) == 15) && (genParticle->status() != 3)) {

			int tauNeutrinos = 0;
			reco::Candidate::LorentzVector MChadtau = genParticle->p4();

			// Examine number of neutrinos
			bool foundElectronOrMuon = false;
			for( unsigned int ii=0; ii < (genParticle->numberOfDaughters()); ii++) {
				daughterCand = genParticle->daughter(ii);

				if( (abs(daughterCand->pdgId()) == 11) || (abs(daughterCand->pdgId()) == 13) ){
					cout << daughterCand->pdgId() << endl;
					foundElectronOrMuon = true;
					break;
				}
			}

			if(foundElectronOrMuon){ continue; }
			
			// Requirement on number of neutrinos 
			if( (tauNeutrinos > 0) ){

				// DeltaR matching
				float deltaR = reco::deltaR(MChadtau.eta(), MChadtau.phi(), iTau.Eta(), iTau.Phi());
				if(deltaR < minDeltaR){
					theGenObject = MChadtau;
					minDeltaR = deltaR;
				} // End of DeltaR requirement

			} // End of neutrino checking
			
		} // End of genParticle id checking

	} // End of genParticle loop

	return TLorentzVector(theGenObject.X(),theGenObject.Y(),theGenObject.Z(),theGenObject.E());
}

TLorentzVector TauTauAnalysis::BoostAndRotateToRestFrame(TLorentzVector & input, TVector3 const & iBoost){
	TLorentzVector result = input;
	result.Boost(iBoost);
	result.RotateZ(TMath::Pi()/2.0 - iBoost.Phi());
	result.RotateX(iBoost.Theta());

	return result;
}

// Book branches and link them to appropriate pointers
void TauTauAnalysis::setupBranches(){
	_TauTauTree = new TTree(_NtupleTreeName.c_str(), "TauTauTree");
	_TauTauTree->Branch("runNumber",&_runNumber);
	_TauTauTree->Branch("eventNumber",&_eventNumber);
	_TauTauTree->Branch("lumiBlock",&_lumiBlock);
	_TauTauTree->Branch("NumberOfHadronicGenTaus",&_NumberOfHadronicGenTaus);
	_TauTauTree->Branch("comboNumber",&_comboNumber);
	_TauTauTree->Branch("numInteractionsBXm1",&_numInteractionsBXm1);
	_TauTauTree->Branch("numInteractionsBX0",&_numInteractionsBX0);
	_TauTauTree->Branch("numInteractionsBXp1",&_numInteractionsBXp1);
	_TauTauTree->Branch("GenMET",&_GenMET);
	_TauTauTree->Branch("GenMETphi",&_GenMETphi);
	_TauTauTree->Branch("Tau1GenPt",&_Tau1GenPt);
	_TauTauTree->Branch("Tau2GenPt",&_Tau2GenPt);
	_TauTauTree->Branch("Tau1GenE",&_Tau1GenE);
	_TauTauTree->Branch("Tau2GenE",&_Tau2GenE);
	_TauTauTree->Branch("Tau1GenEta",&_Tau1GenEta);
	_TauTauTree->Branch("Tau2GenEta",&_Tau2GenEta);
	_TauTauTree->Branch("Tau1GenPhi",&_Tau1GenPhi);
	_TauTauTree->Branch("Tau2GenPhi",&_Tau2GenPhi);
	_TauTauTree->Branch("Tau1GenParentMass",&_Tau1GenParentMass);
	_TauTauTree->Branch("Tau2GenParentMass",&_Tau2GenParentMass);
	_TauTauTree->Branch("TauTauPlusMetGenMass",&_TauTauPlusMetGenMass);


	_TauTauTree->Branch("GenTau1P", &_GenTau1P);
	_TauTauTree->Branch("GenTau2P", &_GenTau2P);
	_TauTauTree->Branch("GenTau1Pt", &_GenTau1Pt);
	_TauTauTree->Branch("GenTau2Pt", &_GenTau2Pt);
	_TauTauTree->Branch("GenTau1Phi", &_GenTau1Phi);
	_TauTauTree->Branch("GenTau2Phi", &_GenTau2Phi);
	_TauTauTree->Branch("GenTau1Theta", &_GenTau1Theta);
	_TauTauTree->Branch("GenTau2Theta", &_GenTau2Theta);
	_TauTauTree->Branch("GenTausOmega", &_GenTausOmega);

	_TauTauTree->Branch("BoostedGenTau1P", &_BoostedGenTau1P);
	_TauTauTree->Branch("BoostedGenTau2P", &_BoostedGenTau2P);
	_TauTauTree->Branch("BoostedGenTau1Pt", &_BoostedGenTau1Pt);
	_TauTauTree->Branch("BoostedGenTau2Pt", &_BoostedGenTau2Pt);
	_TauTauTree->Branch("BoostedGenTau1Phi", &_BoostedGenTau1Phi);
	_TauTauTree->Branch("BoostedGenTau2Phi", &_BoostedGenTau2Phi);
	_TauTauTree->Branch("BoostedGenTau1Theta", &_BoostedGenTau1Theta);
	_TauTauTree->Branch("BoostedGenTau2Theta", &_BoostedGenTau2Theta);
	_TauTauTree->Branch("BoostedGenTausOmega", &_BoostedGenTausOmega);

	_TauTauTree->Branch("Tau1Matched",&_Tau1Matched);
	_TauTauTree->Branch("Tau2Matched",&_Tau2Matched);
	_TauTauTree->Branch("Tau1MotherId",&_Tau1MotherId);
	_TauTauTree->Branch("Tau2MotherId",&_Tau2MotherId);
	_TauTauTree->Branch("Tau1PdgId",&_Tau1PdgId);
	_TauTauTree->Branch("Tau2PdgId",&_Tau2PdgId);
	_TauTauTree->Branch("Tau1MotherPdgId",&_Tau1MotherPdgId);
	_TauTauTree->Branch("Tau2MotherPdgId",&_Tau2MotherPdgId);
	_TauTauTree->Branch("Tau1_MatchesGenHadronic",&_Tau1_MatchesGenHadronic);
	_TauTauTree->Branch("Tau2_MatchesGenHadronic",&_Tau2_MatchesGenHadronic);
	_TauTauTree->Branch("Tau1_ParentTauMatched",&_Tau1_ParentTauMatched);
	_TauTauTree->Branch("Tau2_ParentTauMatched",&_Tau2_ParentTauMatched);
	_TauTauTree->Branch("Tau1_ZtauMatched",&_Tau1_ZtauMatched);
	_TauTauTree->Branch("Tau2_ZtauMatched",&_Tau2_ZtauMatched);
	_TauTauTree->Branch("Tau1_ZeMatched",&_Tau1_ZeMatched);
	_TauTauTree->Branch("Tau2_ZeMatched",&_Tau2_ZeMatched);
	_TauTauTree->Branch("NumPV",&_NumPV);

	_TauTauTree->Branch("DeltaR_recoTau1_genTau1", &_DeltaR_recoTau1_genTau1);
	_TauTauTree->Branch("DeltaR_recoTau2_genTau1", &_DeltaR_recoTau2_genTau1);
	_TauTauTree->Branch("DeltaR_recoTau1_genTau2", &_DeltaR_recoTau1_genTau2);
	_TauTauTree->Branch("DeltaR_recoTau2_genTau2", &_DeltaR_recoTau2_genTau2);

	_TauTauTree->Branch("Tau1E",&_Tau1E);
	_TauTauTree->Branch("Tau2E",&_Tau2E);
	_TauTauTree->Branch("Tau1Et",&_Tau1Et);
	_TauTauTree->Branch("Tau2Et",&_Tau2Et);
	_TauTauTree->Branch("Tau1Pt",&_Tau1Pt);
	_TauTauTree->Branch("Tau2Pt",&_Tau2Pt);
	_TauTauTree->Branch("Tau1LTvalid",&_Tau1LTvalid);
	_TauTauTree->Branch("Tau2LTvalid",&_Tau2LTvalid);
	_TauTauTree->Branch("Tau1LTPt",&_Tau1LTPt);
	_TauTauTree->Branch("Tau2LTPt",&_Tau2LTPt);
	_TauTauTree->Branch("Tau1Charge",&_Tau1Charge);
	_TauTauTree->Branch("Tau2Charge",&_Tau2Charge);
	_TauTauTree->Branch("Tau1Eta",&_Tau1Eta);
	_TauTauTree->Branch("Tau2Eta",&_Tau2Eta);
	_TauTauTree->Branch("Tau1Phi",&_Tau1Phi);
	_TauTauTree->Branch("Tau2Phi",&_Tau2Phi);
	_TauTauTree->Branch("Tau1LTIpVtdxy",&_Tau1LTIpVtdxy);
	_TauTauTree->Branch("Tau1LTIpVtdz",&_Tau1LTIpVtdz);
	_TauTauTree->Branch("Tau1LTIpVtdxyError",&_Tau1LTIpVtdxyError);
	_TauTauTree->Branch("Tau1LTIpVtdzError",&_Tau1LTIpVtdzError);
	_TauTauTree->Branch("Tau1LTvx",&_Tau1LTvx);
	_TauTauTree->Branch("Tau1LTvy",&_Tau1LTvy);
	_TauTauTree->Branch("Tau1LTvz",&_Tau1LTvz);
	_TauTauTree->Branch("Tau1LTValidHits",&_Tau1LTValidHits);
	_TauTauTree->Branch("Tau1LTNormChiSqrd",&_Tau1LTNormChiSqrd);
	_TauTauTree->Branch("Tau2LTIpVtdxy",&_Tau2LTIpVtdxy);
	_TauTauTree->Branch("Tau2LTIpVtdz",&_Tau2LTIpVtdz);
	_TauTauTree->Branch("Tau2LTIpVtdxyError",&_Tau2LTIpVtdxyError);
	_TauTauTree->Branch("Tau2LTIpVtdzError",&_Tau2LTIpVtdzError);
	_TauTauTree->Branch("Tau2LTvx",&_Tau2LTvx);
	_TauTauTree->Branch("Tau2LTvy",&_Tau2LTvy);
	_TauTauTree->Branch("Tau2LTvz",&_Tau2LTvz);
	_TauTauTree->Branch("Tau2LTValidHits",&_Tau2LTValidHits);
	_TauTauTree->Branch("Tau2LTNormChiSqrd",&_Tau2LTNormChiSqrd);
	_TauTauTree->Branch("Tau1NProngs",&_Tau1NProngs);
	_TauTauTree->Branch("Tau2NProngs",&_Tau2NProngs);
	_TauTauTree->Branch("Tau1NSignalGammas",&_Tau1NSignalGammas);
	_TauTauTree->Branch("Tau2NSignalGammas",&_Tau2NSignalGammas);
	_TauTauTree->Branch("Tau1NSignalNeutrals",&_Tau1NSignalNeutrals);
	_TauTauTree->Branch("Tau2NSignalNeutrals",&_Tau2NSignalNeutrals);
	_TauTauTree->Branch("Tau1NSignalPiZeros",&_Tau1NSignalPiZeros);
	_TauTauTree->Branch("Tau2NSignalPiZeros",&_Tau2NSignalPiZeros);
	_TauTauTree->Branch("Tau1DecayMode",&_Tau1DecayMode);
	_TauTauTree->Branch("Tau2DecayMode",&_Tau2DecayMode);
	_TauTauTree->Branch("Tau1EmFraction",&_Tau1EmFraction);
	_TauTauTree->Branch("Tau2EmFraction",&_Tau2EmFraction);
	_TauTauTree->Branch("Tau1IsInTheCracks",&_Tau1IsInTheCracks);
	_TauTauTree->Branch("Tau2IsInTheCracks",&_Tau2IsInTheCracks);
	_TauTauTree->Branch("MET", &_MET);
	_TauTauTree->Branch("METphi", &_METphi);
	_TauTauTree->Branch("METl1l2l3corr", &_METl1l2l3corr);
	_TauTauTree->Branch("METl1l2l3corrPhi", &_METl1l2l3corrPhi);
	_TauTauTree->Branch("METsignificance", &_METsignificance);
	_TauTauTree->Branch("TauTauVisibleMass", &_TauTauVisibleMass);
	_TauTauTree->Branch("TauTauVisPlusMetMass", &_TauTauVisPlusMetMass);
	_TauTauTree->Branch("TauTauCollinearMetMass", &_TauTauCollinearMetMass);
	_TauTauTree->Branch("TauTauCosDPhi", &_TauTauCosDPhi);
	_TauTauTree->Branch("TauTauDeltaR", &_TauTauDeltaR);
	_TauTauTree->Branch("TauTauPZeta", &_TauTauPZeta);
	_TauTauTree->Branch("TauTauPZetaVis", &_TauTauPZetaVis);
	_TauTauTree->Branch("Tau1MetCosDphi", &_Tau1MetCosDphi);
	_TauTauTree->Branch("Tau2MetCosDphi", &_Tau2MetCosDphi);
	_TauTauTree->Branch("Tau1MetMt", &_Tau1MetMt);
	_TauTauTree->Branch("Tau2MetMt", &_Tau2MetMt);
	_TauTauTree->Branch("nBtagsHiEffTrkCnt", &_nBtagsHiEffTrkCnt);
	_TauTauTree->Branch("nBtagsHiPurityTrkCnt", &_nBtagsHiPurityTrkCnt);
	_TauTauTree->Branch("nBTagsHiEffSimpleSecVtx", &_nBTagsHiEffSimpleSecVtx);
	_TauTauTree->Branch("nBTagsHiPuritySimpleSecVtx", &_nBTagsHiPuritySimpleSecVtx);
	_TauTauTree->Branch("nBTagsCombSecVtx", &_nBTagsCombSecVtx);
	_TauTauTree->Branch("Tau1Tau2LeadingJetPtSum", &_Tau1Tau2LeadingJetPtSum);
	_TauTauTree->Branch("Tau1LeadingJetDeltaPhi", &_Tau1LeadingJetDeltaPhi);
	_TauTauTree->Branch("Tau2LeadingJetDeltaPhi", &_Tau2LeadingJetDeltaPhi);
	_TauTauTree->Branch("jetSumEt", &_jetSumEt);
	_TauTauTree->Branch("jetMETSumEt", &_jetMETSumEt);
	_TauTauTree->Branch("nJets", &_nJets);
	_TauTauTree->Branch("nExtraJets", &_nExtraJets);
	_TauTauTree->Branch("Tau1hpsPFTauDiscriminationAgainstLooseElectron", &_Tau1hpsPFTauDiscriminationAgainstLooseElectron);
	_TauTauTree->Branch("Tau1hpsPFTauDiscriminationAgainstLooseMuon", &_Tau1hpsPFTauDiscriminationAgainstLooseMuon);
	_TauTauTree->Branch("Tau1hpsPFTauDiscriminationAgainstMediumElectron", &_Tau1hpsPFTauDiscriminationAgainstMediumElectron);
	_TauTauTree->Branch("Tau1hpsPFTauDiscriminationAgainstMediumMuon", &_Tau1hpsPFTauDiscriminationAgainstMediumMuon);
	_TauTauTree->Branch("Tau1hpsPFTauDiscriminationAgainstTightElectron", &_Tau1hpsPFTauDiscriminationAgainstTightElectron);
	_TauTauTree->Branch("Tau1hpsPFTauDiscriminationAgainstTightMuon", &_Tau1hpsPFTauDiscriminationAgainstTightMuon);
	_TauTauTree->Branch("Tau1hpsPFTauDiscriminationByDecayModeFinding", &_Tau1hpsPFTauDiscriminationByDecayModeFinding);
	_TauTauTree->Branch("Tau1hpsPFTauDiscriminationByLooseIsolation", &_Tau1hpsPFTauDiscriminationByLooseIsolation);
	_TauTauTree->Branch("Tau1hpsPFTauDiscriminationByMediumIsolation", &_Tau1hpsPFTauDiscriminationByMediumIsolation);
	_TauTauTree->Branch("Tau1hpsPFTauDiscriminationByTightIsolation", &_Tau1hpsPFTauDiscriminationByTightIsolation);
	_TauTauTree->Branch("Tau1hpsPFTauDiscriminationByVLooseIsolation", &_Tau1hpsPFTauDiscriminationByVLooseIsolation);
	_TauTauTree->Branch("Tau2hpsPFTauDiscriminationAgainstLooseElectron", &_Tau2hpsPFTauDiscriminationAgainstLooseElectron);
	_TauTauTree->Branch("Tau2hpsPFTauDiscriminationAgainstLooseMuon", &_Tau2hpsPFTauDiscriminationAgainstLooseMuon);
	_TauTauTree->Branch("Tau2hpsPFTauDiscriminationAgainstMediumElectron", &_Tau2hpsPFTauDiscriminationAgainstMediumElectron);
	_TauTauTree->Branch("Tau2hpsPFTauDiscriminationAgainstMediumMuon", &_Tau2hpsPFTauDiscriminationAgainstMediumMuon);
	_TauTauTree->Branch("Tau2hpsPFTauDiscriminationAgainstTightElectron", &_Tau2hpsPFTauDiscriminationAgainstTightElectron);
	_TauTauTree->Branch("Tau2hpsPFTauDiscriminationAgainstTightMuon", &_Tau2hpsPFTauDiscriminationAgainstTightMuon);
	_TauTauTree->Branch("Tau2hpsPFTauDiscriminationByDecayModeFinding", &_Tau2hpsPFTauDiscriminationByDecayModeFinding);
	_TauTauTree->Branch("Tau2hpsPFTauDiscriminationByLooseIsolation", &_Tau2hpsPFTauDiscriminationByLooseIsolation);
	_TauTauTree->Branch("Tau2hpsPFTauDiscriminationByMediumIsolation", &_Tau2hpsPFTauDiscriminationByMediumIsolation);
	_TauTauTree->Branch("Tau2hpsPFTauDiscriminationByTightIsolation", &_Tau2hpsPFTauDiscriminationByTightIsolation);
	_TauTauTree->Branch("Tau2hpsPFTauDiscriminationByVLooseIsolation", &_Tau2hpsPFTauDiscriminationByVLooseIsolation);

	_TauTauTree->Branch("Tau1hpsPFTauDiscriminationByVLooseIsolationDBSumPtCorr", &_Tau1hpsPFTauDiscriminationByVLooseIsolationDBSumPtCorr);
	_TauTauTree->Branch("Tau1hpsPFTauDiscriminationByLooseIsolationDBSumPtCorr", &_Tau1hpsPFTauDiscriminationByLooseIsolationDBSumPtCorr);
	_TauTauTree->Branch("Tau1hpsPFTauDiscriminationByMediumIsolationDBSumPtCorr", &_Tau1hpsPFTauDiscriminationByMediumIsolationDBSumPtCorr);
	_TauTauTree->Branch("Tau1hpsPFTauDiscriminationByTightIsolationDBSumPtCorr", &_Tau1hpsPFTauDiscriminationByTightIsolationDBSumPtCorr);
	_TauTauTree->Branch("Tau1hpsPFTauDiscriminationByVLooseCombinedIsolationDBSumPtCorr", &_Tau1hpsPFTauDiscriminationByVLooseCombinedIsolationDBSumPtCorr);
	_TauTauTree->Branch("Tau1hpsPFTauDiscriminationByLooseCombinedIsolationDBSumPtCorr", &_Tau1hpsPFTauDiscriminationByLooseCombinedIsolationDBSumPtCorr);
	_TauTauTree->Branch("Tau1hpsPFTauDiscriminationByMediumCombinedIsolationDBSumPtCorr", &_Tau1hpsPFTauDiscriminationByMediumCombinedIsolationDBSumPtCorr);
	_TauTauTree->Branch("Tau1hpsPFTauDiscriminationByTightCombinedIsolationDBSumPtCorr", &_Tau1hpsPFTauDiscriminationByTightCombinedIsolationDBSumPtCorr);
	_TauTauTree->Branch("Tau2hpsPFTauDiscriminationByVLooseIsolationDBSumPtCorr", &_Tau2hpsPFTauDiscriminationByVLooseIsolationDBSumPtCorr);
	_TauTauTree->Branch("Tau2hpsPFTauDiscriminationByLooseIsolationDBSumPtCorr", &_Tau2hpsPFTauDiscriminationByLooseIsolationDBSumPtCorr);
	_TauTauTree->Branch("Tau2hpsPFTauDiscriminationByMediumIsolationDBSumPtCorr", &_Tau2hpsPFTauDiscriminationByMediumIsolationDBSumPtCorr);
	_TauTauTree->Branch("Tau2hpsPFTauDiscriminationByTightIsolationDBSumPtCorr", &_Tau2hpsPFTauDiscriminationByTightIsolationDBSumPtCorr);
	_TauTauTree->Branch("Tau2hpsPFTauDiscriminationByVLooseCombinedIsolationDBSumPtCorr", &_Tau2hpsPFTauDiscriminationByVLooseCombinedIsolationDBSumPtCorr);
	_TauTauTree->Branch("Tau2hpsPFTauDiscriminationByLooseCombinedIsolationDBSumPtCorr", &_Tau2hpsPFTauDiscriminationByLooseCombinedIsolationDBSumPtCorr);
	_TauTauTree->Branch("Tau2hpsPFTauDiscriminationByMediumCombinedIsolationDBSumPtCorr", &_Tau2hpsPFTauDiscriminationByMediumCombinedIsolationDBSumPtCorr);
	_TauTauTree->Branch("Tau2hpsPFTauDiscriminationByTightCombinedIsolationDBSumPtCorr", &_Tau2hpsPFTauDiscriminationByTightCombinedIsolationDBSumPtCorr);

	_TauTauTree->Branch("Tau1hpsPFTauDiscriminationByRawCombinedIsolationDBSumPtCorr", &_Tau1hpsPFTauDiscriminationByRawCombinedIsolationDBSumPtCorr);
	_TauTauTree->Branch("Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorr", &_Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorr);
	_TauTauTree->Branch("Tau1hpsPFTauDiscriminationByRawGammaIsolationDBSumPtCorr", &_Tau1hpsPFTauDiscriminationByRawGammaIsolationDBSumPtCorr);
	_TauTauTree->Branch("Tau2hpsPFTauDiscriminationByRawCombinedIsolationDBSumPtCorr", &_Tau2hpsPFTauDiscriminationByRawCombinedIsolationDBSumPtCorr);
	_TauTauTree->Branch("Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorr", &_Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorr);
	_TauTauTree->Branch("Tau2hpsPFTauDiscriminationByRawGammaIsolationDBSumPtCorr", &_Tau2hpsPFTauDiscriminationByRawGammaIsolationDBSumPtCorr);

/*	_TauTauTree->Branch("Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p0", &_Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p0);
	_TauTauTree->Branch("Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p1", &_Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p1);
	_TauTauTree->Branch("Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p2", &_Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p2);
	_TauTauTree->Branch("Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p3", &_Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p3);
	_TauTauTree->Branch("Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p4", &_Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p4);
	_TauTauTree->Branch("Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p0", &_Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p0);
	_TauTauTree->Branch("Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p1", &_Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p1);
	_TauTauTree->Branch("Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p2", &_Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p2);
	_TauTauTree->Branch("Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p3", &_Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p3);
	_TauTauTree->Branch("Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p4", &_Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p4); //*/

	
	_TauTauTree->Branch("NSVFitStatus", &_NSVFitStatus);
	_TauTauTree->Branch("NSVFitMass", &_NSVFitMass);
	_TauTauTree->Branch("NSVFitMassUncert", &_NSVFitMassUncert);
	_TauTauTree->Branch("NSVFitMET", &_NSVFitMET);
	_TauTauTree->Branch("NSVFitMETphi", &_NSVFitMETphi);
	_TauTauTree->Branch("NSVFitSystemPt", &_NSVFitSystemPt);
	_TauTauTree->Branch("NSVFitSystemMag", &_NSVFitSystemMag);
	_TauTauTree->Branch("NSVFitSystemPhi", &_NSVFitSystemPhi);
	_TauTauTree->Branch("NSVFitSystemEta", &_NSVFitSystemEta);

	_TauTauTree->Branch("SVFitTau1P", &_SVFitTau1P);
	_TauTauTree->Branch("SVFitTau2P", &_SVFitTau2P);
	_TauTauTree->Branch("SVFitTau1Pt", &_SVFitTau1Pt);
	_TauTauTree->Branch("SVFitTau2Pt", &_SVFitTau2Pt);
	_TauTauTree->Branch("SVFitTau1Phi", &_SVFitTau1Phi);
	_TauTauTree->Branch("SVFitTau2Phi", &_SVFitTau2Phi);
	_TauTauTree->Branch("SVFitTau1Theta", &_SVFitTau1Theta);
	_TauTauTree->Branch("SVFitTau2Theta", &_SVFitTau2Theta);
	_TauTauTree->Branch("SVFitTausOmega", &_SVFitTausOmega);

	_TauTauTree->Branch("BoostedSVFitTau1P", &_BoostedSVFitTau1P);
	_TauTauTree->Branch("BoostedSVFitTau2P", &_BoostedSVFitTau2P);
	_TauTauTree->Branch("BoostedSVFitTau1Pt", &_BoostedSVFitTau1Pt);
	_TauTauTree->Branch("BoostedSVFitTau2Pt", &_BoostedSVFitTau2Pt);
	_TauTauTree->Branch("BoostedSVFitTau1Phi", &_BoostedSVFitTau1Phi);
	_TauTauTree->Branch("BoostedSVFitTau2Phi", &_BoostedSVFitTau2Phi);
	_TauTauTree->Branch("BoostedSVFitTau1Theta", &_BoostedSVFitTau1Theta);
	_TauTauTree->Branch("BoostedSVFitTau2Theta", &_BoostedSVFitTau2Theta);
	_TauTauTree->Branch("BoostedSVFitTausOmega", &_BoostedSVFitTausOmega);


	_TauTauTree->Branch("DeltaR_recoTau1_SVFitTau1", &_DeltaR_recoTau1_SVFitTau1);
	_TauTauTree->Branch("DeltaR_recoTau2_SVFitTau1", &_DeltaR_recoTau2_SVFitTau1);
	_TauTauTree->Branch("DeltaR_recoTau1_SVFitTau2", &_DeltaR_recoTau1_SVFitTau2);
	_TauTauTree->Branch("DeltaR_recoTau2_SVFitTau2", &_DeltaR_recoTau2_SVFitTau2);

}

// Reset variables/pointers
void TauTauAnalysis::initializeVectors(){

	_runNumber								=	-1;
	_eventNumber							=	-1;
	_lumiBlock								=	-1;
	_NumberOfHadronicGenTaus				=	-1;
	_numInteractionsBXm1					=	-1;
	_numInteractionsBX0						=	-1;
	_numInteractionsBXp1					=	-1;
	_comboNumber							=	NULL;
	_GenMET									=	NULL;
	_GenMETphi								=	NULL;
	_Tau1GenPt								=	NULL;
	_Tau2GenPt								=	NULL;
	_Tau1GenE								=	NULL;
	_Tau2GenE								=	NULL;
	_Tau1GenEta								=	NULL;
	_Tau2GenEta								=	NULL;
	_Tau1GenPhi								=	NULL;
	_Tau2GenPhi								=	NULL;
	_Tau1GenParentMass						=	NULL;
	_Tau2GenParentMass						=	NULL;
	_TauTauPlusMetGenMass					=	NULL;

	_GenTau1P								=	0;
	_GenTau2P								=	0;
	_GenTau1Pt								=	0;
	_GenTau2Pt								=	0;
	_GenTau1Phi								=	0;
	_GenTau2Phi								=	0;
	_GenTau1Theta							=	0;
	_GenTau2Theta							=	0;
	_GenTausOmega							=	0;

	_BoostedGenTau1P						=	0;
	_BoostedGenTau2P						=	0;
	_BoostedGenTau1Pt						=	0;
	_BoostedGenTau2Pt						=	0;
	_BoostedGenTau1Phi						=	0;
	_BoostedGenTau2Phi						=	0;
	_BoostedGenTau1Theta					=	0;
	_BoostedGenTau2Theta					=	0;
	_BoostedGenTausOmega					=	0;

	_Tau1Matched							=	NULL;
	_Tau2Matched							=	NULL;
	_Tau1MotherId							=	NULL;
	_Tau2MotherId							=	NULL;
	_Tau1PdgId								=	NULL;
	_Tau2PdgId								=	NULL;
	_Tau1MotherPdgId						=	NULL;
	_Tau2MotherPdgId						=	NULL;
	_Tau1_ParentTauMatched					=	NULL;
	_Tau2_ParentTauMatched					=	NULL;
	_Tau1_ZtauMatched						=	NULL;
	_Tau2_ZtauMatched						=	NULL;
	_Tau1_MatchesGenHadronic				=	NULL;
	_Tau2_MatchesGenHadronic				=	NULL;
	_Tau1_ZeMatched							=	NULL;
	_Tau2_ZeMatched							=	NULL;
	_NumPV									=	NULL;

	_DeltaR_recoTau1_genTau1				=	NULL;
	_DeltaR_recoTau2_genTau1				=	NULL;
	_DeltaR_recoTau1_genTau2				=	NULL;
	_DeltaR_recoTau2_genTau2				=	NULL;

	_Tau1E									=	NULL;
	_Tau2E									=	NULL;
	_Tau1Et									=	NULL;
	_Tau2Et									=	NULL;
	_Tau1Pt									=	NULL;
	_Tau2Pt									=	NULL;
	_Tau1LTvalid							=	NULL;
	_Tau2LTvalid							=	NULL;
	_Tau1LTPt								=	NULL;
	_Tau2LTPt								=	NULL;
	_Tau1Charge								=	NULL;
	_Tau2Charge								=	NULL;
	_Tau1Eta								=	NULL;
	_Tau2Eta								=	NULL;
	_Tau1Phi								=	NULL;
	_Tau2Phi								=	NULL;
	_Tau1LTIpVtdxy							=	NULL;
	_Tau1LTIpVtdz							=	NULL;
	_Tau1LTIpVtdxyError						=	NULL;
	_Tau1LTIpVtdzError						=	NULL;
	_Tau1LTvx								=	NULL;
	_Tau1LTvy								=	NULL;
	_Tau1LTvz								=	NULL;
	_Tau1LTValidHits						=	NULL;
	_Tau1LTNormChiSqrd						=	NULL;
	_Tau2LTIpVtdxy							=	NULL;
	_Tau2LTIpVtdz							=	NULL;
	_Tau2LTIpVtdxyError						=	NULL;
	_Tau2LTIpVtdzError						=	NULL;
	_Tau2LTvx								=	NULL;
	_Tau2LTvy								=	NULL;
	_Tau2LTvz								=	NULL;
	_Tau2LTValidHits						=	NULL;
	_Tau2LTNormChiSqrd						=	NULL;
	_Tau1NProngs							=	NULL;
	_Tau2NProngs							=	NULL;
	_Tau1NSignalGammas						=	NULL;
	_Tau2NSignalGammas						=	NULL;
	_Tau1NSignalNeutrals					=	NULL;
	_Tau2NSignalNeutrals					=	NULL;
	_Tau1NSignalPiZeros						=	NULL;
	_Tau2NSignalPiZeros						=	NULL;
	_Tau1DecayMode							=	NULL;
	_Tau2DecayMode							=	NULL;
	_Tau1EmFraction							=	NULL;
	_Tau2EmFraction							=	NULL;
	_Tau1IsInTheCracks						=	NULL;
	_Tau2IsInTheCracks						=	NULL;
	_MET									=	NULL;
	_METphi									=	NULL;
	_METl1l2l3corr							=	NULL;
	_METl1l2l3corrPhi						=	NULL;
	_METsignificance						=	NULL;
	_TauTauVisibleMass						=	NULL;
	_TauTauVisPlusMetMass					=	NULL;
	_TauTauCollinearMetMass					=	NULL;
	_TauTauCosDPhi							=	NULL;
	_TauTauDeltaR							=	NULL;
	_TauTauPZeta							=	NULL;
	_TauTauPZetaVis							=	NULL;
	_Tau1MetCosDphi							=	NULL;
	_Tau2MetCosDphi							=	NULL;
	_Tau1MetMt								=	NULL;
	_Tau2MetMt								=	NULL;
	_nBtagsHiEffTrkCnt						=	NULL;
	_nBtagsHiPurityTrkCnt					=	NULL;
	_nBTagsHiEffSimpleSecVtx				=	NULL;
	_nBTagsHiPuritySimpleSecVtx				=	NULL;
	_nBTagsCombSecVtx						=	NULL;
	_jetSumEt								=	NULL;
	_jetMETSumEt							=	NULL;
	_nJets									=	NULL;
	_nExtraJets								=	NULL;
	_Tau1Tau2LeadingJetPtSum				=	NULL;
	_Tau1LeadingJetDeltaPhi					=	NULL;
	_Tau2LeadingJetDeltaPhi					=	NULL;

	_Tau1hpsPFTauDiscriminationAgainstLooseElectron		=	NULL;
	_Tau1hpsPFTauDiscriminationAgainstLooseMuon			=	NULL;
	_Tau1hpsPFTauDiscriminationAgainstMediumElectron	=	NULL;
	_Tau1hpsPFTauDiscriminationAgainstMediumMuon	=	NULL;
	_Tau1hpsPFTauDiscriminationAgainstTightElectron		=	NULL;
	_Tau1hpsPFTauDiscriminationAgainstTightMuon			=	NULL;
	_Tau1hpsPFTauDiscriminationByDecayModeFinding		=	NULL;
	_Tau1hpsPFTauDiscriminationByLooseIsolation			=	NULL;
	_Tau1hpsPFTauDiscriminationByMediumIsolation		=	NULL;
	_Tau1hpsPFTauDiscriminationByTightIsolation			=	NULL;
	_Tau1hpsPFTauDiscriminationByVLooseIsolation		=	NULL;
	_Tau2hpsPFTauDiscriminationAgainstLooseElectron		=	NULL;
	_Tau2hpsPFTauDiscriminationAgainstLooseMuon			=	NULL;
	_Tau2hpsPFTauDiscriminationAgainstMediumElectron	=	NULL;
	_Tau2hpsPFTauDiscriminationAgainstMediumMuon	=	NULL;
	_Tau2hpsPFTauDiscriminationAgainstTightElectron		=	NULL;
	_Tau2hpsPFTauDiscriminationAgainstTightMuon			=	NULL;
	_Tau2hpsPFTauDiscriminationByDecayModeFinding		=	NULL;
	_Tau2hpsPFTauDiscriminationByLooseIsolation			=	NULL;
	_Tau2hpsPFTauDiscriminationByMediumIsolation		=	NULL;
	_Tau2hpsPFTauDiscriminationByTightIsolation			=	NULL;
	_Tau2hpsPFTauDiscriminationByVLooseIsolation		=	NULL;

	_Tau1hpsPFTauDiscriminationByVLooseIsolationDBSumPtCorr				= NULL;
	_Tau1hpsPFTauDiscriminationByLooseIsolationDBSumPtCorr				= NULL;
	_Tau1hpsPFTauDiscriminationByMediumIsolationDBSumPtCorr				= NULL;
	_Tau1hpsPFTauDiscriminationByTightIsolationDBSumPtCorr				= NULL;
	_Tau1hpsPFTauDiscriminationByVLooseCombinedIsolationDBSumPtCorr		= NULL;
	_Tau1hpsPFTauDiscriminationByLooseCombinedIsolationDBSumPtCorr		= NULL;
	_Tau1hpsPFTauDiscriminationByMediumCombinedIsolationDBSumPtCorr		= NULL;
	_Tau1hpsPFTauDiscriminationByTightCombinedIsolationDBSumPtCorr		= NULL;
	_Tau2hpsPFTauDiscriminationByVLooseIsolationDBSumPtCorr				= NULL;
	_Tau2hpsPFTauDiscriminationByLooseIsolationDBSumPtCorr				= NULL;
	_Tau2hpsPFTauDiscriminationByMediumIsolationDBSumPtCorr				= NULL;
	_Tau2hpsPFTauDiscriminationByTightIsolationDBSumPtCorr				= NULL;
	_Tau2hpsPFTauDiscriminationByVLooseCombinedIsolationDBSumPtCorr		= NULL;
	_Tau2hpsPFTauDiscriminationByLooseCombinedIsolationDBSumPtCorr		= NULL;
	_Tau2hpsPFTauDiscriminationByMediumCombinedIsolationDBSumPtCorr		= NULL;
	_Tau2hpsPFTauDiscriminationByTightCombinedIsolationDBSumPtCorr		= NULL;

	_Tau1hpsPFTauDiscriminationByRawCombinedIsolationDBSumPtCorr	= NULL;
	_Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorr		= NULL;
	_Tau1hpsPFTauDiscriminationByRawGammaIsolationDBSumPtCorr		= NULL;
	_Tau2hpsPFTauDiscriminationByRawCombinedIsolationDBSumPtCorr	= NULL;
	_Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorr		= NULL;
	_Tau2hpsPFTauDiscriminationByRawGammaIsolationDBSumPtCorr		= NULL;

	/*_Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p0		= NULL;
	_Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p1		= NULL;
	_Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p2		= NULL;
	_Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p3		= NULL;
	_Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p4		= NULL;
	_Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p0		= NULL;
	_Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p1		= NULL;
	_Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p2		= NULL;
	_Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p3		= NULL;
	_Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p4		= NULL; //*/

	_NSVFitStatus						=	 NULL;
	_NSVFitMass							=	 NULL;
	_NSVFitMassUncert				=	 NULL;
	_NSVFitMET							=	 NULL;
	_NSVFitMETphi						=	 NULL;
	_NSVFitSystemPt						=	 NULL;
	_NSVFitSystemMag					=	 NULL;
	_NSVFitSystemPhi					=	 NULL;
	_NSVFitSystemEta					=	 NULL;

	_DeltaR_recoTau1_SVFitTau1			=	NULL;
	_DeltaR_recoTau2_SVFitTau1			=	NULL;
	_DeltaR_recoTau1_SVFitTau2			=	NULL;
	_DeltaR_recoTau2_SVFitTau2			=	NULL;

	_SVFitTau1P							=	 NULL;
	_SVFitTau2P							=	 NULL;
	_SVFitTau1Pt						=	 NULL;
	_SVFitTau2Pt						=	 NULL;
	_SVFitTau1Phi						=	 NULL;
	_SVFitTau2Phi						=	 NULL;
	_SVFitTau1Theta						=	 NULL;
	_SVFitTau2Theta						=	 NULL;
	_SVFitTausOmega						=	 NULL;

	_BoostedSVFitTau1P					=	 NULL;
	_BoostedSVFitTau2P					=	 NULL;
	_BoostedSVFitTau1Pt					=	 NULL;
	_BoostedSVFitTau2Pt					=	 NULL;
	_BoostedSVFitTau1Phi				=	 NULL;
	_BoostedSVFitTau2Phi				=	 NULL;
	_BoostedSVFitTau1Theta				=	 NULL;
	_BoostedSVFitTau2Theta				=	 NULL;
	_BoostedSVFitTausOmega				=	 NULL;


}

// Clear vectors
void TauTauAnalysis::clearVectors(){
	
	_comboNumber				->	clear();
	_GenMET						->	clear();
	_GenMETphi					->	clear();
	_Tau1GenPt					->	clear();
	_Tau2GenPt					->	clear();
	_Tau1GenE					->	clear();
	_Tau2GenE					->	clear();
	_Tau1GenEta					->	clear();
	_Tau2GenEta					->	clear();
	_Tau1GenPhi					->	clear();
	_Tau2GenPhi					->	clear();
	_Tau1GenParentMass			->	clear();
	_Tau2GenParentMass			->	clear();
	_TauTauPlusMetGenMass		->	clear();

	_Tau1Matched				->	clear();
	_Tau2Matched				->	clear();
	_Tau1MotherId				->	clear();
	_Tau2MotherId				->	clear();
	_Tau1PdgId					->	clear();
	_Tau2PdgId					->	clear();
	_Tau1MotherPdgId			->	clear();
	_Tau2MotherPdgId			->	clear();
	_Tau1_ParentTauMatched		->	clear();
	_Tau2_ParentTauMatched		->	clear();
	_Tau1_ZtauMatched			->	clear();
	_Tau2_ZtauMatched			->	clear();
	_Tau1_ZeMatched				->	clear();
	_Tau2_ZeMatched				->	clear();
	_Tau1_MatchesGenHadronic	->	clear();
	_Tau2_MatchesGenHadronic	->	clear();
	_NumPV						->	clear();

	_DeltaR_recoTau1_genTau1	->	clear();
	_DeltaR_recoTau2_genTau1	->	clear();
	_DeltaR_recoTau1_genTau2	->	clear();
	_DeltaR_recoTau2_genTau2	->	clear();

	_Tau1E						->	clear();
	_Tau2E						->	clear();
	_Tau1Et						->	clear();
	_Tau2Et						->	clear();
	_Tau1Pt						->	clear();
	_Tau2Pt						->	clear();
	_Tau1LTvalid				->	clear();
	_Tau2LTvalid				->	clear();
	_Tau1LTPt					->	clear();
	_Tau2LTPt					->	clear();
	_Tau1Charge					->	clear();
	_Tau2Charge					->	clear();
	_Tau1Eta					->	clear();
	_Tau2Eta					->	clear();
	_Tau1Phi					->	clear();
	_Tau2Phi					->	clear();
	_Tau1LTIpVtdxy				->	clear();
	_Tau1LTIpVtdz				->	clear();
	_Tau1LTIpVtdxyError			->	clear();
	_Tau1LTIpVtdzError			->	clear();
	_Tau1LTvx					->	clear();
	_Tau1LTvy					->	clear();
	_Tau1LTvz					->	clear();
	_Tau1LTValidHits			->	clear();
	_Tau1LTNormChiSqrd			->	clear();
	_Tau2LTIpVtdxy				->	clear();
	_Tau2LTIpVtdz				->	clear();
	_Tau2LTIpVtdxyError			->	clear();
	_Tau2LTIpVtdzError			->	clear();
	_Tau2LTvx					->	clear();
	_Tau2LTvy					->	clear();
	_Tau2LTvz					->	clear();
	_Tau2LTValidHits			->	clear();
	_Tau2LTNormChiSqrd			->	clear();
	_Tau1NProngs				->	clear();
	_Tau2NProngs				->	clear();
	_Tau1NSignalGammas			->	clear();
	_Tau2NSignalGammas			->	clear();
	_Tau1NSignalNeutrals		->	clear();
	_Tau2NSignalNeutrals		->	clear();
	_Tau1NSignalPiZeros			->	clear();
	_Tau2NSignalPiZeros			->	clear();
	_Tau1DecayMode				->	clear();
	_Tau2DecayMode				->	clear();
	_Tau1EmFraction				->	clear();
	_Tau2EmFraction				->	clear();
	_Tau1IsInTheCracks			->	clear();
	_Tau2IsInTheCracks			->	clear();
	_MET						->	clear();
	_METphi						->	clear();
	_METl1l2l3corr				->	clear();
	_METl1l2l3corrPhi			->	clear();
	_METsignificance			->	clear();
	_TauTauVisibleMass			->	clear();
	_TauTauVisPlusMetMass		->	clear();
	_TauTauCollinearMetMass		->	clear();
	_TauTauCosDPhi				->	clear();
	_TauTauDeltaR				->	clear();
	_TauTauPZeta				->	clear();
	_TauTauPZetaVis				->	clear();
	_Tau1MetCosDphi				->	clear();
	_Tau2MetCosDphi				->	clear();
	_Tau1MetMt					->	clear();
	_Tau2MetMt					->	clear();
	_nBtagsHiEffTrkCnt			->	clear();
	_nBtagsHiPurityTrkCnt		->	clear();
	_nBTagsHiEffSimpleSecVtx	->	clear();
	_nBTagsHiPuritySimpleSecVtx	->	clear();
	_nBTagsCombSecVtx			->	clear();
	_jetSumEt					->	clear();
	_jetMETSumEt				->	clear();
	_nJets						->	clear();
	_nExtraJets					->	clear();

	_Tau1Tau2LeadingJetPtSum				->clear();
	_Tau1LeadingJetDeltaPhi					->clear();
	_Tau2LeadingJetDeltaPhi					->clear();
	_Tau1hpsPFTauDiscriminationAgainstLooseElectron		->	clear();
	_Tau1hpsPFTauDiscriminationAgainstLooseMuon			->	clear();
	_Tau1hpsPFTauDiscriminationAgainstMediumElectron	->	clear();
	_Tau1hpsPFTauDiscriminationAgainstMediumMuon		->	clear();
	_Tau1hpsPFTauDiscriminationAgainstTightElectron		->	clear();
	_Tau1hpsPFTauDiscriminationAgainstTightMuon			->	clear();
	_Tau1hpsPFTauDiscriminationByDecayModeFinding		->	clear();
	_Tau1hpsPFTauDiscriminationByLooseIsolation			->	clear();
	_Tau1hpsPFTauDiscriminationByMediumIsolation		->	clear();
	_Tau1hpsPFTauDiscriminationByTightIsolation			->	clear();
	_Tau1hpsPFTauDiscriminationByVLooseIsolation		->	clear();
	_Tau2hpsPFTauDiscriminationAgainstLooseElectron		->	clear();
	_Tau2hpsPFTauDiscriminationAgainstLooseMuon			->	clear();
	_Tau2hpsPFTauDiscriminationAgainstMediumElectron	->	clear();
	_Tau2hpsPFTauDiscriminationAgainstMediumMuon		->	clear();
	_Tau2hpsPFTauDiscriminationAgainstTightElectron		->	clear();
	_Tau2hpsPFTauDiscriminationAgainstTightMuon			->	clear();
	_Tau2hpsPFTauDiscriminationByDecayModeFinding		->	clear();
	_Tau2hpsPFTauDiscriminationByLooseIsolation			->	clear();
	_Tau2hpsPFTauDiscriminationByMediumIsolation		->	clear();
	_Tau2hpsPFTauDiscriminationByTightIsolation			->	clear();
	_Tau2hpsPFTauDiscriminationByVLooseIsolation		->	clear();

	_Tau1hpsPFTauDiscriminationByRawCombinedIsolationDBSumPtCorr	->clear();
	_Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorr		->clear();
	_Tau1hpsPFTauDiscriminationByRawGammaIsolationDBSumPtCorr		->clear();
	_Tau2hpsPFTauDiscriminationByRawCombinedIsolationDBSumPtCorr	->clear();
	_Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorr		->clear();
	_Tau2hpsPFTauDiscriminationByRawGammaIsolationDBSumPtCorr		->clear();

	/*_Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p0		->clear();
	_Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p1		->clear();
	_Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p2		->clear();
	_Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p3		->clear();
	_Tau1hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p4		->clear();
	_Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p0		->clear();
	_Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p1		->clear();
	_Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p2		->clear();
	_Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p3		->clear();
	_Tau2hpsPFTauDiscriminationByRawChargedIsolationDBSumPtCorrMinIsoTrackPt0p4		->clear(); //*/


	_Tau1hpsPFTauDiscriminationByVLooseIsolationDBSumPtCorr				->clear();
	_Tau1hpsPFTauDiscriminationByLooseIsolationDBSumPtCorr				->clear();
	_Tau1hpsPFTauDiscriminationByMediumIsolationDBSumPtCorr				->clear();
	_Tau1hpsPFTauDiscriminationByTightIsolationDBSumPtCorr				->clear();
	_Tau1hpsPFTauDiscriminationByVLooseCombinedIsolationDBSumPtCorr		->clear();
	_Tau1hpsPFTauDiscriminationByLooseCombinedIsolationDBSumPtCorr		->clear();
	_Tau1hpsPFTauDiscriminationByMediumCombinedIsolationDBSumPtCorr		->clear();
	_Tau1hpsPFTauDiscriminationByTightCombinedIsolationDBSumPtCorr		->clear();
	_Tau2hpsPFTauDiscriminationByVLooseIsolationDBSumPtCorr				->clear();
	_Tau2hpsPFTauDiscriminationByLooseIsolationDBSumPtCorr				->clear();
	_Tau2hpsPFTauDiscriminationByMediumIsolationDBSumPtCorr				->clear();
	_Tau2hpsPFTauDiscriminationByTightIsolationDBSumPtCorr				->clear();
	_Tau2hpsPFTauDiscriminationByVLooseCombinedIsolationDBSumPtCorr		->clear();
	_Tau2hpsPFTauDiscriminationByLooseCombinedIsolationDBSumPtCorr		->clear();
	_Tau2hpsPFTauDiscriminationByMediumCombinedIsolationDBSumPtCorr		->clear();
	_Tau2hpsPFTauDiscriminationByTightCombinedIsolationDBSumPtCorr		->clear();


	_NSVFitStatus		->clear();
	_NSVFitMass			->clear();
	_NSVFitMassUncert	->clear();
	_NSVFitMET			->clear();
	_NSVFitMETphi		->clear();
	_NSVFitSystemPt		->clear();
	_NSVFitSystemMag	->clear();
	_NSVFitSystemPhi	->clear();
	_NSVFitSystemEta	->clear();

	_DeltaR_recoTau1_SVFitTau1	->clear();
	_DeltaR_recoTau2_SVFitTau1	->clear();
	_DeltaR_recoTau1_SVFitTau2	->clear();
	_DeltaR_recoTau2_SVFitTau2	->clear();

	_SVFitTau1P		->clear();
	_SVFitTau2P		->clear();
	_SVFitTau1Pt		->clear();
	_SVFitTau2Pt		->clear();
	_SVFitTau1Phi	->clear();
	_SVFitTau2Phi	->clear();
	_SVFitTau1Theta	->clear();
	_SVFitTau2Theta	->clear();
	_SVFitTausOmega	->clear();

	_BoostedSVFitTau1P		->clear();
	_BoostedSVFitTau2P		->clear();
	_BoostedSVFitTau1Pt		->clear();
	_BoostedSVFitTau2Pt		->clear();
	_BoostedSVFitTau1Phi	->clear();
	_BoostedSVFitTau2Phi	->clear();
	_BoostedSVFitTau1Theta	->clear();
	_BoostedSVFitTau2Theta	->clear();
	_BoostedSVFitTausOmega	->clear();

}
  
//define this as a plug-in
DEFINE_FWK_MODULE(TauTauAnalysis);
