import FWCore.ParameterSet.Config as cms
import copy
import sys

#parentId = 23 # Z
#parentId = 32 # Z'
parentId = 36 # A0/H03


tau1Prongs='-1' # '1' '2' '3' '12' '13' '23' '123'
tau2Prongs='-1' # '1' '2' '3' '12' '13' '23' '123'
MinimumMET=00

# Acceptance
CutOnDRtau1tau2      =   0   
tauMaxEta			 =   999
tauMinPt			 =   0
CutOnLTptTau1        =   0   
CutOnLTptTau2        =   0   

# ID
MinH3x3OverP            =   0.0
RejectMuons             =   0   
RejectElectrons         =   0   

# Topology
SignReq                 =   'All'
CutOnCosDPhi            =   0   
CutOnMET                =   0   

# Parse external arguments
import FWCore.ParameterSet.VarParsing as VarParsing
options = VarParsing.VarParsing("analysis")
options.register("analysisType",
       #"coll", # default value
       #"mc", # default value
       "signal", # default value
       VarParsing.VarParsing.multiplicity.singleton, # singleton or list
       VarParsing.VarParsing.varType.string,         # string, int, or float
       "is MC or is Data?"
       )   
# get and parse the command line arguments
options.parseArguments()

if( options.analysisType == "signal" ):
	iAnalysisType = "signal"
	isMC   = True
	signal = True
	doMatching = True
elif( options.analysisType=="mc" ):
	iAnalysisType = "mc"
	isMC   = True
	signal = False
	#doMatching = True
	doMatching = False
elif( options.analysisType=="coll" ):
	iAnalysisType = "coll"
	isMC   = False
	signal = False
	doMatching = False
else:
	sys.exit("Analysis type not understood")	

print '#########################################'
print 'isMC: %d' % isMC
print 'signal: %d' % signal
print 'parentID: %d' % parentId
print ''
print '### Aceptance' 
print 'CutOnDRtau1tau2: %d' % CutOnDRtau1tau2
print 'tauMaxEta: %f' % tauMaxEta
print 'tauMinPt: %f' % tauMinPt
print 'CutOnLTptTau1: %d' % CutOnLTptTau1
print 'CutOnLTptTau2: %d' % CutOnLTptTau2
print ''
print '### ID'
print 'MinH3x3OverP: %d' % MinH3x3OverP
print 'RejectMuons: %d' % RejectMuons
print 'RejectElectrons: %d' % RejectElectrons
print 'Tau1Prongs: %s' % tau1Prongs
print 'Tau2Prongs: %s' % tau2Prongs
print ''
print '### Topology'
print 'SignReq: %s' % SignReq
print 'CutOnCosDPhi: %d' % CutOnCosDPhi
print 'CutOnMET: %d' % CutOnMET
print 'MinimumMET: %d' % MinimumMET
print ''
print '#########################################'

if( isMC == 1 ): 
    inputForGenParticles = 'genParticles'
    inputForCalculatePUSystematics = False
else:
    inputForGenParticles = ''
    inputForCalculatePUSystematics = False 



process = cms.Process('HiMassTau')

process.load('Configuration.StandardSequences.Services_cff')
process.load('FWCore.MessageService.MessageLogger_cfi')
process.MessageLogger.cerr.FwkReport.reportEvery = 1000
process.options   = cms.untracked.PSet( wantSummary = cms.untracked.bool(True) )

process.maxEvents = cms.untracked.PSet(
    #input = cms.untracked.int32( -1 )
    input = cms.untracked.int32( 100 )
)
process.source = cms.Source("PoolSource",
    skipEvents = cms.untracked.uint32(0),
    fileNames = cms.untracked.vstring(
		#'dcache:/pnfs/cms/WAX/11/store/user/lpctau/HighMassTau/nvallsve/DYToTauTau_M-20_TuneZ2_7TeV-pythia6-tauola_Summer11-PU_S3_START42_V11-v2_AODSIM/nvallsve/DYToTauTau_M-20_TuneZ2_7TeV-pythia6-tauola/skimPAT-03Oct2011_DYToTauTau_M-20_TuneZ2_7TeV-pythia6-tauola_Summer11-PU_S3_START42_V11-v2_AODSIM/b92f49ba3b40abcaa84c1c5c8db063c8/ZprimeTauTau_skimPat_100_2_7Gn.root'
		#'dcache:/pnfs/cms/WAX/11/store/user/lpctau/HighMassTau/nvallsve//SUSYGluGluToHToTauTau_M-300_7TeV-pythia6-tauola_Summer11-PU_S4_START42_V11-v1_AODSIM/nvallsve/SUSYGluGluToHToTauTau_M-300_7TeV-pythia6-tauola/skimPAT-05Nov2011-v1_SUSYGluGluToHToTauTau_M-300_7TeV-pythia6-tauola_Summer11-PU_S4_START42_V11-v1_AODSIM/b92f49ba3b40abcaa84c1c5c8db063c8/ZprimeTauTau_skimPat_11_1_HVY.root'
		#'dcache:/pnfs/cms/WAX/11/store/user/nitish/ZprimeSSMToTauTau_M-1000_TuneZ2_7TeV-pythia6-tauola/1Oct2011_ZprimeSSMToTauTau_M-1000_TauTauPatTuple/fed11dc00ca8480fc91bc8ec14eb4003/TauTauSkimPat_1_1_DfQ.root'
		'file:/uscms/home/nvallsve/CMSSW_4_2_4/src/HighMassAnalysis/Configuration/test/DoubleHadronicTau_skimPat.root'
		#'dcache:/pnfs/cms/WAX/11/store/user/lpctau/HighMassTau/nvallsve//SUSYGluGluToHToTauTau_M-180_7TeV-pythia6-tauola_Summer11-PU_S4_START42_V11-v1_AODSIM/nvallsve/SUSYGluGluToHToTauTau_M-180_7TeV-pythia6-tauola/skimPAT-24Nov2011-rawIsoDisc-v1_SUSYGluGluToHToTauTau_M-180_7TeV-pythia6-tauola_Summer11-PU_S4_START42_V11-v1_AODSIM/ee592ee911b6331ff4b3efda38f5d6a1/DoubleHadronicTau_skimPat_10_2_f2w.root'
    )
)

process.load("HighMassAnalysis.Skimming.genLevelSequence_cff")
process.load("HighMassAnalysis.Skimming.genDecaysFromZPrimes_cfi")  
process.load('CommonTools/RecoAlgos/HBHENoiseFilter_cfi')

process.TFileService = cms.Service("TFileService", 
    fileName = cms.string("ditau_nTuple.root")
    #fileName = cms.string("/uscms/home/nvallsve/datan/HighMassTau/110918_Indaras_PromptReco_goodEvents_wElectrons/ditau_nTuple.root")
)

process.analyzeHiMassTau = cms.EDAnalyzer('TauTauAnalysis',
	#AnalysisType		= cms.string(iAnalysisType),
	AnalysisType		= cms.string(options.analysisType),
    AnalyzeData 		= cms.bool(False),
    KeepElectronCollInNtuple	= cms.bool(False), # Keep or not electron collection in ntuple

    #-----Generator level Inputs
    GenParticleSource = cms.untracked.InputTag(inputForGenParticles),				# gen particle collection

    #-----Inputs to determine which channel to analyze
    AnalyzeTauForLeg1		= cms.bool(True),					# if true, taus will be used for leg1
    AnalyzeMuonForLeg1		= cms.bool(False),					# if true, muons will be used for leg1
    AnalyzeElectronForLeg1	= cms.bool(False),					# if true, electrons will be used for leg1
    AnalyzeTauForLeg2		= cms.bool(True),					# if true, taus will be used for leg2
    AnalyzeMuonForLeg2		= cms.bool(False),					# if true, muons will be used for leg2
    AnalyzeElectronForLeg2	= cms.bool(False),					# if true, electrons will be used for leg2
                     			   					# with candidates passing skim requirements
    #-----Reco Tau Inputs
    #RecoTauSource = cms.InputTag('selectedLayer1ShrinkingConeHighEffPFTaus'), 
    RecoTauSource = cms.InputTag('selectedLayer1HPSPFTaus'), 
                                                    #'selectedLayer1ShrinkingConePFTaus'),
													# other choices include:
													# selectedLayer1FixedConeHighEffPFTaus
                                                    # selectedLayer1FixedConePFTaus  
													# selectedLayer1ShrinkingConeHighEffPFTaus
													# selectedLayer1ShrinkingConePFTaus

    RecoTauEtaCut 					= cms.double(tauMaxEta),										# require tau |eta|<=X
    RecoTauPtMinCut 				= cms.vdouble(tauMinPt,tauMinPt),									# require tau pt>=X
    RecoTauPtMaxCut 				= cms.vdouble(9999.,9999.),										# require tau pt<=X
    DoRecoTauDiscrByLeadTrack 		= cms.vuint32((CutOnLTptTau1 != 0),(CutOnLTptTau1 != 0)),					# if true, tau is required to pass a lead track pt cut
    UseRecoTauDiscrByLeadTrackFlag 	= cms.bool(True), 							# if true, default seed track discriminator is used
                                                     								# if false, seed track cut will be recalculated using the parameters below
    RecoTauDiscrByLeadTrack 		= cms.untracked.string('leadingTrackPtCut'),		# name of the lead track discriminator flag
    DoRecoTauDiscrByLeadTrackNhits 	= cms.vuint32(True,True),							# if true, tau leading track is required to have >= X hits
    RecoTauLeadTrackMinHits 		= cms.vint32(12,12),									# tau leading track hits >= X
    DoRecoTauDiscrByH3x3OverP 		= cms.vuint32((RejectElectrons != 0),(RejectElectrons != 0)),
    RecoTauH3x3OverP 				= cms.vdouble(MinH3x3OverP,MinH3x3OverP),


	DoRecoTauDiscrByIsolation = cms.vuint32(True,True), 					# if true, isolation will be applied
	UseTrackAndGammaCombinedIso = cms.bool(False),

	UseRecoTauDiscrByIsolationFlag = cms.bool(False), 					# if true, the default isolation discriminator is used
                                                      					# if false, isolation is recalculated using the parameters below
    RecoTauDiscrByIsolation = cms.untracked.string('byIsolation'),			# name of the isolation discriminator flag
    UseRecoTauIsoSumPtInsteadOfNiso = cms.bool(True),					# if true, sum pt is used for tau isolation instead
											# of the number of isolation candidates
    UseRecoTauEllipseForEcalIso = cms.bool(True),					# if true, an ellipse in eta-phi space will be used to define
											# the signal to isolation annulus for ECAL isolation
    RecoTauEcalIsoRphiForEllipse = cms.double(0.15),					# a:  dphi^2 / a^2 + deta^2 / b^2  (ECAL ellipse)
    RecoTauEcalIsoRetaForEllipse = cms.double(0.07),					# b:  dphi^2 / a^2 + deta^2 / b^2  (ECAL ellipse)
    RecoTauNisoMax = cms.vint32(0,0),							# number of isolation candidates <=X
    RecoTauIsoSumPtMaxCutValue = cms.vdouble(6.0,6.0),					# sum pt of tracks & gammas < X
    RecoTauIsoSumPtMinCutValue = cms.vdouble(0.0,0.0),  				# sum pt of tracks & gammas >= X

    RecoTauTrackNisoMax = cms.vint32(0,0),
    RecoTauTrackIsoSumPtMaxCutValue = cms.vdouble(1.0,1.0),
    RecoTauTrackIsoSumPtMinCutValue = cms.vdouble(0.0,0.0),
#    RecoTauGammaNisoMax = cms.int32(0),
#    RecoTauGammaIsoSumPtMaxCutValue = cms.double(1.0),
#    RecoTauGammaIsoSumPtMinCutValue = cms.double(0.0),
    RecoTauEcalNisoMax = cms.vint32(0,0),
    RecoTauEcalIsoSumPtMaxCutValue = cms.vdouble(1.0,1.0),
    RecoTauEcalIsoSumPtMinCutValue = cms.vdouble(0.0,0.0),


	RecoTau1ProngRequirement = cms.vstring(tau1Prongs,tau2Prongs),

    DoRecoTauDiscrBySignalTracksAndGammasMass = cms.bool(False),
    RecoTauSignal3ProngAndGammasMassMinCutValue = cms.double(0.9),
    RecoTauSignal3ProngAndGammasMassMaxCutValue = cms.double(999.0),
    RecoTauSignal1ProngAndGammasMassForPionMinCutValue = cms.double(0.08),
    RecoTauSignal1ProngAndGammasMassForPionMaxCutValue = cms.double(0.2),
    RecoTauSignal1ProngAndGammasMassForKaonVetoMinCutValue = cms.double(0.5),
    RecoTauSignal1ProngAndGammasMassForKaonVetoMaxCutValue = cms.double(999.0),
    RecoTauLeadTrackThreshold = cms.vdouble(5.0,5.0), 					# seed track pt > X
    RecoTauSigGamThreshold = cms.vdouble(5.0,5.0),						# gamma pt > X
    RecoTauIsoDeltaRCone = cms.double(0.5),	 					# tau outer isolation conesize
    RecoTauTrackIsoTrkThreshold = cms.double(1.0), 					# min pt requirement for isolation tracks
    RecoTauGammaIsoGamThreshold = cms.double(1.5), 					# min pt requirement for isolation gammas
    DoRecoTauDiscrAgainstElectron = cms.bool(False),					# if true, electron veto will be applied
    RecoTauDiscrAgainstElectron = cms.untracked.string('againstMediumElectron'),		# name of electron veto discriminator flag
    DoRecoTauDiscrByCrackCut = cms.bool(False),						# if true, taus that fall on the cracks will not be considered
    DoRecoTauDiscrAgainstMuon = cms.vuint32((RejectMuons != 0),(RejectMuons != 0)),					# if true, muon veto will be applied
    RecoTauDiscrAgainstMuon = cms.untracked.string('againstLooseMuon'),			# name of muon veto discriminator flag
    SetTANC = cms.bool(False),								# set true if wanting to fill TanC info in the Ntuple

    #-----Reco Muon Inputs
    RecoMuonSource = cms.InputTag('selectedPatMuons'),					# muon collection
    RecoMuonEtaCut = cms.double(999.1),							# require muon |eta|<=X
    RecoMuonPtMinCut = cms.double(0.),							# require muon pt>=X
    RecoMuonPtMaxCut = cms.double(9999.),						# require muon pt<=X
    DoRecoMuonDiscrByGlobal = cms.bool(False),						# if true, muon will be required to be a 'global muon
    DoRecoMuonDiscrByIsolation = cms.bool(False),					# if true, muon isolation will be applied
    RecoMuonTrackIsoSumPtMaxCutValue = cms.double(1.0),					# sum pt of isolation tracks & ecal rechits < X
    RecoMuonTrackIsoSumPtMinCutValue = cms.double(0.0),					# sum pt of isolation tracks & ecal rechits >= X
    RecoMuonEcalIsoSumPtMaxCutValue = cms.double(1.0),					# sum pt of isolation tracks & ecal rechits < X
    RecoMuonEcalIsoSumPtMinCutValue = cms.double(0.0),					# sum pt of isolation tracks & ecal rechits >= X
    RecoMuonIsoDeltaRCone = cms.double(0.4),						# outer conesize used for isolation
    RecoMuonTrackIsoTrkThreshold = cms.double(0.7),					# isolation tracks are required to have pt>X
    RecoMuonEcalIsoRecHitThreshold = cms.double(0.3),					# isolation rechits are required to have pt>X
    DoRecoMuonDiscrByIp = cms.bool(True),						# if true, muon will be required to have |d0|<X
    RecoMuonIpCut = cms.double(0.2),							# |d0|<X
    DoRecoMuonDiscrByPionVeto = cms.bool(True),                                        # if true, muon will be required to pass pion veto cut
    RecoMuonCaloCompCoefficient = cms.double(0.8),                                      # a -> pion veto: a*caloComp + b*segmComp
    RecoMuonSegmCompCoefficient = cms.double(1.2),                                      # b -> pion veto: a*caloComp + b*segmComp
    RecoMuonAntiPionCut = cms.double(1.0),                                              # pion veto > X

    #-----Reco Electron Inputs
    #RecoElectronSource = cms.InputTag('selectedPatElectrons'),				# electron collection
    RecoElectronSource = cms.InputTag('heepPatElectrons'),			        # electron collection
    UseHeepInfo = cms.bool(True),							# if true, heep discriminators and
											# variables will be used instead of pat defaults
    RecoElectronEtaCut = cms.double(999.1),						# require electron |eta|<=X
    RecoElectronPtMinCut = cms.double(0.),						# require electron pt>=X
    RecoElectronPtMaxCut = cms.double(9999.),						# require electron pt<=X
    DoRecoElectronDiscrByTrackIsolation = cms.bool(False), 				# if true, electrons will be required to pass track isolation
    RecoElectronTrackIsoSumPtMaxCutValue = cms.double(1.0), 				# sum pt of tracks < X
    RecoElectronTrackIsoSumPtMinCutValue = cms.double(0.0), 				# sum pt of tracks < X
    RecoElectronTrackIsoDeltaRCone = cms.double(0.4), 					# isolation conesize used to calculate sum pt of tracks
    RecoElectronTrackIsoTrkThreshold = cms.double(1.0), 				# min pt requirement for isolation tracks
    DoRecoElectronDiscrByEcalIsolation = cms.bool(False), 				# if true, electrons will be required to pass ecal isolation
    RecoElectronEcalIsoSumPtMaxCutValue = cms.double(1.0), 				# sum pt of ecal rechits < X
    RecoElectronEcalIsoSumPtMinCutValue = cms.double(0.0), 				# sum pt of ecal rechits < X
    RecoElectronEcalIsoDeltaRCone = cms.double(0.6), 					# isolation conesize used to calculate sum pt of ecal rechits
    RecoElectronEcalIsoRecHitThreshold = cms.double(1.0),				# min pt requirement for isolation rechits
    DoRecoElectronDiscrByIp = cms.bool(False), 						# if true, electron will be required to have |d0|<X
    RecoElectronIpCut = cms.double(999.), 						# |d0(impact parameter)| < X
    DoRecoElectronDiscrByEoverP = cms.bool(False), 					# if true, require the electron to have X < E/p < Y
    RecoElectronEoverPMax = cms.double(1.3), 						# E/p < Y
    RecoElectronEoverPMin = cms.double(0.7), 						# E/p > X
    DoRecoElectronDiscrByHoverEm = cms.bool(False), 					# if true, require the electron to have H/Em < X
    RecoElectronHoverEmCut = cms.double(0.05),	 					# H/Em < X
    DoRecoElectronDiscrBySigmaIEtaIEta = cms.bool(False),				# if true, apply sigmaIEtaIEta cut
    RecoElectronSigmaIEtaIEta = cms.double(0.03),					# sigmaIEtaIEta <= X (will be used
											# ONLY if heep info is NOT used
    DoRecoElectronDiscrByDEtaIn = cms.bool(False),					# if true, apply 
    RecoElectronEEDEtaIn = cms.double(0.007),
    RecoElectronEBDEtaIn = cms.double(0.005),
    DoRecoElectronDiscrByDPhiIn = cms.bool(False),
    RecoElectronEEDPhiIn = cms.double(0.09),
    RecoElectronEBDPhiIn = cms.double(0.09),
    DoRecoElectronDiscrBySCE2by5Over5by5 = cms.bool(False),			
    RecoElectronEBscE1by5Over5by5 = cms.double(0.83),					
    RecoElectronEBscE2by5Over5by5 = cms.double(0.94),					
    DoRecoElectronDiscrByMissingHits = cms.bool(False),
    RecoElectronMissingHits = cms.int32(1),
    DoRecoElectronDiscrByEcalDrivenSeed = cms.bool(False),				# if true, require electron to be ecal driven
    DoRecoElectronDiscrByTrackerDrivenSeed = cms.bool(False),				# if true, require electron to be tracker driven

    #-----Reco Jet Inputs
    RecoJetSource                       = cms.InputTag('patJetsPF'),           	# jet collection
    RecoJetEtaMinCut                    = cms.double(0.0),                              # require jet |eta|>=X
    RecoJetEtaMaxCut                    = cms.double(2.5),                            # require jet |eta|<=X
    RecoJetPtCut                        = cms.double(15.0),                             	# require jet pt>=X
    UseCorrectedJet                     = cms.bool(True),                              # if true, jet corrections are used
    RemoveJetOverlapWithMuons           = cms.bool(False),                              # if true, jets w/ dR(muon,jet)<X will not be considered
    JetMuonMatchingDeltaR               = cms.double(0.5),                              # dR(muon,jet)<X used for removing jets from the "good jet" list
    RemoveJetOverlapWithElectrons       = cms.bool(False),                              # if true, jets w/ dR(electron,jet)<X will not be considered
	JetElectronMatchingDeltaR           = cms.double(0.5),                              # dR(electron,jet)<X used for removing jets from the "good jet" list
	RemoveJetOverlapWithTaus            = cms.bool(False),                              # if true, jets w/ dR(tau,jet)<X will not be considered
	JetTauMatchingDeltaR                = cms.double(0.25),                             # dR(tau,jet)<X used for removing jets from the "good jet" list
	ApplyJetBTagging		 			= cms.bool(False),                              # if true, apply track counting high$
	JetBTaggingTCHEcut		 			= cms.double(3.3),                              # tagged as b-jet if TCHE > X

    #-----Vertex Inputs
    RecoVertexSource = cms.InputTag('offlinePrimaryVertices'), 				# vertex collection
    RecoVertexMaxZposition = cms.double(999.0),						# vertex |z| < X
    RecoVertexMinTracks = cms.int32(-1),						# vertex must have >= 2 "good" tracks used to reconstruct it
    RecoVertexTrackWeight = cms.double(0.5),						# weight used to define "good" tracks used to reconstruct vertex

    #-----Trigger Inputs
    #RecoTriggerSource = cms.InputTag("TriggerResults","",inputForTriggerCollection),			# trigger collection
    RecoTriggerSource = cms.InputTag("TriggerResults","","hiMassTau"),			# trigger collection
    #RecoTriggerSource = cms.InputTag("TriggerResults","","HLT"),			# trigger collection
    ApplyTriggerRequirements = cms.bool(False),
    TriggerRequirements = cms.vstring('HLT_Mu8_Jet40_v5'),					# trigger path name

    #-----Topology Inputs
    #RecoMetSource = cms.InputTag('patMETsPF'),						# met collection
    RecoMetSource = cms.InputTag('patMETsPFL1L2L3Cor'),						# met collection
											# particle flow met for 2X	= layer1PFMETs
											# particle flow met for 3X	= layer1METsPF
											# standard calo met for 2X & 3X	= layer1METs
    

    DoDiTauDiscrByDeltaPt = cms.bool(False),						# if true, apply cut on deltapt(leg2,leg1)
    DiTauDeltaPtMinCutValue = cms.double(30.0),						# ( pt_leg2 - pt_leg1 ) >= X
    DiTauDeltaPtMaxCutValue = cms.double(9999.0),					# ( pt_leg2 - pt_leg1 ) <= X


    DoDiscrByMet = cms.bool((CutOnMET != 0)), 							# if true, met will be required to be > X
    CalculateMetUsingOnlyLeg1AndLeg2 = cms.bool(False),                                 # if true, recalculate met using leg1 and leg2 momenta
    RecoMetCut = cms.double(MinimumMET), 							# met > X
    DoDiTauDiscrByDeltaR = cms.bool( (CutOnDRtau1tau2 != 0) ), 						# if true, ditau pairs must have dR(leg1,leg2) > X
    DiTauDeltaRCut = cms.double(0.7),	 						# dR(leg1,leg2) > X
    DiTauDiscrByOSLSType = cms.string(SignReq),		 				# if 'OS', product of leg1 charge and leg2 charge < 0
											# if 'LS', product of leg1 charge and leg2 charge > 0
											# if anything else is used, no OSLS requirement is applied
    UseTauSeedTrackForDiTauDiscrByOSLS = cms.bool(True), 				# if true, then use q(tau) = q(tau seed track)
                                                          				# if false, then use q(tau) = sum of q(tau jet tracks)
    DoDiTauDiscrByCosDphi = cms.bool((CutOnCosDPhi != 0)), 						# if true, require X <= cosine dphi(leg1,leg2) <= Y
    DiTauCosDphiMaxCut = cms.double(-0.95), 						# cosine dphi(leg1, leg2) <= Y
    DiTauCosDphiMinCut = cms.double(-1.00), 						# cosine dphi(leg1, leg2) >= X
    DoDiscrByMassReco = cms.bool(False), 						# if true, apply a mass cut on leg1+leg2+met combinations
    UseVectorSumOfVisProductsAndMetMassReco = cms.bool(True), 				# if true, calculate invariant mass using
                                                               				# vector sum of leg1, leg2, and met
    UseCollinearApproxMassReco = cms.bool(False), 					# if true, use the collinear approximation for the invariant mass
                                                 					# if both are set to false, then use only the visible products
											# to calculate the mass (leg1 + leg2)
    MassMinCut = cms.double(150.0), 							# mass > X
    MassMaxCut = cms.double(1000.0), 							# mass < Y
    DoDiTauDiscrByCDFzeta2D = cms.bool(False),						# if true, apply 2D zeta cut ( a*pzeta + b*pzetaVis > X )
    PZetaCutCoefficient = cms.double(1.0),						# a -- ( a*pzeta + b*pzetaVis > X )
    PZetaVisCutCoefficient = cms.double(-0.875),					# b -- ( a*pzeta + b*pzetaVis > X )
    CDFzeta2DCutValue = cms.double(-7.00),						# X -- ( a*pzeta + b*pzetaVis > X )
    DoDiTauDiscrByDeltaPtDivSumPt = cms.bool(False),					# if true, apply cut on deltapt(leg2,leg1) / sumpt(leg1,leg2)
    DiTauDeltaPtDivSumPtMinCutValue = cms.double(0.1),					# ( pt_leg2 - pt_leg1 ) / ( pt_leg2 + pt_leg1 ) >= X
    DiTauDeltaPtDivSumPtMaxCutValue = cms.double(1.0),					# ( pt_leg2 - pt_leg1 ) / ( pt_leg2 + pt_leg1 ) <= X
    DoDiscrByLeg1MetDphi = cms.bool(False),						# if true, require X <= cosine dphi(leg1,met) <= Y
    Leg1MetDphiMinCut = cms.double(1.30),						# cosine dphi(leg1, met) >= X
    Leg1MetDphiMaxCut = cms.double(3.15),						# cosine dphi(leg1, met) <= Y
    DoDiscrByLeg2MetDphi = cms.bool(False),						# if true, require X <= cosine dphi(leg2,met) <= Y
    Leg2MetDphiMinCut = cms.double(1.30),						# cosine dphi(leg2, met) >= X
    Leg2MetDphiMaxCut = cms.double(3.15),						# cosine dphi(leg2, met) <= Y
    DoTauDiscrByIsZeeCut = cms.bool(False),
	DoDiscrByLeg1MetMt		 = cms.bool(False),
	Leg1MetMtMinCut		 	 = cms.double(3.0),
	Leg1MetMtMaxCut		 	 = cms.double(3.0),
	DoDiscrByLeg2MetMt		 = cms.bool(False),
	Leg2MetMtMinCut		 	 = cms.double(3.0),
	Leg2MetMtMaxCut		 	 = cms.double(3.0),

    #-----do matching to gen?
    MatchLeptonToGen = cms.bool(False),							# if true, match reco lepton to a gen lepton
    UseLeptonMotherId = cms.bool(False),						# if true, require the matched lepton to come from a certain 
											# 'mother' particle
    UseLeptonGrandMotherId = cms.bool(False),						# if true, require the matched lepton to come from a certain
											# 'grandmother' particle
    LeptonMotherId = cms.int32(parentId),							# pdgId of the 'mother' particle
    DaughterId = cms.int32(11),							# pdgId of the 'daughter' particle
    doCheckGenNu = cms.bool(False),							# Check whether there is one gen neutrino per leg or not
    LeptonGrandMotherId = cms.int32(0),							# pdgId of the 'grandmother' particle
    #MatchTauToGen = cms.vuint32(signal,signal),							# if true, match reco tau to a gen had tau
    MatchTauToGen = cms.vuint32(doMatching, doMatching),							# if true, match reco tau to a gen had tau
    UseTauMotherId = cms.bool(True),							# if true, require the matched tau to come from a certain
	DoZeeMatching = cms.bool(False),
											# 'mother' particle ('mother' here is NOT 15!!!!! Matching
											# for the had tau leg already requires the vis had tau to come
											# from a tau lepton)
    UseTauGrandMotherId = cms.bool(False),						# if true, require the matched tau to come from a certain
											# 'grandmother' particle
    TauMotherId = cms.int32(parentId),							# pdgId of the 'mother' particle
    TauGrandMotherId = cms.int32(1),							# pdgId of the 'grandmother' particle
    TauToGenMatchingDeltaR = cms.double(0.25),						# matching dR:  dR(vis gen tau,reco tau)<X

    #-----ntuple Inputs
    DoProduceNtuple = cms.bool(True),							# if true, Ntuple will be filled
    NtupleTreeName = cms.untracked.string('HMTTree'),					# name of the Ntuple tree

    #-----Fill Histograms? Histograms are filled for events passing the specified cuts
    DoProduceHistograms = cms.bool(False),							# if true, Histograms will be filled
    FillRecoVertexHists = cms.bool(False),						# if true, fill histograms for vertices
    FillGenTauHists = cms.bool(False),							# if true, fill histograms for gen had taus
    FillRecoTauHists = cms.bool(False),							# if true, fill histograms for reco taus
    FillRecoMuonHists = cms.bool(False),							# if true, fill histograms for reco muons
    FillRecoElectronHists = cms.bool(False),						# if true, fill histograms for reco electrons
    FillRecoJetHists = cms.bool(False),							# if true, fill histograms for reco jets
    FillTopologyHists = cms.bool(False),						# if true, fill topology histograms (e.g. met, mass, ...)

    #-----Event Sequence inputs
    RecoTriggersNmin = cms.int32(0),							# require event to pass >=X trigger paths defined above
    RecoVertexNmin = cms.int32(0),							# require event to have >=X vertices passing specified cuts
    RecoVertexNmax = cms.int32(1000),							# require event to have <=X vertices passing specified cuts
    RecoNmin = cms.vuint32(1,1),							# require event to have >=X leg1,leg2 objects passing specified cuts
    RecoNmax = cms.vuint32(1000,1000),							# require event to have <=X leg1,leg2 objects passing specified cuts
    RecoJetNmin = cms.int32(0),								# require event to have >=X "jets" passing specified cuts
    RecoJetNmax = cms.int32(1000),							# require event to have <=X "jets" passing specified cuts
    CombinationsNmin = cms.int32(1),							# require event to have >=X leg1+leg2+met combinations 
											# passing specified cuts
    CombinationsNmax = cms.int32(5000),							# require event to have <=X leg1+leg2+met combinations
											# passing specified cuts

    EventSelectionSequence = cms.vstring('RecoTriggersNmin','RecoVertexNmin','RecoVertexNmax','RecoLeg1Nmin','RecoLeg1Nmax','RecoLeg2Nmin','RecoLeg2Nmax','RecoJetNmin','RecoJetNmax','CombinationsNmin','CombinationsNmax'),

    #-----Inputs for systematic uncertainties
    CalculatePdfSystematicUncertanties = cms.bool(False),				# if true, pdf systematic uncertanties will be calculated
    PdfWeightTags = cms.untracked.VInputTag("cteq65PdfWeights"),			# collection of weights for systematic uncertanties
    #PdfWeightTags = cms.untracked.VInputTag("cteq65PdfWeights", "MRST2006nnloPdfWeights", "MRST2007lomodPdfWeights"),
    CalculateFSRSystematics = cms.bool(False),
    CalculateISRGluonSystematics = cms.bool(False),
    CalculateISRGammaSystematics = cms.bool(False),
    SmearTheMuon = cms.bool(False),
    MuonPtScaleOffset = cms.double(1.0),
    MuonPtSigmaOffset = cms.double(1.0),
    MuonEtaScaleOffset = cms.double(1.0),
    MuonEtaSigmaOffset = cms.double(1.0),
    MuonPhiScaleOffset = cms.double(1.0),
    MuonPhiSigmaOffset = cms.double(1.0),
    SmearTheElectron = cms.bool(False),
    ElectronPtScaleOffset = cms.double(1.0),
    ElectronPtSigmaOffset = cms.double(1.0),
    ElectronEtaScaleOffset = cms.double(1.0),
    ElectronEtaSigmaOffset = cms.double(1.0),
    ElectronPhiScaleOffset = cms.double(1.0),
    ElectronPhiSigmaOffset = cms.double(1.0),
    SmearTheTau = cms.bool(False),
    TauPtScaleOffset = cms.double(1.0),
    TauPtSigmaOffset = cms.double(1.0),
    TauEtaScaleOffset = cms.double(1.0),
    TauEtaSigmaOffset = cms.double(1.0),
    TauPhiScaleOffset = cms.double(1.0),
    TauPhiSigmaOffset = cms.double(1.0),
    SmearTheJet = cms.bool(False),
    JetEnergyScaleOffset = cms.double(1.0),    
    SmearThePt = cms.bool(False),
    SmearTheEta = cms.bool(False),
    SmearThePhi = cms.bool(False),
    CalculatePUSystematics = cms.bool(inputForCalculatePUSystematics),
    PUConstantWeightFactor = cms.double(-4.70508),
    PUConstantWeightFactorRMS = cms.double(0.0),
    PUSlopeWeightParameter = cms.double(2.15181),
    PUSlopeWeightParameterRMS = cms.double(0.0),
    BosonPtBinEdges = cms.untracked.vdouble(
               0.,  1.,  2.,  3.,  4.,  5.,  6.,  7.,  8.,  9.
            , 10., 11., 12., 13., 14., 15., 16., 17., 18., 19.
            , 20., 21., 22., 23., 24., 25., 26., 27., 28., 29.
            , 30., 31., 32., 33., 34., 35., 36., 37., 38., 39.
            , 40., 41., 42., 43., 44., 45., 46., 47., 48., 49.
            , 999999.
    ),
    PtWeights = cms.untracked.vdouble( 
              0.800665, 0.822121, 0.851249, 0.868285, 0.878733
            , 0.953853, 0.928108, 0.982021, 1.00659 , 1.00648
            , 1.03218 , 1.04924 , 1.03621 , 1.08743 , 1.01951
            , 1.10519 , 0.984263, 1.04853 , 1.06724 , 1.10183
            , 1.0503  , 1.13162 , 1.03837 , 1.12936 , 0.999173
            , 1.01453 , 1.11435 , 1.10545 , 1.07199 , 1.04542
            , 1.00828 , 1.0822  , 1.09667 , 1.16144 , 1.13906
            , 1.27974 , 1.14936 , 1.23235 , 1.06667 , 1.06363
            , 1.14225 , 1.22955 , 1.12674 , 1.03944 , 1.04639
            , 1.13667 , 1.20493 , 1.09349 , 1.2107  , 1.21073
    ),  
	ApplyMuonTriggerScaleFactors		 	= cms.bool(False),
	ApplyElectronTriggerScaleFactors		= cms.bool(False),
    ApplyTauTriggerScaleFactors 			= cms.bool( (isMC != 0) ),
	TauTrigPtWeights						= cms.untracked.vdouble(0.305506, 0.659009, 0.829212, 0.891797, 0.929665, 0.947633, 0.956128, 0.950087, 0.971564, 0.959950, 0.961631, 0.967742, 0.970588, 0.970588),
	TauTrigPtBinEdges						= cms.untracked.vdouble(15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 80, 100, 125, 150),
    TauTrigEtaBinEdges 						= cms.untracked.vdouble(-999999., -2.1, -1.2, -0.8, 0.8, 1.2, 2.1, 999999.),
    TauTrigEtaWeights 						= cms.untracked.vdouble(1., 1., 1., 1., 1., 1., 1.)
)

if(signal):
  process.p = cms.Path(
      process.genParticlesFromZPrimes *
      process.genTausFromZPrimes *
#      process.genLevelTauTauSequence *
      process.analyzeHiMassTau
  )
elif(isMC):
  process.p = cms.Path(
      process.analyzeHiMassTau
  )
else:
  process.p = cms.Path(
	  process.HBHENoiseFilter *
      process.analyzeHiMassTau
  )
  

# print-out all python configuration parameter information
#print process.dumpPython()

