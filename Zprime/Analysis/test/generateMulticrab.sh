#!/bin/bash

input="$1"

if [ -z "$input" ]; then
	echo "ERROR: Must provide input file with datasets"
	exit 1;
elif [ ! -f "$input" ]; then
	echo "ERROR: Problem opening file '$input', it may not exist" 
	exit 1;
fi


while read line; do

dataset="$(echo $line | awk '{print $1}')"
atype="$(echo $line | awk '{print $2}')"

echo "[]
CMSSW.datasetpath = $dataset
CMSSW.pycfg_params  = analysisType=$atype
"

done < "$input"


