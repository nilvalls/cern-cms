import FWCore.ParameterSet.Config as cms

from PhysicsTools.PatAlgos.patEventContent_cff import *
from Configuration.EventContent.EventContent_cff import *

#--------------------------------------------------------------------------------
# per default, drop everything that is not specifically kept
#--------------------------------------------------------------------------------

patTupleEventContent = cms.PSet(
    outputCommands = cms.untracked.vstring('drop *')
)

#--------------------------------------------------------------------------------
# keep PAT layer 1 objects
#--------------------------------------------------------------------------------

#patTupleEventContent.outputCommands.extend(patEventContentNoCleaning)
#patTupleEventContent.outputCommands.extend(patExtraAodEventContent)
#patTupleEventContent.outputCommands.extend(patTriggerEventContent)

#--------------------------------------------------------------------------------
# keep collections of additional collections
#--------------------------------------------------------------------------------

patTupleEventContent.outputCommands.extend(
    [
#    'keep recoPFCandidate*_particleFlow__*',
#    'keep recoTracks_generalTracks_*_*',
#    'keep recoTrackExtras_generalTracks_*_*',
	'keep recoGsfTracks_*_*_*',
#    'keep patJets_patJetsAK5PF_*_*',
#    'keep *_ak5PFJetsLegacyHPSPiZeros_*_*',
#    'keep *_ak5PFJetsLegacyTaNCPiZeros_*_*',
#    'keep *_ak5PFJetsRecoTauPiZeros_*_*',
#    'keep *_hcalnoise_*_*',
#    'keep *_towerMaker_*_*',
#    'keep *_offlineBeamSpot_*_*',
    'keep *_offlinePrimaryVertices*_*_*'
    ]
)

#--------------------------------------------------------------------------------
# keep collections of selected PAT objects particles
#--------------------------------------------------------------------------------

#patTupleEventContent.outputCommands.extend(
#    [ 'keep *_selectedPatTaus*_*_*',
#      'keep *_selectedPatJets_*_*', 
#      'keep *_selectedPatPFParticles*_*_*',
#      'keep *_selectedPatTrackCands*_*_*',
#    ]
#)

### tau related
patTupleEventContent.outputCommands.extend(
   [
 #     'keep *_selectedLayer1HPSPFTaus_*_*',
      'keep *_hpsPFTau*_*_*',
 #     'keep *_hpsTancTausDiscrimination*_*_*',
 #     'keep *_ak5PFJetsLegacyHPSPiZeros_*_*',
 #     'keep *_ak5PFJetsLegacyTaNCPiZeros_*_*',
 #     'keep *_ak5PFJetsRecoTauPiZeros_*_*'
   ]
)
### MET related
#patTupleEventContent.outputCommands.extend(
#  [
#    'keep *_patMETsPFL1L2L3Cor_*_*'
#  ]
#)


### PU related
patTupleEventContent.outputCommands.extend(
  [
    'keep *_addPileupInfo_*_*'
  ]
)
