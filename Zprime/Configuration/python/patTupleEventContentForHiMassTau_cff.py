import FWCore.ParameterSet.Config as cms

from PhysicsTools.PatAlgos.patEventContent_cff import *
from Configuration.EventContent.EventContent_cff import *

#--------------------------------------------------------------------------------
# per default, drop everything that is not specifically kept
#--------------------------------------------------------------------------------

patTupleEventContent = cms.PSet(
    outputCommands = cms.untracked.vstring('drop *')
)

#--------------------------------------------------------------------------------
# keep PAT layer 1 objects
#--------------------------------------------------------------------------------

patTupleEventContent.outputCommands.extend(patEventContentNoCleaning)
patTupleEventContent.outputCommands.extend(patExtraAodEventContent)
patTupleEventContent.outputCommands.extend(patTriggerEventContent)

#--------------------------------------------------------------------------------
# keep collections of additional collections
#--------------------------------------------------------------------------------

patTupleEventContent.outputCommands.extend(
    [
    'keep *_TriggerResults_*_*',
    'keep *_patTriggerEvent_*_*',
    'keep patTriggerAlgorithms_patTrigger_*_*',
    'keep patTriggerConditions_patTrigger_*_*',
    'keep patTriggerObjects_patTrigger_*_*',
    'keep patTriggerFilters_patTrigger_*_*',
    'keep patTriggerPaths_patTrigger_*_*',
    'keep recoPFCandidate*_particleFlow__*',
    'keep recoTracks_generalTracks_*_*',
    'keep recoTrackExtras_generalTracks_*_*',
    'keep recoMuons_muons_*_*',
    'keep *_muonMETValueMapProducer_*_*',
    'keep booledmValueMap_muid*_*_*',
    'keep recoGsfElectronCores_*_*_*',
    'keep recoSuperClusters_*_*_*',
    'keep patJets_patJetsAK5PF_*_*',
    'keep *_generator_*_*',      
    'keep DcsStatuss_scalersRawToDigi_*_*',
    'keep *_ak5PFJetsLegacyHPSPiZeros_*_*',
    'keep *_ak5PFJetsLegacyTaNCPiZeros_*_*',
    'keep *_ak5PFJetsRecoTauPiZeros_*_*',
    'keep *_hcalnoise_*_*',
    'keep *_towerMaker_*_*',
    'keep *_offlineBeamSpot_*_*',
    'keep *_offlinePrimaryVertices*_*_*'
    ]
)

#--------------------------------------------------------------------------------
# keep collections of selected PAT objects particles
#--------------------------------------------------------------------------------

patTupleEventContent.outputCommands.extend(
    [ 'keep *_selectedPatElectrons_*_*',
      'keep *_heepPatElectrons_*_*',
      'keep *_heepPatElectrons_*_*',
      'keep *_selectedPatMuons_*_*',
      'keep *_selectedPatPhotons*_*_*',
      'keep *_selectedPatTaus*_*_*',
      'keep *_selectedPatJets_*_*', 
      'keep *_selectedPatPFParticles*_*_*',
      'keep *_selectedPatTrackCands*_*_*',
    ]
)

### tau related
patTupleEventContent.outputCommands.extend(
   [
      #'keep *_selectedLayer1ShrinkingConePFTaus_*_*',
      #'keep *_selectedLayer1ShrinkingConeHighEffPFTaus_*_*',
      'keep *_selectedLayer1HPSPFTaus_*_*',
      'keep *_hpsPFTau*_*_*',
      #'keep *_hpsTancTausDiscrimination*_*_*',
      #'keep *_shrinkingConePFTau*_*_*',
      'keep *_ak5PFJetsLegacyHPSPiZeros_*_*',
      'keep *_ak5PFJetsLegacyTaNCPiZeros_*_*',
      'keep *_ak5PFJetsRecoTauPiZeros_*_*',
      'keep recoGsfTracks_*_*_*'
   ]
)
### MET related
patTupleEventContent.outputCommands.extend(
  [
    'keep *_layer1PFMETs_*_*',
    'keep *_layer1METsPF_*_*',
    'keep *_patMETs_*_*',
    'keep *_patMETsPF_*_*',
    'keep *_patJetsPF_*_*',
    'keep *_patJets_*_*',
    'keep *_patMETsPFL1L2L3Cor_*_*',
    'keep *_patMETsPFL2L3Cor_*_*',
    'keep *_genMetCalo__hiMassTau',
    'keep *_genMetTrue__hiMassTau',
    'keep *_generator_*_*',      
    'keep recoGsfElectrons_*_*_*',
    'keep recoGsfElectronCores_*_*_*',
    'keep recoSuperClusters_*_*_*',
    'keep DcsStatuss_scalersRawToDigi_*_*',
    'keep *_tauGenJets*_*_*',
    'keep *_hltL1GtObjectMap_*_*',
    'keep edmTriggerResults_*_*_*',
    'keep triggerTriggerEvent_*_*_*',
    'drop *_hlt*_*_*',
    'drop CaloTowers*_*_*_*',
  ]
)

### PU related
patTupleEventContent.outputCommands.extend(
  [
    'keep *_addPileupInfo_*_*'
  ]
)
### Iso related
patTupleEventContent.outputCommands.extend(
  [
    'keep *_*IsoDeposit*_*_*',
    'keep *_*IsoValue*_*_*'
  ]
)
