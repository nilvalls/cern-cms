import FWCore.ParameterSet.Config as cms
import copy

### This files controls whether we are working with data/MC, signalMC (for skim), and channel.
### Make sure to set it to the desired process.
from HighMassAnalysis.Configuration.hiMassSetup_cfi import *

process = cms.Process('hiMassTau')

# import of standard configurations for RECOnstruction
process.load('Configuration/StandardSequences/Services_cff')

process.load("RecoTauTag.Configuration.RecoPFTauTag_cff") # testing

# initialize MessageLogger and output report
process.load("FWCore.MessageLogger.MessageLogger_cfi")
process.MessageLogger.cerr.FwkReport.reportEvery = 10000
process.MessageLogger.cerr.threshold = 'INFO'
process.options   = cms.untracked.PSet( wantSummary = cms.untracked.bool(True) )

# standard sequences
process.load('Configuration/StandardSequences/GeometryIdeal_cff')
process.load('Configuration/StandardSequences/MagneticField_cff')
process.load('Configuration/StandardSequences/Reconstruction_cff')
process.load('Configuration/StandardSequences/FrontierConditions_GlobalTag_cff')

#process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_noesprefer_cff')

# Parse external arguments
import FWCore.ParameterSet.VarParsing as VarParsing
options = VarParsing.VarParsing("analysis")
options.register("analysisType",
       #"coll", # default value
       "mc", # default value
       VarParsing.VarParsing.multiplicity.singleton, # singleton or list
       VarParsing.VarParsing.varType.string,         # string, int, or float
       "is MC or is Data?"
       )   

# get and parse the command line arguments
options.parseArguments()

if( options.analysisType=="mc" ):
	data = False
	signal = False
elif( options.analysisType=="coll" ):
	data = True
	signal = False
else:
    sys.exit("Analysis type not understood") 

# Global tags
if(data):
	process.GlobalTag.globaltag = 'GR_R_42_V14::All'
else:
	process.GlobalTag.globaltag = 'START41_V0::All'


# import particle data table - needed for print-out of generator level information
process.load("SimGeneral.HepPDTESSource.pythiapdt_cfi")

# import sequence for PAT-tuple production
process.load("HighMassAnalysis.Configuration.producePatTuple_cff")

# import event-content definition of products to be stored in patTuple
#from HighMassAnalysis.Configuration.patTupleEventContentForHiMassTau_cff import *
#
#process.savePatTuple = cms.OutputModule("PoolOutputModule",
#    patTupleEventContent,                                               
#    fastCloning = cms.untracked.bool(False),
#    fileName = cms.untracked.string('skimPat.root')
#)

from HighMassAnalysis.Configuration.patTupleEventContentForHiMassTau_cff import *

process.out = cms.OutputModule("PoolOutputModule",
    patTupleEventContent,                                               
    fileName = cms.untracked.string('DoubleHadronicTau_skimPat.root'),
    # save only events passing the full path
    SelectEvents   = cms.untracked.PSet( SelectEvents = cms.vstring('p') ),
    fastCloning = cms.untracked.bool(False)
)

process.outpath = cms.EndPath(process.out)

process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32( 100 ) )

process.source = cms.Source("PoolSource",
    skipEvents = cms.untracked.uint32(0),
    fileNames = cms.untracked.vstring(
        #'/store/data/Run2011A/SingleMu/RECO/PromptReco-v1/000/161/103/FE4260FF-8556-E011-8184-003048F1BF68.root'
	#'/store/data/Run2011A/SingleMu/AOD/PromptReco-v1/000/161/312/F8AEC745-DF57-E011-8D23-001D09F290BF.root'
        #'file:/uscms_data/d2/luiggi/rootFiles/MuEGMay10ReRecoAOD.root' 
		#'store/data/Run2011A/Tau/AOD/PromptReco-v6/000/174/084/BE2EA628-13D1-E011-8541-485B3962633D.root'
		'/store/mc/Summer11/DYToEE_M-20_TuneZ2_7TeV-pythia6/AODSIM/PU_S3_START42_V11-v2/0000/00278D82-8D7C-E011-8EA6-003048D437DA.root'
    )
    #skipBadFiles = cms.untracked.bool(True) 
)

process.scrapingVeto = cms.EDFilter("FilterOutScraping",
                                     applyfilter = cms.untracked.bool(True),
                                     debugOn = cms.untracked.bool(False),
                                     numtrack = cms.untracked.uint32(10),
                                     thresh = cms.untracked.double(0.2)
                                     )

# include particle flow based MET
from PhysicsTools.PatAlgos.tools.metTools import *
addPfMET(process, 'PF')

# include particle flow based jets
#from PhysicsTools.PatAlgos.tools.jetTools import *
#addJetCollection(process,cms.InputTag('ak5PFJets'),
#                  'AK5', 'PF',
#                 doJTA        = True,
#                 doBTagging   = True,
#                 jetCorrLabel = ('AK5','PF'),
#                 doType1MET   = False,
#                 doL1Cleaning = False,                 
#                 doL1Counters = False,
#                 genJetCollection=cms.InputTag("ak5GenJets"),
#                 doJetID          = False
#                 )

from PhysicsTools.PatAlgos.tools.electronTools import *
#addElectronUserIsolation(process,["Tracker"])
#addElectronUserIsolation(process,["Ecal"])
#addElectronUserIsolation(process,["Hcal"])

from PhysicsTools.PatAlgos.tools.muonTools import *
addMuonUserIsolation(process)

# trigger + Skim sequence
process.load("HighMassAnalysis.Skimming.triggerReq_cfi")
process.load("HighMassAnalysis.Skimming.genLevelSequence_cff")

if(channel == "emu"):
  process.load("HighMassAnalysis.Skimming.leptonLeptonSkimSequence_cff")
  process.theSkim = cms.Sequence(
    process.muElecSkimSequence
  )
  process.hltFilter = cms.Sequence(
    process.emuHLTFilter
  )
  process.genLevelSelection = cms.Sequence(
    process.genLevelElecMuSequence
  )
if(channel == "etau"):
  process.load("HighMassAnalysis.Skimming.elecTauSkimSequence_cff")
  process.theSkim = cms.Sequence(
    process.elecTauSkimSequence
  )
  process.hltFilter = cms.Sequence(
     process.etauHLTFilter
  )
  process.genLevelSelection = cms.Sequence(
    process.genLevelElecTauSequence
  )
if(channel == "mutau"):
  process.load("HighMassAnalysis.Skimming.muTauSkimSequence_cff")
  process.theSkim = cms.Sequence(
    process.muTauSkimSequence
  )
  process.hltFilter = cms.Sequence(
     process.mutauHLTFilter
  )
  process.genLevelSelection = cms.Sequence(
    process.genLevelMuTauSequence
  )
if(channel == "tautau"):
  process.load("HighMassAnalysis.Skimming.TauTauSkimSequence_cff")
  process.theSkim = cms.Sequence(
    process.TauTauSkimSequence
  )
  process.hltFilter = cms.Sequence(
     process.tautauHLTFilter
  )
  process.genLevelSelection = cms.Sequence(
    process.genLevelTauTauSequence
  )


# Remove skimming requirements
process.selectedPFTaus.cut = cms.string("")
process.TauTauPairs.deltaRMin = cms.double(0.0)

pythonDump = open("skimPAT_pythondump.py", "write")
print >> pythonDump,  process.dumpPython()

if(data):
  process.p = cms.Path(
    process.scrapingVeto + 
    process.hltFilter +       
	process.PFTau + # testing
    process.theSkim +	       
    process.producePatTuple
#    process.savePatTuple	       
  )
else:
  if(signal):
    process.p = cms.Path(
      process.genLevelSelection +
	  process.PFTau + # testing
      process.producePatTuple
#      process.savePatTuple	       
    )
  else:
    process.p = cms.Path(
	  process.PFTau + # testing
      process.theSkim +	       
      process.producePatTuple
#      process.savePatTuple	       
    )
