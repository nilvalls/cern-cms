import FWCore.ParameterSet.Config as cms

data = False
signal = False
channel = "mutautau"

process = cms.Process("PATTuple")

## MessageLogger
process.load("FWCore.MessageLogger.MessageLogger_cfi")
process.MessageLogger.cerr.FwkReport.reportEvery = 100

## Geometry and Detector Conditions (needed for a few patTuple production steps)
process.load("Configuration.StandardSequences.Geometry_cff")
process.load("Configuration.StandardSequences.FrontierConditions_GlobalTag_cff")
from Configuration.AlCa.autoCond import autoCond
process.GlobalTag.globaltag = cms.string( autoCond[ 'startup' ] )
process.load("Configuration.StandardSequences.MagneticField_cff")

## Options and Output Report
process.options   = cms.untracked.PSet( wantSummary = cms.untracked.bool(True) )

process.source = cms.Source("PoolSource", 
     fileNames = cms.untracked.vstring(
        '/store/mc/Fall11/TTJets_TuneZ2_7TeV-madgraph-tauola/AODSIM/PU_S6_START44_V9B-v1/0000/80D132A6-CA36-E111-93A7-003048678A7E.root'
      )
)
process.options   = cms.untracked.PSet( wantSummary = cms.untracked.bool(True))

process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(500) )
## Output Module Configuration (expects a path 'p')
from HighMassAnalysis.Configuration.patTupleEventContentForHiMassTau_cff import *
process.out = cms.OutputModule("PoolOutputModule",
	patTupleEventContent,
	fileName = cms.untracked.string('muTauTauPatSkim.root'),
    	# save only events passing the full path
	SelectEvents   = cms.untracked.PSet( SelectEvents = cms.vstring('p') ),
	fastCloning = cms.untracked.bool(False)
)

process.scrapingVeto = cms.EDFilter("FilterOutScraping",
                                     applyfilter = cms.untracked.bool(True),
                                     debugOn = cms.untracked.bool(False),
                                     numtrack = cms.untracked.uint32(10),
                                     thresh = cms.untracked.double(0.2)
                                     )

# trigger + Skim sequence
process.load("HighMassAnalysis.Skimming.triggerReq_cfi")
process.load("HighMassAnalysis.Skimming.genLevelSequence_cff")


if(channel == "emu"):
  process.load("HighMassAnalysis.Skimming.leptonLeptonSkimSequence_cff")
  process.theSkim = cms.Sequence(
    process.muElecSkimSequence
  )
  process.hltFilter = cms.Sequence(
    process.emuHLTFilter
  )
  process.genLevelSelection = cms.Sequence(
    process.genLevelElecMuSequence
  )
if(channel == "etau"):
  process.load("HighMassAnalysis.Skimming.elecTauSkimSequence_cff")
  process.theSkim = cms.Sequence(
    process.elecTauSkimSequence
  )
  process.hltFilter = cms.Sequence(
     process.etauHLTFilter
  )
  process.genLevelSelection = cms.Sequence(
    process.genLevelElecTauSequence
  )
if(channel == "mutau"):
  process.load("HighMassAnalysis.Skimming.muTauSkimSequence_cff")
  process.theSkim = cms.Sequence(
    process.muTauSkimSequence
  )
  process.hltFilter = cms.Sequence(
     process.mutauHLTFilter
  )
  process.genLevelSelection = cms.Sequence(
    process.genLevelMuTauSequence
  )
if(channel == "tautau"):
  process.load("HighMassAnalysis.Skimming.TauTauSkimSequence_cff")
  process.theSkim = cms.Sequence(
    process.TauTauSkimSequence
  )
  process.hltFilter = cms.Sequence(
     process.tautauHLTFilter
  )
  process.genLevelSelection = cms.Sequence(
    process.genLevelTauTauSequence
  )
if(channel == "mutautau"):
  process.load("HighMassAnalysis.Skimming.TauTauSkimSequence_cff")
  process.load("HighMassAnalysis.Skimming.WHTauTauGenLevel_cfi")
  process.theSkim = cms.Sequence(
    process.MuTauTauSkimSequence
  )
  process.hltFilter = cms.Sequence(
     process.mutauHLTFilter
  )
  process.genLevelSelection = cms.Sequence(
    process.WHToMuTauTauFilter +
    process.genLevelTauTauSequence
  )
if(channel == "electautau"):
  process.load("HighMassAnalysis.Skimming.TauTauSkimSequence_cff")
  process.load("HighMassAnalysis.Skimming.WHTauTauGenLevel_cfi")
  process.theSkim = cms.Sequence(
    process.ElecTauTauSkimSequence
  )
  process.hltFilter = cms.Sequence(
     process.etauHLTFilter
  )
  process.genLevelSelection = cms.Sequence(
    process.WHToElecTauTauFilter +
    process.genLevelTauTauSequence
  )
# load the PAT config
process.load("PhysicsTools.PatAlgos.patSequences_cff")

if(data):
  from PhysicsTools.PatAlgos.tools.coreTools import *
  removeMCMatching(process, ['All'])

# Configure PAT to use PF2PAT instead of AOD sources
# this function will modify the PAT sequences. 
#from PhysicsTools.PatAlgos.tools.pfTools import *
#
#postfix = "PFlow"
#jetAlgo="AK5"
#usePF2PAT(process,runPF2PAT=True, jetAlgo=jetAlgo, runOnMC=True, postfix=postfix) 
#
# to switch default tau to HPS tau uncomment the following:
#adaptPFTaus(process,"hpsPFTau",postfix=postfix)
from PhysicsTools.PatAlgos.tools.tauTools import *
switchToPFTauHPS(process)

## uncomment the following line to add different jet collections
## to the event content
from PhysicsTools.PatAlgos.tools.jetTools import *


## uncomment the following lines to add ak5JPTJets to your PAT output
#addJetCollection(process,cms.InputTag('JetPlusTrackZSPCorJetAntiKt5'),
#                 'AK5', 'JPT',
#                 doJTA        = True,
#                 doBTagging   = True,
#                 jetCorrLabel = ('AK5JPT', cms.vstring(['L1Offset', 'L1JPTOffset', 'L2Relative', 'L3Absolute'])),
#                 doType1MET   = False,
#                 doL1Cleaning = False,
#                 doL1Counters = True,                 
#                 genJetCollection = cms.InputTag("ak5GenJets"),
#                 doJetID      = True,
#                 jetIdLabel   = "ak5"
#                 )

## uncomment the following lines to add ak7CaloJets to your PAT output
addJetCollection(process,cms.InputTag('ak7CaloJets'),
                 'AK7', 'Calo',
                 doJTA        = True,
                 doBTagging   = False,
                 jetCorrLabel = ('AK7Calo', cms.vstring(['L1Offset', 'L2Relative', 'L3Absolute'])),
                 doType1MET   = True,
                 doL1Cleaning = True,                 
                 doL1Counters = False,
                 genJetCollection=cms.InputTag("ak7GenJets"),
                 doJetID      = True,
                 jetIdLabel   = "ak7"
                 )

## uncomment the following lines to add kt4CaloJets to your PAT output
addJetCollection(process,cms.InputTag('kt4CaloJets'),
                 'KT4', 'Calo',
                 doJTA        = True,
                 doBTagging   = True,
                 jetCorrLabel = ('KT4Calo', cms.vstring(['L2Relative', 'L3Absolute'])),
                 doType1MET   = True,
                 doL1Cleaning = True,                 
                 doL1Counters = False,
                 genJetCollection=cms.InputTag("kt4GenJets"),
                 doJetID      = True,
                 jetIdLabel   = "kt4"
                 )

## uncomment the following lines to add kt6CaloJets to your PAT output
#addJetCollection(process,cms.InputTag('kt6CaloJets'),
#                 'KT6', 'Calo',
#                 doJTA        = True,
#                 doBTagging   = False,
#                 jetCorrLabel = ('KT6Calo', cms.vstring(['L2Relative', 'L3Absolute'])),
#                 doType1MET   = False,
#                 doL1Cleaning = True,                 
#                 doL1Counters = False,
#                 genJetCollection=cms.InputTag("kt6GenJets"),
#                 doJetID      = True,
#                 jetIdLabel   = "kt6"
#                 )

## uncomment the following lines to add ak5PFJets to your PAT output
switchJetCollection(process,cms.InputTag('ak5PFJets'),
                 doJTA        = True,
                 doBTagging   = True,
                 jetCorrLabel = ('AK5PF', cms.vstring(['L1Offset', 'L2Relative', 'L3Absolute'])),
                 doType1MET   = True,
                 genJetCollection=cms.InputTag("ak5GenJets"),
                 doJetID      = True
                 )

process.load("RecoTauTag.Configuration.RecoPFTauTag_cff")
from PhysicsTools.PatAlgos.tools.metTools import *
addPfMET(process, 'PF')

# adding pf iso to lepton collection
from CommonTools.ParticleFlow.Tools.pfIsolation import setupPFElectronIso, setupPFMuonIso
process.eleIsoSequence = setupPFElectronIso(process, 'selectedPatElectrons')
process.muIsoSequence = setupPFMuonIso(process, 'selectedPatMuons')

from SHarper.HEEPAnalyzer.HEEPSelectionCuts_cfi import *
process.heepPatElectrons = cms.EDProducer("HEEPAttStatusToPAT",
                                          eleLabel = cms.InputTag("selectedPatElectrons"),
                                          barrelCuts = cms.PSet(heepBarrelCuts),
                                          endcapCuts = cms.PSet(heepEndcapCuts)
                                          )

# Let it run
process.p = cms.Path(
    process.PFTau  
    + process.patDefaultSequence
    + process.heepPatElectrons
    #+ getattr(process,"patPF2PATSequence"+postfix)
)

if(data):
  process.p = cms.Path(
    process.scrapingVeto + 
    process.PFTau +
    process.patDefaultSequence + 
    process.heepPatElectrons +
    process.hltFilter +       
    process.theSkim	       
  )
else:
  if(signal):
    process.p = cms.Path(
      process.genLevelSelection +
      process.PFTau +
      process.patDefaultSequence + 
      process.heepPatElectrons
    )
  else:
    process.p = cms.Path(
      #process.hltFilter +       
      process.PFTau +
      process.patDefaultSequence + 
      process.eleIsoSequence +
      process.muIsoSequence +
      process.heepPatElectrons +
      process.theSkim
    )
process.outpath = cms.EndPath(process.out)

## top projections in PF2PAT:
#getattr(process,"pfNoPileUp"+postfix).enable = True 
#getattr(process,"pfNoMuon"+postfix).enable = True 
#getattr(process,"pfNoElectron"+postfix).enable = True 
#getattr(process,"pfNoTau"+postfix).enable = False 
#getattr(process,"pfNoJet"+postfix).enable = True
#
## verbose flags for the PF2PAT modules
#getattr(process,"pfNoMuon"+postfix).verbose = False
#
## enable delta beta correction for muon selection in PF2PAT? 
#getattr(process,"pfIsolatedMuons"+postfix).doDeltaBetaCorrection = True
