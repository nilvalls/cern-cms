// system include files
#include <memory>

// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDFilter.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "DataFormats/PatCandidates/interface/Tau.h"
#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/PatCandidates/interface/CompositeCandidate.h"
#include "DataFormats/Math/interface/LorentzVector.h"
#include "DataFormats/Math/interface/LorentzVectorFwd.h"
#include "DataFormats/JetReco/interface/GenJetCollection.h"
#include "DataFormats/JetReco/interface/GenJet.h"
#include "DataFormats/PatCandidates/interface/Isolation.h"
#include "DataFormats/Math/interface/deltaR.h"
#include "DataFormats/Math/interface/normalizedPhi.h"
#include "DataFormats/TauReco/interface/PFTau.h"
#include "DataFormats/Common/interface/RefVector.h"
#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/Candidate/interface/Candidate.h"
#include "DataFormats/Common/interface/Ref.h"
#include "DataFormats/Common/interface/ValueMap.h"
#include "DataFormats/VertexReco/interface/Vertex.h"
#include "DataFormats/VertexReco/interface/VertexFwd.h"
#include "FWCore/Common/interface/TriggerNames.h"
#include "DataFormats/Common/interface/TriggerResults.h"
#include "CLHEP/Random/RandGauss.h"
#include "CommonTools/CandUtils/interface/Booster.h"
#include <Math/VectorUtil.h>

#include "DataFormats/HLTReco/interface/TriggerObject.h"
#include "DataFormats/HLTReco/interface/TriggerEvent.h"

using namespace std;
using namespace edm;
using namespace reco;

class DitauPATskimmer : public edm::EDFilter {
	public:
		explicit DitauPATskimmer(const edm::ParameterSet&);
		~DitauPATskimmer();

	private:
		virtual void beginJob() ;
		virtual bool filter(edm::Event&, const edm::EventSetup&);
		virtual void endJob() ;
		void getCollections(const edm::Event&, const edm::EventSetup&);  
		bool passProngCut(const pat::Tau& patTau);
		bool passMinimumPtCut(const pat::Tau& patTau);
		bool passElectronVeto(const pat::Tau& patTau);
		bool passMuonVeto(const pat::Tau& patTau);
		bool passDeltaRCut(const pat::Tau& patTau1, const pat::Tau& patTau2);
		pair<bool, reco::Candidate::LorentzVector> matchToGen(const pat::Tau& theObject);
		bool isInTheCracks(float eta);

		// Collections and their input tags
		InputTag _GenParticleSource;
		Handle< reco::GenParticleCollection >	_genParticles;
		InputTag _RecoTauSource;
		Handle< pat::TauCollection >			_patTaus;
		InputTag _RecoMetSource;
		Handle< pat::METCollection >			_patMETs;
		InputTag _RecoTriggerSource;
		Handle< edm::TriggerResults >			_triggerResults;

		// Generator matching variables
		bool	_MatchTauToGen;
		bool	_UseTauMotherId;
		bool	_UseTauGrandMotherId;
		int		_TauMotherId;
		int		_TauGrandMotherId;
		double	_TauToGenMatchingDeltaR;

		// Cut switches, thresholds and requirements
		bool	_SelectRecoTau1Prongs;
		bool	_SelectRecoTau2Prongs;
		bool	_SelectRecoTau3Prongs;
		double	_RecoTauMinimumPt;
		double	_DitauMinimumDeltaR;
		bool	_DoRecoTauDiscrByLeadTrackNhits;
		int		_RecoTauLeadTrackMinHits;
		bool	_DoRecoTauDiscrByH3x3OverP;
		double	_RecoTauH3x3OverP;
		bool	_DoRecoTauDiscrAgainstMuon;
		bool	_DoRecoTauDiscrByCrackCut;
		string	_RecoTauDiscrAgainstMuon;

};


void DitauPATskimmer::getCollections(const Event& iEvent, const EventSetup& iSetup){
	if(_GenParticleSource.label() != "") { iEvent.getByLabel(_GenParticleSource, _genParticles); }
	iEvent.getByLabel(_RecoTauSource, _patTaus);
	iEvent.getByLabel(_RecoMetSource, _patMETs);
	iEvent.getByLabel(_RecoTriggerSource, _triggerResults);
}    

// Select events based on the number of prongs (both legs)
bool DitauPATskimmer::passProngCut(const pat::Tau& patTau){
	bool result			= false;
	bool pass1Prongs	= false;
	bool pass2Prongs	= false;
	bool pass3Prongs	= false;
	int  numTracks		= 0;

	// Get the number of tracks from the appropriate collection
	if (patTau.isCaloTau())	{ numTracks = patTau.signalTracks().size();				}
	else 					{ numTracks = patTau.signalPFChargedHadrCands().size(); }

	// Figure out what we're looking for and what we have
	pass1Prongs =	_SelectRecoTau1Prongs && ( numTracks == 1 );	// Only true when we are indeed interested in 1 prongs and we have a 1 prong  
	pass2Prongs =	_SelectRecoTau2Prongs && ( numTracks == 2 ); 	// Only true when we are indeed interested in 2 prongs and we have a 2 prong 
	pass3Prongs =	_SelectRecoTau3Prongs && ( numTracks == 3 );  	// Only true when we are indeed interested in 3 prongs and we have a 3 prong

	result = (pass1Prongs || pass2Prongs) || pass3Prongs;   // If any of the conditions above is true, return true!

	return result;
}


//-----Matching Taus to generator level objects
pair<bool, reco::Candidate::LorentzVector> DitauPATskimmer::matchToGen(const pat::Tau& theObject) {
	bool isGenMatched = false;
	reco::Candidate::LorentzVector MChadtau;
	const reco::Candidate * daughterCand;
	const reco::Candidate * motherCand;
	const reco::Candidate * grandMotherCand;

	reco::Candidate::LorentzVector theGenObject(0,0,0,0);
	for( GenParticleCollection::const_iterator genParticle = _genParticles->begin(); genParticle != _genParticles->end(); ++genParticle) {
		if((abs(genParticle->pdgId()) == 15) && (genParticle->status() != 3)) {
			int neutrinos = 0; 
			MChadtau = genParticle->p4();
			if(genParticle->mother(0)->pdgId() == genParticle->pdgId()) {
				motherCand = genParticle->mother(0)->mother(0);
				if(motherCand->mother(0)->pdgId() == motherCand->pdgId()) {grandMotherCand = motherCand->mother(0)->mother(0);}
				else {grandMotherCand = motherCand->mother(0);}
			} else {
				motherCand = genParticle->mother(0);
				if(motherCand->mother(0)->pdgId() == motherCand->pdgId()) {grandMotherCand = motherCand->mother(0)->mother(0);}
				else {grandMotherCand = motherCand->mother(0);}
			}    
			for(int ii=0; ii<(int)(genParticle->numberOfDaughters()); ii++) {
				daughterCand = genParticle->daughter(ii);
				if( (abs(daughterCand->pdgId()) == 12) || (abs(daughterCand->pdgId()) == 14) || (abs(daughterCand->pdgId()) == 16) ) {
					neutrinos++;
					MChadtau = MChadtau - daughterCand->p4();
				}    
			}    
			if(neutrinos == 1) { 
				if(reco::deltaR(MChadtau.eta(), MChadtau.phi(), theObject.eta(), theObject.phi()) < _TauToGenMatchingDeltaR) {
					if(_UseTauMotherId) {
						if(abs(motherCand->pdgId()) == _TauMotherId) {
							if(_UseTauGrandMotherId) {
								if(abs(grandMotherCand->pdgId()) == _TauGrandMotherId) {isGenMatched = true;theGenObject = MChadtau;}
							} else {
								isGenMatched = true;
								theGenObject = MChadtau;
							}    
						}    
					} else {isGenMatched = true;theGenObject = MChadtau;}
				}    
			}    
		}    
	}    
	pair<bool, reco::Candidate::LorentzVector> GenMatchedInformation(isGenMatched,theGenObject);
	return GenMatchedInformation;
}    


bool DitauPATskimmer::passMinimumPtCut(const pat::Tau& patTau){
	bool result;
	result = patTau.pt() >= _RecoTauMinimumPt;

	return result;
}


bool DitauPATskimmer::passDeltaRCut(const pat::Tau& patTau1, const pat::Tau& patTau2){
	bool result;
	result = reco::deltaR(patTau1.p4(), patTau2.p4()) >= _DitauMinimumDeltaR;

	return result;
}

bool DitauPATskimmer::passElectronVeto(const pat::Tau& patTau){
	bool result = true;

	// ----"eVeto" - Lead track minimimum hits requirement && H3x3/LTpt
	if (_DoRecoTauDiscrByLeadTrackNhits) {
		if (patTau.isCaloTau()) {
			if( (!(patTau.leadTrack().isNonnull())) || ((int)(patTau.leadTrack()->recHitsSize()) < _RecoTauLeadTrackMinHits) ) {return false;}
		} else {
			if( (!(patTau.leadPFChargedHadrCand().isNonnull())) || (!(patTau.leadPFChargedHadrCand()->trackRef().isNonnull())) ) {return false;}
			if( (int)(patTau.leadPFChargedHadrCand()->trackRef()->recHitsSize()) < _RecoTauLeadTrackMinHits ) {return false;}
		}    
	}    
	if (_DoRecoTauDiscrByH3x3OverP) {
		if (patTau.isCaloTau()) {
			if( (!(patTau.leadTrack().isNonnull())) || ((patTau.hcal3x3OverPLead()) <= _RecoTauH3x3OverP) ) {return false;}
		} else {
			if( (!(patTau.leadPFChargedHadrCand().isNonnull())) || (patTau.hcal3x3OverPLead() <= _RecoTauH3x3OverP) ) {return false;}
		}    
	}    
	if (_DoRecoTauDiscrByCrackCut) { if(isInTheCracks(patTau.eta())) {return false;} }

	return result;
}

bool DitauPATskimmer::passMuonVeto(const pat::Tau& patTau){
	bool result = true;
	if (_DoRecoTauDiscrAgainstMuon) { if ( (patTau.tauID(_RecoTauDiscrAgainstMuon.data()) < 0.5) ) {return false;} }

	return result;
}

bool DitauPATskimmer::isInTheCracks(float eta){
	return (fabs(eta) < 0.018 || 
			(fabs(eta)>0.423 && fabs(eta)<0.461) ||
			(fabs(eta)>0.770 && fabs(eta)<0.806) ||
			(fabs(eta)>1.127 && fabs(eta)<1.163) ||
			(fabs(eta)>1.460 && fabs(eta)<1.558));
}    

