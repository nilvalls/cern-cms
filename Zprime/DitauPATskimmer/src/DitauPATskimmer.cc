// -*- C++ -*-
//
// Package:    DitauPATskimmer
// Class:      DitauPATskimmer
// 
/**\class DitauPATskimmer DitauPATskimmer.cc HighMassAnalysis/DitauPATskimmer/src/DitauPATskimmer.cc

 Description: [one line class summary]

 Implementation:
     [Notes on implementation]
*/
//
// Original Author:  Nil Valls <nil.valls@cern.ch>
//         Created:  Fri Mar 25 14:39:28 CDT 2011
//		   Last mod: Fri Apr 01 

#include "HighMassAnalysis/DitauPATskimmer/interface/DitauPATskimmer.h"

using namespace std;

// Constructor
DitauPATskimmer::DitauPATskimmer(const edm::ParameterSet& iConfig){
	_GenParticleSource = iConfig.getUntrackedParameter<InputTag>("GenParticleSource");
    _RecoTauSource		= iConfig.getParameter<InputTag>("RecoTauSource");
    _RecoMetSource		= iConfig.getParameter<InputTag>("RecoMetSource");
    _RecoTriggerSource	= iConfig.getParameter<InputTag>("RecoTriggerSource");

	// Get generator matching parameters
	_MatchTauToGen = iConfig.getParameter<bool>("MatchTauToGen");
	_UseTauMotherId = iConfig.getParameter<bool>("UseTauMotherId");
	_UseTauGrandMotherId = iConfig.getParameter<bool>("UseTauGrandMotherId");
	_TauMotherId = iConfig.getParameter<int>("TauMotherId");
	_TauGrandMotherId = iConfig.getParameter<int>("TauGrandMotherId");
	_TauToGenMatchingDeltaR = iConfig.getParameter<double>("TauToGenMatchingDeltaR");

	// Get cut parameters
	_RecoTauMinimumPt					= iConfig.getParameter<double>("RecoTauMinimumPt");
	_SelectRecoTau1Prongs				= iConfig.getParameter<bool>("SelectRecoTau1Prongs");
	_SelectRecoTau2Prongs				= iConfig.getParameter<bool>("SelectRecoTau2Prongs");
	_SelectRecoTau3Prongs				= iConfig.getParameter<bool>("SelectRecoTau3Prongs");
	_DitauMinimumDeltaR					= iConfig.getParameter<double>("DitauMinimumDeltaR");
	_DoRecoTauDiscrByLeadTrackNhits 	= iConfig.getParameter<bool>("DoRecoTauDiscrByLeadTrackNhits");
	_RecoTauLeadTrackMinHits 			= iConfig.getParameter<int>("RecoTauLeadTrackMinHits");
	_DoRecoTauDiscrByH3x3OverP 			= iConfig.getParameter<bool>("DoRecoTauDiscrByH3x3OverP");
	_RecoTauH3x3OverP 					= iConfig.getParameter<double>("RecoTauH3x3OverP");
	_DoRecoTauDiscrByCrackCut 			= iConfig.getParameter<bool>("DoRecoTauDiscrByCrackCut");
	_DoRecoTauDiscrAgainstMuon 			= iConfig.getParameter<bool>("DoRecoTauDiscrAgainstMuon");
	_RecoTauDiscrAgainstMuon 			= iConfig.getUntrackedParameter<string>("RecoTauDiscrAgainstMuon");

}


// Destructor
DitauPATskimmer::~DitauPATskimmer(){}

// ------------ method called on each new Event  ------------
bool DitauPATskimmer::filter(edm::Event& iEvent, const edm::EventSetup& iSetup){
	bool result = false;

	Handle< pat::TauCollection > goodPatTaus;

	getCollections(iEvent, iSetup);


	for (pat::TauCollection::const_iterator patTau1 = _patTaus->begin(); patTau1 != _patTaus->end(); ++patTau1 ) {
		if ( !passProngCut(*patTau1)		||
			 !passMinimumPtCut(*patTau1)	|| 
		     !passElectronVeto(*patTau1)	||
			 !passMuonVeto(*patTau1)
		){ continue; }

		for (pat::TauCollection::const_iterator patTau2 = patTau1; patTau2 != _patTaus->end(); ++patTau2 ) {
			if( patTau1 == patTau2 ) continue; // Make sure we don't double-count: only compare pairs in which the tau2 iterator is larger than the tau 1 iterator, else skip combo

			if ( !passProngCut(*patTau2)		||
				 !passMinimumPtCut(*patTau2)	|| 
				 !passElectronVeto(*patTau2)	||
				 !passMuonVeto(*patTau2)
			){ continue; }

			if ( passDeltaRCut(*patTau1,*patTau2)){ result = true; }
		}
	}


	return result;
}

// ------------ method called once each job just before starting event loop  ------------
void DitauPATskimmer::beginJob(){
}

// ------------ method called once each job just after ending the event loop  ------------
void DitauPATskimmer::endJob() {
}

//define this as a plug-in
DEFINE_FWK_MODULE(DitauPATskimmer);
