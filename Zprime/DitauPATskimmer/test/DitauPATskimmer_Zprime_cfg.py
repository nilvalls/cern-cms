import FWCore.ParameterSet.Config as cms
import copy

# Defauls cut values

# Acceptance
MinDeltaR			=   0.7
tauMinPt			=	20.0
MinPTCut = tauMinPt

# ID
RejectMuons			=	1
RejectElectrons		=	1
Consider1Prongs		=	1
Consider2Prongs		=	0
Consider3Prongs		=	0

# Topology

# Override default cut values
#from filename import *


# Parse external arguments
import FWCore.ParameterSet.VarParsing as VarParsing
options = VarParsing.VarParsing("analysis")
options.register("isMC",
       0, # default value
       VarParsing.VarParsing.multiplicity.singleton, # singleton or list
       VarParsing.VarParsing.varType.int,         # string, int, or float
       "is MC or is Data?"
       )   
# get and parse the command line arguments
options.parseArguments()


print '#########################################'
print 'isMC: %d' % options.isMC
print ''
print '### Aceptance' 
print 'MinDeltaR: %f' % MinDeltaR 
print 'tauMinPt: %f' % tauMinPt
print ''
print '### ID' 
print 'RejectMuons: %d' % RejectMuons
print 'RejectElectrons: %d' % RejectElectrons
print 'Consider 1prongs?: %d' % Consider1Prongs
print 'Consider 2prongs?: %d' % Consider2Prongs
print 'Consider 3prongs?: %d' % Consider3Prongs
print ''
print '#########################################'


inputForOutputFile = "Ditau_lightweightPATuple.root"

if( options.isMC == 1 ):
	inputForGenParticles = 'genParticles'
	inputForTriggerCollection = "REDIGI38X"
else:
	inputForGenParticles = ''
	inputForTriggerCollection = "HLT"

signal = True

process = cms.Process('DitauPATskim')

process.load('Configuration.StandardSequences.Services_cff')
process.load('FWCore.MessageService.MessageLogger_cfi')
process.MessageLogger.cerr.FwkReport.reportEvery = 1000
process.options   = cms.untracked.PSet( wantSummary = cms.untracked.bool(True) )

process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32( -1 )
)
process.source = cms.Source("PoolSource",
    skipEvents = cms.untracked.uint32(0),
    fileNames = cms.untracked.vstring(
#		'file:/data/ndpc2/b/nvallsve/HighMassTau/ZeeChecks/ElectronData/skimPat_135_1_TUC.root'
		'dcache:/pnfs/cms/WAX/11/store/user/lpctau/HighMassTau/gurrola/ZPrime500TauTau_Tauola_GenSimRaw/Zprime500386MuTauSkimPat_HcalNoise/95f2b972203a31b8526d00a1c595757b/skimPat_1_1_wqu.root'
    )
)

process.outputResults = cms.OutputModule("PoolOutputModule",
		fileName = cms.untracked.string(inputForOutputFile),
		SelectEvents = cms.untracked.PSet(
			SelectEvents = cms.vstring('theSkim')
		),
		outputCommands = cms.untracked.vstring(
			'keep *_*_*_hiMassTau',
			'drop *_*_*_RECO'
		)
)

process.load("HighMassAnalysis.Skimming.genLevelSequence_cff")
process.load("HighMassAnalysis.Skimming.genDecaysFromZPrimes_cfi")  

process.skimDitauPAT = cms.EDFilter('DitauPATskimmer',

    #-----Generator level Inputs
    GenParticleSource = cms.untracked.InputTag(inputForGenParticles),				# MC gen particle collection

    #-----Reco Tau Inputs
    RecoTauSource = cms.InputTag('selectedLayer1ShrinkingConeHighEffPFTaus'),
									# other choices include:
									#	*	selectedLayer1FixedConeHighEffPFTaus
									#	*	selectedLayer1FixedConePFTaus  
									#	*	selectedLayer1ShrinkingConeHighEffPFTaus
									#	*	selectedLayer1ShrinkingConePFTaus

    RecoMetSource = cms.InputTag('patMETsPF'),					# met collection
																# particle flow met for 2X	= layer1PFMETs
																# particle flow met for 3X	= layer1METsPF
																# standard calo met for 2X & 3X	= layer1METs

	RecoTriggerSource = cms.InputTag("TriggerResults","",inputForTriggerCollection),			# trigger collection

    MatchTauToGen = cms.bool(False),							# if true, match reco tau to a gen had tau
    UseTauMotherId = cms.bool(False),							# if true, require the matched tau to come from a certain
																# 'mother' particle ('mother' here is NOT 15!!!!! Matching
																# for the had tau leg already requires the vis had tau to come
																# from a tau lepton)
    UseTauGrandMotherId = cms.bool(False),						# if true, require the matched tau to come from a certain
																# 'grandmother' particle
    TauMotherId = cms.int32(23),								# pdgId of the 'mother' particle
    TauGrandMotherId = cms.int32(1),							# pdgId of the 'grandmother' particle
    TauToGenMatchingDeltaR = cms.double(0.25),					# matching dR:  dR(vis gen tau,reco tau)<X


    RecoTauMinimumPt = cms.double(MinPTCut),					# require tau pt>=X

    SelectRecoTau1Prongs = cms.bool( (Consider1Prongs != 0) ),	# Consider 1 prongs?		
    SelectRecoTau2Prongs = cms.bool( (Consider2Prongs != 0) ),	# Consider 2 prongs?		
    SelectRecoTau3Prongs = cms.bool( (Consider3Prongs != 0) ),	# Consider 3 prongs?	 			

	DitauMinimumDeltaR = cms.double(MinDeltaR),

    DoRecoTauDiscrByLeadTrackNhits = cms.bool(True),                    # if true, tau leading track is required to have >= X hits
    RecoTauLeadTrackMinHits = cms.int32(12),                        # tau leading track hits >= X
    DoRecoTauDiscrByH3x3OverP = cms.bool( (RejectElectrons != 0) ),     
    RecoTauH3x3OverP = cms.double(0.03),    

    RecoTauDiscrAgainstElectron = cms.untracked.string('againstElectron'),      # name of electron veto discriminator flag
    DoRecoTauDiscrByCrackCut = cms.bool(False),                     # if true, taus that fall on the cracks will not be considered
    DoRecoTauDiscrAgainstMuon = cms.bool( (RejectMuons != 0) ),                 # if true, muon veto will be applied
    RecoTauDiscrAgainstMuon = cms.untracked.string('againstMuon'),          # name of muon veto discriminator flag

)


if(signal):
  process.theSkim = cms.Path(
		  process.genParticlesFromZPrimes *
		  process.genTausFromZPrimes *
		  process.genLevelTauTauSequence *
		  process.skimDitauPAT
  )
else:
  process.theSkim = cms.Path(
		  process.skimDitauPAT
  )

process.theOutput = cms.EndPath(process.outputResults)

# print-out all python configuration parameter information
#print process.dumpPython()

