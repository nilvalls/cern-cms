// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDAnalyzer.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "DataFormats/PatCandidates/interface/Tau.h"
#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/PatCandidates/interface/CompositeCandidate.h"
#include "DataFormats/Math/interface/LorentzVector.h"
#include "DataFormats/Math/interface/LorentzVectorFwd.h"
#include "DataFormats/JetReco/interface/GenJetCollection.h"
#include "DataFormats/JetReco/interface/GenJet.h"
#include "DataFormats/PatCandidates/interface/Isolation.h"
#include "DataFormats/Math/interface/deltaR.h"
#include "DataFormats/Math/interface/normalizedPhi.h"
#include "DataFormats/TauReco/interface/PFTau.h"
#include "DataFormats/Common/interface/RefVector.h"
#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/Candidate/interface/Candidate.h"
#include "DataFormats/Common/interface/Ref.h"
#include "DataFormats/Common/interface/ValueMap.h"
#include "DataFormats/VertexReco/interface/Vertex.h"
#include "DataFormats/VertexReco/interface/VertexFwd.h"
#include "FWCore/Common/interface/TriggerNames.h"
#include "DataFormats/Common/interface/TriggerResults.h"
#include "CLHEP/Random/RandGauss.h"
#include "CommonTools/CandUtils/interface/Booster.h"
#include <Math/VectorUtil.h>
#include "DataFormats/Common/interface/Handle.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "CommonTools/UtilAlgos/interface/TFileService.h"
#include <TH1.h>
#include <TH2.h>
#include <TFile.h>
#include <TTree.h>
#include <string>
#include <vector>
#include <map>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <TRandom3.h>



using namespace std;
using namespace edm;
using namespace reco;

// class declaration


class DitauTriggerEfficiency : public edm::EDAnalyzer {
	public:
		explicit DitauTriggerEfficiency(const edm::ParameterSet&);
		~DitauTriggerEfficiency();


	private:
		virtual void beginJob();
		virtual void initializeVectors();
		virtual void setupBranches();
		virtual void analyze(const edm::Event&, const edm::EventSetup&);	
		virtual void fillNtuple(const edm::Event&);
		virtual void clearVectors();
		virtual void endJob();
		void getCollections(const edm::Event&, const edm::EventSetup&);  
		pair<bool, reco::Candidate::LorentzVector> matchToGen(const pat::Tau& theObject);
		int getNIsoGammas (const pat::Tau&);
		int getNIsoTracks (const pat::Tau&);


		// Collections and their input tags
		InputTag _GenParticleSource;
		InputTag _RecoTauSource;

		Handle< reco::GenParticleCollection >	_genParticles;
		Handle< pat::TauCollection >			_patTaus;
		Handle< reco::VertexCollection >		_primaryEventVertexCollection;


	  	int numTotalEvents;

		//define tree variables

                edm::Service<TFileService> file;
  
 //               TFile* file;

  //              string _outputFile;
		TTree* _TauTauTree;
		
		double                          _runNumber;
		double                          _eventNumber;
		double                          _lumiBlock;

		vector<float>*          _TauGenPt;
		vector<float>*          _TauGenE;
		vector<float>*          _TauGenEta;
		vector<float>*          _TauGenPhi;
		vector<bool>*           _TauMatched;
		vector<int>*            _TauDiMotherId;
		vector<int>*            _TauPdgId;
		vector<float>*          _TauE;
		vector<float>*          _TauEt;
		vector<float>*          _TauPt;
                vector<float>*          _TauEta;
                vector<float>*          _TauPhi;
                vector<int>*            _TauNProngs;
                vector<float>*          _TauNumIsoTracks;
                vector<float>*          _TauNumIsoGammas;
		vector<float>*          _TauLTPt;
		vector<int>*            _TauCharge;
	        int 	                _TotalNEvents;
                vector<float>*          _TauHcalTotOverPLead;
		vector<float>*          _TauHCalMaxOverPLead;
       		vector<float>*          _TauHCal3x3OverPLead;

	
		// ----------member data ---------------------------
		
		reco::Candidate::LorentzVector MChadtau;
		const reco::Candidate * daughterCand;
		const reco::Candidate * motherCand;
		const reco::Candidate * grandMotherCand;

		// Generator matching variables
		bool _MatchTauToGen;
		bool _UseTauMotherId;
		bool _UseTauGrandMotherId;
		int  _TauMotherId;
		int  _TauGrandMotherId;
		double  _TauToGenMatchingDeltaR;

		// Isolation varbiables
		double _RecoTauIsoDeltaRCone;
		double _RecoTauSignalDeltaRCone;
		double _RecoTauTrackIsoTrkThreshold;
		double _RecoTauGammaIsoGamThreshold;
		double _MaxNumIsoTracks;
		double _MaxNumIsoGammas;
		InputTag _RecoVertexSource;


		template <typename PatObject> std::pair<unsigned int, unsigned int> getMatchedPdgId(const PatObject&);

};


template <typename PatObject>
pair<unsigned int, unsigned int> DitauTriggerEfficiency::getMatchedPdgId(const PatObject& patObject){
        pair<unsigned int, unsigned int> theTrackAndMotherPdgId;
        float minDeltaPt = 1000.;
        float minDeltaR = 0.2;
        unsigned int thePdgId = 0;
        unsigned int theMotherPdgId = 0;

        for(reco::GenParticleCollection::const_iterator genParticle = _genParticles->begin();genParticle != _genParticles->end();++genParticle){
                if(patObject.charge() != genParticle->charge() || genParticle->status() != 1)continue;  // match only to final states...
                if(reco::deltaR(patObject.eta(), patObject.phi(), genParticle->eta(), genParticle->phi()) > minDeltaR) continue ;
                float theDeltaPt = fabs(patObject.pt() - genParticle->pt());
                if(theDeltaPt < minDeltaPt){
                        minDeltaPt = theDeltaPt;
                        thePdgId = abs(genParticle->pdgId());
                        theMotherPdgId = abs(genParticle->mother()->pdgId());
                }
        }
        theTrackAndMotherPdgId = make_pair<unsigned int, unsigned int>(thePdgId, theMotherPdgId);
        return theTrackAndMotherPdgId;
}


void DitauTriggerEfficiency::getCollections(const Event& iEvent, const EventSetup& iSetup){
	iEvent.getByLabel(_GenParticleSource, _genParticles); 
	iEvent.getByLabel(_RecoTauSource, _patTaus);
	iEvent.getByLabel(_RecoVertexSource, _primaryEventVertexCollection);
}    


// This function compares the patTau passed as an argument with every genParticle object, and tries to find a match
// If found, it is returned in a pair, whose bool part indicates whether it has been matched or not
pair<bool, reco::Candidate::LorentzVector> DitauTriggerEfficiency::matchToGen(const pat::Tau& theObject) {
	bool isGenMatched = false; // Flag indicating whether patTau could be matched to gen info or not

	reco::Candidate::LorentzVector visibleTau;
	const reco::Candidate* daughterCand;
	const reco::Candidate* motherCand;
	const reco::Candidate* grandMotherCand;

	reco::Candidate::LorentzVector theGenObject(0,0,0,0);
	
	// Loop over genParticles
	for( GenParticleCollection::const_iterator genParticle = _genParticles->begin(); genParticle != _genParticles->end(); ++genParticle) {

		// Look for a decayed (status 3) tau (pdgID 15)
		if( (abs(genParticle->pdgId()) == 15) && (genParticle->status() != 3) ) {
			int neutrinos = 0; // Reset neutrino counter 
			visibleTau = genParticle->p4(); // Get the tau for momentum (neutrinos p4 to be subtracted form here)

			// Check to see in which generation the tau's mother is, and get that object as "motherCand" 
			if( genParticle->mother(0)->pdgId() == genParticle->pdgId() ){
				motherCand = genParticle->mother(0)->mother(0);
				// Figure out in which generation the tau's grand mother is, and get that object as "grandMotherCand"
				if(	motherCand->mother(0)->pdgId() == motherCand->pdgId() )	{ grandMotherCand = motherCand->mother(0)->mother(0);	}
				else														{ grandMotherCand = motherCand->mother(0);				}
			} else {
				motherCand = genParticle->mother(0);
				// Figure out in which generation the tau's grand mother is, and get that object as "grandMotherCand"
				if( motherCand->mother(0)->pdgId() == motherCand->pdgId() )	{ grandMotherCand = motherCand->mother(0)->mother(0);	}
				else														{ grandMotherCand = motherCand->mother(0);				}
			}    

			// Loop over all the daughters of the gen tau
			for( int daughterNum = 0; daughterNum < (int)(genParticle->numberOfDaughters()); daughterNum++){
				daughterCand = genParticle->daughter(daughterNum); // Get the daughter of the tau found previously
				// Check to see whether that daughter is a neutrino or antineutrno
				if( (abs(daughterCand->pdgId()) == 12) || (abs(daughterCand->pdgId()) == 14) || (abs(daughterCand->pdgId()) == 16) ) {
					neutrinos++; // If neutrino, count it!
					visibleTau = visibleTau - daughterCand->p4(); // Subtract each neutrino's p4 so in the end we have vis p4 only
				}    
			}    

			if( neutrinos == 1 ){ // Stick with genTaus with one neutrino only (or lepton number is violated) 

				float recoDeltaR = reco::deltaR(visibleTau.eta(), visibleTau.phi(), theObject.eta(), theObject.phi()); // Compute the DeltaR between gen and reco object

				if( recoDeltaR < _TauToGenMatchingDeltaR ){ // Check whether DeltaR is within limits
					if( _UseTauMotherId ){ // Check matching with tau's mother? I.e., W, Z...?

						if( abs(motherCand->pdgId()) == _TauMotherId ){ // ...if so, does it match?
							if (_UseTauGrandMotherId){ // Check matching with tau's grand mother?
								if(abs(grandMotherCand->pdgId()) == _TauGrandMotherId ){ isGenMatched = true; theGenObject = visibleTau; }
							}else{
							isGenMatched = true; theGenObject = visibleTau;
							}    
						}    

					}else{ isGenMatched = true; theGenObject = visibleTau; }
				}    

			} // fi of neutrinos == 1  
		} // fi of tau selection    
	} // rof of genParticles 


	pair<bool, reco::Candidate::LorentzVector> result(isGenMatched, theGenObject); // Make the pair to be returned
	return result;
}    



// This function returns the number of tracks inside the isolation cone
int DitauTriggerEfficiency::getNIsoTracks(const pat::Tau& patTau){
	float result = -1;  

	int nIsoTrks = 0;
	double sumPtIsoTrks = 0;
	const reco::Vertex& thePrimaryEventVertex = (*(_primaryEventVertexCollection)->begin());

	if (patTau.leadPFChargedHadrCand().isNonnull()) { 
		PFCandidateRefVector TauIsolTracks = patTau.isolationPFChargedHadrCands();
		for(PFCandidateRefVector::const_iterator iTrk = TauIsolTracks.begin(); iTrk != TauIsolTracks.end(); ++iTrk) {
        //                                if ((**iTrk).trackRef().isNonnull()){

			if(		   (reco::deltaR((**iTrk).eta(),(**iTrk).phi(),patTau.leadPFChargedHadrCand()->eta(),patTau.leadPFChargedHadrCand()->phi()) < _RecoTauIsoDeltaRCone)
					&& (reco::deltaR((**iTrk).eta(),(**iTrk).phi(),patTau.leadPFChargedHadrCand()->eta(),patTau.leadPFChargedHadrCand()->phi()) > _RecoTauSignalDeltaRCone)
					&& ((**iTrk).pt() > _RecoTauTrackIsoTrkThreshold)
					&& ((**iTrk).trackRef()->numberOfValidHits() > 8) 
					&& ((**iTrk).trackRef().isNonnull()) 
					&& (fabs((**iTrk).trackRef()->dxy(thePrimaryEventVertex.position())) < 0.03) 
					&& (fabs((**iTrk).trackRef()->dz(thePrimaryEventVertex.position())) < 0.2)
			  ){
				nIsoTrks++;
				sumPtIsoTrks = sumPtIsoTrks + (**iTrk).pt();
		//	}
                 }	
            }
	}

	result = nIsoTrks;
	return result;
}

// This function returns the number of photons inside the isolation cone
int DitauTriggerEfficiency::getNIsoGammas(const pat::Tau& patTau){
	float result = -1;
	int nIsoGams = 0;
	double sumPtIsoGams = 0;

	if (patTau.leadPFChargedHadrCand().isNonnull()) { 
		PFCandidateRefVector TauIsolGammas = patTau.isolationPFGammaCands();
		for(PFCandidateRefVector::const_iterator iGam=TauIsolGammas.begin();iGam!=TauIsolGammas.end();++iGam) {
			if(		(reco::deltaR((**iGam).eta(),(**iGam).phi(),patTau.leadPFChargedHadrCand()->eta(),patTau.leadPFChargedHadrCand()->phi()) < _RecoTauIsoDeltaRCone) 
				&&	(reco::deltaR((**iGam).eta(),(**iGam).phi(),patTau.leadPFChargedHadrCand()->eta(),patTau.leadPFChargedHadrCand()->phi()) > _RecoTauSignalDeltaRCone) 
				&& ((**iGam).pt() > _RecoTauGammaIsoGamThreshold)) {
				nIsoGams++;
				sumPtIsoGams = sumPtIsoGams + (**iGam).pt();
			}
		}
	}
		
	result = nIsoGams;
	return result;
}




