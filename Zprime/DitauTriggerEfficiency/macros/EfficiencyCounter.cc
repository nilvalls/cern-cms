
#define EfficiencyCounter_cxx
#include "EfficiencyCounter.h"
#include <TStopwatch.h>
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <stdlib.h>
#include <fstream>
#include <iomanip>

using namespace std;



// Default constructor
EfficiencyCounter::EfficiencyCounter(){
	relEfficiencies.clear();
	cumEfficiencies.clear();
	topologies.clear();
	cuts.clear();
}


EfficiencyCounter::~EfficiencyCounter(){
}

void EfficiencyCounter::AddTopology(string iTopology, vector< pair<string,string>* >* iRelEfficiencies, vector< pair<string,string>* >* iCumEfficiencies){

	topologies.push_back(iTopology);

	map<string,string>* toInsert;
	
	toInsert = new map<string,string>();
	toInsert->clear();
	for (vector<pair<string,string>* >::const_iterator cut = iRelEfficiencies->begin(); cut != iRelEfficiencies->end(); ++cut){
		toInsert->insert(pair<string,string>((*cut)->first, (*cut)->second));

		string cutstr = (*cut)->first;
		std::vector<string>::iterator found = std::find(cuts.begin(), cuts.end(), cutstr);

		if(found != cuts.end()){ continue; }
		else{ cuts.push_back(cutstr); }
	}
	relEfficiencies.push_back(toInsert);


	toInsert = new map<string,string>();
	toInsert->clear();
	for (vector<pair<string,string>* >::const_iterator cut = iCumEfficiencies->begin(); cut != iCumEfficiencies->end(); ++cut){
		toInsert->insert(pair<string,string>((*cut)->first, (*cut)->second));

		string cutstr = (*cut)->first;
		std::vector<string>::iterator found = std::find(cuts.begin(), cuts.end(), cutstr);

		if(found != cuts.end()){ continue; }
		else{ cuts.push_back(cutstr); }
	}
	cumEfficiencies.push_back(toInsert);

}

string EfficiencyCounter::EfficienciesHTML(bool doRel, bool doCum){

	if( !doRel && !doCum ){ return ""; }

	// Set up string stream
	stringstream css; css.str("");

	css << "<html><table border=1 cellspacing=0>";
	css << "<tr><th></th>";
		for (unsigned int topo = 0; topo < topologies.size(); topo++){
			if( doRel && doCum ){ css << "<th colspan=2>" + topologies.at(topo) + "</th>"; }
			else{ css << "<th>" + topologies.at(topo) + "</th>"; }
		}
	css << "</tr>";

	css << "<tr><th>Cut</th>";
		for (unsigned int topo = 0; topo < topologies.size(); topo++){
			if(doRel){ css << "<th>Relative</th>"; }
			if(doCum){ css << "<th>Cummulative</th>"; }
		}
	css << "</tr>";

	for (unsigned int cut = 0; cut < cuts.size(); cut++){

		string cutstr = cuts.at(cut);

		css << "<tr>";
		css << "<td>" + cutstr + "</td>";

		for (unsigned int topo = 0; topo < topologies.size(); topo++){

			if(doRel){ 
				string relEffStr = (*(relEfficiencies.at(topo)))[cutstr];
				css << "<td>" << relEffStr << "</td>";
			}

			if(doCum){ 
				string cumEffStr = (*(cumEfficiencies.at(topo)))[cutstr];
				css << "<td>" << cumEffStr << "</td>";
			}
		}

		css << "</tr>";
	}

	css << "</table>";
	css << "<style type=\"text/css\">td{text-align: center; } </style>";
	css << "</html>";

	string cssString = css.str();
	size_t pos = 0;
	string oldStr = "+/-";
	string newStr = "&plusmn";
	while((pos = cssString.find(oldStr, pos)) != std::string::npos){
		cssString.replace(pos, oldStr.length(), newStr);
		pos += newStr.length();
	}

	return cssString;

}

string EfficiencyCounter::EfficienciesTex(bool doRel, bool doCum){

	if( !doRel && !doCum ){ return ""; }

	// Set up string stream
	stringstream css; css.str("");

	css << "<pre>";
	css << "\\begin{table}\\begin{tabular}{l";

		for (unsigned int topo = 0; topo < topologies.size(); topo++){
			css << "cc";
		}
		css << "}" << endl;

		for (unsigned int topo = 0; topo < topologies.size(); topo++){
		}

	for (unsigned int cut = 0; cut < cuts.size(); cut++){

		string cutstr = cuts.at(cut);

		for (unsigned int topo = 0; topo < topologies.size(); topo++){
			string relEffStr = (*(relEfficiencies.at(topo)))[cutstr];
			string cumEffStr = (*(cumEfficiencies.at(topo)))[cutstr];
		}

	}

	css << "\\end{tabular}\\end{table}";
	css << "</pre>";

	string cssString = css.str();
	size_t pos = 0;
	string oldStr = "+/-";
	string newStr = "$\\pm$";
	while((pos = cssString.find(oldStr, pos)) != std::string::npos){
		cssString.replace(pos, oldStr.length(), newStr);
		pos += newStr.length();
	}


	return cssString;

}

string EfficiencyCounter::EfficienciesTwiki(bool doRel, bool doCum){

	if( !doRel && !doCum ){ return ""; }

	// Set up string stream
	stringstream css; css.str("");

	css << "<pre>";
	css << "|**";

	for (unsigned int topo = 0; topo < topologies.size(); topo++){
		if( doRel && doCum ){ css << "|*" << topologies.at(topo) << "*|"; }
		else{ css << "|*" << topologies.at(topo) << "*"; }
	}
	css << "|" << endl;

	css << "|*Requirement*";

	for (unsigned int topo = 0; topo < topologies.size(); topo++){
		if(doRel){ css << "|*Relative*"; }
		if(doCum){ css << "|*Cummulative*"; }
	}
	css << "|" << endl;

	for (unsigned int cut = 0; cut < cuts.size(); cut++){

		string cutstr = cuts.at(cut);

		css << "|" << cutstr;

		for (unsigned int topo = 0; topo < topologies.size(); topo++){

			if(doRel){ 
				string relEffStr = (*(relEfficiencies.at(topo)))[cutstr];
				css << "|" << relEffStr;
			}

			if(doCum){
				string cumEffStr = (*(cumEfficiencies.at(topo)))[cutstr];
				css << "|" << cumEffStr;
			}
		}
		css << "|" << endl;

	}

	string cssString = css.str();
	size_t pos = 0;
	string oldStr = "+/-";
	string newStr = "<literal>&plusmn</literal>";
	while((pos = cssString.find(oldStr, pos)) != std::string::npos){
		cssString.replace(pos, oldStr.length(), newStr);
		pos += newStr.length();
	}

	css << "</pre>";

	return cssString;

}

void EfficiencyCounter::PrintEfficiencies(string iOutputFile){
	
	stringstream css; css.str("");

	css << EfficienciesHTML(true,true) << "<br><br><hr>";
	//css << EfficienciesHTML(true,false) << "<br><br><hr>";
	//css << EfficienciesHTML(false,true) << "<br><br><hr>";
	css << EfficienciesTex(true,true) << "<br><br><hr>";
	//css << EfficienciesTex(true,false) << "<br><br><hr>";
	//css << EfficienciesTex(false,true) << "<br><br><hr>";
	css << EfficienciesTwiki(true,true) << "<br><br><hr>";
	//css << EfficienciesTwiki(true,false) << "<br><br><hr>";
	//css << EfficienciesTwiki(false,true) << "<br><br><hr>";


	// Write to file
	if (iOutputFile.length() == 0){
		cout << css.str() << endl;
	}else{
		ofstream fout(iOutputFile.c_str());
		if (fout.is_open()){
			fout << css.str() << endl;
			fout.close();
		}else{ cout << ">>> ERROR: Unable to open file" << endl; exit(1); }
	}

}


