
#ifndef EfficiencyCounter_h
#define EfficiencyCounter_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TGraphAsymmErrors.h>
#include <math.h>
#include <map>
#include <vector>
#include <sstream>
#include <iostream>
#include <utility>
#include <algorithm>


using namespace std;

class EfficiencyCounter {
	public :
		vector< map<string,string>* > relEfficiencies;
		vector< map<string,string>* > cumEfficiencies;
		vector<string> topologies;
		vector<string> cuts;

		// Default constructor
		EfficiencyCounter();
		virtual ~EfficiencyCounter();
		virtual void AddTopology(string, vector< pair<string,string>* >*, vector< pair<string,string>* >* );
		virtual void PrintEfficiencies(string iOutputFile="");
		virtual string EfficienciesHTML(bool,bool);
		virtual string EfficienciesTex(bool,bool);
		virtual string EfficienciesTwiki(bool,bool);


};

#endif

#ifdef EfficiencyCounter_cxx



#endif
