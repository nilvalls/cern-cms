//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu May 19 06:10:55 2011 by ROOT version 5.27/06b
// from TTree HMTTree/TauTauTree
// found on file: ditau_nTuple.root
//////////////////////////////////////////////////////////

#ifndef TauTauPlotter_h
#define TauTauPlotter_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TGraphAsymmErrors.h>
#include <math.h>
#include <map>
#include <vector>
#include <sstream>
#include <iostream>
#include <utility>

using namespace std;

class TauTauPlotter{
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Double_t        runNumber;
   Double_t        eventNumber;
   Double_t        lumiBlock;
   vector<float>   *TauTriggerEfficiency;
   vector<float>   *Tau1GenPt;
   vector<float>   *Tau2GenPt;
   vector<float>   *Tau1GenE;
   vector<float>   *Tau2GenE;
   vector<float>   *Tau1GenEta;
   vector<float>   *Tau2GenEta;
   vector<float>   *Tau1GenPhi;
   vector<float>   *Tau2GenPhi;
   vector<bool>    *Tau1Matched;
   vector<bool>    *Tau2Matched;
   vector<int>     *Tau1MotherId;
   vector<int>     *Tau2MotherId;
   vector<int>     *Tau1PdgId;
   vector<int>     *Tau2PdgId;
   vector<int>     *Tau1MotherPdgId;
   vector<int>     *Tau2MotherPdgId;
   vector<float>   *Tau1E;
   vector<float>   *Tau2E;
   vector<float>   *Tau1Et;
   vector<float>   *Tau2Et;
   vector<float>   *Tau1Pt;
   vector<float>   *Tau2Pt;
   vector<float>   *Tau1LTPt;
   vector<float>   *Tau2LTPt;
   vector<int>     *Tau1Charge;
   vector<int>     *Tau2Charge;
   vector<float>   *Tau1Eta;
   vector<float>   *Tau2Eta;
   vector<float>   *Tau1Phi;
   vector<float>   *Tau2Phi;
   vector<float>   *Tau1LTIpVtx;
   vector<float>   *Tau1LTIpVtxError;
   vector<int>     *Tau1LTValidHits;
   vector<float>   *Tau1LTNormChiSqrd;
   vector<float>   *Tau2LTIpVtx;
   vector<float>   *Tau2LTIpVtxError;
   vector<int>     *Tau2LTValidHits;
   vector<float>   *Tau2LTNormChiSqrd;
   vector<int>     *Tau1NProngs;
   vector<int>     *Tau2NProngs;
   vector<float>   *Tau1EmFraction;
   vector<float>   *Tau2EmFraction;
   vector<float>   *Tau1HcalTotOverPLead;
   vector<float>   *Tau2HcalTotOverPLead;
   vector<float>   *Tau1HCalMaxOverPLead;
   vector<float>   *Tau2HCalMaxOverPLead;
   vector<float>   *Tau1HCal3x3OverPLead;
   vector<float>   *Tau2HCal3x3OverPLead;
   vector<float>   *Tau1DiscAgainstElectron;
   vector<float>   *Tau2DiscAgainstElectron;
   vector<int>     *Tau1IsInTheCracks;
   vector<int>     *Tau2IsInTheCracks;
   vector<float>   *Tau1SumPtIsoTracks;
   vector<float>   *Tau2SumPtIsoTracks;
   vector<float>   *Tau1SumPtIsoGammas;
   vector<float>   *Tau2SumPtIsoGammas;
   vector<float>   *mEt;
   vector<float>   *TauTauVisibleMass;
   vector<float>   *TauTauVisPlusMetMass;
   vector<float>   *TauTauCollinearMetMass;
   vector<float>   *TauTauCosDPhi;
   vector<float>   *TauTauDeltaR;
   vector<float>   *TauTauPZeta;
   vector<float>   *TauTauPZetaVis;
   vector<float>   *Tau1MetCosDphi;
   vector<float>   *Tau2MetCosDphi;
   vector<float>   *nBtagsHiEffTrkCnt;
   vector<float>   *nBtagsHiPurityTrkCnt;
   vector<float>   *nBTagsHiEffSimpleSecVtx;
   vector<float>   *nBTagsHiPuritySimpleSecVtx;
   vector<float>   *nBTagsCombSecVtx;
   vector<float>   *jetSumEt;
   vector<float>   *jetMETSumEt;
   vector<int>     *nJets;
   vector<float>   *nExtraJets;

   // List of branches
   TBranch        *b_runNumber;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_lumiBlock;   //!
   TBranch        *b_TauTriggerEfficiency;   //!
   TBranch        *b_Tau1GenPt;   //!
   TBranch        *b_Tau2GenPt;   //!
   TBranch        *b_Tau1GenE;   //!
   TBranch        *b_Tau2GenE;   //!
   TBranch        *b_Tau1GenEta;   //!
   TBranch        *b_Tau2GenEta;   //!
   TBranch        *b_Tau1GenPhi;   //!
   TBranch        *b_Tau2GenPhi;   //!
   TBranch        *b_Tau1Matched;   //!
   TBranch        *b_Tau2Matched;   //!
   TBranch        *b_Tau1MotherId;   //!
   TBranch        *b_Tau2MotherId;   //!
   TBranch        *b_Tau1PdgId;   //!
   TBranch        *b_Tau2PdgId;   //!
   TBranch        *b_Tau1MotherPdgId;   //!
   TBranch        *b_Tau2MotherPdgId;   //!
   TBranch        *b_Tau1E;   //!
   TBranch        *b_Tau2E;   //!
   TBranch        *b_Tau1Et;   //!
   TBranch        *b_Tau2Et;   //!
   TBranch        *b_Tau1Pt;   //!
   TBranch        *b_Tau2Pt;   //!
   TBranch        *b_Tau1LTPt;   //!
   TBranch        *b_Tau2LTPt;   //!
   TBranch        *b_Tau1Charge;   //!
   TBranch        *b_Tau2Charge;   //!
   TBranch        *b_Tau1Eta;   //!
   TBranch        *b_Tau2Eta;   //!
   TBranch        *b_Tau1Phi;   //!
   TBranch        *b_Tau2Phi;   //!
   TBranch        *b_Tau1LTIpVtx;   //!
   TBranch        *b_Tau1LTIpVtxError;   //!
   TBranch        *b_Tau1LTValidHits;   //!
   TBranch        *b_Tau1LTNormChiSqrd;   //!
   TBranch        *b_Tau2LTIpVtx;   //!
   TBranch        *b_Tau2LTIpVtxError;   //!
   TBranch        *b_Tau2LTValidHits;   //!
   TBranch        *b_Tau2LTNormChiSqrd;   //!
   TBranch        *b_Tau1NProngs;   //!
   TBranch        *b_Tau2NProngs;   //!
   TBranch        *b_Tau1EmFraction;   //!
   TBranch        *b_Tau2EmFraction;   //!
   TBranch        *b_Tau1HcalTotOverPLead;   //!
   TBranch        *b_Tau2HcalTotOverPLead;   //!
   TBranch        *b_Tau1HCalMaxOverPLead;   //!
   TBranch        *b_Tau2HCalMaxOverPLead;   //!
   TBranch        *b_Tau1HCal3x3OverPLead;   //!
   TBranch        *b_Tau2HCal3x3OverPLead;   //!
   TBranch        *b_Tau1DiscAgainstElectron;   //!
   TBranch        *b_Tau2DiscAgainstElectron;   //!
   TBranch        *b_Tau1IsInTheCracks;   //!
   TBranch        *b_Tau2IsInTheCracks;   //!
   TBranch        *b_Tau1SumPtIsoTracks;   //!
   TBranch        *b_Tau2SumPtIsoTracks;   //!
   TBranch        *b_Tau1SumPtIsoGammas;   //!
   TBranch        *b_Tau2SumPtIsoGammas;   //!
   TBranch        *b_mEt;   //!
   TBranch        *b_TauTauVisibleMass;   //!
   TBranch        *b_TauTauVisPlusMetMass;   //!
   TBranch        *b_TauTauCollinearMetMass;   //!
   TBranch        *b_TauTauCosDPhi;   //!
   TBranch        *b_TauTauDeltaR;   //!
   TBranch        *b_TauTauPZeta;   //!
   TBranch        *b_TauTauPZetaVis;   //!
   TBranch        *b_Tau1MetCosDphi;   //!
   TBranch        *b_Tau2MetCosDphi;   //!
   TBranch        *b_nBtagsHiEffTrkCnt;   //!
   TBranch        *b_nBtagsHiPurityTrkCnt;   //!
   TBranch        *b_nBTagsHiEffSimpleSecVtx;   //!
   TBranch        *b_nBTagsHiPuritySimpleSecVtx;   //!
   TBranch        *b_nBTagsCombSecVtx;   //!
   TBranch        *b_jetSumEt;   //!
   TBranch        *b_jetMETSumEt;   //!
   TBranch        *b_nJets;   //!
   TBranch        *b_nExtraJets;   //!

   TauTauPlotter(TTree *tree=0);
   virtual ~TauTauPlotter();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef TauTauPlotter_cxx
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
TauTauPlotter::TauTauPlotter(TTree *tree){
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("ditau_nTuple.root");
      if (!f) {
         f = new TFile("../test/ditau_nTuple.root");
      }
      tree = (TTree*)gDirectory->Get("HMTTree");

   }
   Init(tree);
}

TauTauPlotter::~TauTauPlotter(){
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

// Read contents of entry.
Int_t TauTauPlotter::GetEntry(Long64_t entry){
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}

// Set the environment to read one entry
Long64_t TauTauPlotter::LoadTree(Long64_t entry){
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (!fChain->InheritsFrom(TChain::Class()))  return centry;
   TChain *chain = (TChain*)fChain;
   if (chain->GetTreeNumber() != fCurrent) {
      fCurrent = chain->GetTreeNumber();
      Notify();
   }
   return centry;
}

// The Init() function is called when the selector needs to initialize
// a new tree or chain. Typically here the branch addresses and branch
// pointers of the tree will be set.
// It is normally not necessary to make changes to the generated
// code, but the routine can be extended by the user if needed.
// Init() will be called many times when running on PROOF
// (once per file to be processed).
void TauTauPlotter::Init(TTree *tree){

   // Set object pointer
   TauTriggerEfficiency = 0;
   Tau1GenPt = 0;
   Tau2GenPt = 0;
   Tau1GenE = 0;
   Tau2GenE = 0;
   Tau1GenEta = 0;
   Tau2GenEta = 0;
   Tau1GenPhi = 0;
   Tau2GenPhi = 0;
   Tau1Matched = 0;
   Tau2Matched = 0;
   Tau1MotherId = 0;
   Tau2MotherId = 0;
   Tau1PdgId = 0;
   Tau2PdgId = 0;
   Tau1MotherPdgId = 0;
   Tau2MotherPdgId = 0;
   Tau1E = 0;
   Tau2E = 0;
   Tau1Et = 0;
   Tau2Et = 0;
   Tau1Pt = 0;
   Tau2Pt = 0;
   Tau1LTPt = 0;
   Tau2LTPt = 0;
   Tau1Charge = 0;
   Tau2Charge = 0;
   Tau1Eta = 0;
   Tau2Eta = 0;
   Tau1Phi = 0;
   Tau2Phi = 0;
   Tau1LTIpVtx = 0;
   Tau1LTIpVtxError = 0;
   Tau1LTValidHits = 0;
   Tau1LTNormChiSqrd = 0;
   Tau2LTIpVtx = 0;
   Tau2LTIpVtxError = 0;
   Tau2LTValidHits = 0;
   Tau2LTNormChiSqrd = 0;
   Tau1NProngs = 0;
   Tau2NProngs = 0;
   Tau1EmFraction = 0;
   Tau2EmFraction = 0;
   Tau1HcalTotOverPLead = 0;
   Tau2HcalTotOverPLead = 0;
   Tau1HCalMaxOverPLead = 0;
   Tau2HCalMaxOverPLead = 0;
   Tau1HCal3x3OverPLead = 0;
   Tau2HCal3x3OverPLead = 0;
   Tau1DiscAgainstElectron = 0;
   Tau2DiscAgainstElectron = 0;
   Tau1IsInTheCracks = 0;
   Tau2IsInTheCracks = 0;
   Tau1SumPtIsoTracks = 0;
   Tau2SumPtIsoTracks = 0;
   Tau1SumPtIsoGammas = 0;
   Tau2SumPtIsoGammas = 0;
   mEt = 0;
   TauTauVisibleMass = 0;
   TauTauVisPlusMetMass = 0;
   TauTauCollinearMetMass = 0;
   TauTauCosDPhi = 0;
   TauTauDeltaR = 0;
   TauTauPZeta = 0;
   TauTauPZetaVis = 0;
   Tau1MetCosDphi = 0;
   Tau2MetCosDphi = 0;
   nBtagsHiEffTrkCnt = 0;
   nBtagsHiPurityTrkCnt = 0;
   nBTagsHiEffSimpleSecVtx = 0;
   nBTagsHiPuritySimpleSecVtx = 0;
   nBTagsCombSecVtx = 0;
   jetSumEt = 0;
   jetMETSumEt = 0;
   nJets = 0;
   nExtraJets = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("lumiBlock", &lumiBlock, &b_lumiBlock);
   fChain->SetBranchAddress("TauTriggerEfficiency", &TauTriggerEfficiency, &b_TauTriggerEfficiency);
   fChain->SetBranchAddress("Tau1GenPt", &Tau1GenPt, &b_Tau1GenPt);
   fChain->SetBranchAddress("Tau2GenPt", &Tau2GenPt, &b_Tau2GenPt);
   fChain->SetBranchAddress("Tau1GenE", &Tau1GenE, &b_Tau1GenE);
   fChain->SetBranchAddress("Tau2GenE", &Tau2GenE, &b_Tau2GenE);
   fChain->SetBranchAddress("Tau1GenEta", &Tau1GenEta, &b_Tau1GenEta);
   fChain->SetBranchAddress("Tau2GenEta", &Tau2GenEta, &b_Tau2GenEta);
   fChain->SetBranchAddress("Tau1GenPhi", &Tau1GenPhi, &b_Tau1GenPhi);
   fChain->SetBranchAddress("Tau2GenPhi", &Tau2GenPhi, &b_Tau2GenPhi);
   fChain->SetBranchAddress("Tau1Matched", &Tau1Matched, &b_Tau1Matched);
   fChain->SetBranchAddress("Tau2Matched", &Tau2Matched, &b_Tau2Matched);
   fChain->SetBranchAddress("Tau1MotherId", &Tau1MotherId, &b_Tau1MotherId);
   fChain->SetBranchAddress("Tau2MotherId", &Tau2MotherId, &b_Tau2MotherId);
   fChain->SetBranchAddress("Tau1PdgId", &Tau1PdgId, &b_Tau1PdgId);
   fChain->SetBranchAddress("Tau2PdgId", &Tau2PdgId, &b_Tau2PdgId);
   fChain->SetBranchAddress("Tau1MotherPdgId", &Tau1MotherPdgId, &b_Tau1MotherPdgId);
   fChain->SetBranchAddress("Tau2MotherPdgId", &Tau2MotherPdgId, &b_Tau2MotherPdgId);
   fChain->SetBranchAddress("Tau1E", &Tau1E, &b_Tau1E);
   fChain->SetBranchAddress("Tau2E", &Tau2E, &b_Tau2E);
   fChain->SetBranchAddress("Tau1Et", &Tau1Et, &b_Tau1Et);
   fChain->SetBranchAddress("Tau2Et", &Tau2Et, &b_Tau2Et);
   fChain->SetBranchAddress("Tau1Pt", &Tau1Pt, &b_Tau1Pt);
   fChain->SetBranchAddress("Tau2Pt", &Tau2Pt, &b_Tau2Pt);
   fChain->SetBranchAddress("Tau1LTPt", &Tau1LTPt, &b_Tau1LTPt);
   fChain->SetBranchAddress("Tau2LTPt", &Tau2LTPt, &b_Tau2LTPt);
   fChain->SetBranchAddress("Tau1Charge", &Tau1Charge, &b_Tau1Charge);
   fChain->SetBranchAddress("Tau2Charge", &Tau2Charge, &b_Tau2Charge);
   fChain->SetBranchAddress("Tau1Eta", &Tau1Eta, &b_Tau1Eta);
   fChain->SetBranchAddress("Tau2Eta", &Tau2Eta, &b_Tau2Eta);
   fChain->SetBranchAddress("Tau1Phi", &Tau1Phi, &b_Tau1Phi);
   fChain->SetBranchAddress("Tau2Phi", &Tau2Phi, &b_Tau2Phi);
   fChain->SetBranchAddress("Tau1LTIpVtx", &Tau1LTIpVtx, &b_Tau1LTIpVtx);
   fChain->SetBranchAddress("Tau1LTIpVtxError", &Tau1LTIpVtxError, &b_Tau1LTIpVtxError);
   fChain->SetBranchAddress("Tau1LTValidHits", &Tau1LTValidHits, &b_Tau1LTValidHits);
   fChain->SetBranchAddress("Tau1LTNormChiSqrd", &Tau1LTNormChiSqrd, &b_Tau1LTNormChiSqrd);
   fChain->SetBranchAddress("Tau2LTIpVtx", &Tau2LTIpVtx, &b_Tau2LTIpVtx);
   fChain->SetBranchAddress("Tau2LTIpVtxError", &Tau2LTIpVtxError, &b_Tau2LTIpVtxError);
   fChain->SetBranchAddress("Tau2LTValidHits", &Tau2LTValidHits, &b_Tau2LTValidHits);
   fChain->SetBranchAddress("Tau2LTNormChiSqrd", &Tau2LTNormChiSqrd, &b_Tau2LTNormChiSqrd);
   fChain->SetBranchAddress("Tau1NProngs", &Tau1NProngs, &b_Tau1NProngs);
   fChain->SetBranchAddress("Tau2NProngs", &Tau2NProngs, &b_Tau2NProngs);
   fChain->SetBranchAddress("Tau1EmFraction", &Tau1EmFraction, &b_Tau1EmFraction);
   fChain->SetBranchAddress("Tau2EmFraction", &Tau2EmFraction, &b_Tau2EmFraction);
   fChain->SetBranchAddress("Tau1HcalTotOverPLead", &Tau1HcalTotOverPLead, &b_Tau1HcalTotOverPLead);
   fChain->SetBranchAddress("Tau2HcalTotOverPLead", &Tau2HcalTotOverPLead, &b_Tau2HcalTotOverPLead);
   fChain->SetBranchAddress("Tau1HCalMaxOverPLead", &Tau1HCalMaxOverPLead, &b_Tau1HCalMaxOverPLead);
   fChain->SetBranchAddress("Tau2HCalMaxOverPLead", &Tau2HCalMaxOverPLead, &b_Tau2HCalMaxOverPLead);
   fChain->SetBranchAddress("Tau1HCal3x3OverPLead", &Tau1HCal3x3OverPLead, &b_Tau1HCal3x3OverPLead);
   fChain->SetBranchAddress("Tau2HCal3x3OverPLead", &Tau2HCal3x3OverPLead, &b_Tau2HCal3x3OverPLead);
   fChain->SetBranchAddress("Tau1DiscAgainstElectron", &Tau1DiscAgainstElectron, &b_Tau1DiscAgainstElectron);
   fChain->SetBranchAddress("Tau2DiscAgainstElectron", &Tau2DiscAgainstElectron, &b_Tau2DiscAgainstElectron);
   fChain->SetBranchAddress("Tau1IsInTheCracks", &Tau1IsInTheCracks, &b_Tau1IsInTheCracks);
   fChain->SetBranchAddress("Tau2IsInTheCracks", &Tau2IsInTheCracks, &b_Tau2IsInTheCracks);
   fChain->SetBranchAddress("Tau1SumPtIsoTracks", &Tau1SumPtIsoTracks, &b_Tau1SumPtIsoTracks);
   fChain->SetBranchAddress("Tau2SumPtIsoTracks", &Tau2SumPtIsoTracks, &b_Tau2SumPtIsoTracks);
   fChain->SetBranchAddress("Tau1SumPtIsoGammas", &Tau1SumPtIsoGammas, &b_Tau1SumPtIsoGammas);
   fChain->SetBranchAddress("Tau2SumPtIsoGammas", &Tau2SumPtIsoGammas, &b_Tau2SumPtIsoGammas);
   fChain->SetBranchAddress("mEt", &mEt, &b_mEt);
   fChain->SetBranchAddress("TauTauVisibleMass", &TauTauVisibleMass, &b_TauTauVisibleMass);
   fChain->SetBranchAddress("TauTauVisPlusMetMass", &TauTauVisPlusMetMass, &b_TauTauVisPlusMetMass);
   fChain->SetBranchAddress("TauTauCollinearMetMass", &TauTauCollinearMetMass, &b_TauTauCollinearMetMass);
   fChain->SetBranchAddress("TauTauCosDPhi", &TauTauCosDPhi, &b_TauTauCosDPhi);
   fChain->SetBranchAddress("TauTauDeltaR", &TauTauDeltaR, &b_TauTauDeltaR);
   fChain->SetBranchAddress("TauTauPZeta", &TauTauPZeta, &b_TauTauPZeta);
   fChain->SetBranchAddress("TauTauPZetaVis", &TauTauPZetaVis, &b_TauTauPZetaVis);
   fChain->SetBranchAddress("Tau1MetCosDphi", &Tau1MetCosDphi, &b_Tau1MetCosDphi);
   fChain->SetBranchAddress("Tau2MetCosDphi", &Tau2MetCosDphi, &b_Tau2MetCosDphi);
   fChain->SetBranchAddress("nBtagsHiEffTrkCnt", &nBtagsHiEffTrkCnt, &b_nBtagsHiEffTrkCnt);
   fChain->SetBranchAddress("nBtagsHiPurityTrkCnt", &nBtagsHiPurityTrkCnt, &b_nBtagsHiPurityTrkCnt);
   fChain->SetBranchAddress("nBTagsHiEffSimpleSecVtx", &nBTagsHiEffSimpleSecVtx, &b_nBTagsHiEffSimpleSecVtx);
   fChain->SetBranchAddress("nBTagsHiPuritySimpleSecVtx", &nBTagsHiPuritySimpleSecVtx, &b_nBTagsHiPuritySimpleSecVtx);
   fChain->SetBranchAddress("nBTagsCombSecVtx", &nBTagsCombSecVtx, &b_nBTagsCombSecVtx);
   fChain->SetBranchAddress("jetSumEt", &jetSumEt, &b_jetSumEt);
   fChain->SetBranchAddress("jetMETSumEt", &jetMETSumEt, &b_jetMETSumEt);
   fChain->SetBranchAddress("nJets", &nJets, &b_nJets);
   fChain->SetBranchAddress("nExtraJets", &nExtraJets, &b_nExtraJets);
   Notify();
}

// The Notify() function is called when a new file is opened. This
// can be either for a new TTree in a TChain or when when a new TTree
// is started when using PROOF. It is normally not necessary to make changes
// to the generated code, but the routine can be extended by the
// user if needed. The return value is currently not used.
Bool_t TauTauPlotter::Notify(){
   return kTRUE;
}

// Print contents of entry.
// If entry is not specified, print current entry
void TauTauPlotter::Show(Long64_t entry){
   if (!fChain) return;
   fChain->Show(entry);
}

// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
Int_t TauTauPlotter::Cut(Long64_t entry){
   return 1;
}
#endif // #ifdef TauTauPlotter_cxx
