/*
    Author:			Nil Valls <nil.valls@cern.ch>
    Date:			07 Jun 2011
    Description:	Plotting macro for ditau analysis.
    Notes:
	    For config file parser check
	    http://www.codeproject.com/KB/files/config-file-parser.aspx

*/

#define TriggerEfficiency_cxx
#include "TriggerEfficiency.h"
#include <TStopwatch.h>
#include <TSystem.h>
#include <TH2.h>
#include <TStyle.h>
#include <TSystem.h>
#include <TCanvas.h>
#include <stdlib.h>
#include <fstream>
#include <iomanip>

using namespace std;


// Default constructor
TriggerEfficiency::TriggerEfficiency(int iMaxEvents){
	NumEvAnalyzed	= 0;

	SetMaxEvents(iMaxEvents);
	messages = true;
	messages = false;
	reportRate = 100;
	topology = "";

	CutOnGenTauPt			= false;
	CutOnGenTauEta			= false;
     //   DoMatching 			= false;
        CutOnRecoTauEta                 = false;
        CutOnRecoTauPt                  = false;
        CutOnRecoTauLTPt		= false;
	CutOnTrackIso		        = false;
	CutOnGammaIso	         	= false;
  


}


// Default destructor
TriggerEfficiency::~TriggerEfficiency(){
	if (!fChain) return;
	delete fChain->GetCurrentFile();
}

void TriggerEfficiency::SetReportRate(int iReportRate){
	reportRate = iReportRate;
}

void TriggerEfficiency::SetTopology(string iTopology){
	topology = iTopology;
}

void TriggerEfficiency::SetPassingEventsOutput(string iFile){
	passingEventsFile = iFile;
	fevents.open(passingEventsFile.c_str());

	fevents << "</html>";
	fevents << "<style type=\"text/css\">td{text-align: center; } </style>";
	fevents << "<html><table border=1 cellspacing=0><tr>";
	fevents << "<th>Topology</th>"
	                "<th>Run</th>"
			"<th>LS</th>"
			"<th>Event</th>"
			"<th>TauGenPt</th>"
		        "<th>TauGenEta</th>"
			"<th>TauPt</th>"
			"<th>TauEta</th>"
        		"<th>TauLTPt</th>"
        		"<th>TauNumIsoTracks</th>"
        		"<th>TauNumIsoGammas</th>"
			<<endl;
       
	fevents << "</tr>";
	fevents.close();
}


void TriggerEfficiency::Loop(){
	cout << ">>> Starting loop... "; cout.flush();
	if (fChain == 0){ cout << endl << "ERROR: empty TChain. Exiting."; return; }

	// Set up passing events output file
	fevents.open(passingEventsFile.c_str(), fstream::app);
	if (!fevents.is_open()){ cout << ">>> ERROR: Unable to open file" << endl; exit(1); }

	Long64_t nentries = fChain->GetEntries();
	cout << " " << nentries << " entries available: ";
	if(maxEvents <= 0 || maxEvents >= nentries){ cout << "Processing all of them..." << string(14,'.') << " "; }
	else{ cout << "Stopping at " << maxEvents << " as per-user request" << string(14,'.') << " "; }
	cout.flush();
	

	ResetCounters();
	Long64_t nbytes = 0, nb = 0;

	NumEvAnalyzed=0;

	for (Long64_t jentry=0; jentry<nentries; jentry++) {
	     Long64_t ientry = LoadTree(jentry);
		if (ientry < 0) break;
		nb = fChain->GetEntry(jentry);   nbytes += nb;
	  NumEvAnalyzed++;	
	if (Cut(ientry) < 0) continue;

           		
          	// Loop over the Gen Taus in each event and count how many pass the eta and pt cut  

                bool PassesAllGenSelections = false;
  
                for (unsigned int i = 0; i < TauGenPt->size(); i++){

                PassesAllGenSelections = PassesGenCuts(i);

		}
                 
           	// Loop over the tau in each event and count how many pass the eta and pt cut  
                bool PassesAllRecoSelections = false;

         	for (unsigned int i = 0; i < TauPt->size(); i++){
		  
 		PassesAllRecoSelections = PassesRecoCuts(i);
                
		 }                


                // Increase counters for those events with at least one pair passing the individual cuts
		for(map<string, int>::iterator cut = numberOfGenPassing.begin(); cut != numberOfGenPassing.end(); ++cut){
			string cutstring = (*cut).first;
			int GenPassing = (*cut).second;
			if (GenPassing >= 2){ numberOfEventsGenPassing[cutstring]++; }
			(*cut).second = 0;
		}
           
 		// Increase counters for those events with at least one pair passing the individual cuts
                for(map<string, int>::iterator cut = numberOfRecoPassing.begin(); cut != numberOfRecoPassing.end(); ++cut){
                        string cutstring = (*cut).first;
                        int RecoPassing = (*cut).second;
                        if (RecoPassing >= 2){ numberOfEventsRecoPassing[cutstring]++; }
                        (*cut).second = 0;
                }


		// Keep user informed of the number of events processed and if there is a termination due to reaching of the limit
		int prevLength = 0;
		if (jentry>0 && jentry%reportRate == 0){ 
			stringstream jentryss; jentryss.str("");
			jentryss << jentry;
			cout << string((jentryss.str()).length(),'\b') << jentryss.str(); cout.flush(); 
			prevLength = (jentryss.str()).length();
		}//*/

		if ( maxEvents > 0 && jentry >= (unsigned int)maxEvents){ cout << "\n>>> INFO: Reached user-imposed event number limit (" << maxEvents << "), skipping the rest." << endl; break; }
	}

	fevents.close();
}

/*
void TriggerEfficiency::endJob(){
cout << "\nTrigger Efficiency: " << GetEfficiency(counterGammaIso,counterEta) << endl;
}
*/

void TriggerEfficiency::SetCutsToApply(string iCutsToApply){

	CutOnGenTauPt			= ApplyThisCut("GenPt", iCutsToApply);
	CutOnGenTauEta			= ApplyThisCut("GenEta", iCutsToApply);
        CutOnRecoTauPt                  = ApplyThisCut("RecoTauPt", iCutsToApply);
        CutOnRecoTauEta                 = ApplyThisCut("RecoTauEta", iCutsToApply);  
	CutOnRecoTauLTPt 		= ApplyThisCut("RecoTauLTPt", iCutsToApply);
	CutOnTrackIso		= ApplyThisCut("RecoTauTrackIso", iCutsToApply);
	CutOnGammaIso		= ApplyThisCut("RecoTauGammaIso", iCutsToApply);

	
	if( CutOnGenTauPt ) { RegisterCut("TeuGenPt"); }
	if( CutOnGenTauEta) { RegisterCut("TauGenEta"); }
	if( CutOnRecoTauPt) { RegisterCut("TauPt"); }
	if( CutOnRecoTauEta) { RegisterCut("TauEta"); }
	if( CutOnRecoTauLTPt) { RegisterCut("TauLTPt"); }
	if( CutOnTrackIso) { RegisterCut("TauNumIsoTracks"); }
        if( CutOnGammaIso) { RegisterCut("TauNumIsoGammas"); }
  


}

void TriggerEfficiency::RegisterCut(string iCut){
	vector<string>::const_iterator found = find(cutsByOrder.begin(), cutsByOrder.end(), iCut);
	if( found == cutsByOrder.end() ){ cutsByOrder.push_back(iCut); }
}

bool TriggerEfficiency::ApplyThisCut(string thisCut, string iCutsToApply){

	bool result = false;

	string cutsToApply = iCutsToApply;
	cutsToApply = " " + cutsToApply + " ";

	size_t found = cutsToApply.find(" " + thisCut + " ");
	size_t length = cutsToApply.length();

	if ( 0 <= found && found <= length ){ result = true; }
	else{ result = false;}

	return result;

}

void TriggerEfficiency::UpCut(string iCut){
	numberOfGenPassing[iCut]++;
        numberOfRecoPassing[iCut]++;
}


bool TriggerEfficiency::PassesGenCuts(unsigned int iPair){

  if(CutOnGenTauPt){
                if (TauGenPt->at(iPair) < 20)                     { return false; }
                UpCut("TauGenPt");
        } else{
                UpCut("TauGenPt");
        } 
  

  if(CutOnGenTauEta){
                if (fabs(TauGenEta->at(iPair)) > 2.1)                     { return false; }
                UpCut("TauGenEta");
        } else{ 
		UpCut("TauGenEta");
	}
  
  

}

bool TriggerEfficiency::PassesRecoCuts(unsigned int iPair){
 
  if(CutOnRecoTauPt){
                if (TauPt->at(iPair) < 20)                     { return false; }
                UpCut("TauPt");
        }  else{
		UpCut("TauPt");
	}

  if(CutOnRecoTauEta){
                if (fabs(TauEta->at(iPair)) > 2.1)             { return false; }
                UpCut("TauEta");
        }  else{
		UpCut("TauEta");
	} 

  if(CutOnRecoTauLTPt){
                if(TauLTPt->at(iPair) < 5.0)                   { return false; }            
                UpCut("TauLTPt");
       }  else{
		UpCut("TauLTPt");
       }	

  if(CutOnTrackIso){
               if(TauNumIsoTracks->at(iPair) != 0)            { return false; }
		UpCut("TauNumIsoTracks");
       }  else{
	        UpCut("TauNumIsoTracks");
       } 
 
  if(CutOnGammaIso){
               if(TauNumIsoGammas->at(iPair) != 0)            { return false; }
                UpCut("TauNumIsoGammas");
       }  else{
	        UpCut("TauNumIsoGammas");
       }        

  
}

void  TriggerEfficiency::SetMaxEvents(int iMaxEvents){
	maxEvents = iMaxEvents;
}

float TriggerEfficiency::GetEffectiveLumi(string iPath, string iTreeName, int numEventsInDS, float targetLumi){

	float effectiveLumi = 0;

	fChain = GetTChain(iPath, iTreeName);
	int fChainEntries = fChain->GetEntries();

	if( 0 <= maxEvents && maxEvents < fChainEntries ){ effectiveLumi = (maxEvents/(double)numEventsInDS)*targetLumi; }
	else{ effectiveLumi = (fChainEntries/(double)numEventsInDS)*targetLumi; }

	return effectiveLumi;
}


void TriggerEfficiency::ResetCounters(){
	//cutsByOrder.clear();
	for(map<string, int>::iterator cut = numberOfGenPassing.begin(); cut != numberOfGenPassing.end(); ++cut){ (*cut).second = 0; }
        for(map<string, int>::iterator cut = numberOfRecoPassing.begin(); cut != numberOfRecoPassing.end(); ++cut){ (*cut).second = 0; }
	for(map<string, int>::iterator cut = numberOfEventsGenPassing.begin(); cut != numberOfEventsGenPassing.end(); ++cut){ (*cut).second = 0; }
        for(map<string, int>::iterator cut = numberOfEventsRecoPassing.begin(); cut != numberOfEventsRecoPassing.end(); ++cut){ (*cut).second = 0; }
 

}

TChain* TriggerEfficiency::GetTChain(string iPath, string iTreeName){

	// Text file to sore the list of .root files in given directory 'iPath'
	string tempList = ".fileListForTChain.tmp";

	// Chain to return
	TChain* result = new TChain(iTreeName.c_str());

	// Make use of bash to store list of root files in 'iPath' in a text file (to be read in afterward)
	TString sysCommand = "ls -1 " + iPath + "/*.root > " + tempList;
	if(gSystem->Exec(sysCommand) > 0){ cout << ">>> ERROR: Unable to list path " << iPath << "  making TChain." << endl; exit(1); }// exit(0);
	// Set up file reader
	ifstream iFile(tempList.c_str());
	if (iFile.is_open()){
		while ( iFile.good() ){
			string line;
			getline (iFile,line); // Read next line
			if (line.length() <= 0){ continue; }    // The last line may be blank. Skip any blank lines.
			result->Add(line.c_str()); // Otherwise add the file to the TChain
		}   
		iFile.close(); // Close text file
	}else{ cout << ">>> ERROR: Unable to open list file " + tempList + " for making TChain."; exit(1); } 

	// Remove temp file containing list of .root files now that they are all added to the TChain
	if(gSystem->Exec(TString("rm -f " + tempList)) > 0){ cout << ">>> WARNING: Could not remove temporary file list ("+tempList+")." << endl; }

	// Return TChain
	return result;

}

void TriggerEfficiency::MakeNewEfficiencyCounters(){
	numberOfGenPassing.clear();
        numberOfRecoPassing.clear();
  	numberOfEventsGenPassing.clear();
        numberOfEventsRecoPassing.clear();

}

string TriggerEfficiency::GetEfficiency(string iNum, string iDen){
	stringstream result;    
	result.str("");
	int num = numberOfEventsRecoPassing[iNum];
	int den = numberOfEventsGenPassing[iDen];

	float eff   =  num/(double)den;
	float effErr = eff * sqrt(1/(double)num + 1/(double)den);

	result << setprecision(2) << eff << " +/- " << effErr;

	return result.str();
}

// Make and return all plots
vector< pair<string,string>* >* TriggerEfficiency::GetEfficiencies(string type){
	vector<pair<string,string> > test;

	vector< pair<string,string>* >* result = new vector< pair<string,string>* >();
	result->clear();

	int width = 30;

	string effstr = "---efficiency---";
	for(unsigned int cut = 1; cut < cutsByOrder.size(); cut++){
		string cutstr = cutsByOrder.at(cut);
		if (type.compare("relative") == 0){
			effstr = GetEfficiency( cutsByOrder.at(cut), cutsByOrder.at(cut-1) );
		}else if (type.compare("cummulative") == 0){
			effstr = GetEfficiency( cutsByOrder.at(cut), "positiveMass" );
		}

		result->push_back(new pair<string,string>(cutstr,effstr));
	}

	return result;
}

/*
// This function fills all the histograms for the selected pair
void TriggerEfficiency::FillHistos(int pair){


	float tau1Weight 	= 1.0;
	float tau2Weight 	= 1.0;
	float eventWeight	= 1.0;



}
*/
Int_t TriggerEfficiency::GetEntry(Long64_t entry){
	// Read contents of entry.
	if (!fChain) return 0;
	return fChain->GetEntry(entry);
}

Long64_t TriggerEfficiency::LoadTree(Long64_t entry){
	// Set the environment to read one entry
	if (!fChain) return -5;
	Long64_t centry = fChain->LoadTree(entry);
	if (centry < 0) return centry;
	if (!fChain->InheritsFrom(TChain::Class()))  return centry;
	TChain *chain = (TChain*)fChain;
	if (chain->GetTreeNumber() != fCurrent) {
		fCurrent = chain->GetTreeNumber();
		Notify();
	}
	return centry;
}



