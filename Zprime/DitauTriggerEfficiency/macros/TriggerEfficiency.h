//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Jun  7 03:28:02 2011 by ROOT version 5.27/06b
// from TTree HMTTree/TauTauTree
// found on file: ditau_nTuple.root
//////////////////////////////////////////////////////////

#ifndef TriggerEfficiency_h
#define TriggerEfficiency_h

#include <TROOT.h>
#include <TSystem.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TStopwatch.h>
#include <TGraphAsymmErrors.h>
#include <math.h>
#include <map>
#include <vector>
#include <sstream>
#include <fstream>
#include <iostream>
#include <utility>
#include <algorithm>
#include "configParser/config.h"

using namespace std;




class TriggerEfficiency {
	public :
		TChain*          	fChain;   //!pointer to the analyzed TTree or TChain
		Int_t           	fCurrent; //!current Tree number in a TChain
		TriggerEfficiency*	tauTrigger;
		int		        	NumEvAnalyzed;
		int 				maxEvents;
		bool				applyTrigger;
		bool				messages;
		Config*				histoConfig;
		bool				likeSign;
		int					reportRate;
		map<string,int>		numberOfGenPassing;
                map<string,int>         numberOfRecoPassing;
		map<string,int>		numberOfEventsGenPassing;
                map<string,int>         numberOfEventsRecoPassing;

		vector<string>		cutsByOrder;
		string				topology;
		string				passingEventsFile;
		ofstream 			fevents;

		
		bool CutOnGenTauPt;
		bool CutOnGenTauEta;
		bool CutOnRecoTauPt;
		bool CutOnRecoTauEta;
		bool CutOnRecoTauLTPt;
		bool CutOnTrackIso;
		bool CutOnGammaIso;
		

		int		counterGenPt;
		int		counterGenEta;
		int		counterRecoPt;
		int		counterRecoEta;
		int		counterRecoLTPt;
		int		counterTrackIso;
		int		counterGammaIso;

		// Declaration of leaf types
                double                  runNumber;
                double                  eventNumber;
                double                  lumiBlock;
                vector<float>		*TauGenPt;
                vector<float>		*TauGenE;
                vector<float>           *TauGenEta;
                vector<float>           *TauGenPhi;
                vector<bool>            *TauMatched;
                vector<int>             *TauDiMotherId;
                vector<int>             *TauPdgId;
                vector<float>           *TauE;
                vector<float>           *TauEt;
                vector<float>           *TauPt;
                vector<float>           *TauLTPt;
                vector<int>             *TauCharge;
                vector<float>           *TauEta;
                vector<float>           *TauPhi;
                vector<int>		*TauNProngs;
		vector<float>           *TauNumIsoTracks;
                vector<float>           *TauNumIsoGammas;
                Int_t                   TotalNEvents;
		vector<float>	        *TauHcalTotOverPLead;
		vector<float>           *TauHCalMaxOverPLead;
		vector<float>           *TauHCal3x3OverPLead;

		// List of branches
         	TBranch 	*b_runNumber;   //!
		TBranch 	*b_eventNumber;   //!
		TBranch 	*b_lumiBlock;   //!
                TBranch 	*b_TauGenPt;   //!
	        TBranch 	*b_TauGenE;   //!
                TBranch 	*b_TauGenEta;   //!
  	 	TBranch 	*b_TauGenPhi;   //!
  	        TBranch 	*b_TauMatched;   //!
 		TBranch 	*b_TauDiMotherId;   //!
		TBranch 	*b_TauPdgId;   //!
		TBranch 	*b_TauE;   //!
		TBranch 	*b_TauEt;   //!
		TBranch 	*b_TauPt;   //!
		TBranch 	*b_TauLTPt;   //!
		TBranch 	*b_TauCharge;   //!
  		TBranch 	*b_TauEta;   //!
   		TBranch 	*b_TauPhi;   //!
   		TBranch 	*b_TauNProngs;   //!
   		TBranch 	*b_TauNumIsoTracks;   //!
   		TBranch 	*b_TauNumIsoGammas;   //!
   		TBranch 	*b_TotalNEvents;   //!
   		TBranch		*b_TauHcalTotOverPLead;   //!
   		TBranch		*b_TauHCalMaxOverPLead;   //!
   		TBranch		*b_TauHCal3x3OverPLead;   //!

		TriggerEfficiency(int iMaxEvents);
		virtual void SetMaxEvents(int);
		virtual ~TriggerEfficiency();
		virtual Int_t					Cut(Long64_t entry);
		virtual Int_t					GetEntry(Long64_t entry);
		virtual Long64_t				LoadTree(Long64_t entry);
		virtual void					Init();
		virtual void					Loop();
		virtual Bool_t					Notify();
		virtual TChain*					GetTChain(string, string);
		virtual void					Show(Long64_t entry = -1);
		virtual float 					GetEffectiveLumi(string, string, int, float);
	//	virtual void					BookHistos();
	//	virtual void					FillHistos(int);
		virtual bool					PassesGenCuts(unsigned int);
                virtual bool                                    PassesRecoCuts(unsigned int);
		virtual string					GetEfficiency(string,string);
		virtual vector< pair<string,string>* >*		GetEfficiencies(string);
		virtual bool					ApplyThisCut(string,string);
		virtual void					SetCutsToApply(string);
		virtual void					SetReportRate(int);
		virtual void					SetPassingEventsOutput(string);
     		virtual void					SetTopology(string);
		virtual void					ResetCounters();
		virtual void 					MakeNewEfficiencyCounters();
		virtual void					UpCut(string);
		virtual void					RegisterCut(string);


			};

#endif

#ifdef TriggerEfficiency_cxx

void TriggerEfficiency::Init(){
	// The Init() function is called when the selector needs to initialize
	// a new tree or chain. Typically here the branch addresses and branch
	// pointers of the tree will be set.
	// It is normally not necessary to make changes to the generated
	// code, but the routine can be extended by the user if needed.
	// Init() will be called many times when running on PROOF
	// (once per file to be processed).

	// Set object pointer
//	TauTriggerEfficiency = 0;
	TauGenPt = 0;
        TauGenE = 0;
 	TauGenEta = 0;
   	TauGenPhi = 0;
   	TauMatched = 0;
   	TauDiMotherId = 0;
   	TauPdgId = 0;
   	TauE = 0;
   	TauEt = 0;
   	TauPt = 0;
   	TauLTPt = 0;
   	TauCharge = 0;
   	TauEta = 0;
   	TauPhi = 0;
   	TauNProngs = 0;
   	TauNumIsoTracks = 0;
   	TauNumIsoGammas = 0;
   	TauHcalTotOverPLead = 0;
   	TauHCalMaxOverPLead = 0;
   	TauHCal3x3OverPLead = 0;
	// Set branch addresses and branch pointers
	if (!fChain) return;
	//fChain->Add(tree);
	fCurrent = -1;
	fChain->SetMakeClass(1);

	fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
	fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
	fChain->SetBranchAddress("lumiBlock", &lumiBlock, &b_lumiBlock);
//	fChain->SetBranchAddress("TauTriggerEfficiency", &TauTriggerEfficiency, &b_TauTriggerEfficiency);
        fChain->SetBranchAddress("TauGenPt", &TauGenPt, &b_TauGenPt);
	fChain->SetBranchAddress("TauGenE", &TauGenE, &b_TauGenE);
   	fChain->SetBranchAddress("TauGenEta", &TauGenEta, &b_TauGenEta);
   	fChain->SetBranchAddress("TauGenPhi", &TauGenPhi, &b_TauGenPhi);
   	fChain->SetBranchAddress("TauMatched", &TauMatched, &b_TauMatched);
   	fChain->SetBranchAddress("TauDiMotherId", &TauDiMotherId, &b_TauDiMotherId);
   	fChain->SetBranchAddress("TauPdgId", &TauPdgId, &b_TauPdgId);
   	fChain->SetBranchAddress("TauE", &TauE, &b_TauE);
   	fChain->SetBranchAddress("TauEt", &TauEt, &b_TauEt);
   	fChain->SetBranchAddress("TauPt", &TauPt, &b_TauPt);
   	fChain->SetBranchAddress("TauLTPt", &TauLTPt, &b_TauLTPt);
   	fChain->SetBranchAddress("TauCharge", &TauCharge, &b_TauCharge);
   	fChain->SetBranchAddress("TauEta", &TauEta, &b_TauEta);
   	fChain->SetBranchAddress("TauPhi", &TauPhi, &b_TauPhi);
   	fChain->SetBranchAddress("TauNProngs", &TauNProngs, &b_TauNProngs);
   	fChain->SetBranchAddress("TauNumIsoTracks", &TauNumIsoTracks, &b_TauNumIsoTracks);
   	fChain->SetBranchAddress("TauNumIsoGammas", &TauNumIsoGammas, &b_TauNumIsoGammas);
   	fChain->SetBranchAddress("TotalNEvents", &TotalNEvents, &b_TotalNEvents);
   	fChain->SetBranchAddress("TauHcalTotOverPLead", &TauHcalTotOverPLead, &b_TauHcalTotOverPLead);
   	fChain->SetBranchAddress("TauHCalMaxOverPLead", &TauHCalMaxOverPLead, &b_TauHCalMaxOverPLead);
   	fChain->SetBranchAddress("TauHCal3x3OverPLead", &TauHCal3x3OverPLead, &b_TauHCal3x3OverPLead);
	Notify();
}

/*
void TriggerEfficiency::BookHistos(){

	

}
*/
Bool_t TriggerEfficiency::Notify(){
	// The Notify() function is called when a new file is opened. This
	// can be either for a new TTree in a TChain or when when a new TTree
	// is started when using PROOF. It is normally not necessary to make changes
	// to the generated code, but the routine can be extended by the
	// user if needed. The return value is currently not used.

	return kTRUE;
}

void TriggerEfficiency::Show(Long64_t entry){
	// Print contents of entry.
	// If entry is not specified, print current entry
	if (!fChain) return;
	fChain->Show(entry);
}


Int_t TriggerEfficiency::Cut(Long64_t entry){
	// This function may be called from Loop.
	// returns  1 if entry is accepted.
	// returns -1 otherwise.
	return 1;
}

#endif
