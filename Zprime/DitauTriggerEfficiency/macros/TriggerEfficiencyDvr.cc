#include "TriggerEfficiencyDvr.h"

#define TriggerEfficiencyDvr_cxx
using namespace std;

int main(int argc, char **argv){
	gROOT->Reset();
	gROOT->SetStyle("Plain");
	setTDRStyle();


	gErrorIgnoreLevel = kError;
	TDatime clock;
	TStopwatch* stopwatch = new TStopwatch();

	cout << ">>> Starting analysis..." << endl;
	stopwatch->Start();

	// Instatiate configuration parser and take the first argument as the config file
	Config theConfig(argv[1], argv);
	TString sysCommand;

	// Read in global parameters 
	int	maxEvents					= theConfig.pDouble("maxEvents");
	string lumiExpression			= theConfig.pString("luminosity");
//	string tauTriggerEffLowEdges	= theConfig.pString("tauTriggerEffLowEdge");
//	string tauTriggerEfficiency 	= theConfig.pString("tauTriggerEfficiency");
	double luminosity 				= atof(lumiExpression.substr(0,lumiExpression.find('/')).c_str());
	string lumiUnits				= lumiExpression.substr(lumiExpression.find('/'));
	string analyses					= theConfig.pString("analyses");

	// Read in output parameters
	string webDir			= theConfig.pString("webDir");
	string output_eff		= theConfig.pString("output_eff");
	string output_events	= theConfig.pString("output_events");
//	string output_histos	= theConfig.pString("output_histos");

	// Create output dir if it doesn't exists already
	sysCommand = "if [ ! -d " + webDir + " ]; then mkdir -p " + webDir + "; fi";
	if(gSystem->Exec(sysCommand) > 0){ cout << ">>> ERROR: problem creating dir for plots " << webDir << endl; exit(1); }// exit(0);

	TriggerEfficiency* tautauAnalysis = new TriggerEfficiency(maxEvents);
	EfficiencyCounter* effCounter = new EfficiencyCounter();

	// Switch on/off specified selection cuts
	tautauAnalysis->SetCutsToApply(theConfig.pString("cutsToApply"));

	// Set report rate
	tautauAnalysis->SetReportRate(theConfig.pInt("reportEvery"));
	
	// Set passing event list output
	tautauAnalysis->SetPassingEventsOutput(webDir+output_events);


        // Read in and add topologies
	map<string, Config*> topologies = theConfig.getGroups();
	const string topologyPrefix = "topology_";
	for (map<string, Config*>::iterator i = topologies.begin(); i != topologies.end(); ++i) {
		string groupName = i->first;
		Config* group = i->second;
		if (groupName.substr(0, topologyPrefix.length()) == topologyPrefix) {
			NewSection(stopwatch);
			groupName = groupName.substr(topologyPrefix.length());
			cout << ">>> " << "INFO: Found topology \"" << groupName << "\"" << endl;

			string rootLabel		= group->pString("rootLabel");
			bool enabled        = group->pBool("enabled");
			if(!enabled){ cout << ">>> INFO: Skipped" << endl; continue; }

			string inputFile		= group->pString("inputFile");
			string treeName			= group->pString("treeName");
			bool isQCD				= group->pBool("isQCD");
			bool isSignal			= group->pBool("isSignal");
			bool isBackground		= group->pBool("isBackground");
			int color				= group->pInt("color");
			float crossSection		= group->pDouble("crossSection");
			float branchingRatio	= group->pDouble("branchingRatio");
			float skimEff			= group->pDouble("skimEff");
			float otherSF			= group->pDouble("otherSF");
			int numEvInDS			= group->pInt("numEvInDS");

			float lumiScaleFactor = luminosity*crossSection*skimEff*otherSF;

			if((!isSignal) && (!isBackground) && (!isQCD)){
				cout << ">>> INFO: Calculating effective luminosity... "; cout.flush();
				float effectiveLumi	= luminosity;
				if (maxEvents > 0){
					effectiveLumi = tautauAnalysis->GetEffectiveLumi(inputFile, treeName, numEvInDS, luminosity);
				}
				
				NewSection(stopwatch);
			}


    }
 }
	NewSection(stopwatch);


	return 0;
}


