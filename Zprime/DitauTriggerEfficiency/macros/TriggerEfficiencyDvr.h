#include "configParser/config.h"
#include <iomanip>
#include <iostream>
#include <fstream>
#include "TError.h"
#include "TSystem.h"
#include "TriggerEfficiency.h"
#include "TStopwatch.h"
#include "EfficiencyCounter.h"
#include "TDatime.h"
#include "style-CMSTDR.h"

#ifndef TriggerEfficiencyDvr_h
#define TriggerEfficiencyDvr_h

using namespace std;

//bool DoThisAnalysis(string,string);
bool DoThisAnalysis(string analysisList, string thisAnalysis){
	bool result = false;
	analysisList = " " + analysisList + " ";
	size_t found = analysisList.find(" " + thisAnalysis + " ");
	size_t length = analysisList.length();
	if ( 0 <= found && found <= length ){ result = true; }
	else{ result = false;}
	return result;
}

void NewSection(TStopwatch* iStopWatch){

	float realSecs = iStopWatch->RealTime();
	float cpuSecs = iStopWatch->CpuTime();
	iStopWatch->Continue();

	TDatime clock;
	
	cout << "\n--- " << clock.AsString() << " ----- Elapsed: " << setw(7) << setfill(' ') << setprecision(3) << realSecs << "s ("
	<< setw(7) << setfill(' ')  << cpuSecs << " CPUs) "  << string(50, '-') << endl;
}


#endif
