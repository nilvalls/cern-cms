// -*- C++ -*-
//
// Package:    DitauTriggerEfficiency
// Class:      DitauTriggerEfficiency
// 
/**\class DitauTriggerEfficiency DitauTriggerEfficiency.cc HighMassAnalysis/DitauTriggerEfficiency/src/DitauTriggerEfficiency.cc

Description: Calculation of the ditau trigger efficiency with the possibility of assymetric pT thresholds

**/
//
// Original Author:  Nil Valls <nil.valls@cern.ch>
//         Created:  Sat Apr  9 04:27:40 CDT 2011
// $Id: DitauTriggerEfficiency.cc,v 1.17 2011/07/01 11:48:18 calpas Exp $



#include "HighMassAnalysis/DitauTriggerEfficiency/interface/DitauTriggerEfficiency.h"

  using namespace edm;
  using namespace reco;
  using namespace std;
  
// constructors and destructor
DitauTriggerEfficiency::DitauTriggerEfficiency(const edm::ParameterSet& iConfig){

      //  file = new TFile(_outputFile.c_str(),"recreate");
        

	// Collections
	_GenParticleSource	= iConfig.getUntrackedParameter<InputTag>("GenParticleSource");
	_RecoTauSource		= iConfig.getParameter<InputTag>("RecoTauSource");

	// Get generator matching parameters
        _MatchTauToGen                  = iConfig.getParameter<bool>("MatchTauToGen");    
	_UseTauMotherId			= iConfig.getParameter<bool>("UseTauMotherId");
	_UseTauGrandMotherId	= iConfig.getParameter<bool>("UseTauGrandMotherId");
	_TauMotherId			= iConfig.getParameter<int>("TauMotherId");
	_TauGrandMotherId		= iConfig.getParameter<int>("TauGrandMotherId");
	_TauToGenMatchingDeltaR = iConfig.getParameter<double>("TauToGenMatchingDeltaR");

	// Isolation parameters
	_RecoTauIsoDeltaRCone			= iConfig.getParameter<double>("RecoTauIsoDeltaRCone");
	_RecoTauSignalDeltaRCone		= iConfig.getParameter<double>("RecoTauSignalDeltaRCone");
	_RecoTauTrackIsoTrkThreshold	= iConfig.getParameter<double>("RecoTauTrackIsoTrkThreshold");
	_RecoTauGammaIsoGamThreshold	= iConfig.getParameter<double>("RecoTauGammaIsoGamThreshold");
	_MaxNumIsoTracks				= iConfig.getParameter<int>("MaxNumIsoTracks");
	_MaxNumIsoGammas				= iConfig.getParameter<int>("MaxNumIsoGammas");
	_RecoVertexSource				= iConfig.getParameter<InputTag>("RecoVertexSource");

}


DitauTriggerEfficiency::~DitauTriggerEfficiency(){
	// do anything here that needs to be done at desctruction time
	// (e.g. close files, deallocate resources etc.)
}


// ------------ method called to for each event  ------------
void DitauTriggerEfficiency::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup){

	getCollections(iEvent, iSetup);
	numTotalEvents++;
//   cout<<numTotalEvents<<endl;
	fillNtuple(iEvent);
	clearVectors(); 

}


// ------------ method called once each job just before starting event loop  ------------
void DitauTriggerEfficiency::beginJob(){
	numTotalEvents					= 0;
       	initializeVectors();
	setupBranches(); 
}

// ------------ method called once each job just after ending the event loop  ------------
void DitauTriggerEfficiency::endJob(){
   _TotalNEvents = numTotalEvents;
//  file->cd(); 
//  file->Write();
//  file->Close(); 

}


void DitauTriggerEfficiency::fillNtuple(const Event& iEvent){

//	const reco::Vertex& thePrimaryEventVertex = (*(_primaryEventVertexCollection)->begin());


                        _runNumber              =      iEvent.id().run();
                        _eventNumber            =      iEvent.id().event();
                        _lumiBlock              =      iEvent.id().luminosityBlock();


	// Tau loops

	for ( pat::TauCollection::const_iterator patTau = _patTaus->begin(); patTau != _patTaus->end(); ++patTau ) {

			if(_MatchTauToGen){       

				_TauGenPt		->	push_back(matchToGen(*patTau).second.pt()); 
				_TauGenE		->	push_back(matchToGen(*patTau).second.energy()); 
				_TauGenEta		->	push_back(matchToGen(*patTau).second.eta()); 
				_TauGenPhi		->	push_back(matchToGen(*patTau).second.phi()); 
				_TauMatched	->	push_back(int(matchToGen(*patTau).first));                             

				const GenParticleRef theGenTauRef = patTau->genParticleRef(); 
				if ( theGenTauRef.isNonnull() ){ _TauDiMotherId->push_back(abs(theGenTauRef->mother()->mother()->pdgId())); }               
				else{ _TauDiMotherId->push_back(0); }                                                 

				_TauPdgId		->	push_back(getMatchedPdgId(*patTau).second);
				
	} // Closing brace for Match to Gen if statement 

          else {
        			_TauGenPt               ->      push_back(0);
                                _TauGenE                ->      push_back(0);
                                _TauGenEta              ->      push_back(999);
                                _TauGenPhi              ->      push_back(0);
 				_TauMatched             ->      push_back(0);
				_TauDiMotherId          ->      push_back(0); 
				_TauPdgId               ->      push_back(0);

         }  // Closing brace for else		


			// Reco Tau variables
			_TauE->push_back( patTau->energy() );
			_TauEt->push_back( patTau->et() );
			_TauPt->push_back( patTau->pt() );
			_TauEta->push_back( patTau->eta() );
			_TauPhi->push_back( patTau->phi() );
	        	_TauNProngs->push_back( patTau->signalPFChargedHadrCands().size() );
			_TauNumIsoTracks->push_back( getNIsoTracks(*patTau) ); 
			_TauNumIsoGammas->push_back( getNIsoGammas(*patTau) );
	          	_TauHcalTotOverPLead		->  push_back( patTau->hcalTotOverPLead() );
			_TauHCalMaxOverPLead		->  push_back( patTau->hcalMaxOverPLead() );
			_TauHCal3x3OverPLead		->  push_back( patTau->hcal3x3OverPLead() );
			// Leading track variables
			if(patTau->leadPFChargedHadrCand().isNonnull()){
				_TauLTPt->push_back(patTau->leadPFChargedHadrCand()->pt());
				_TauCharge->push_back(patTau->leadPFChargedHadrCand()->charge());
				
			}else{
				_TauLTPt->push_back(-1);
				_TauCharge->push_back(999);
			}

		   }  // end of tau loop


  

	// Fill the ntuple
	_TauTauTree->Fill();
}

// Book branches and link them to appropriate pointers
void DitauTriggerEfficiency::setupBranches(){

        _TauTauTree = file->make<TTree>("TauTauTree", "Tau Tau Tree");
//	_TauTauTree = new TTree("TauTauTree", "Tau Tau Tree");
	_TauTauTree->Branch("runNumber",&_runNumber);
	_TauTauTree->Branch("eventNumber",&_eventNumber);
	_TauTauTree->Branch("lumiBlock",&_lumiBlock);

	_TauTauTree->Branch("TauGenPt",&_TauGenPt);
	_TauTauTree->Branch("TauGenE",&_TauGenE);
	_TauTauTree->Branch("TauGenEta",&_TauGenEta);
	_TauTauTree->Branch("TauGenPhi",&_TauGenPhi);
	
	_TauTauTree->Branch("TauMatched",&_TauMatched);
	_TauTauTree->Branch("TauDiMotherId",&_TauDiMotherId);
	_TauTauTree->Branch("TauPdgId",&_TauPdgId);
	_TauTauTree->Branch("TauE",&_TauE);
	_TauTauTree->Branch("TauEt",&_TauEt);
	_TauTauTree->Branch("TauPt",&_TauPt);
       	_TauTauTree->Branch("TauLTPt",&_TauLTPt);
	_TauTauTree->Branch("TauCharge",&_TauCharge);
	_TauTauTree->Branch("TauEta",&_TauEta);
	_TauTauTree->Branch("TauPhi",&_TauPhi);
 	_TauTauTree->Branch("TauNProngs",&_TauNProngs);
 	_TauTauTree->Branch("TauNumIsoTracks",&_TauNumIsoTracks);
        _TauTauTree->Branch("TauNumIsoGammas",&_TauNumIsoGammas);
        _TauTauTree->Branch("TotalNEvents",&_TotalNEvents);
	_TauTauTree->Branch("TauHcalTotOverPLead",&_TauHcalTotOverPLead);
        _TauTauTree->Branch("TauHCalMaxOverPLead",&_TauHCalMaxOverPLead);
        _TauTauTree->Branch("TauHCal3x3OverPLead",&_TauHCal3x3OverPLead);
 
}

// Reset variables/pointers
void DitauTriggerEfficiency::initializeVectors(){

	_runNumber					=	-1;
	_eventNumber		             		=	-1;
	_lumiBlock					=	-1;

	_TauGenPt					=	NULL;
	_TauGenE					=	NULL;
	_TauGenEta					=	NULL;
	_TauGenPhi					=	NULL;

	_TauMatched		    			=	NULL;
	_TauDiMotherId					=	NULL;
	_TauPdgId					=	NULL;
	_TauE						=	NULL;
	_TauEt						=	NULL;
	_TauPt						=	NULL;
        _TauLTPt					=	NULL;
 	_TauCharge					=	NULL;
	_TauEta						=	NULL;
	_TauPhi						=	NULL;
 	_TauNProngs					=	NULL;	
 	_TauNumIsoTracks				=	NULL;
 	_TauNumIsoGammas       		                =	NULL;
        _TauHcalTotOverPLead                            =       NULL; 
        _TauHCalMaxOverPLead                            =       NULL;
        _TauHCal3x3OverPLead           			=       NULL;

}

// Clear vectors
void DitauTriggerEfficiency::clearVectors(){

	_TauGenPt					->	clear();
	_TauGenE					->	clear();
	_TauGenEta					->	clear();
	_TauGenPhi					->	clear();

	_TauMatched					->	clear();
	_TauDiMotherId					->	clear();
	_TauPdgId					->	clear();
	_TauE						->	clear();
	_TauEt						->	clear();
	_TauPt						->	clear();
        _TauLTPt					->	clear();
 	_TauCharge					->	clear();
	_TauEta						->	clear();
	_TauPhi						->	clear();
 	_TauNProngs					->	clear();
 	_TauNumIsoTracks				->	clear();
 	_TauNumIsoGammas         			->      clear();
	_TauHcalTotOverPLead                            ->      clear();
        _TauHCalMaxOverPLead                            ->      clear();
        _TauHCal3x3OverPLead  				->      clear();	
 }

//define this as a plug-in
DEFINE_FWK_MODULE(DitauTriggerEfficiency);
