import FWCore.ParameterSet.Config as cms
import copy


ProcessAtMost=2000;

print 'Max events: %d' % ProcessAtMost

maxAbsEta=2.1
minTightPt=40.0
minLoosePt=40.0
minLeadingTrackPT=5.0



TauMotherId=32						

process = cms.Process('DitauTriggerEfficiencyCalculation')
process.load("FWCore.MessageService.MessageLogger_cfi")

process.TFileService = cms.Service("TFileService", 
    fileName = cms.string("ditauTriggerEfficiency_X500.root")
)

process.MessageLogger.cerr.FwkReport.reportEvery = 100
process.options   = cms.untracked.PSet( wantSummary = cms.untracked.bool(False) )

process.maxEvents=cms.untracked.PSet(input=cms.untracked.int32(ProcessAtMost))



process.source = cms.Source("PoolSource",
		fileNames = cms.untracked.vstring(
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_9_1_w9T.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_8_1_acq.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_7_1_LzK.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_6_1_smF.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_5_1_eCW.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_50_1_IOH.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_4_1_8Hq.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_49_1_6tI.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_48_1_ZAK.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_47_1_NvI.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_46_1_xoC.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_45_1_go7.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_44_1_ZC3.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_43_1_6Xl.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_42_1_eTK.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_41_1_q32.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_40_1_gGA.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_3_1_x8N.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_39_1_Cyv.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_38_1_4vc.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_37_1_FY3.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_36_1_i0b.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_35_1_FxG.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_34_1_LbL.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_33_1_BLv.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_32_1_HTv.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_31_1_l2C.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_30_1_k90.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_2_1_MDx.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_29_1_kbF.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_28_1_ijl.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_27_1_vd6.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_26_1_YvZ.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_25_1_7ls.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_24_1_xV9.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_23_1_zj7.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_22_1_01v.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_21_1_osI.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_20_1_1L9.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_1_1_wqu.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_19_1_nv2.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_18_1_NCr.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_17_1_u0m.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_16_1_mEU.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_15_1_lbZ.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_14_1_nLS.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_13_1_ReB.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_12_1_TZD.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_11_1_pHa.root',
			'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Zprime500_MC_38X/skimPat_10_1_gVd.root'
		)
)


process.main = cms.EDAnalyzer('DitauTriggerEfficiency',

	# Basic trigger requirments
	maxAbsEta			= cms.double(maxAbsEta),
	minTightPt			= cms.double(minTightPt),
	minLoosePt			= cms.double(minLoosePt),
	minLeadingTrackPT	= cms.double(minLeadingTrackPT),

	# Generator level inputs
    GenParticleSource	=  cms.untracked.InputTag('genParticles'),				# MC gen particle collection
    RecoTauSource		=  cms.InputTag('selectedLayer1ShrinkingConeHighEffPFTaus'),
    UseTauMotherId			= cms.bool(True),
    UseTauGrandMotherId		= cms.bool(False),
    TauMotherId				= cms.int32(TauMotherId),						
    TauGrandMotherId		= cms.int32(1),
    TauToGenMatchingDeltaR	= cms.double(0.25),

	# Isolation parameters
    RecoVertexSource			= cms.InputTag('offlinePrimaryVertices'),    
    RecoTauIsoDeltaRCone		= cms.double(0.5),
	RecoTauSignalDeltaRCone		= cms.double(0.2),
	RecoTauTrackIsoTrkThreshold	= cms.double(1.0),
	RecoTauGammaIsoGamThreshold	= cms.double(1.5),
	MaxNumIsoTracks				= cms.int32(0), 
	MaxNumIsoGammas				= cms.int32(0)

)


process.p = cms.Path(process.main)
