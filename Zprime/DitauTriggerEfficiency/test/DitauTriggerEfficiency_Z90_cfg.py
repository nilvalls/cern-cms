import FWCore.ParameterSet.Config as cms
import copy


ProcessAtMost=10000;

print 'Max events: %d' % ProcessAtMost

#maxAbsEta=2.1
#minTightPt=40.0
#minLoosePt=40.0
#minLeadingTrackPT=5.0



TauMotherId=23						

process = cms.Process('DitauTriggerEfficiency')
process.load("FWCore.MessageService.MessageLogger_cfi")

process.TFileService = cms.Service("TFileService", 
    fileName = cms.string("NTuple.root")
)

process.MessageLogger.cerr.FwkReport.reportEvery = 100
process.options   = cms.untracked.PSet( wantSummary = cms.untracked.bool(False) )

process.maxEvents=cms.untracked.PSet(input=cms.untracked.int32(ProcessAtMost))



process.source = cms.Source("PoolSource",
		fileNames = cms.untracked.vstring(

#	'file:/data/ndpc2/b/nvallsve/DitauTrigger/HLTefficiency/Ztautau_MC_38X/skimPat_1_1_K3k.root'
#        'dcache:/pnfs/cms/WAX/11/store/user/nitish/DYToTauTau_M-20_TuneZ2_7TeV-pythia6-tauola/24Jun2011_DYToTauTau_TuneZ2_pythia6_tauola_Summer11_AODSIM_TauTauPatTuple/a0d6f6dd4e8301735e5c5b66e46d63d5/TauTau_Summer11AODSIM_SkimPat_9_1_SrF.root'
       'file:TauTau_Summer11AODSIM_SkimPat_2_2_xWP.root',
       'file:TauTau_Summer11AODSIM_SkimPat_9_1_SrF.root'   
      
		)
)


process.main = cms.EDAnalyzer('DitauTriggerEfficiency',

	# Basic trigger requirments
#	maxAbsEta			= cms.double(maxAbsEta),
#	minTightPt			= cms.double(minTightPt),
#	minLoosePt			= cms.double(minLoosePt),
#	minLeadingTrackPT	= cms.double(minLeadingTrackPT),

	# Generator level inputs
    GenParticleSource	=  cms.untracked.InputTag('genParticles'),				# MC gen particle collection
    RecoTauSource		=  cms.InputTag('selectedLayer1ShrinkingConeHighEffPFTaus'),
#    RecoTauSource       = cms.InputTag('selectedLayer1HPSPFTaus'),
    MatchTauToGen               =  cms.bool(True),
    UseTauMotherId			= cms.bool(True),
    UseTauGrandMotherId		= cms.bool(False),
    TauMotherId				= cms.int32(TauMotherId),						
    TauGrandMotherId		= cms.int32(1),
    TauToGenMatchingDeltaR	= cms.double(0.25),

	# Isolation parameters
    RecoVertexSource			= cms.InputTag('offlinePrimaryVertices'),    
    RecoTauIsoDeltaRCone		= cms.double(0.5),
	RecoTauSignalDeltaRCone		= cms.double(0.2),
	RecoTauTrackIsoTrkThreshold	= cms.double(1.0),
	RecoTauGammaIsoGamThreshold	= cms.double(1.5),
	MaxNumIsoTracks				= cms.int32(0), 
	MaxNumIsoGammas				= cms.int32(0)

)


process.p = cms.Path(process.main)
