#!/bin/bash

if [ -z $1 ]; then
	echo "ERROR: no input file given."
	exit 1
fi

if [ ! -e "$1" ]; then
	echo "ERROR: file $1 does not exist."
	exit 1
fi


echo -e ">>>  Ditau Trigger Efficiency Results  <<< \n Max_Abs_eta	Min_tau1_pT	Min_tau2_pT	Efficiency" && grep -A14 "Max. |" $1 | sed "s/--/########################################################/g"
