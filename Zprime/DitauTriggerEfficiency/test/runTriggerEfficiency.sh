#!/bin/bash


# User input to process the Z or Z'(500) sample
choice=$1
if [ -z $1 ]; then
		echo "ERROR: no option provided--specify 90 or 500"
		exit 1
fi
if [ "$choice" == "90" ]; then
	baseCfg="DitauTriggerEfficiency_Z90_cfg.py"
	logfile="Z90.log"
	tmpfile="Z90.py"
else
	if [ "$choice" == "500" ]; then
		baseCfg="DitauTriggerEfficiency_X500_cfg.py"
		logfile="X500.log"
		tmpfile="X500.py"
	else
		echo "ERROR: invalid option \"$choice\"--specify 90 or 500"
		exit 1
	fi
fi

# cmsenv so we can run cmsRun (npi)
cd ~/CMSSW_3_8_6/src/ && eval `scramv1 runtime -sh` && cd -

# Start a fresh logfile
rm -f "$logfile" "$tmpfile"

# Thresholds to be used for efficiency calculation
tau1pts=(	"30.0" "30.0" "35.0" "40.0" "40.0" "40.0" "40.0" )
tau2pts=(	"30.0" "30.0" "35.0" "40.0" "40.0" "30.0" "35.0" )
etas=(		 "2.5"  "2.1"  "2.1"  "2.5"  "2.1"  "2.1"  "2.1" )
ltPts=(		   "5"    "5"    "5"    "5"    "5"    "5"    "5" )
#tau1pts=(	"15.0" "30.0" "30.0" "30.0" "35.0" "40.0" "40.0" "40.0" )
#tau2pts=(	"15.0" "25.0" "25.0" "30.0" "35.0" "40.0" "30.0" "35.0" )
#etas=(		 "2.5" "2.5"  "2.1"  "2.1"  "2.1"  "2.1"  "2.1"  "2.1" )
#ltPts=(	   "5"   "5"    "5"    "5"    "5"    "5"    "5"    "5" )

# Process up to 
maxNevents=5000

# Begin loop over specified thresholds
iter=0
for eta in "${etas[@]}"; do
	tau1pt=${tau1pts[$iter]}
	tau2pt=${tau2pts[$iter]}
	ltPt=${ltPts[$iter]}

	# Copy generig config file and modify thresholds accordingly
	/bin/cp -f "$baseCfg" "$tmpfile"
	sed -i 's/maxAbsEta=.*/maxAbsEta='$eta'/g' "$tmpfile"
	sed -i 's/minTightPt=.*/minTightPt='$tau1pt'/g' "$tmpfile"
	sed -i 's/minLoosePt=.*/minLoosePt='$tau2pt'/g' "$tmpfile"
	sed -i 's/minLeadingTrackPT=.*/minLeadingTrackPT='$ltPt'/g' "$tmpfile"
	sed -i 's/ProcessAtMost=.*/ProcessAtMost='$maxNevents'/g' "$tmpfile"

	# Append a descriptive suffix with pT and eta thresholds
	fileSuffix="_"$tau1pt"_"$tau2pt"_Trk"$ltPt"_eta"$eta
	fileSuffix="$( echo $fileSuffix | sed 's/\./p/g')"
	if [ "$choice" == "90" ]; then
		sed -i 's/Z90\.root/Z90'$fileSuffix'\.root/g' "$tmpfile"
	elif [ "$choice" == "500" ]; then
		sed -i 's/X500\.root/X500'$fileSuffix'\.root/g' "$tmpfile"
	fi

	# Run the job, both stdout and stderr go to logfile
	cmsRun "$tmpfile" >> "$logfile" 2>&1
	rm -rf "$tmpfile"

	iter=$(( $iter + 1 ))
done

# Clean up
rm -f "$tmpfile"



