1) We must have each sample in a directory labeled accordingly.
2) The generateInFiles_forIMP.sh must be executed like:

	./generateInFiles_forIMP.sh
	
	and this produces a bunch of .in files (one per sample per cut),
	and a list of root calls (invariantMassPlotter.sh), pointing to
	the appropriate version of the InvariantMassPlotterXX.C (MC or 
	Data) and the appropriate .in file.

3) To execute the list of root calls, source such a file:

	source invariantMassPlotter.sh

	and root files tagged with the infix *_IMP_* will be output
	into each sample's directory.

4) The QCD samples must now be merged. Execute hadd_QCDsamples.sh:

	./hadd_QCDsamples.sh

	and this will create a new directory (QCD) with the luminosity-
	normalized files merged together for each missing cut.

5) Now we need to generate .in files for the DataAndMC_Plotter.C,
	of as I like to call it: stacker. Execute it like:

	./generateInFiles_forStacker.sh
	
	and .in files will be produced, one per missing cut, as well as
	a list of root calls for each of those.

6) Source the list of root calls like:

	source stackDataAndMC.sh

	and one root file per missing cut will be produced in the base dir.
