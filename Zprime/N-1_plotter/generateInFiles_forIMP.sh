#!/bin/bash

# Function that will output text to an .in file, taking in the appropriate variables
function generateOutput(){

output="rootfile "$sample"/"$sample"_N_1_"$missingCut".root
directory analyzeHiMassTau 
process Nminus1 
scaleFactor 1 
scaleFactorError 0 
effectiveXsection "$crossSection" 
useForAcceptance 1 
useForElectronID 1 
useForMuonID 1 
useForTauID 1 
useForTopology 1 
useForFinalMassPlot 1
luminosity 36.15 
skimmingEff "$skimEff"
branchingRatio "$branchingRatio" 
branchingRatioError 0.0 
outputPostscriptFile "$sample"/"$sample"_IMP_"$missingCut".ps 
outputRootFileForNormalizedHistos "$sample"/"$sample"NormalizedHistos_IMP_"$missingCut".root 
outputRootFileForProbabilityHistos "$sample"/"$sample"ProbabilityHistos_IMP_"$missingCut".root 
outputRootFileForFitter "$sample"/"$sample"_IMP_"$missingCut".root 
TtreeNameForFitter testTree 
TemplateDirectoryNameForFitter "$sample"TemplateDirectory 
outputLogFile "$sample"/"$sample"_IMP_"$missingCut".log"

	echo "$output" > $sample"_IMP_"$missingCut".in"
}


# Array of cuts
cuts=("CosDelPhi" "GammaIso" "MET" "OS" "Prongs" "TauEta" "TauPt" "SeedTrackPt" "TrkIso")

# Array of samples (these must be equal to the directory they reside
#samples=("Data" "TTbar" "WJets" "Zprime500" "Ztautau" "QCDDijetPt15to30" "QCDDijetPt30to50" "QCDDijetPt50to80" "QCDDijetPt80to120" "QCDDijetPt120to170" "QCDDijetPt170to300" "Zee") 

samples=(		"Data"	"TTbar"	"WJets"	"minusWJets"	"Zprime500"	"Ztautau" 	"dd-QCD-tightIso" 	"Zee"	"ZJets") 
# Array with the cross sections for each sample (order must coincide with samples array)
crossSections=( "1" 	"149.6" "29349" "29349"			"1.94" 		"1653" 		"1" 				"1653"	"2321")
# Array with skimming efficiencies for each sample (order must coincide with samples array)
skimEff=( 		"1" 	"0.975" "0.126" "0.126"			"1" 		"0.254" 	"1" 				"0.398"	"0.4896")

# Array with branching ratios for each sample (order must coincide with samples)
branchingRatios=( "1" 	"1" 	"1" 	"1"				"0.4225" 	"1" 		"1" 				"1"		"1")

# Delete any old list of execution lines (to be executed after the end of generation of root files)
rm -f invariantMassPlotter.sh

# Delete any old QCD files from the hadd-ition
rm -f QCD/*

# Loop over all the samples
sampleNumber=0;
for sample in ${samples[@]}; do
	
	# These parameters are taken from the arrays and will be passed to the function above
	crossSection=${crossSections[$sampleNumber]};
	skimEff=${skimEff[$sampleNumber]};
	branchingRatio=${branchingRatios[$sampleNumber]};


	# Remove root files from each sample's directory, produced during a previous iteration. Otherwise, IMP crashes.
	rm -f $sample/*IMP*;
	rm -f IMP.std*

	# Loop over every cut in the array
	for missingCut in ${cuts[@]}; do
		# Generate the .in file for such a cut and sample
		generateOutput

		# Generate the list of execution lines that will load each .in file previously generated into the IMP
		if [ $sample == "Data" ] || [ $sample == "dd-QCD-tightIso" ]; then dataOrMC="Data"; else dataOrMC="MC"; fi # If the sample is data, make sure to call IMP_data, else call IMP_mc...
		echo "root -q InvariantMassPlotter"$dataOrMC".C\(\\\"$sample"_IMP_"$missingCut".in"\\\"\) 1>> IMP.stdout 2>> IMP.stderr" >> "invariantMassPlotter.sh"

	done

	sampleNumber=$(( $sampleNumber + 1 ))
done
