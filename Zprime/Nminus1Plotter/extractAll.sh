#!/bin/bash

echo "Extracting interesting histos..."

cd ~/CMSSW_3_8_6/src/ && eval `scramv1 runtime -sh` && cd -

#baseOutputDir="$HOME/www/HighMassTau/110519_tightHoverP/"
baseOutputDir="$HOME/www/HighMassTau/110519_looseHoverP/"

function processDir(){
	if [ -d $baseOutputDir$targetDir ]; then
		rm -rf $baseOutputDir/$targetDir/*
	fi
	mkdir -p $baseOutputDir/$targetDir

	root -l -q -b extractInterestingHistos_allCuts.C\(\"$inputDir\"\,\"$baseOutputDir/$targetDir\"\)
}

#inputDir="allCuts_MET30_QCDMET15"
#argetDir="allSamples_MET30_QCDMET15"
#processDir
 
#inputDir="allCuts_MET00"
#targetDir="allSamples_MET00"
#processDir
 
#inputDir="allCuts_MET05"
#targetDir="allSamples_MET05"
#processDir
 
#inputDir="allCuts_MET10"
#targetDir="allSamples_MET10"
#processDir
 
#inputDir="allCuts_MET15"
#targetDir="allSamples_MET15"
#processDir
 
#inputDir="allCuts_MET20"
#targetDir="allSamples_MET20"
#processDir
 
#inputDir="allCuts_MET25"
#targetDir="allSamples_MET25"
#processDir
 
inputDir="allCuts_MET30"
targetDir="allSamples_MET30"
processDir
