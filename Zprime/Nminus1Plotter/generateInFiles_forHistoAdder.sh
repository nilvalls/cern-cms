#!/bin/bash

# Function to generate the text output that will go inside the .in file
function generateOutput(){

output="rootfile dd-QCD/dd-QCDNormalizedHistos_IMP_"$missingCut".root
factor 1.0
rootfile minusWJets/minusWJetsNormalizedHistos_IMP_"$missingCut".root
factor -1.0
finalFactor 1.10
outputRootFile dd-QCDfinal/dd-QCDfinalNormalizedHistos_IMP_"$missingCut".root
"
	echo "$output" > "prepare_ddQCD_"$missingCut".in"
}

# List of cuts
#cuts=("CosDelPhi" "GammaIso" "MET" "OS" "Prongs" "TauEta" "TauPt" "SeedTrackPt" "TrkIso")
cuts=(		"CosDelPhi" "MET" "OS" "Tau1Prongs" "Tau2Prongs" "Tau1Eta" "Tau2Eta" "Tau1Pt" "Tau2Pt" "Tau1SeedPt" "Tau2SeedPt" "Tau1TrkIso" "Tau2TrkIso" "Tau1GammaIso" "Tau2GammaIso" )

# Remove any old list of execution lines (root calls), since we're producing a new one
rm -f "preapare_ddQCD.sh";
rm -rf dd-QCDfinal/
mkdir dd-QCDfinal


echo "/bin/rm -rf dd-QCDfinal/ && mkdir dd-QCDfinal" > "prepare_ddQCD.sh"

cutnumber=0
# For each cut, generate a .in file
for missingCut in ${cuts[@]}; do
	qcdEvents=${qcdEvents[$cutnumber]}
	generateOutput

	# Also add a line to the root call list with the call to the stack plotter and appropriate .in file for the cut
	echo "root -q -l -b addHistos.C\(\\\"prepare_ddQCD_"$missingCut".in\\\"\)" >> "prepare_ddQCD.sh"

	cutnumber=$(( $cutnumber + 1 ))
done

