#!/bin/bash

/bin/rm -rf results/
mkdir results

# Function to generate the text output that will go inside the .in file
function generateOutput(){

output="rootfile Data/DataNormalizedHistos_IMP_"$missingCut".root
process Data
rootfile dd-QCDfinal/dd-QCDfinalNormalizedHistos_IMP_"$missingCut".root
process QCD 
rootfile TTbar/TTbarNormalizedHistos_IMP_"$missingCut".root
process t+#bar{t}
rootfile WJets/WJetsNormalizedHistos_IMP_"$missingCut".root
process W+jets
rootfile Zee/ZeeNormalizedHistos_IMP_"$missingCut".root
process Z#rightarrowee
rootfile Ztautau/ZtautauNormalizedHistos_IMP_"$missingCut".root
process Z#rightarrow#tau#tau
rootfile Zprime500/Zprime500NormalizedHistos_IMP_"$missingCut".root
process Z'#rightarrow#tau#tau
luminosity 36.15
outputRootFile results/stackedDataAndMC_"$missingCut".root
"
	echo "$output" > "stackedDataAndMC_"$missingCut".in"
}

# List of cuts
#cuts=("CosDelPhi" "GammaIso" "MET" "OS" "Prongs" "TauEta" "TauPt" "SeedTrackPt" "TrkIso")
cuts=("CosDelPhi" "MET" "OS" "Tau1Prongs" "Tau2Prongs" "Tau1Eta" "Tau2Eta" "Tau1Pt" "Tau2Pt" "Tau1SeedPt" "Tau2SeedPt" "Tau1TrkIso" "Tau2TrkIso" "Tau1GammaIso" "Tau2GammaIso" )


# Remove any old list of execution lines (root calls), since we're producing a new one
rm -f "stackDataAndMC.sh";
rm -f stackedDataAndMC*.root
rm -f stacker.std*


echo "rm -rf stacked*.root" > "stackDataAndMC.sh"
# For each cut, generate a .in file
for missingCut in ${cuts[@]}; do
	generateOutput

	# Also add a line to the root call list with the call to the stack plotter and appropriate .in file for the cut
	echo "root -q -l -b Plotter_MCandDATA.C\(\\\"stackedDataAndMC_"$missingCut".in\\\"\) 1>> stacker.stdout 2>> stacker.stderr" >> "stackDataAndMC.sh"


done

