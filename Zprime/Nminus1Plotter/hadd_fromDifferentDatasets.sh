#!/bin/bash

if [ -z $1 ]; then
	echo "ERROR: no directory specified. Please do!" 1>&2
	exit 1;
fi

# List of cuts
cuts=("CosDelPhi" "MET" "OS" "Tau1Prongs" "Tau2Prongs" "Tau1Eta" "Tau2Eta" "Tau1Pt" "Tau2Pt" "Tau1SeedPt" "Tau2SeedPt" "Tau1TrkIso" "Tau2TrkIso" "Tau1GammaIso" "Tau2GammaIso" )

datasets=( "MinimumBias_141878to141949" "JetMET" "BTau" )

dir=${1%%\/}

# For each cut, generate a .in file
for missingCut in ${cuts[@]}; do

	inFiles=""	
	for dataset in ${datasets[@]}; do
		inFiles="$inFiles "$dir"/"$dataset"_"$missingCut".root"
	done

	hadd -f $dir"/"$dir"_N_1_"$missingCut".root" $inFiles
	

done

