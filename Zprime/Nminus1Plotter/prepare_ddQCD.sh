rm -rf ddQCDfinal/ && mkdir ddQCDfinal
root -q -l -b addHistos.C\(\"prepare_ddQCD_CosDelPhi.in\"\)
root -q -l -b addHistos.C\(\"prepare_ddQCD_GammaIso.in\"\)
root -q -l -b addHistos.C\(\"prepare_ddQCD_MET.in\"\)
root -q -l -b addHistos.C\(\"prepare_ddQCD_OS.in\"\)
root -q -l -b addHistos.C\(\"prepare_ddQCD_Prongs.in\"\)
root -q -l -b addHistos.C\(\"prepare_ddQCD_TauEta.in\"\)
root -q -l -b addHistos.C\(\"prepare_ddQCD_TauPt.in\"\)
root -q -l -b addHistos.C\(\"prepare_ddQCD_SeedTrackPt.in\"\)
root -q -l -b addHistos.C\(\"prepare_ddQCD_TrkIso.in\"\)
