rm -rf stacked*.root
root -q -l -b Plotter_MCandDATA.C\(\"stackedDataAndMC_CosDelPhi.in\"\) 1>> stacker.stdout 2>> stacker.stderr
root -q -l -b Plotter_MCandDATA.C\(\"stackedDataAndMC_GammaIso.in\"\) 1>> stacker.stdout 2>> stacker.stderr
root -q -l -b Plotter_MCandDATA.C\(\"stackedDataAndMC_MET.in\"\) 1>> stacker.stdout 2>> stacker.stderr
root -q -l -b Plotter_MCandDATA.C\(\"stackedDataAndMC_OS.in\"\) 1>> stacker.stdout 2>> stacker.stderr
root -q -l -b Plotter_MCandDATA.C\(\"stackedDataAndMC_Prongs.in\"\) 1>> stacker.stdout 2>> stacker.stderr
root -q -l -b Plotter_MCandDATA.C\(\"stackedDataAndMC_TauEta.in\"\) 1>> stacker.stdout 2>> stacker.stderr
root -q -l -b Plotter_MCandDATA.C\(\"stackedDataAndMC_TauPt.in\"\) 1>> stacker.stdout 2>> stacker.stderr
root -q -l -b Plotter_MCandDATA.C\(\"stackedDataAndMC_SeedTrackPt.in\"\) 1>> stacker.stdout 2>> stacker.stderr
root -q -l -b Plotter_MCandDATA.C\(\"stackedDataAndMC_TrkIso.in\"\) 1>> stacker.stdout 2>> stacker.stderr
