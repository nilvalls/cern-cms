import FWCore.ParameterSet.Config as cms

selectedElectrons = cms.EDFilter("GsfElectronSelector",
  src = cms.InputTag("gsfElectrons"),
  cut = cms.string("pt > 8 & abs(eta) < 2.5"),
  filter = cms.bool(True)
)

skimmedPATElectrons = cms.EDFilter("PATElectronSelector",
  src = cms.InputTag("selectedPatElectrons"),
  cut = cms.string("pt > 15 & abs(eta) < 2.1"),
  filter = cms.bool(True)
)
