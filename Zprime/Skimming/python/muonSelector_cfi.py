import FWCore.ParameterSet.Config as cms

selectedMuons = cms.EDFilter("MuonSelector",
  src = cms.InputTag('muons'),
  cut = cms.string("isGlobalMuon & pt > 15 & abs(eta) < 2.1"),
  filter = cms.bool(True)
)

skimmedPATMuons = cms.EDFilter("PATMuonSelector",
  src = cms.InputTag('selectedPatMuons'),
  cut = cms.string("isGlobalMuon & pt > 15 & abs(eta) < 2.1"),
  filter = cms.bool(True)
)
