import FWCore.ParameterSet.Config as cms

#--------------------------------------------------------------------------------
# select CaloTaus
#--------------------------------------------------------------------------------

selectedCaloTaus = cms.EDFilter("CaloTauSelector",
  src = cms.InputTag('caloRecoTauProducer'),
  discriminators = cms.VPSet(
    cms.PSet(
      discriminator = cms.InputTag("caloRecoTauDiscriminationByLeadingTrackPtCut"),
      selectionCut = cms.double(0.5)
    )
  ),
  filter = cms.bool(True)
)

#--------------------------------------------------------------------------------
# select PFTaus
#--------------------------------------------------------------------------------

selectedPFTaus = cms.EDFilter("PFTauSelector",
  src = cms.InputTag('shrinkingConePFTauProducer'),
  discriminators = cms.VPSet(
    cms.PSet(
      discriminator = cms.InputTag("shrinkingConePFTauDiscriminationByLeadingPionPtCut"),
      selectionCut = cms.double(0.5)
    )
  ),
  filter = cms.bool(True)
)

selectedHPSTaus = cms.EDFilter("PFTauSelector",
  src = cms.InputTag('hpsPFTauProducer'),
  discriminators = cms.VPSet(
    cms.PSet(
      discriminator = cms.InputTag("hpsPFTauDiscriminationByDecayModeFinding"),
      selectionCut = cms.double(0.5)      
    ),
    cms.PSet(
      discriminator = cms.InputTag("hpsPFTauDiscriminationByLooseIsolation"),
      selectionCut = cms.double(0.5)
    )
  ),
  cut = cms.string('pt > 20 && abs(eta) < 2.1'),
  filter = cms.bool(True)
)

selectedLooseHPSTaus = cms.EDFilter("PFTauSelector",
  src = cms.InputTag('hpsPFTauProducer'),
  discriminators = cms.VPSet(
    cms.PSet(
      discriminator = cms.InputTag("hpsPFTauDiscriminationByDecayModeFinding"),
      selectionCut = cms.double(0.5)      
    )
  ),
  cut = cms.string('pt > 10 && abs(eta) < 2.1'),
  filter = cms.bool(True)
)

selectedHPSPatTau = cms.EDFilter("PATTauSelector",
  src = cms.InputTag('selectedPatTaus'),
  cut = cms.string('pt > 20 & abs(eta) < 2.1 & tauID("decayModeFinding") > 0.5 & tauID("byLooseIsolationDeltaBetaCorr") > 0.5'),
  filter = cms.bool(True)
 )

selectedLooseHPSPatTau = cms.EDFilter("PATTauSelector",
  src = cms.InputTag('selectedPatTaus'),
  cut = cms.string('pt > 10 & abs(eta) < 2.1 & tauID("decayModeFinding") > 0.5'),
  filter = cms.bool(True)
 )
