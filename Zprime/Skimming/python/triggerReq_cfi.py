import FWCore.ParameterSet.Config as cms
import HLTrigger.HLTfilters.triggerResultsFilter_cfi as hlt

emuHLTFilter = hlt.triggerResultsFilter.clone(
    hltResults = cms.InputTag('TriggerResults::HLT'),
    triggerConditions = (
      'HLT_Mu17_Ele8_CaloIdL*',
      'HLT_Mu8_Ele17_CaloIdL*'
    ),
    l1tResults = '',
    throw = False
)

etauHLTFilter = hlt.triggerResultsFilter.clone(
    hltResults = cms.InputTag('TriggerResults::HLT'),
    triggerConditions = (
      'HLT_Ele15_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_LooseIsoPFTau15*',
      'HLT_Ele15_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_LooseIsoPFTau20*',
      'HLT_Ele18_CaloIdVT_CaloIsoT_TrkIdT_TrkIsoT_LooseIsoPFTau20*'
    ),
    l1tResults = '',
    throw = False
)

mutauHLTFilter = hlt.triggerResultsFilter.clone(
    hltResults = cms.InputTag('TriggerResults::HLT'),
    triggerConditions = (
      'HLT_Mu24',
      'HLT_Mu24*',
      'HLT_IsoMu17',
      'HLT_IsoMu17*',
      'HLT_IsoMu12_LooseIsoPFTau10',
      'HLT_IsoMu12_LooseIsoPFTau10*',
      'HLT_Mu15_LooseIsoPFTau20',
      'HLT_Mu15_LooseIsoPFTau20*',
    ),
    l1tResults = '',
    throw = False
)

tautauHLTFilter = hlt.triggerResultsFilter.clone(
    hltResults = cms.InputTag('TriggerResults::HLT'),
    triggerConditions = (
		'HLT_DoubleMediumIsoPFTau35_Trk5_eta2p1_Prong1_v*',
    ),
    l1tResults = '',
    throw = True
)

