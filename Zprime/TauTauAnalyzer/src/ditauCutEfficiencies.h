#include <TFile.h>
#include <TTree.h>
#include <TH1F.h>
#include <TCut.h>
#include <TObject.h>
#include <TROOT.h>
#include <vector>
#include <iostream>
#include <sstream>
#include <utility>
#include "TApplication.h"
TTree* getTheTree(std::string);

//======ePt

TH1F* ePt_Zp350            = new TH1F("ePt_Zp350",          "ePt_Zp350",          100, 0, 200);
TH1F* ePt_Zp500            = new TH1F("ePt_Zp500",          "ePt_Zp500",          100, 0, 200);
TH1F* ePt_Ztt              = new TH1F("ePt_Ztt",	    "ePt_Ztt",            100, 0, 200);
TH1F* ePt_Zee              = new TH1F("ePt_Zee",	    "ePt_Zee",            100, 0, 200);
TH1F* ePt_ttbar            = new TH1F("ePt_ttbar",          "ePt_ttbar",          100, 0, 200);
TH1F* ePt_wjets            = new TH1F("ePt_ttbar",          "ePt_wjets",          100, 0, 200);
TH1F* ePt_qcdEmPt20to30    = new TH1F("ePt_qcdEmPt20to30",  "ePt_qcdEmPt20to30",  100, 0, 200);
TH1F* ePt_qcdEmPt30to80    = new TH1F("ePt_qcdEmPt30to80",  "ePt_qcdEmPt30to80",  100, 0, 200);
TH1F* ePt_qcdEmPt80to170   = new TH1F("ePt_qcdEmPt80to170", "ePt_qcdEmPt80to170", 100, 0, 200);
TH1F* ePt_qcdBCPt20to30    = new TH1F("ePt_qcdBCPt20to30",  "ePt_qcdBCPt20to30",  100, 0, 200);
TH1F* ePt_qcdBCPt30to80    = new TH1F("ePt_qcdBCPt30to80",  "ePt_qcdBCPt30to80",  100, 0, 200);
TH1F* ePt_qcdBCPt80to170   = new TH1F("ePt_qcdBCPt80to170", "ePt_qcdBCPt80to170", 100, 0, 200);
TH1F* ePt_GjetsPt15to20    = new TH1F("ePt_GjetsPt15to20",  "ePt_GjetsPt15to20",  100, 0, 200);
TH1F* ePt_GjetsPt20to30    = new TH1F("ePt_GjetsPt20to30",  "ePt_GjetsPt20to30",  100, 0, 200);
TH1F* ePt_GjetsPt30to50    = new TH1F("ePt_GjetsPt30to50",  "ePt_GjetsPt30to50",  100, 0, 200);
TH1F* ePt_GjetsPt50to80    = new TH1F("ePt_GjetsPt50to80",  "ePt_GjetsPt50to80",  100, 0, 200);
TH1F* ePt_GjetsPt80to120   = new TH1F("ePt_GjetsPt80to120", "ePt_GjetsPt80to120", 100, 0, 200);
TH1F* ePt_GjetsPt120to170  = new TH1F("ePt_GjetsPt120to170","ePt_GjetsPt120to170",100, 0, 200);
TH1F* ePt_GjetsPt170to300  = new TH1F("ePt_GjetsPt170to300","ePt_GjetsPt170to300",100, 0, 200);

//=======Tau1 Pt

TH1F* tau1Pt_Zp350            = new TH1F("tau1Pt_Zp350",          "tau1Pt_Zp350",          100, 0, 200);
TH1F* tau1Pt_Zp500            = new TH1F("tau1Pt_Zp500",          "tau1Pt_Zp500",          100, 0, 200);
TH1F* tau1Pt_Ztt              = new TH1F("tau1Pt_Ztt",            "tau1Pt_Ztt",            100, 0, 200);
TH1F* tau1Pt_Zee              = new TH1F("tau1Pt_Zee",            "tau1Pt_Zee",            100, 0, 200);
TH1F* tau1Pt_ttbar            = new TH1F("tau1Pt_ttbar",          "tau1Pt_ttbar",          100, 0, 200);
TH1F* tau1Pt_wjets            = new TH1F("tau1Pt_ttbar",          "tau1Pt_wjets",          100, 0, 200);
TH1F* tau1Pt_qcdEmPt20to30    = new TH1F("tau1Pt_qcdEmPt20to30",  "tau1Pt_qcdEmPt20to30",  100, 0, 200);
TH1F* tau1Pt_qcdEmPt30to80    = new TH1F("tau1Pt_qcdEmPt30to80",  "tau1Pt_qcdEmPt30to80",  100, 0, 200);
TH1F* tau1Pt_qcdEmPt80to170   = new TH1F("tau1Pt_qcdEmPt80to170", "tau1Pt_qcdEmPt80to170", 100, 0, 200);
TH1F* tau1Pt_qcdBCPt20to30    = new TH1F("tau1Pt_qcdBCPt20to30",  "tau1Pt_qcdBCPt20to30",  100, 0, 200);
TH1F* tau1Pt_qcdBCPt30to80    = new TH1F("tau1Pt_qcdBCPt30to80",  "tau1Pt_qcdBCPt30to80",  100, 0, 200);
TH1F* tau1Pt_qcdBCPt80to170   = new TH1F("tau1Pt_qcdBCPt80to170", "tau1Pt_qcdBCPt80to170", 100, 0, 200);
TH1F* tau1Pt_GjetsPt15to20    = new TH1F("tau1Pt_GjetsPt15to20",  "tau1Pt_GjetsPt15to20",  100, 0, 200);
TH1F* tau1Pt_GjetsPt20to30    = new TH1F("tau1Pt_GjetsPt20to30",  "tau1Pt_GjetsPt20to30",  100, 0, 200);
TH1F* tau1Pt_GjetsPt30to50    = new TH1F("tau1Pt_GjetsPt30to50",  "tau1Pt_GjetsPt30to50",  100, 0, 200);
TH1F* tau1Pt_GjetsPt50to80    = new TH1F("tau1Pt_GjetsPt50to80",  "tau1Pt_GjetsPt50to80",  100, 0, 200);
TH1F* tau1Pt_GjetsPt80to120   = new TH1F("tau1Pt_GjetsPt80to120", "tau1Pt_GjetsPt80to120", 100, 0, 200);
TH1F* tau1Pt_GjetsPt120to170  = new TH1F("tau1Pt_GjetsPt120to170","tau1Pt_GjetsPt120to170",100, 0, 200);
TH1F* tau1Pt_GjetsPt170to300  = new TH1F("tau1Pt_GjetsPt170to300","tau1Pt_GjetsPt170to300",100, 0, 200);

//=======Tau2 Pt

TH1F* tau2Pt_Zp350            = new TH1F("tau2Pt_Zp350",          "tau2Pt_Zp350",          100, 0, 200);
TH1F* tau2Pt_Zp500            = new TH1F("tau2Pt_Zp500",          "tau2Pt_Zp500",          100, 0, 200);
TH1F* tau2Pt_Ztt              = new TH1F("tau2Pt_Ztt",            "tau2Pt_Ztt",            100, 0, 200);
TH1F* tau2Pt_Zee              = new TH1F("tau2Pt_Zee",            "tau2Pt_Zee",            100, 0, 200);
TH1F* tau2Pt_ttbar            = new TH1F("tau2Pt_ttbar",          "tau2Pt_ttbar",          100, 0, 200);
TH1F* tau2Pt_wjets            = new TH1F("tau2Pt_ttbar",          "tau2Pt_wjets",          100, 0, 200);
TH1F* tau2Pt_qcdEmPt20to30    = new TH1F("tau2Pt_qcdEmPt20to30",  "tau2Pt_qcdEmPt20to30",  100, 0, 200);
TH1F* tau2Pt_qcdEmPt30to80    = new TH1F("tau2Pt_qcdEmPt30to80",  "tau2Pt_qcdEmPt30to80",  100, 0, 200);
TH1F* tau2Pt_qcdEmPt80to170   = new TH1F("tau2Pt_qcdEmPt80to170", "tau2Pt_qcdEmPt80to170", 100, 0, 200);
TH1F* tau2Pt_qcdBCPt20to30    = new TH1F("tau2Pt_qcdBCPt20to30",  "tau2Pt_qcdBCPt20to30",  100, 0, 200);
TH1F* tau2Pt_qcdBCPt30to80    = new TH1F("tau2Pt_qcdBCPt30to80",  "tau2Pt_qcdBCPt30to80",  100, 0, 200);
TH1F* tau2Pt_qcdBCPt80to170   = new TH1F("tau2Pt_qcdBCPt80to170", "tau2Pt_qcdBCPt80to170", 100, 0, 200);
TH1F* tau2Pt_GjetsPt15to20    = new TH1F("tau2Pt_GjetsPt15to20",  "tau2Pt_GjetsPt15to20",  100, 0, 200);
TH1F* tau2Pt_GjetsPt20to30    = new TH1F("tau2Pt_GjetsPt20to30",  "tau2Pt_GjetsPt20to30",  100, 0, 200);
TH1F* tau2Pt_GjetsPt30to50    = new TH1F("tau2Pt_GjetsPt30to50",  "tau2Pt_GjetsPt30to50",  100, 0, 200);
TH1F* tau2Pt_GjetsPt50to80    = new TH1F("tau2Pt_GjetsPt50to80",  "tau2Pt_GjetsPt50to80",  100, 0, 200);
TH1F* tau2Pt_GjetsPt80to120   = new TH1F("tau2Pt_GjetsPt80to120", "tau2Pt_GjetsPt80to120", 100, 0, 200);
TH1F* tau2Pt_GjetsPt120to170  = new TH1F("tau2Pt_GjetsPt120to170","tau2Pt_GjetsPt120to170",100, 0, 200);
TH1F* tau2Pt_GjetsPt170to300  = new TH1F("tau2Pt_GjetsPt170to300","tau2Pt_GjetsPt170to300",100, 0, 200);

//---Invariant Mass

TH1F* M_Zp350            = new TH1F("M_Zp350",          "M_Zp350",          150, 0, 1500);
TH1F* M_Zp400            = new TH1F("M_Zp400",          "M_Zp400",          150, 0, 1500);
TH1F* M_Zp500            = new TH1F("M_Zp500",          "M_Zp500",          150, 0, 1500);
TH1F* M_Zp600            = new TH1F("M_Zp600",          "M_Zp600",          150, 0, 1500);
TH1F* M_Zp1000           = new TH1F("M_Zp1000",         "M_Zp1000",         150, 0, 1500);
TH1F* M_Ztt              = new TH1F("M_Ztt",            "M_Ztt",            150, 0, 1500);
TH1F* M_Zee              = new TH1F("M_Zee",            "M_Zee",            150, 0, 1500);
TH1F* M_ttbar            = new TH1F("M_ttbar",          "M_ttbar",          150, 0, 1500);
TH1F* M_wjets            = new TH1F("M_ttbar",          "M_wjets",          150, 0, 1500); 
TH1F* M_qcdEmPt20to30    = new TH1F("M_qcdEmPt20to30",  "M_qcdEmPt20to30",  150, 0, 1500);
TH1F* M_qcdEmPt30to80    = new TH1F("M_qcdEmPt30to80",  "M_qcdEmPt30to80",  150, 0, 1500);
TH1F* M_qcdEmPt80to170   = new TH1F("M_qcdEmPt80to170", "M_qcdEmPt80to170", 150, 0, 1500);
TH1F* M_qcdBCPt20to30    = new TH1F("M_qcdBCPt20to30",  "M_qcdBCPt20to30",  150, 0, 1500);
TH1F* M_qcdBCPt30to80    = new TH1F("M_qcdBCPt30to80",  "M_qcdBCPt30to80",  150, 0, 1500);
TH1F* M_qcdBCPt80to170   = new TH1F("M_qcdBCPt80to170", "M_qcdBCPt80to170", 150, 0, 1500);
TH1F* M_GjetsPt15to20    = new TH1F("M_GjetsPt15to20",  "M_GjetsPt15to20",  150, 0, 1500);
TH1F* M_GjetsPt20to30    = new TH1F("M_GjetsPt20to30",  "M_GjetsPt20to30",  150, 0, 1500);
TH1F* M_GjetsPt30to50    = new TH1F("M_GjetsPt30to50",  "M_GjetsPt30to50",  150, 0, 1500);
TH1F* M_GjetsPt50to80    = new TH1F("M_GjetsPt50to80",  "M_GjetsPt50to80",  150, 0, 1500);
TH1F* M_GjetsPt80to120   = new TH1F("M_GjetsPt80to120", "M_GjetsPt80to120", 150, 0, 1500);
TH1F* M_GjetsPt120to170  = new TH1F("M_GjetsPt120to170","M_GjetsPt120to170",150, 0, 1500);
TH1F* M_GjetsPt170to300  = new TH1F("M_GjetsPt170to300","M_GjetsPt170to300",150, 0, 1500);

 //----eTrkIso
 TH1F* eTrkIso_Zp350	       = new TH1F("eTrkIso_Zp350",	    "eTrkIso_Zp350",	      20, 0, 20);
 TH1F* eTrkIso_Zp500	       = new TH1F("eTrkIso_Zp500",	    "eTrkIso_Zp500",	      20, 0, 20);
 TH1F* eTrkIso_Ztt	       = new TH1F("eTrkIso_Ztt",	    "eTrkIso_Ztt",	      20, 0, 20);
 TH1F* eTrkIso_Zee	       = new TH1F("eTrkIso_Zee",	    "eTrkIso_Zee",	      20, 0, 20);
 TH1F* eTrkIso_ttbar	       = new TH1F("eTrkIso_ttbar",	    "eTrkIso_ttbar",	      20, 0, 20);
 TH1F* eTrkIso_wjets	       = new TH1F("eTrkIso_ttbar",	    "eTrkIso_wjets",	      20, 0, 20);
 TH1F* eTrkIso_qcdEmPt20to30    = new TH1F("eTrkIso_qcdEmPt20to30",  "eTrkIso_qcdEmPt20to30",  20, 0, 20);
 TH1F* eTrkIso_qcdEmPt30to80    = new TH1F("eTrkIso_qcdEmPt30to80",  "eTrkIso_qcdEmPt30to80",  20, 0, 20);
 TH1F* eTrkIso_qcdEmPt80to170   = new TH1F("eTrkIso_qcdEmPt80to170", "eTrkIso_qcdEmPt80to170", 20, 0, 20);
 TH1F* eTrkIso_qcdBCPt20to30    = new TH1F("eTrkIso_qcdBCPt20to30",  "eTrkIso_qcdBCPt20to30",  20, 0, 20);
 TH1F* eTrkIso_qcdBCPt30to80    = new TH1F("eTrkIso_qcdBCPt30to80",  "eTrkIso_qcdBCPt30to80",  20, 0, 20);
 TH1F* eTrkIso_qcdBCPt80to170   = new TH1F("eTrkIso_qcdBCPt80to170", "eTrkIso_qcdBCPt80to170", 20, 0, 20);
 TH1F* eTrkIso_GjetsPt15to20    = new TH1F("eTrkIso_GjetsPt15to20",  "eTrkIso_GjetsPt15to20",  20, 0, 20);
 TH1F* eTrkIso_GjetsPt20to30    = new TH1F("eTrkIso_GjetsPt20to30",  "eTrkIso_GjetsPt20to30",  20, 0, 20);
 TH1F* eTrkIso_GjetsPt30to50    = new TH1F("eTrkIso_GjetsPt30to50",  "eTrkIso_GjetsPt30to50",  20, 0, 20);
 TH1F* eTrkIso_GjetsPt50to80    = new TH1F("eTrkIso_GjetsPt50to80",  "eTrkIso_GjetsPt50to80",  20, 0, 20);
 TH1F* eTrkIso_GjetsPt80to120   = new TH1F("eTrkIso_GjetsPt80to120", "eTrkIso_GjetsPt80to120", 20, 0, 20);
 TH1F* eTrkIso_GjetsPt120to170  = new TH1F("eTrkIso_GjetsPt120to170","eTrkIso_GjetsPt120to170",20, 0, 20);
 TH1F* eTrkIso_GjetsPt170to300  = new TH1F("eTrkIso_GjetsPt170to300","eTrkIso_GjetsPt170to170",20, 0, 20);
   
     
	  //----eEcalIso
	  TH1F* eEcalIso_Zp350		= new TH1F("eEcalIso_Zp350",	      "eEcalIso_Zp350", 	 20, 0, 20);
	  TH1F* eEcalIso_Zp500		= new TH1F("eEcalIso_Zp500",	      "eEcalIso_Zp500", 	 20, 0, 20);
	  TH1F* eEcalIso_Ztt		= new TH1F("eEcalIso_Ztt",	      "eEcalIso_Ztt",		 20, 0, 20);
	  TH1F* eEcalIso_Zee		= new TH1F("eEcalIso_Zee",	      "eEcalIso_Zee",		 20, 0, 20);
	  TH1F* eEcalIso_ttbar		= new TH1F("eEcalIso_ttbar",	      "eEcalIso_ttbar", 	 20, 0, 20);
	  TH1F* eEcalIso_wjets		= new TH1F("eEcalIso_ttbar",	      "eEcalIso_wjets", 	 20, 0, 20);
	  TH1F* eEcalIso_qcdEmPt20to30	= new TH1F("eEcalIso_qcdEmPt20to30",  "eEcalIso_qcdEmPt20to30",  20, 0, 20);
	  TH1F* eEcalIso_qcdEmPt30to80	= new TH1F("eEcalIso_qcdEmPt30to80",  "eEcalIso_qcdEmPt30to80",  20, 0, 20);
	  TH1F* eEcalIso_qcdEmPt80to170	= new TH1F("eEcalIso_qcdEmPt80to170", "eEcalIso_qcdEmPt80to170", 20, 0, 20);
	  TH1F* eEcalIso_qcdBCPt20to30	= new TH1F("eEcalIso_qcdBCPt20to30",  "eEcalIso_qcdBCPt20to30",  20, 0, 20);
	  TH1F* eEcalIso_qcdBCPt30to80	= new TH1F("eEcalIso_qcdBCPt30to80",  "eEcalIso_qcdBCPt30to80",  20, 0, 20);
	  TH1F* eEcalIso_qcdBCPt80to170	= new TH1F("eEcalIso_qcdBCPt80to170", "eEcalIso_qcdBCPt80to170", 20, 0, 20);
	  TH1F* eEcalIso_GjetsPt15to20	= new TH1F("eEcalIso_GjetsPt15to20",  "eEcalIso_GjetsPt15to20",  20, 0, 20);
	  TH1F* eEcalIso_GjetsPt20to30	= new TH1F("eEcalIso_GjetsPt20to30",  "eEcalIso_GjetsPt20to30",  20, 0, 20);
	  TH1F* eEcalIso_GjetsPt30to50	= new TH1F("eEcalIso_GjetsPt30to50",  "eEcalIso_GjetsPt30to50",  20, 0, 20);
	  TH1F* eEcalIso_GjetsPt50to80	= new TH1F("eEcalIso_GjetsPt50to80",  "eEcalIso_GjetsPt50to80",  20, 0, 20);
	  TH1F* eEcalIso_GjetsPt80to120	= new TH1F("eEcalIso_GjetsPt80to120", "eEcalIso_GjetsPt80to120", 20, 0, 20);
	  TH1F* eEcalIso_GjetsPt120to170  = new TH1F("eEcalIso_GjetsPt120to170","eEcalIso_GjetsPt120to170",20, 0, 20);
	  TH1F* eEcalIso_GjetsPt170to300  = new TH1F("eEcalIso_GjetsPt170to300","eEcalIso_GjetsPt170to170",20, 0, 20);

	  //===E/P EndCap

	  TH1F* eEoverP_ec_Zp350            = new TH1F("eEoverP_ec_Zp350",	  "eEoverP_ec_Zp350",	       20, 0, 2);
	  TH1F* eEoverP_ec_Zp500            = new TH1F("eEoverP_ec_Zp500",	  "eEoverP_ec_Zp500",	       20, 0, 2);
	  TH1F* eEoverP_ec_Ztt		  = new TH1F("eEoverP_ec_Ztt",  	  "eEoverP_ec_Ztt",	       20, 0, 2);
	  TH1F* eEoverP_ec_Zee		  = new TH1F("eEoverP_ec_Zee",  	  "eEoverP_ec_Zee",	       20, 0, 2);
	  TH1F* eEoverP_ec_ttbar  	  = new TH1F("eEoverP_ec_ttbar",	  "eEoverP_ec_ttbar",	       20, 0, 2);
	  TH1F* eEoverP_ec_wjets  	  = new TH1F("eEoverP_ec_ttbar",	  "eEoverP_ec_wjets",	       20, 0, 2);
	  TH1F* eEoverP_ec_qcdEmPt20to30    = new TH1F("eEoverP_ec_qcdEmPt20to30",  "eEoverP_ec_qcdEmPt20to30",  20, 0, 2);
	  TH1F* eEoverP_ec_qcdEmPt30to80    = new TH1F("eEoverP_ec_qcdEmPt30to80",  "eEoverP_ec_qcdEmPt30to80",  20, 0, 2);
	  TH1F* eEoverP_ec_qcdEmPt80to170   = new TH1F("eEoverP_ec_qcdEmPt80to170", "eEoverP_ec_qcdEmPt80to170", 20, 0, 2);
	  TH1F* eEoverP_ec_qcdBCPt20to30    = new TH1F("eEoverP_ec_qcdBCPt20to30",  "eEoverP_ec_qcdBCPt20to30",  20, 0, 2);
	  TH1F* eEoverP_ec_qcdBCPt30to80    = new TH1F("eEoverP_ec_qcdBCPt30to80",  "eEoverP_ec_qcdBCPt30to80",  20, 0, 2);
	  TH1F* eEoverP_ec_qcdBCPt80to170   = new TH1F("eEoverP_ec_qcdBCPt80to170", "eEoverP_ec_qcdBCPt80to170", 20, 0, 2);
	  TH1F* eEoverP_ec_GjetsPt15to20    = new TH1F("eEoverP_ec_GjetsPt15to20",  "eEoverP_ec_GjetsPt15to20",  20, 0, 2);
	  TH1F* eEoverP_ec_GjetsPt20to30    = new TH1F("eEoverP_ec_GjetsPt20to30",  "eEoverP_ec_GjetsPt20to30",  20, 0, 2);
	  TH1F* eEoverP_ec_GjetsPt30to50    = new TH1F("eEoverP_ec_GjetsPt30to50",  "eEoverP_ec_GjetsPt30to50",  20, 0, 2);
	  TH1F* eEoverP_ec_GjetsPt50to80    = new TH1F("eEoverP_ec_GjetsPt50to80",  "eEoverP_ec_GjetsPt50to80",  20, 0, 2);
	  TH1F* eEoverP_ec_GjetsPt80to120   = new TH1F("eEoverP_ec_GjetsPt80to120", "eEoverP_ec_GjetsPt80to120", 20, 0, 2);
	  TH1F* eEoverP_ec_GjetsPt120to170  = new TH1F("eEoverP_ec_GjetsPt120to170","eEoverP_ec_GjetsPt120to170",20, 0, 2);
	  TH1F* eEoverP_ec_GjetsPt170to300  = new TH1F("eEoverP_ec_GjetsPt170to300","eEoverP_ec_GjetsPt170to170",20, 0, 2);

	  //===E/P Barrel

	  TH1F* eEoverP_b_Zp350		 = new TH1F("eEoverP_b_Zp350",  	"eEoverP_b_Zp350",	    20, 0, 2);
	  TH1F* eEoverP_b_Zp500		 = new TH1F("eEoverP_b_Zp500",  	"eEoverP_b_Zp500",	    20, 0, 2);
	  TH1F* eEoverP_b_Ztt		 = new TH1F("eEoverP_b_Ztt",		"eEoverP_b_Ztt",	    20, 0, 2);
	  TH1F* eEoverP_b_Zee		 = new TH1F("eEoverP_b_Zee",		"eEoverP_b_Zee",	    20, 0, 2);
	  TH1F* eEoverP_b_ttbar		 = new TH1F("eEoverP_b_ttbar",  	"eEoverP_b_ttbar",	    20, 0, 2);
	  TH1F* eEoverP_b_wjets		 = new TH1F("eEoverP_b_ttbar",  	"eEoverP_b_wjets",	    20, 0, 2);
	  TH1F* eEoverP_b_qcdEmPt20to30	 = new TH1F("eEoverP_b_qcdEmPt20to30",  "eEoverP_b_qcdEmPt20to30",  20, 0, 2);
	  TH1F* eEoverP_b_qcdEmPt30to80	 = new TH1F("eEoverP_b_qcdEmPt30to80",  "eEoverP_b_qcdEmPt30to80",  20, 0, 2);
	  TH1F* eEoverP_b_qcdEmPt80to170   = new TH1F("eEoverP_b_qcdEmPt80to170", "eEoverP_b_qcdEmPt80to170", 20, 0, 2);
	  TH1F* eEoverP_b_qcdBCPt20to30	 = new TH1F("eEoverP_b_qcdBCPt20to30",  "eEoverP_b_qcdBCPt20to30",  20, 0, 2);
	  TH1F* eEoverP_b_qcdBCPt30to80	 = new TH1F("eEoverP_b_qcdBCPt30to80",  "eEoverP_b_qcdBCPt30to80",  20, 0, 2);
	  TH1F* eEoverP_b_qcdBCPt80to170   = new TH1F("eEoverP_b_qcdBCPt80to170", "eEoverP_b_qcdBCPt80to170", 20, 0, 2);
	  TH1F* eEoverP_b_GjetsPt15to20	 = new TH1F("eEoverP_b_GjetsPt15to20",  "eEoverP_b_GjetsPt15to20",  20, 0, 2);
	  TH1F* eEoverP_b_GjetsPt20to30	 = new TH1F("eEoverP_b_GjetsPt20to30",  "eEoverP_b_GjetsPt20to30",  20, 0, 2);
	  TH1F* eEoverP_b_GjetsPt30to50	 = new TH1F("eEoverP_b_GjetsPt30to50",  "eEoverP_b_GjetsPt30to50",  20, 0, 2);
	  TH1F* eEoverP_b_GjetsPt50to80	 = new TH1F("eEoverP_b_GjetsPt50to80",  "eEoverP_b_GjetsPt50to80",  20, 0, 2);
	  TH1F* eEoverP_b_GjetsPt80to120   = new TH1F("eEoverP_b_GjetsPt80to120", "eEoverP_b_GjetsPt80to120", 20, 0, 2);
	  TH1F* eEoverP_b_GjetsPt120to170  = new TH1F("eEoverP_b_GjetsPt120to170","eEoverP_b_GjetsPt120to170",20, 0, 2);
	  TH1F* eEoverP_b_GjetsPt170to300  = new TH1F("eEoverP_b_GjetsPt170to300","eEoverP_b_GjetsPt170to170",20, 0, 2);

	  //===H/E EndCap

	  TH1F* eHoverE_ec_Zp350            = new TH1F("eHoverE_ec_Zp350",	  "eHoverE_ec_Zp350",	       50, 0, 0.1);
	  TH1F* eHoverE_ec_Zp500            = new TH1F("eHoverE_ec_Zp500",	  "eHoverE_ec_Zp500",	       50, 0, 0.1);
	  TH1F* eHoverE_ec_Ztt		  = new TH1F("eHoverE_ec_Ztt",  	  "eHoverE_ec_Ztt",	       50, 0, 0.1);
	  TH1F* eHoverE_ec_Zee		  = new TH1F("eHoverE_ec_Zee",  	  "eHoverE_ec_Zee",	       50, 0, 0.1);
	  TH1F* eHoverE_ec_ttbar  	  = new TH1F("eHoverE_ec_ttbar",	  "eHoverE_ec_ttbar",	       50, 0, 0.1);
	  TH1F* eHoverE_ec_wjets  	  = new TH1F("eHoverE_ec_ttbar",	  "eHoverE_ec_wjets",	       50, 0, 0.1);
	  TH1F* eHoverE_ec_qcdEmPt20to30    = new TH1F("eHoverE_ec_qcdEmPt20to30",  "eHoverE_ec_qcdEmPt20to30",  50, 0, 0.1);
	  TH1F* eHoverE_ec_qcdEmPt30to80    = new TH1F("eHoverE_ec_qcdEmPt30to80",  "eHoverE_ec_qcdEmPt30to80",  50, 0, 0.1);
	  TH1F* eHoverE_ec_qcdEmPt80to170   = new TH1F("eHoverE_ec_qcdEmPt80to170", "eHoverE_ec_qcdEmPt80to170", 50, 0, 0.1);
	  TH1F* eHoverE_ec_qcdBCPt20to30    = new TH1F("eHoverE_ec_qcdBCPt20to30",  "eHoverE_ec_qcdBCPt20to30",  50, 0, 0.1);
	  TH1F* eHoverE_ec_qcdBCPt30to80    = new TH1F("eHoverE_ec_qcdBCPt30to80",  "eHoverE_ec_qcdBCPt30to80",  50, 0, 0.1);
	  TH1F* eHoverE_ec_qcdBCPt80to170   = new TH1F("eHoverE_ec_qcdBCPt80to170", "eHoverE_ec_qcdBCPt80to170", 50, 0, 0.1);
	  TH1F* eHoverE_ec_GjetsPt15to20    = new TH1F("eHoverE_ec_GjetsPt15to20",  "eHoverE_ec_GjetsPt15to20",  50, 0, 0.1);
	  TH1F* eHoverE_ec_GjetsPt20to30    = new TH1F("eHoverE_ec_GjetsPt20to30",  "eHoverE_ec_GjetsPt20to30",  50, 0, 0.1);
	  TH1F* eHoverE_ec_GjetsPt30to50    = new TH1F("eHoverE_ec_GjetsPt30to50",  "eHoverE_ec_GjetsPt30to50",  50, 0, 0.1);
	  TH1F* eHoverE_ec_GjetsPt50to80    = new TH1F("eHoverE_ec_GjetsPt50to80",  "eHoverE_ec_GjetsPt50to80",  50, 0, 0.1);
	  TH1F* eHoverE_ec_GjetsPt80to120   = new TH1F("eHoverE_ec_GjetsPt80to120", "eHoverE_ec_GjetsPt80to120", 50, 0, 0.1);
	  TH1F* eHoverE_ec_GjetsPt120to170  = new TH1F("eHoverE_ec_GjetsPt120to170","eHoverE_ec_GjetsPt120to170",50, 0, 0.1);
	  TH1F* eHoverE_ec_GjetsPt170to300  = new TH1F("eHoverE_ec_GjetsPt170to300","eHoverE_ec_GjetsPt170to170",50, 0, 0.1);

	  //===H/E Barrel

	  TH1F* eHoverE_b_Zp350		 = new TH1F("eHoverE_b_Zp350",  	"eHoverE_b_Zp350",	    50, 0, 0.1);
	  TH1F* eHoverE_b_Zp500		 = new TH1F("eHoverE_b_Zp500",  	"eHoverE_b_Zp500",	    50, 0, 0.1);
	  TH1F* eHoverE_b_Ztt		 = new TH1F("eHoverE_b_Ztt",		"eHoverE_b_Ztt",	    50, 0, 0.1);
	  TH1F* eHoverE_b_Zee		 = new TH1F("eHoverE_b_Zee",		"eHoverE_b_Zee",	    50, 0, 0.1);
	  TH1F* eHoverE_b_ttbar		 = new TH1F("eHoverE_b_ttbar",  	"eHoverE_b_ttbar",	    50, 0, 0.1);
	  TH1F* eHoverE_b_wjets		 = new TH1F("eHoverE_b_ttbar",  	"eHoverE_b_wjets",	    50, 0, 0.1);
	  TH1F* eHoverE_b_qcdEmPt20to30	 = new TH1F("eHoverE_b_qcdEmPt20to30",  "eHoverE_b_qcdEmPt20to30",  50, 0, 0.1);
	  TH1F* eHoverE_b_qcdEmPt30to80	 = new TH1F("eHoverE_b_qcdEmPt30to80",  "eHoverE_b_qcdEmPt30to80",  50, 0, 0.1);
	  TH1F* eHoverE_b_qcdEmPt80to170   = new TH1F("eHoverE_b_qcdEmPt80to170", "eHoverE_b_qcdEmPt80to170", 50, 0, 0.1);
	  TH1F* eHoverE_b_qcdBCPt20to30	 = new TH1F("eHoverE_b_qcdBCPt20to30",  "eHoverE_b_qcdBCPt20to30",  50, 0, 0.1);
	  TH1F* eHoverE_b_qcdBCPt30to80	 = new TH1F("eHoverE_b_qcdBCPt30to80",  "eHoverE_b_qcdBCPt30to80",  50, 0, 0.1);
	  TH1F* eHoverE_b_qcdBCPt80to170   = new TH1F("eHoverE_b_qcdBCPt80to170", "eHoverE_b_qcdBCPt80to170", 50, 0, 0.1);
	  TH1F* eHoverE_b_GjetsPt15to20	 = new TH1F("eHoverE_b_GjetsPt15to20",  "eHoverE_b_GjetsPt15to20",  50, 0, 0.1);
	  TH1F* eHoverE_b_GjetsPt20to30	 = new TH1F("eHoverE_b_GjetsPt20to30",  "eHoverE_b_GjetsPt20to30",  50, 0, 0.1);
	  TH1F* eHoverE_b_GjetsPt30to50	 = new TH1F("eHoverE_b_GjetsPt30to50",  "eHoverE_b_GjetsPt30to50",  50, 0, 0.1);
	  TH1F* eHoverE_b_GjetsPt50to80	 = new TH1F("eHoverE_b_GjetsPt50to80",  "eHoverE_b_GjetsPt50to80",  50, 0, 0.1);
	  TH1F* eHoverE_b_GjetsPt80to120   = new TH1F("eHoverE_b_GjetsPt80to120", "eHoverE_b_GjetsPt80to120", 50, 0, 0.1);
	  TH1F* eHoverE_b_GjetsPt120to170  = new TH1F("eHoverE_b_GjetsPt120to170","eHoverE_b_GjetsPt120to170",50, 0, 0.1);
	  TH1F* eHoverE_b_GjetsPt170to300  = new TH1F("eHoverE_b_GjetsPt170to300","eHoverE_b_GjetsPt170to170",50, 0, 0.1);

	  //---tau1 Iso
	  TH1F* tau1Iso_Zp350            = new TH1F("tau1Iso_Zp350",          "tau1Iso_Zp350",          50, 0, 50);
	  TH1F* tau1Iso_Zp500            = new TH1F("tau1Iso_Zp500",          "tau1Iso_Zp500",          50, 0, 50);
	  TH1F* tau1Iso_Ztt              = new TH1F("tau1Iso_Ztt",            "tau1Iso_Ztt",            50, 0, 50);
	  TH1F* tau1Iso_Zee              = new TH1F("tau1Iso_Zee",            "tau1Iso_Zee",            50, 0, 50);
	  TH1F* tau1Iso_ttbar            = new TH1F("tau1Iso_ttbar",          "tau1Iso_ttbar",          50, 0, 50);
	  TH1F* tau1Iso_wjets            = new TH1F("tau1Iso_ttbar",          "tau1Iso_wjets",          50, 0, 50);
	  TH1F* tau1Iso_qcdEmPt20to30    = new TH1F("tau1Iso_qcdEmPt20to30",  "tau1Iso_qcdEmPt20to30",  50, 0, 50);
	  TH1F* tau1Iso_qcdEmPt30to80    = new TH1F("tau1Iso_qcdEmPt30to80",  "tau1Iso_qcdEmPt30to80",  50, 0, 50);
	  TH1F* tau1Iso_qcdEmPt80to170   = new TH1F("tau1Iso_qcdEmPt80to170", "tau1Iso_qcdEmPt80to170", 50, 0, 50);
	  TH1F* tau1Iso_qcdBCPt20to30    = new TH1F("tau1Iso_qcdBCPt20to30",  "tau1Iso_qcdBCPt20to30",  50, 0, 50);
	  TH1F* tau1Iso_qcdBCPt30to80    = new TH1F("tau1Iso_qcdBCPt30to80",  "tau1Iso_qcdBCPt30to80",  50, 0, 50);
	  TH1F* tau1Iso_qcdBCPt80to170   = new TH1F("tau1Iso_qcdBCPt80to170", "tau1Iso_qcdBCPt80to170", 50, 0, 50);
	  TH1F* tau1Iso_GjetsPt15to20    = new TH1F("tau1Iso_GjetsPt15to20",  "tau1Iso_GjetsPt15to20",  50, 0, 50);
	  TH1F* tau1Iso_GjetsPt20to30    = new TH1F("tau1Iso_GjetsPt20to30",  "tau1Iso_GjetsPt20to30",  50, 0, 50);
	  TH1F* tau1Iso_GjetsPt30to50    = new TH1F("tau1Iso_GjetsPt30to50",  "tau1Iso_GjetsPt30to50",  50, 0, 50);
	  TH1F* tau1Iso_GjetsPt50to80    = new TH1F("tau1Iso_GjetsPt50to80",  "tau1Iso_GjetsPt50to80",  50, 0, 50);
	  TH1F* tau1Iso_GjetsPt80to120   = new TH1F("tau1Iso_GjetsPt80to120", "tau1Iso_GjetsPt80to120", 50, 0, 50);
	  TH1F* tau1Iso_GjetsPt120to170  = new TH1F("tau1Iso_GjetsPt120to170","tau1Iso_GjetsPt120to170",50, 0, 50);
	  TH1F* tau1Iso_GjetsPt170to300  = new TH1F("tau1Iso_GjetsPt170to300","tau1Iso_GjetsPt170to170",50, 0, 50);

	  //=======tau1 N prongs
	  TH1F* tau1Nprongs_Zp500           = new TH1F("tau1NProngs_Zp500",          "tau1 N Prongs", 10, 0, 10);
	  TH1F* tau1Nprongs_Zp350           = new TH1F("tau1NProngs_Zp350",          "tau1 N Prongs", 10, 0, 10);
	  TH1F* tau1Nprongs_Ztt             = new TH1F("tau1NProngs_Ztt"  ,          "tau1 N Prongs", 10, 0, 10);
	  TH1F* tau1Nprongs_Zee             = new TH1F("tau1NProngs_Zee"  ,          "tau1 N Prongs", 10, 0, 10);
	  TH1F* tau1Nprongs_ttbar           = new TH1F("tau1NProngs_ttbar",          "tau1 N Prongs", 10, 0, 10);
	  TH1F* tau1Nprongs_wjets           = new TH1F("tau1NProngs_wjets",          "tau1 N Prongs", 10, 0, 10);
	  TH1F* tau1Nprongs_qcdEmPt20to30   = new TH1F("tau1NProngs_qcdEmPt20to30"  ,"tau1 N Prongs", 10, 0, 10);
	  TH1F* tau1Nprongs_qcdEmPt30to80   = new TH1F("tau1NProngs_qcdEmPt30to80"  ,"tau1 N Prongs", 10, 0, 10);
	  TH1F* tau1Nprongs_qcdEmPt80to170  = new TH1F("tau1NProngs_qcdEmPt80to170" ,"tau1 N Prongs", 10, 0, 10);
	  TH1F* tau1Nprongs_qcdBCPt20to30   = new TH1F("tau1NProngs_qcdBCPt20to30"  ,"tau1 N Prongs", 10, 0, 10);
	  TH1F* tau1Nprongs_qcdBCPt30to80   = new TH1F("tau1NProngs_qcdBCPt30to80"  ,"tau1 N Prongs", 10, 0, 10);
	  TH1F* tau1Nprongs_qcdBCPt80to170  = new TH1F("tau1NProngs_qcdBCPt80to170" ,"tau1 N Prongs", 10, 0, 10);
	  TH1F* tau1Nprongs_GjetsPt15to20   = new TH1F("tau1Nprongs_GjetsPt15to20"  ,"tau1 N Prongs", 10, 0, 10);
	  TH1F* tau1Nprongs_GjetsPt20to30   = new TH1F("tau1Nprongs_GjetsPt20to30"  ,"tau1 N Prongs", 10, 0, 10);
	  TH1F* tau1Nprongs_GjetsPt30to50   = new TH1F("tau1Nprongs_GjetsPt30to50"  ,"tau1 N Prongs", 10, 0, 10);
	  TH1F* tau1Nprongs_GjetsPt50to80   = new TH1F("tau1Nprongs_GjetsPt50to80"  ,"tau1 N Prongs", 10, 0, 10);
	  TH1F* tau1Nprongs_GjetsPt80to120  = new TH1F("tau1Nprongs_GjetsPt80to120" ,"tau1 N Prongs", 10, 0, 10);
	  TH1F* tau1Nprongs_GjetsPt120to170 = new TH1F("tau1Nprongs_GjetsPt120to170","tau1 N Prongs", 10, 0, 10);
	  TH1F* tau1Nprongs_GjetsPt170to300 = new TH1F("tau1Nprongs_GjetsPt170to300","tau1 N Prongs", 10, 0, 10);

	  TH1F* Resoltau1Pt_Ztt = new TH1F("Resoltau1Pt_Ztt", "tau1 Et Resolution", 200, -1,1);
	  TH1F* ResolElecPt_Ztt = new TH1F("ResolElecPt_Ztt", "e Et Resolution", 200, -1,1);


	  //=======tau1 Eta

	  TH1F* tau1Eta_Zp350            = new TH1F("tau1Eta_Zp350",          "tau1Eta_Zp350",          20, -2.5, 2.5);
	  TH1F* tau1Eta_Zp500            = new TH1F("tau1Eta_Zp500",          "tau1Eta_Zp500",          20, -2.5, 2.5);
	  TH1F* tau1Eta_Ztt              = new TH1F("tau1Eta_Ztt",            "tau1Eta_Ztt",            20, -2.5, 2.5);
	  TH1F* tau1Eta_Zee              = new TH1F("tau1Eta_Zee",            "tau1Eta_Zee",            20, -2.5, 2.5);
	  TH1F* tau1Eta_ttbar            = new TH1F("tau1Eta_ttbar",          "tau1Eta_ttbar",          20, -2.5, 2.5);
	  TH1F* tau1Eta_wjets            = new TH1F("tau1Eta_ttbar",          "tau1Eta_wjets",          20, -2.5, 2.5);
	  TH1F* tau1Eta_qcdEmPt20to30    = new TH1F("tau1Eta_qcdEmPt20to30",  "tau1Eta_qcdEmPt20to30",  20, -2.5, 2.5);
	  TH1F* tau1Eta_qcdEmPt30to80    = new TH1F("tau1Eta_qcdEmPt30to80",  "tau1Eta_qcdEmPt30to80",  20, -2.5, 2.5);
	  TH1F* tau1Eta_qcdEmPt80to170   = new TH1F("tau1Eta_qcdEmPt80to170", "tau1Eta_qcdEmPt80to170", 20, -2.5, 2.5);
	  TH1F* tau1Eta_qcdBCPt20to30    = new TH1F("tau1Eta_qcdBCPt20to30",  "tau1Eta_qcdBCPt20to30",  20, -2.5, 2.5);
	  TH1F* tau1Eta_qcdBCPt30to80    = new TH1F("tau1Eta_qcdBCPt30to80",  "tau1Eta_qcdBCPt30to80",  20, -2.5, 2.5);
	  TH1F* tau1Eta_qcdBCPt80to170   = new TH1F("tau1Eta_qcdBCPt80to170", "tau1Eta_qcdBCPt80to170", 20, -2.5, 2.5);
	  TH1F* tau1Eta_GjetsPt15to20    = new TH1F("tau1Eta_GjetsPt15to20",  "tau1Eta_GjetsPt15to20",  20, -2.5, 2.5);
	  TH1F* tau1Eta_GjetsPt20to30    = new TH1F("tau1Eta_GjetsPt20to30",  "tau1Eta_GjetsPt20to30",  20, -2.5, 2.5);
	  TH1F* tau1Eta_GjetsPt30to50    = new TH1F("tau1Eta_GjetsPt30to50",  "tau1Eta_GjetsPt30to50",  20, -2.5, 2.5);
	  TH1F* tau1Eta_GjetsPt50to80    = new TH1F("tau1Eta_GjetsPt50to80",  "tau1Eta_GjetsPt50to80",  20, -2.5, 2.5);
	  TH1F* tau1Eta_GjetsPt80to120   = new TH1F("tau1Eta_GjetsPt80to120", "tau1Eta_GjetsPt80to120", 20, -2.5, 2.5);
	  TH1F* tau1Eta_GjetsPt120to170  = new TH1F("tau1Eta_GjetsPt120to170","tau1Eta_GjetsPt120to170",20, -2.5, 2.5);
	  TH1F* tau1Eta_GjetsPt170to300  = new TH1F("tau1Eta_GjetsPt170to300","tau1Eta_GjetsPt170to300",20, -2.5, 2.5);


	  //---tau2 Iso
	  TH1F* tau2Iso_Zp350            = new TH1F("tau2Iso_Zp350",          "tau2Iso_Zp350",          50, 0, 50);
	  TH1F* tau2Iso_Zp500            = new TH1F("tau2Iso_Zp500",          "tau2Iso_Zp500",          50, 0, 50);
	  TH1F* tau2Iso_Ztt              = new TH1F("tau2Iso_Ztt",            "tau2Iso_Ztt",            50, 0, 50);
	  TH1F* tau2Iso_Zee              = new TH1F("tau2Iso_Zee",            "tau2Iso_Zee",            50, 0, 50);
	  TH1F* tau2Iso_ttbar            = new TH1F("tau2Iso_ttbar",          "tau2Iso_ttbar",          50, 0, 50);
	  TH1F* tau2Iso_wjets            = new TH1F("tau2Iso_ttbar",          "tau2Iso_wjets",          50, 0, 50);
	  TH1F* tau2Iso_qcdEmPt20to30    = new TH1F("tau2Iso_qcdEmPt20to30",  "tau2Iso_qcdEmPt20to30",  50, 0, 50);
	  TH1F* tau2Iso_qcdEmPt30to80    = new TH1F("tau2Iso_qcdEmPt30to80",  "tau2Iso_qcdEmPt30to80",  50, 0, 50);
	  TH1F* tau2Iso_qcdEmPt80to170   = new TH1F("tau2Iso_qcdEmPt80to170", "tau2Iso_qcdEmPt80to170", 50, 0, 50);
	  TH1F* tau2Iso_qcdBCPt20to30    = new TH1F("tau2Iso_qcdBCPt20to30",  "tau2Iso_qcdBCPt20to30",  50, 0, 50);
	  TH1F* tau2Iso_qcdBCPt30to80    = new TH1F("tau2Iso_qcdBCPt30to80",  "tau2Iso_qcdBCPt30to80",  50, 0, 50);
	  TH1F* tau2Iso_qcdBCPt80to170   = new TH1F("tau2Iso_qcdBCPt80to170", "tau2Iso_qcdBCPt80to170", 50, 0, 50);
	  TH1F* tau2Iso_GjetsPt15to20    = new TH1F("tau2Iso_GjetsPt15to20",  "tau2Iso_GjetsPt15to20",  50, 0, 50);
	  TH1F* tau2Iso_GjetsPt20to30    = new TH1F("tau2Iso_GjetsPt20to30",  "tau2Iso_GjetsPt20to30",  50, 0, 50);
	  TH1F* tau2Iso_GjetsPt30to50    = new TH1F("tau2Iso_GjetsPt30to50",  "tau2Iso_GjetsPt30to50",  50, 0, 50);
	  TH1F* tau2Iso_GjetsPt50to80    = new TH1F("tau2Iso_GjetsPt50to80",  "tau2Iso_GjetsPt50to80",  50, 0, 50);
	  TH1F* tau2Iso_GjetsPt80to120   = new TH1F("tau2Iso_GjetsPt80to120", "tau2Iso_GjetsPt80to120", 50, 0, 50);
	  TH1F* tau2Iso_GjetsPt120to170  = new TH1F("tau2Iso_GjetsPt120to170","tau2Iso_GjetsPt120to170",50, 0, 50);
	  TH1F* tau2Iso_GjetsPt170to300  = new TH1F("tau2Iso_GjetsPt170to300","tau2Iso_GjetsPt170to170",50, 0, 50);

	  //=======tau2 N prongs
	  TH1F* tau2Nprongs_Zp500           = new TH1F("tau2NProngs_Zp500",          "tau2 N Prongs", 10, 0, 10);
	  TH1F* tau2Nprongs_Zp350           = new TH1F("tau2NProngs_Zp350",          "tau2 N Prongs", 10, 0, 10);
	  TH1F* tau2Nprongs_Ztt             = new TH1F("tau2NProngs_Ztt"  ,          "tau2 N Prongs", 10, 0, 10);
	  TH1F* tau2Nprongs_Zee             = new TH1F("tau2NProngs_Zee"  ,          "tau2 N Prongs", 10, 0, 10);
	  TH1F* tau2Nprongs_ttbar           = new TH1F("tau2NProngs_ttbar",          "tau2 N Prongs", 10, 0, 10);
	  TH1F* tau2Nprongs_wjets           = new TH1F("tau2NProngs_wjets",          "tau2 N Prongs", 10, 0, 10);
	  TH1F* tau2Nprongs_qcdEmPt20to30   = new TH1F("tau2NProngs_qcdEmPt20to30"  ,"tau2 N Prongs", 10, 0, 10);
	  TH1F* tau2Nprongs_qcdEmPt30to80   = new TH1F("tau2NProngs_qcdEmPt30to80"  ,"tau2 N Prongs", 10, 0, 10);
	  TH1F* tau2Nprongs_qcdEmPt80to170  = new TH1F("tau2NProngs_qcdEmPt80to170" ,"tau2 N Prongs", 10, 0, 10);
	  TH1F* tau2Nprongs_qcdBCPt20to30   = new TH1F("tau2NProngs_qcdBCPt20to30"  ,"tau2 N Prongs", 10, 0, 10);
	  TH1F* tau2Nprongs_qcdBCPt30to80   = new TH1F("tau2NProngs_qcdBCPt30to80"  ,"tau2 N Prongs", 10, 0, 10);
	  TH1F* tau2Nprongs_qcdBCPt80to170  = new TH1F("tau2NProngs_qcdBCPt80to170" ,"tau2 N Prongs", 10, 0, 10);
	  TH1F* tau2Nprongs_GjetsPt15to20   = new TH1F("tau2Nprongs_GjetsPt15to20"  ,"tau2 N Prongs", 10, 0, 10);
	  TH1F* tau2Nprongs_GjetsPt20to30   = new TH1F("tau2Nprongs_GjetsPt20to30"  ,"tau2 N Prongs", 10, 0, 10);
	  TH1F* tau2Nprongs_GjetsPt30to50   = new TH1F("tau2Nprongs_GjetsPt30to50"  ,"tau2 N Prongs", 10, 0, 10);
	  TH1F* tau2Nprongs_GjetsPt50to80   = new TH1F("tau2Nprongs_GjetsPt50to80"  ,"tau2 N Prongs", 10, 0, 10);
	  TH1F* tau2Nprongs_GjetsPt80to120  = new TH1F("tau2Nprongs_GjetsPt80to120" ,"tau2 N Prongs", 10, 0, 10);
	  TH1F* tau2Nprongs_GjetsPt120to170 = new TH1F("tau2Nprongs_GjetsPt120to170","tau2 N Prongs", 10, 0, 10);
	  TH1F* tau2Nprongs_GjetsPt170to300 = new TH1F("tau2Nprongs_GjetsPt170to300","tau2 N Prongs", 10, 0, 10);

	  TH1F* Resoltau2Pt_Ztt = new TH1F("Resoltau2Pt_Ztt", "tau2 Et Resolution", 200, -1,1);
	  TH1F* ResolElecPt_Ztt = new TH1F("ResolElecPt_Ztt", "e Et Resolution", 200, -1,1);


	  //=======tau2 Eta

	  TH1F* tau2Eta_Zp350            = new TH1F("tau2Eta_Zp350",          "tau2Eta_Zp350",          20, -2.5, 2.5);
	  TH1F* tau2Eta_Zp500            = new TH1F("tau2Eta_Zp500",          "tau2Eta_Zp500",          20, -2.5, 2.5);
	  TH1F* tau2Eta_Ztt              = new TH1F("tau2Eta_Ztt",            "tau2Eta_Ztt",            20, -2.5, 2.5);
	  TH1F* tau2Eta_Zee              = new TH1F("tau2Eta_Zee",            "tau2Eta_Zee",            20, -2.5, 2.5);
	  TH1F* tau2Eta_ttbar            = new TH1F("tau2Eta_ttbar",          "tau2Eta_ttbar",          20, -2.5, 2.5);
	  TH1F* tau2Eta_wjets            = new TH1F("tau2Eta_ttbar",          "tau2Eta_wjets",          20, -2.5, 2.5);
	  TH1F* tau2Eta_qcdEmPt20to30    = new TH1F("tau2Eta_qcdEmPt20to30",  "tau2Eta_qcdEmPt20to30",  20, -2.5, 2.5);
	  TH1F* tau2Eta_qcdEmPt30to80    = new TH1F("tau2Eta_qcdEmPt30to80",  "tau2Eta_qcdEmPt30to80",  20, -2.5, 2.5);
	  TH1F* tau2Eta_qcdEmPt80to170   = new TH1F("tau2Eta_qcdEmPt80to170", "tau2Eta_qcdEmPt80to170", 20, -2.5, 2.5);
	  TH1F* tau2Eta_qcdBCPt20to30    = new TH1F("tau2Eta_qcdBCPt20to30",  "tau2Eta_qcdBCPt20to30",  20, -2.5, 2.5);
	  TH1F* tau2Eta_qcdBCPt30to80    = new TH1F("tau2Eta_qcdBCPt30to80",  "tau2Eta_qcdBCPt30to80",  20, -2.5, 2.5);
	  TH1F* tau2Eta_qcdBCPt80to170   = new TH1F("tau2Eta_qcdBCPt80to170", "tau2Eta_qcdBCPt80to170", 20, -2.5, 2.5);
	  TH1F* tau2Eta_GjetsPt15to20    = new TH1F("tau2Eta_GjetsPt15to20",  "tau2Eta_GjetsPt15to20",  20, -2.5, 2.5);
	  TH1F* tau2Eta_GjetsPt20to30    = new TH1F("tau2Eta_GjetsPt20to30",  "tau2Eta_GjetsPt20to30",  20, -2.5, 2.5);
	  TH1F* tau2Eta_GjetsPt30to50    = new TH1F("tau2Eta_GjetsPt30to50",  "tau2Eta_GjetsPt30to50",  20, -2.5, 2.5);
	  TH1F* tau2Eta_GjetsPt50to80    = new TH1F("tau2Eta_GjetsPt50to80",  "tau2Eta_GjetsPt50to80",  20, -2.5, 2.5);
	  TH1F* tau2Eta_GjetsPt80to120   = new TH1F("tau2Eta_GjetsPt80to120", "tau2Eta_GjetsPt80to120", 20, -2.5, 2.5);
	  TH1F* tau2Eta_GjetsPt120to170  = new TH1F("tau2Eta_GjetsPt120to170","tau2Eta_GjetsPt120to170",20, -2.5, 2.5);
	  TH1F* tau2Eta_GjetsPt170to300  = new TH1F("tau2Eta_GjetsPt170to300","tau2Eta_GjetsPt170to300",20, -2.5, 2.5);

	  //========eEta

	  TH1F* eEta_Zp350            = new TH1F("eEta_Zp350",          "eEta_Zp350",          20, -2.5, 2.5);
	  TH1F* eEta_Zp500            = new TH1F("eEta_Zp500",          "eEta_Zp500",          20, -2.5, 2.5);
	  TH1F* eEta_Ztt              = new TH1F("eEta_Ztt",            "eEta_Ztt",            20, -2.5, 2.5);
	  TH1F* eEta_Zee              = new TH1F("eEta_Zee",            "eEta_Zee",            20, -2.5, 2.5);
	  TH1F* eEta_ttbar            = new TH1F("eEta_ttbar",          "eEta_ttbar",          20, -2.5, 2.5);
	  TH1F* eEta_wjets            = new TH1F("eEta_ttbar",          "eEta_wjets",          20, -2.5, 2.5);
	  TH1F* eEta_qcdEmPt20to30    = new TH1F("eEta_qcdEmPt20to30",  "eEta_qcdEmPt20to30",  20, -2.5, 2.5);
	  TH1F* eEta_qcdEmPt30to80    = new TH1F("eEta_qcdEmPt30to80",  "eEta_qcdEmPt30to80",  20, -2.5, 2.5);
	  TH1F* eEta_qcdEmPt80to170   = new TH1F("eEta_qcdEmPt80to170", "eEta_qcdEmPt80to170", 20, -2.5, 2.5);
	  TH1F* eEta_qcdBCPt20to30    = new TH1F("eEta_qcdBCPt20to30",  "eEta_qcdBCPt20to30",  20, -2.5, 2.5);
	  TH1F* eEta_qcdBCPt30to80    = new TH1F("eEta_qcdBCPt30to80",  "eEta_qcdBCPt30to80",  20, -2.5, 2.5);
	  TH1F* eEta_qcdBCPt80to170   = new TH1F("eEta_qcdBCPt80to170", "eEta_qcdBCPt80to170", 20, -2.5, 2.5);
	  TH1F* eEta_GjetsPt15to20    = new TH1F("eEta_GjetsPt15to20",  "eEta_GjetsPt15to20",  20, -2.5, 2.5);
	  TH1F* eEta_GjetsPt20to30    = new TH1F("eEta_GjetsPt20to30",  "eEta_GjetsPt20to30",  20, -2.5, 2.5);
	  TH1F* eEta_GjetsPt30to50    = new TH1F("eEta_GjetsPt30to50",  "eEta_GjetsPt30to50",  20, -2.5, 2.5);
	  TH1F* eEta_GjetsPt50to80    = new TH1F("eEta_GjetsPt50to80",  "eEta_GjetsPt50to80",  20, -2.5, 2.5);
	  TH1F* eEta_GjetsPt80to120   = new TH1F("eEta_GjetsPt80to120", "eEta_GjetsPt80to120", 20, -2.5, 2.5);
	  TH1F* eEta_GjetsPt120to170  = new TH1F("eEta_GjetsPt120to170","eEta_GjetsPt120to170",20, -2.5, 2.5);
	  TH1F* eEta_GjetsPt170to300  = new TH1F("eEta_GjetsPt170to300","eEta_GjetsPt170to300",20, -2.5, 2.5);

	  //----Impact Parameter
	  TH1F* eIp_Zp350 	   = new TH1F("eIp_Zp350",	     "eIp_Zp350",	     100, -1, 1);
	  TH1F* eIp_Zp500 	   = new TH1F("eIp_Zp500",	     "eIp_Zp500",	     100, -1, 1);
	  TH1F* eIp_Ztt		   = new TH1F("eIp_Ztt",	     "eIp_Ztt", 	     100, -1, 1);
	  TH1F* eIp_Zee		   = new TH1F("eIp_Zee",	     "eIp_Zee", 	     100, -1, 1);
	  TH1F* eIp_wjets 	   = new TH1F("eIp_wjets",	     "eIp_wjets",	     100, -1, 1);
	  TH1F* eIp_ttbar 	   = new TH1F("eIp_ttbar",	     "eIp_ttbar",	     100, -1, 1);
	  TH1F* eIp_GjetsPt15to20    = new TH1F("eIp_GjetsPt15to20  " ,"eIp_GjetsPt15to20  " , 100, -1, 1);
	  TH1F* eIp_GjetsPt20to30    = new TH1F("eIp_GjetsPt20to30  " ,"eIp_GjetsPt20to30  " , 100, -1, 1);
	  TH1F* eIp_GjetsPt30to50    = new TH1F("eIp_GjetsPt30to50  " ,"eIp_GjetsPt30to50  " , 100, -1, 1);
	  TH1F* eIp_GjetsPt50to80    = new TH1F("eIp_GjetsPt50to80  " ,"eIp_GjetsPt50to80  " , 100, -1, 1);
	  TH1F* eIp_GjetsPt80to120   = new TH1F("eIp_GjetsPt80to120 " ,"eIp_GjetsPt80to120 " , 100, -1, 1);
	  TH1F* eIp_GjetsPt120to170  = new TH1F("eIp_GjetsPt120to170" ,"eIp_GjetsPt120to170" , 100, -1, 1);
	  TH1F* eIp_GjetsPt170to300  = new TH1F("eIp_GjetsPt170to300" ,"eIp_GjetsPt170to300" , 100, -1, 1);
	  TH1F* eIp_qcdEmPt20to30    = new TH1F("eIp_qcdEmPt20to30  " ,"eIp_qcdEmPt20to30  " , 100, -1, 1);
	  TH1F* eIp_qcdEmPt80to170   = new TH1F("eIp_qcdEmPt80to170 " ,"eIp_qcdEmPt80to170 " , 100, -1, 1);
	  TH1F* eIp_qcdBCPt20to30    = new TH1F("eIp_qcdBCPt20to30  " ,"eIp_qcdBCPt20to30  " , 100, -1, 1);
	  TH1F* eIp_qcdBCPt30to80    = new TH1F("eIp_qcdBCPt30to80  " ,"eIp_qcdBCPt30to80  " , 100, -1, 1);
	  TH1F* eIp_qcdBCPt80to170   = new TH1F("eIp_qcdBCPt80to170 " ,"eIp_qcdBCPt80to170 " , 100, -1, 1);
	  //-----d0Error
	  TH1F* d0Error_Zp350	       = new TH1F("d0Error_Zp350",           "d0Error_Zp350",          50, -0.5, 0.5);
	  TH1F* d0Error_Zp400	       = new TH1F("d0Error_Zp400",           "d0Error_Zp400",          50, -0.5, 0.5);
	  TH1F* d0Error_Zp500            = new TH1F("d0Error_Zp500",           "d0Error_Zp500",          50, -0.5, 0.5);
	  TH1F* d0Error_Zp600            = new TH1F("d0Error_Zp600",           "d0Error_Zp600",          50, -0.5, 0.5);
	  TH1F* d0Error_Zp1000           = new TH1F("d0Error_Zp1000",          "d0Error_Zp1000",         50, -0.5, 0.5);
	  TH1F* d0Error_Ztt	       = new TH1F("d0Error_Ztt",             "d0Error_Ztt",            50, -0.5, 0.5);
	  TH1F* d0Error_Zee	       = new TH1F("d0Error_Zee",             "d0Error_Zee",            50, -0.5, 0.5);
	  TH1F* d0Error_ttbar	       = new TH1F("d0Error_ttbar",           "d0Error_ttbar",          50, -0.5, 0.5);
	  TH1F* d0Error_wjets	       = new TH1F("d0Error_ttbar",           "d0Error_wjets",          50, -0.5, 0.5);
	  TH1F* d0Error_qcdEmPt20to30    = new TH1F(" d0Error_qcdEmPt20to30",  "d0Error_qcdEmPt20to30",  50, -0.5, 0.5);
	  TH1F* d0Error_qcdEmPt30to80    = new TH1F(" d0Error_qcdEmPt30to80",  "d0Error_qcdEmPt30to80",  50, -0.5, 0.5);
	  TH1F* d0Error_qcdEmPt80to170   = new TH1F(" d0Error_qcdEmPt80to170", "d0Error_qcdEmPt80to170", 50, -0.5, 0.5);
	  TH1F* d0Error_qcdBCPt20to30    = new TH1F(" d0Error_qcdBCPt20to30",  "d0Error_qcdBCPt20to30",  50, -0.5, 0.5);
	  TH1F* d0Error_qcdBCPt30to80    = new TH1F(" d0Error_qcdBCPt30to80",  "d0Error_qcdBCPt30to80",  50, -0.5, 0.5);
	  TH1F* d0Error_qcdBCPt80to170   = new TH1F(" d0Error_qcdBCPt80to170", "d0Error_qcdBCPt80to170", 50, -0.5, 0.5);
	  TH1F* d0Error_GjetsPt15to20    = new TH1F(" d0Error_GjetsPt15to20",  "d0Error_GjetsPt15to20",  50, -0.5, 0.5);
	  TH1F* d0Error_GjetsPt20to30    = new TH1F(" d0Error_GjetsPt20to30",  "d0Error_GjetsPt20to30",  50, -0.5, 0.5);
	  TH1F* d0Error_GjetsPt30to50    = new TH1F(" d0Error_GjetsPt30to50",  "d0Error_GjetsPt30to50",  50, -0.5, 0.5);
	  TH1F* d0Error_GjetsPt50to80    = new TH1F(" d0Error_GjetsPt50to80",  "d0Error_GjetsPt50to80",  50, -0.5, 0.5);
	  TH1F* d0Error_GjetsPt80to120   = new TH1F(" d0Error_GjetsPt80to120", "d0Error_GjetsPt80to120", 50, -0.5, 0.5);
	  TH1F* d0Error_GjetsPt120to170  = new TH1F(" d0Error_GjetsPt120to170","d0Error_GjetsPt120to170",50, -0.5, 0.5);
	  TH1F* d0Error_GjetsPt170to300  = new TH1F(" d0Error_GjetsPt170to300","d0Error_GjetsPt170to300",50, -0.5, 0.5);

	  //----Resolution

	  TH1F* tau1PtResol_0to50    = new TH1F("tau1PtResol_0to50"   , "#tau1 P_{t} Resolution", 200, -1, 1);
	  TH1F* tau1PtResol_50to100  = new TH1F("tau1PtResol_50to100" , "#tau1 P_{t} Resolution", 200, -1, 1);
	  TH1F* tau1PtResol_100to150 = new TH1F("tau1PtResol_100to150", "#tau1 P_{t} Resolution", 200, -1, 1);
	  TH1F* tau1PtResol_150to200 = new TH1F("tau1PtResol_150to200", "#tau1 P_{t} Resolution", 200, -1, 1);
	  TH1F* tau1PtResol_200to250 = new TH1F("tau1PtResol_200to250", "#tau1 P_{t} Resolution", 200, -1, 1);
	  TH1F* tau1PtResol_250      = new TH1F("tau1PtResol_250"     , "#tau1 P_{t} Resolution", 200, -1, 1);

	  TH1F* tau2PtResol_0to50    = new TH1F("tau2PtResol_0to50"   , "#tau2 P_{t} Resolution", 200, -1, 1);
	  TH1F* tau2PtResol_50to100  = new TH1F("tau2PtResol_50to100" , "#tau2 P_{t} Resolution", 200, -1, 1);
	  TH1F* tau2PtResol_100to150 = new TH1F("tau2PtResol_100to150", "#tau2 P_{t} Resolution", 200, -1, 1);
	  TH1F* tau2PtResol_150to200 = new TH1F("tau2PtResol_150to200", "#tau2 P_{t} Resolution", 200, -1, 1);
	  TH1F* tau2PtResol_200to250 = new TH1F("tau2PtResol_200to250", "#tau2 P_{t} Resolution", 200, -1, 1);
	  TH1F* tau2PtResol_250      = new TH1F("tau2PtResol_250"     , "#tau2 P_{t} Resolution", 200, -1, 1);

	  TH1F* ePtResol_0to50    = new TH1F("ePtResol_0to50"   , "e P_{t} Resolution", 200, -1, 1);
	  TH1F* ePtResol_50to100  = new TH1F("ePtResol_50to100" , "e P_{t} Resolution", 200, -1, 1);
	  TH1F* ePtResol_100to150 = new TH1F("ePtResol_100to150", "e P_{t} Resolution", 200, -1, 1);
	  TH1F* ePtResol_150to200 = new TH1F("ePtResol_150to200", "e P_{t} Resolution", 200, -1, 1);
	  TH1F* ePtResol_200to250 = new TH1F("ePtResol_200to250", "e P_{t} Resolution", 200, -1, 1);
	  TH1F* ePtResol_250      = new TH1F("ePtResol_250"     , "e P_{t} Resolution", 200, -1, 1);

	  //----Smeared Mass

	  TH1F* eTauSmearedMass_h = new TH1F("eTauSmearedMass", "M(e,#tau)", 150, 0, 1500);
	  TH1F* eTauRecoMass_h    = new TH1F("eTauRecoMass",    "M(e,#tau)", 150, 0, 1500);
	  TH1F* TauTauSmearedMass_h = new TH1F("TauTauSmearedMass", "M(#tau1,#tau2)", 150, 0, 1500);
	  TH1F* TauTauRecoMass_h    = new TH1F("TauTauRecoMass",    "M(#tau1,#tau2)", 150, 0, 1500);
	  TH1F* recoTau1Pt         = new TH1F("recoTau1Pt_h",    "#tau1 P_{t}", 100, 0, 500);
	  TH1F* smearedTau1Pt      = new TH1F("smearedTau1Pt_h", "#tau1 P_{t}", 100, 0, 500);
	  TH1F* recoTau2Pt         = new TH1F("recoTau2Pt_h",    "#tau2 P_{t}", 100, 0, 500);
	  TH1F* smearedTau2Pt      = new TH1F("smearedTau2Pt_h", "#tau2 P_{t}", 100, 0, 500);
	  TH1F* recoElectronPt    = new TH1F("recoElectronPt_h",    "e P_{t}", 100, 0, 500);
	  TH1F* smearedElectronPt = new TH1F("smearedElectronPt_h", "e P_{t}", 100, 0, 500);

	  void drawHistos(std::string, TCut, float, float, std::string);
	  void doMatchingCheck(std::string, TCut, float, float, std::string);
	  void drawMass(TCut, std::vector<std::string>, std::vector<std::pair<double, double> >, std::string);
	  std::pair<double, std::pair<double, double> > getEfficiency(std::string, TCut, TCut);
	  std::pair<double, std::pair<double, double> > getEfficiency(unsigned int, unsigned int);
	  std::pair<double, std::pair<double, double> > getEfficiency_2(double, double);

	  std::pair<double, double> getSurvivingEvents(std::pair<double, double>,
	  std::pair<double, std::pair<double, double> >);
	  std::vector<std::pair<TH1F*, const char*> > doTheHistos(std::string, TCut, float,float);
	  std::vector<std::pair<TH1F*, const char*> > doTheHistos(std::vector<std::string>
	  theFiles, std::string theVar, TCut theCuts, float lowLimit, float hiLimit);
	  void FillePtHistos(size_t, unsigned int);
	  void FilltauPtHistos(size_t, unsigned int);
	  void FillMassHistos(size_t, unsigned int);
	  void FillMassHistos_Fac(size_t, unsigned int);
	  void FilleIpHistos(size_t, unsigned int);
	  void FilleEtaHistos(size_t, unsigned int);
	  void Filld0ErrorHistos(size_t, unsigned int);
	  void FilltauEtaHistos(size_t, unsigned int);
	  void FillHoverEHistos(size_t, unsigned int);
	  void FillEoverPHistos(size_t, unsigned int);
	  void FilleIsoHistos(size_t, unsigned int);
	  void FilleIsoHistos_Fac(size_t, unsigned int);
	  void FilltauIsoHistos(size_t, unsigned int);
	  void FilltauIsoHistos_Fac(size_t, unsigned int);
	  void FilltauNprongsHistos(size_t, unsigned int);
	  TH1F* HistoStyle  (TH1F* h, double, TString titlex, TString titley, Color_t color, Marker_t marker, TString drawOpt);
	  TH1F* HistoStyle_2(TH1F* h, double, TString titlex, TString titley, Color_t color, TString drawOpt);
	  bool passedTrkQualityInfo(unsigned int theIndex);
	  bool passedDR(unsigned int);
	  bool passedSkim(unsigned int);
	  bool passedAcceptance(unsigned int);
	  bool passedEId(unsigned int);
	  bool passedEtrkIso(unsigned int);
	  bool passedTauId(unsigned int);
	  bool passedDiTau(unsigned int);
	  bool passedEId_Fac(unsigned int);
	  bool passedd0Error(unsigned int);
	  bool passedTauId_Fac(unsigned int);
	  bool passedTauiso_relax(unsigned int);
	  bool passedTauiso_Fac(unsigned int);
	  bool passedDiTau_Fac(unsigned int);
	  bool passedeEcalIso_Fac(unsigned int);
	  bool passedEiso_Fac(unsigned int);
	  bool eBarrel(unsigned int);
	  bool eEndCap(unsigned int);
	  bool tauInTheCracks(unsigned int);
	  bool passedTauPt(unsigned int);
	  bool passedTauEta(unsigned int);
	  bool passedEPt(unsigned int);
	  bool passedEEta(unsigned int);
	  bool passedTauLTPt (unsigned int);
	  bool passedEoverP (unsigned int);
	  bool passedHoverEm (unsigned int);
	  bool passedeEcalIso (unsigned int);
	  bool passedEiso (unsigned int);
	  bool passed1Prong (unsigned int);
	  bool passed1or3Prong (unsigned int);
	  bool passedTauiso (unsigned int);
	  bool passedTauHPLT (unsigned int);
	  bool passedCosDphi (unsigned int);
	  bool passedOSLS (unsigned int);
	  bool passedMass (unsigned int);
	  bool passedDisc(unsigned int);
	  bool passedMET (unsigned int);
	  bool passedPZeta(unsigned int);
	  bool isMatched(unsigned int);
	  bool isMatched();
	  bool eGOLDEN(unsigned int);
	  bool eBIGBREM(unsigned int);
	  bool eNARROW(unsigned int);
	  bool eSHOWERING(unsigned int);
	  bool tauSHOWERING(unsigned int);
	  bool eCRACK (unsigned int);
	  bool eUNKNOWN(unsigned int);
	  bool eTauPtAssimetry (unsigned int);
	  int getMatchedCand();
	  void setupBranches(TTree*);
	  bool IsZee(unsigned int);
	  bool eTauPtAssimetry(unsigned int);
	  bool IsbJet(unsigned int);
	  std::vector<unsigned int> *tauMotherId   = NULL;
	  std::vector<int> *tauMatched             = NULL;
	  std::vector<float> *tauE                 = NULL;
	  std::vector<float> *tauEt                = NULL;
	  std::vector<float> *tauCharge            = NULL;
	  std::vector<float> *tauEta               = NULL;
	  std::vector<float> *tauPhi               = NULL;
	  std::vector<unsigned int> *tauNProngs    = NULL;
	  std::vector<float> *tauIsoTrackPtSum     = NULL;
	  std::vector<float> *tauIsoGammaEtSum     = NULL;
	  std::vector<float> *tauLTPt              = NULL;
	  std::vector<float> *tauEmFraction        = NULL;
	  std::vector<float> *tauHcalTotOverPLead  = NULL;
	  std::vector<float> *tauHcalMaxOverPLead  = NULL;
	  std::vector<float> *tauHcal3x3OverPLead  = NULL;
	  std::vector<float> *tauElectronPreId     = NULL;
	  std::vector<float> *tauModifiedEOverP    = NULL;
	  std::vector<float> *tauBremsRecoveryEOverPLead = NULL;
	  std::vector<float> *tauDiscAgainstElec         = NULL;
	  std::vector<int> *tauLTCharge                  = NULL;
	  std::vector<float> *tauLTSignedIp              = NULL;
	  std::vector<float> *gsfElectronEv              = NULL;

	  std::vector<unsigned int> *eMotherId           = NULL;
	  std::vector<int> *eMatched                     = NULL;
	  std::vector<float> *eE                         = NULL;
	  std::vector<float> *eEt                        = NULL;
	  std::vector<float> *ePt                        = NULL;
	  std::vector<float> *eCharge                    = NULL;
	  std::vector<float> *eEta                       = NULL;
	  std::vector<float> *ePhi                       = NULL;
	  std::vector<float> *eSigmaEtaEta               = NULL;
	  std::vector<float> *eSigmaIEtaIEta             = NULL;
	  std::vector<float> *eEOverP                    = NULL;
	  std::vector<float> *eHOverEm                   = NULL;
	  std::vector<float> *eDeltaPhiIn                = NULL;
	  std::vector<float> *eDeltaEtaIn                = NULL;
	  std::vector<float> *eEcalIso                   = NULL;
	  std::vector<float> *eHcalIso                   = NULL;
	  std::vector<float> *eTrkIso                    = NULL;
	  std::vector<float> *eIso                       = NULL;
	  std::vector<float> *eSCE1x5                    = NULL;
	  std::vector<float> *eSCE2x5                    = NULL;
	  std::vector<float> *eSCE5x5                    = NULL;
	  std::vector<float> *eIp                        = NULL;


	  std::vector<float> *eTauMass                   = NULL;
	  std::vector<float> *eTauMetMass                = NULL;
	  std::vector<float> *eTauCosDPhi                = NULL;
	  std::vector<float> *eTauDeltaR                 = NULL;

	  std::vector<float> *tauPt                      = NULL;
	  std::vector<float> *diTauMass                  = NULL;
	  std::vector<float> *diTauPt                    = NULL;
	  std::vector<float> *diTauEt                    = NULL;
	  std::vector<float> *METv                       = NULL;

	  std::vector<int> *eIDv                         = NULL;
	  std::vector<int> *tauIDv                       = NULL;
	  std::vector<float> *Statusv                    = NULL;
	  std::vector<float>* tauLTSignificanceIpv       = NULL;
	  std::vector<float>* tauLTChi2v                 = NULL;
	  std::vector<int>* tauLTRecHitsSizev            = NULL;
	  std::vector<unsigned int>* eventIsZeev         = NULL;
	  std::vector<float>* jetPhiv                    = NULL;
	  std::vector<float>* jetEtav                    = NULL;
	  std::vector<float>* jetPtv                     = NULL;
	  std::vector<float>* jetEtv                     = NULL;
	  std::vector<float>* jetEv                      = NULL;
	  std::vector<float>* jetEmFractionv             = NULL;
	  std::vector<float>* tauDgenPtv                 = NULL;
	  std::vector<float>* tauDgenEv                  = NULL;
	  std::vector<float>* tauDgenEtav                = NULL; 
	  std::vector<float>* tauDgenPhiv                = NULL;
	  std::vector<float>* eDgenPtv                   = NULL;
	  std::vector<float>* eDgenEv                    = NULL;
	  std::vector<float>* eDgenEtav                  = NULL;
	  std::vector<float>* eDgenPhiv                  = NULL;
	  std::vector<float>* eTauPZetav                 = NULL;
	  std::vector<float>* eTauPZetaVisv              = NULL;
	  std::vector<float>* tauIsoGammaEtSumDR0_75MinPt1_0 = NULL;
	  std::vector<float>* tauIsoTrackSumPtDR0_75MinPt0_5 = NULL;
	  std::vector<float>* bJetDiscrByTrackCountingv      = NULL;

	  std::vector<int>* isEEv          = NULL;
	  std::vector<int>* isEBv          = NULL;
	  std::vector<float>* d0Errorv     = NULL;
	  std::vector<float>* eIp_ctfv     = NULL;  
	  std::vector<float>* d0Error_ctfv = NULL;

	  std::vector<float>* eTauSmearedMassv    = NULL;
	  std::vector<float>* smearedTauPtv       = NULL;
	  std::vector<float>* smearedTauEtav      = NULL;
	  std::vector<float>* smearedTauPhiv      = NULL;
	  std::vector<float>* smearedElectronPtv  = NULL;
	  std::vector<float>* smearedElectronEtav = NULL;
	  std::vector<float>* smearedElectronPhiv = NULL;


	  TBranch* tauMotherIdB       = NULL;
	  TBranch* tauMatchedB        = NULL;
	  TBranch* tauEB              = NULL;
	  TBranch* tauEtB             = NULL;
	  TBranch* tauPtB             = NULL;
	  TBranch* tauChargeB         = NULL;
	  TBranch* tauEtaB            = NULL;
	  TBranch* tauPhiB            = NULL;
	  TBranch* tauNProngsB        = NULL;
	  TBranch* tauIsoTrackPtSumB  = NULL;
	  TBranch* tauIsoGammaEtSumB  = NULL;
	  TBranch* tauLTPtB           = NULL;
	  TBranch* tauEmFractionB     = NULL;
	  TBranch* tauHcalTotOverPLeadB = NULL;
	  TBranch* tauHcalMaxOverPLeadB = NULL;
	  TBranch* tauHcal3x3OverPLeadB = NULL;
	  TBranch* tauElectronPreIdB    = NULL;
	  TBranch* tauModifiedEOverPB   = NULL;
	  TBranch* tauBremsRecoveryEOverPLeadB = NULL;
	  TBranch* tauDiscAgainstElecB         = NULL;
	  TBranch* tauLTChargeB                = NULL;
	  TBranch* tauLTSignedIpB              = NULL;
	  TBranch* eMotherIdB                  = NULL;
	  TBranch* eMatchedB                   = NULL;
	  TBranch* eEB                         = NULL;
	  TBranch* eEtB                        = NULL;
	  TBranch* ePtB                        = NULL;
	  TBranch* eChargeB                    = NULL;
	  TBranch* eEtaB                       = NULL;
	  TBranch* ePhiB                       = NULL;
	  TBranch* eSigmaEtaEtaB               = NULL;
	  TBranch* eSigmaIEtaIEtaB             = NULL;
	  TBranch* eEOverPB                    = NULL;
	  TBranch* eHOverEmB                   = NULL;
	  TBranch* eDeltaPhiInB                = NULL;
	  TBranch* eDeltaEtaInB                = NULL;
	  TBranch* eEcalIsoB                   = NULL;
	  TBranch* eHcalIsoB                   = NULL;
	  TBranch* eTrkIsoB                    = NULL;
	  TBranch* eIsoB                       = NULL;
	  TBranch* eSCE1x5B                    = NULL;
	  TBranch* eSCE2x5B                    = NULL;
	  TBranch* eSCE5x5B                    = NULL;
	  TBranch* eIpB                        = NULL;
	  TBranch* eTauMassB                   = NULL;
	  TBranch* eTauMetMassB                = NULL;
	  TBranch* eTauCosDPhiB                = NULL;
	  TBranch* eTauDeltaRB                 = NULL;
	  TBranch* diTauMassB                  = NULL;
	  TBranch* diTauPtB                    = NULL;
	  TBranch* diTauEtB                    = NULL;
	  TBranch* eIDB                        = NULL;
	  TBranch* tauIDB                      = NULL;
	  TBranch* METB                        = NULL;
	  TBranch* StatusB                     = NULL;
	  TBranch* tauLTSignificanceIpB        = NULL;
	  TBranch* tauLTChi2B                  = NULL;
	  TBranch* tauLTRecHitsSizeB           = NULL;
	  TBranch* eventIsZeeB                 = NULL;

	  TBranch* jetPhiB                     = NULL;
	  TBranch* jetEtaB                     = NULL;
	  TBranch* jetPtB                      = NULL;
	  TBranch* jetEtB                      = NULL;
	  TBranch* jetEB                       = NULL;
	  TBranch* jetEmFractionB              = NULL;
	  TBranch* tauDgenPtB                  = NULL;
	  TBranch* tauDgenEB                   = NULL;
	  TBranch* tauDgenEtaB                 = NULL;
	  TBranch* tauDgenPhiB                 = NULL;
	  TBranch* eDgenPtB                    = NULL;
	  TBranch* eDgenEB                     = NULL;
	  TBranch* eDgenEtaB                   = NULL;
	  TBranch* eDgenPhiB                   = NULL;

	  TBranch* gsfElectronEB               = NULL;

	  TBranch* eTauPZetaB                  = NULL;
	  TBranch* eTauPZetaVisB               = NULL;
	  TBranch* tauIsoGammaEtSumDR0_75MinPt1_0B = NULL;
	  TBranch* tauIsoTrackSumPtDR0_75MinPt0_5B = NULL;
	  TBranch* bJetDiscrByTrackCountingB       = NULL;
	  TBranch* isEEB                           = NULL;
	  TBranch* isEBB                           = NULL;
	  TBranch* d0ErrorB                        = NULL;
	  TBranch* eIp_ctfB                        = NULL;
	  TBranch* d0Error_ctfB                    = NULL;

	  TBranch* eTauSmearedMassB    = NULL; 
	  TBranch* smearedTauPtB       = NULL;
	  TBranch* smearedTauEtaB      = NULL;
	  TBranch* smearedTauPhiB      = NULL;
	  TBranch* smearedElectronPtB  = NULL;
	  TBranch* smearedElectronEtaB = NULL;
	  TBranch* smearedElectronPhiB = NULL;

