#include <TFile.h>
#include <TTree.h>
#include <TH1F.h>
#include <TCut.h>
#include <TObject.h>
#include <TROOT.h>
#include <vector>
#include <iostream>
#include <sstream>
#include <utility>
#include "TApplication.h"
TTree* getTheTree(std::string);

//---Invariant Mass

TH1F* M_Zp500  = new TH1F("M_Zp500", "M_Zp500", 150, 0, 1500);



std::pair<double, std::pair<double, double> > getEfficiency(std::string, TCut, TCut);
std::pair<double, std::pair<double, double> > getEfficiency(unsigned int, unsigned int);
std::pair<double, std::pair<double, double> > getEfficiency_2(double, double);
std::pair<double, double> getSurvivingEvents(std::pair<double, double>,
std::pair<double, std::pair<double, double> >);
//TH1F* HistoStyle  (TH1F* h, double, char* titlex, char* titley, Color_t color, Marker_t marker,  char* drawOpt);
//TH1F* HistoStyle_2(TH1F* h, double, char* titlex, char* titley, Color_t color,  char* drawOpt);

bool passedDR(unsigned int theIndex);
bool passedTau1Pt(unsigned int theIndex);
bool passedTau2Pt(unsigned int theIndex);
bool passedTau1Eta(unsigned int theIndex);
bool passedTau2Eta(unsigned int theIndex);
bool tau1IsInTheCracks(unsigned int theIndex);
bool tau2IsInTheCracks(unsigned int theIndex);
int  getMatchedCand();
bool Tau1passedAcceptance(unsigned int theIndex);
bool Tau2passedAcceptance(unsigned int theIndex);
bool Tau1passedEId(unsigned int theIndex);
bool Tau2passedEId(unsigned int theIndex);
bool Tau1passedMuVeto(unsigned int theIndex);
bool Tau2passedMuVeto(unsigned int theIndex);
bool Tau1passedTauTrkIso(unsigned int theIndex);
bool Tau2passedTauTrkIso(unsigned int theIndex);
bool Tau1passedTauEcalIso(unsigned int theIndex);
bool Tau2passedTauEcalIso(unsigned int theIndex);
bool Tau1passed1Prong(unsigned int theIndex);
bool Tau2passedTau1Prong(unsigned int theIndex);
bool passedTauOS(unsigned int theIndex);
bool passedCosDelPhi(unsigned int theIndex);
bool passedPtAsymm(unsigned int theIndex);
bool passedMassCut(unsigned int theIndex);
bool Tau1passedLooseTauTrkIso_Fact(unsigned int theIndex);
bool Tau2passedLooseTauTrkIso_Fact(unsigned int theIndex);
bool Tau1passedLooseTauGammaIso_Fact(unsigned int theIndex);
bool Tau2passedLooseTauGammaIso_Fact(unsigned int theIndex);
bool passedLooseCosDelPhi_Fact(unsigned int theIndex);
bool passedLoosePtAsymm_Fact(unsigned int theIndex);
bool passedLooseMassCut_Fact(unsigned int theIndex);
bool Tau1passedTightTauTrkIso_Fact(unsigned int theIndex);
bool Tau2passedTightTauTrkIso_Fact(unsigned int theIndex);
bool Tau1passedTightTauGammaIso_Fact(unsigned int theIndex);
bool Tau2passedTightTauGammaIso_Fact(unsigned int theIndex);
bool Tau1passedTight1Prong_Fact(unsigned int theIndex);
bool Tau2passedTight1Prong_Fact(unsigned int theIndex);
bool passedTightOS_Fact(unsigned int theIndex);
std::vector<unsigned int> *tau1PdgId     = NULL;
std::vector<unsigned int> *tau1MotherId   = NULL;
std::vector<int> *tau1Matched             = NULL;
std::vector<float> *tau1E                 = NULL;
std::vector<float> *tau1Et                = NULL;
std::vector<float> *tau1Charge            = NULL;
std::vector<float> *tau1Eta               = NULL;
std::vector<float> *tau1Phi               = NULL;
std::vector<float> *tau1IsoTrackPtSum     = NULL;
std::vector<float> *tau1IsoGammaEtSum     = NULL;
std::vector<float> *tau1LTPt              = NULL;
std::vector<float> *tau1EmFraction        = NULL;
std::vector<float> *tau1HcalTotOverPLead  = NULL;
std::vector<float> *tau1HcalMaxOverPLead  = NULL;
std::vector<float> *tau1Hcal3x3OverPLead  = NULL;
std::vector<float> *tau1ElectronPreId     = NULL;
std::vector<float> *tau1ModifiedEOverP    = NULL;
std::vector<float> *tau1BremsRecoveryEOverPLead = NULL;
std::vector<float> *tau1DiscAgainstElec         = NULL;
std::vector<int> *tau1LTCharge                  = NULL;
std::vector<float> *tau1LTSignedIp              = NULL;

std::vector<unsigned int> *tau2PdgId     = NULL;
std::vector<unsigned int> *tau2MotherId   = NULL;
std::vector<int> *tau2Matched             = NULL;
std::vector<float> *tau2E                 = NULL;
std::vector<float> *tau2Et                = NULL;
std::vector<float> *tau2Charge            = NULL;
std::vector<float> *tau2Eta               = NULL;
std::vector<float> *tau2Phi               = NULL;
std::vector<float> *tau2IsoTrackPtSum     = NULL;
std::vector<float> *tau2IsoGammaEtSum     = NULL;
std::vector<float> *tau2LTPt              = NULL;
std::vector<float> *tau2EmFraction        = NULL;
std::vector<float> *tau2HcalTotOverPLead  = NULL;
std::vector<float> *tau2HcalMaxOverPLead  = NULL;
std::vector<float> *tau2Hcal3x3OverPLead  = NULL;
std::vector<float> *tau2ElectronPreId     = NULL;
std::vector<float> *tau2ModifiedEOverP    = NULL;
std::vector<float> *tau2BremsRecoveryEOverPLead = NULL;
std::vector<float> *tau2DiscAgainstElec         = NULL;
std::vector<int> *tau2LTCharge                  = NULL;
std::vector<float> *tau2LTSignedIp              = NULL;

std::vector<float> *mEt              = NULL;
std::vector<float> *TauTauMass              = NULL;
std::vector<float> *TauTauCosDPhi              = NULL;
std::vector<float> *TauTauDeltaR              = NULL;
std::vector<float> *TauTauDeltaPt              = NULL;
std::vector<float> *TauTauMetMass              = NULL;
std::vector<float> *TauTauPZeta              = NULL;
std::vector<float> *TauTauPZetaVis              = NULL;
std::vector<float> *jetPt              = NULL;
std::vector<float> *jetEt              = NULL;
std::vector<float> *jetE              = NULL;
std::vector<float> *jetEta              = NULL;
std::vector<float> *jetPhi              = NULL;
std::vector<float> *jetEmFraction              = NULL;
std::vector<float> *bJetDiscrByTrackCounting              = NULL;
std::vector<float> *bJetDiscrBySimpleSecondaryV              = NULL;
std::vector<float> *bJetDiscrByCombinedSecondaryV              = NULL;

TBranch* tau1PdgIdB       = NULL;
TBranch* tau1MatchedB       = NULL;
TBranch* tau1MotherIdB       = NULL;
TBranch* tau1EB       = NULL;
TBranch* tau1EtB       = NULL;
TBranch* tau1PtB       = NULL;
TBranch* tau1ChargeB       = NULL;
TBranch* tau1EtaB       = NULL;
TBranch* tau1PhiB       = NULL;
TBranch* tau1NProngsB       = NULL;
TBranch* tau1IsoTrackPtSumB      = NULL;
TBranch* tau1IsoGammaEtSumB       = NULL;
TBranch* tau1LTPtB       = NULL;
TBranch* tau1EmFractionB      = NULL;
TBranch* tau1HcalTotOverPLeadB       = NULL;
TBranch* tau1HcalMaxOverPLeadB       = NULL;
TBranch* tau1Hcal3x3OverPLeadB       = NULL;
TBranch* tau1ElectronPreIdB       = NULL;
TBranch* tau1ModifiedEOverPB       = NULL;
TBranch* tau1BremsRecoveryEOverPLeadB       = NULL;
TBranch* tau1DiscAgainstElecB       = NULL;
TBranch* tau1LTChargeB       = NULL;
TBranch* tau1LTSignedIpB       = NULL;
TBranch* tau1IsInTheCracksB       = NULL;
TBranch* tau1TancDiscOnePercentB       = NULL;
TBranch* tau1TancDiscHalfPercentB       = NULL;
TBranch* tau1TancDiscQuarterPercentB       = NULL;
TBranch* tau1TancDiscTenthPercentB      = NULL;


TBranch* tau2PdgIdB       = NULL;
TBranch* tau2MatchedB       = NULL;
TBranch* tau2MotherIdB       = NULL;
TBranch* tau2EB       = NULL;
TBranch* tau2EtB       = NULL;
TBranch* tau2PtB       = NULL;
TBranch* tau2ChargeB       = NULL;
TBranch* tau2EtaB       = NULL;
TBranch* tau2PhiB       = NULL;
TBranch* tau2NProngsB       = NULL;
TBranch* tau2IsoTrackPtSumB      = NULL;
TBranch* tau2IsoGammaEtSumB       = NULL;
TBranch* tau2LTPtB       = NULL;
TBranch* tau2EmFractionB      = NULL;
TBranch* tau2HcalTotOverPLeadB       = NULL;
TBranch* tau2HcalMaxOverPLeadB       = NULL;
TBranch* tau2Hcal3x3OverPLeadB       = NULL;
TBranch* tau2ElectronPreIdB       = NULL;
TBranch* tau2ModifiedEOverPB       = NULL;
TBranch* tau2BremsRecoveryEOverPLeadB       = NULL;
TBranch* tau2DiscAgainstElecB       = NULL;
TBranch* tau2LTChargeB       = NULL;
TBranch* tau2LTSignedIpB       = NULL;
TBranch* tau2IsInTheCracksB       = NULL;
TBranch* tau2TancDiscOnePercentB       = NULL;
TBranch* tau2TancDiscHalfPercentB       = NULL;
TBranch* tau2TancDiscQuarterPercentB       = NULL;
TBranch* tau2TancDiscTenthPercentB      = NULL;

TBranch* mEtB       = NULL;
TBranch* TauTauMassB       = NULL;
TBranch* TauTauCosDPhiB       = NULL;
TBranch* TauTauDeltaRB       = NULL;
TBranch* TauTauDeltaPtB       = NULL;
TBranch* TauTauMetMassB       = NULL;
TBranch* TauTauPZetaB       = NULL;
TBranch* TauTauPZetaVisB       = NULL;
TBranch* jetPtB       = NULL;
TBranch* jetEtB       = NULL;
TBranch* jetEB       = NULL;
TBranch* jetEtaB       = NULL;
TBranch* jetPhiB      = NULL;
TBranch* jetEmFractionB      = NULL;
TBranch* bJetDiscrByTrackCountingB       = NULL;
TBranch* bJetDiscrBySimpleSecondaryVB       = NULL;
TBranch* bJetDiscrByCombinedSecondaryVB       = NULL;




















std::vector<unsigned int> *tau1NProngs    = NULL;

