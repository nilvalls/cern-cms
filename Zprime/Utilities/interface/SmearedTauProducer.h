// -*- C++ -*-
//
// Package:    SmearedTauProducer
// Class:      SmearedTauProducer
// 
/**\class SmearedTauProducer SmearedTauProducer.cc HighMassAnalysis/SmearedTauProducer/src/SmearedTauProducer.cc

 Description: [one line class summary]

 Implementation:
     [Notes on implementation]
*/
//
// Original Author:  Eduardo Luiggi
//         Created:  Fri May  6 16:01:23 CDT 2011
// $Id: SmearedTauProducer.h,v 1.1 2011/05/12 00:07:17 eluiggi Exp $
//
//


// system include files
#include <memory>

// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDProducer.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "DataFormats/Candidate/interface/Candidate.h"
#include "DataFormats/PatCandidates/interface/Tau.h"


//
// class declaration
//

class SmearedTauProducer : public edm::EDProducer {
   public:
      explicit SmearedTauProducer(const edm::ParameterSet&);
      ~SmearedTauProducer();

      static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);

   private:
      virtual void beginJob() ;
      virtual void produce(edm::Event&, const edm::EventSetup&);
      virtual void endJob() ;
      
      virtual void beginRun(edm::Run&, edm::EventSetup const&);
      virtual void endRun(edm::Run&, edm::EventSetup const&);
      virtual void beginLuminosityBlock(edm::LuminosityBlock&, edm::EventSetup const&);
      virtual void endLuminosityBlock(edm::LuminosityBlock&, edm::EventSetup const&);
      std::pair<bool, reco::Candidate::LorentzVector> matchToGen(const pat::Tau&);
      
      edm::Handle<reco::GenParticleCollection> _theGenParticleCollection;
      edm::InputTag _TauSource;
      edm::InputTag _GenCollection;
      double _TauToGenMatchingDeltaR;
      
      bool _SmearThePt;
      double _PtScaleOffset;
      double _PtSigmaOffset;
      
      bool _SmearTheEta;
      double _EtaScaleOffset;
      double _EtaSigmaOffset;
      
      bool _SmearThePhi;
      double _PhiScaleOffset;
      double _PhiSigmaOffset;

      // ----------member data ---------------------------
};

