// -*- C++ -*-
//
// Package:    MCGenEventFilter
// Class:      MCGenEventFilter
// 
/**\class MCGenEventFilter MCGenEventFilter.cc ElectroWeakAnalysis/MCGenEventFilter/src/MCGenEventFilter.cc

 Description: [one line class summary]

 Implementation:
     [Notes on implementation]
*/
//
// Original Author:  Eduardo Luiggi
//         Created:  Tue Sep  6 14:08:38 CDT 2011
// $Id$
//
//


// system include files
#include <memory>

// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDFilter.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"

#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/HepMCCandidate/interface/GenParticleFwd.h"

//
// class declaration
//

class MCGenEventFilter : public edm::EDFilter {
   public:
      explicit MCGenEventFilter(const edm::ParameterSet&);
      ~MCGenEventFilter();

      static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);

   private:
      virtual void beginJob() ;
      virtual bool filter(edm::Event&, const edm::EventSetup&);
      virtual void endJob() ;
      
      virtual bool beginRun(edm::Run&, edm::EventSetup const&);
      virtual bool endRun(edm::Run&, edm::EventSetup const&);
      virtual bool beginLuminosityBlock(edm::LuminosityBlock&, edm::EventSetup const&);
      virtual bool endLuminosityBlock(edm::LuminosityBlock&, edm::EventSetup const&);

      // ----------member data ---------------------------
  
      edm::InputTag _source;
      int _pdgIdCode;
      int _pdgStatusCode;
      int _daughterPdgIdCode;
  
};

//
// constants, enums and typedefs
//

//
// static data member definitions
//

//
// constructors and destructor
//
MCGenEventFilter::MCGenEventFilter(const edm::ParameterSet& iConfig){
   //now do what ever initialization is needed
   _source = iConfig.getParameter<edm::InputTag>("GenSource");
   _pdgIdCode  = iConfig.getUntrackedParameter<int>("ParticleID");
   _pdgStatusCode = iConfig.getUntrackedParameter<int>("ParticleStatus");
   _daughterPdgIdCode = iConfig.getUntrackedParameter<int>("DaughterParticleID");
}


MCGenEventFilter::~MCGenEventFilter(){
 
   // do anything here that needs to be done at desctruction time
   // (e.g. close files, deallocate resources etc.)

}


//
// member functions
//

// ------------ method called on each new Event  ------------
bool MCGenEventFilter::filter(edm::Event& iEvent, const edm::EventSetup& iSetup){
   using namespace edm;
   bool foundMatch = false;
   edm::Handle<reco::GenParticleCollection> genParticles;
   iEvent.getByLabel(_source, genParticles);   
   
   for(reco::GenParticleCollection::const_iterator mcIt = genParticles->begin(); mcIt != genParticles->end(); mcIt++){
     if(abs(mcIt->pdgId()) == _pdgIdCode && mcIt->status() == _pdgStatusCode){
       //skip if looking for W's and found one from top
       if(_pdgIdCode == 24 && abs(mcIt->mother()->pdgId()) == 6) continue;
       //Look for daughter?
       if(_daughterPdgIdCode > 0){
         //get daughters
	 for(unsigned int dauIt = 0; dauIt < mcIt->numberOfDaughters(); dauIt++){
	   if(abs(mcIt->daughter(dauIt)->pdgId()) == _daughterPdgIdCode){
	     foundMatch = true;
	     break;
	   }
	 }
       }
       else{
         foundMatch = true;
         break;
       }
     }
   }
   return foundMatch;
}

// ------------ method called once each job just before starting event loop  ------------
void MCGenEventFilter::beginJob(){
}

// ------------ method called once each job just after ending the event loop  ------------
void MCGenEventFilter::endJob() {
}

// ------------ method called when starting to processes a run  ------------
bool MCGenEventFilter::beginRun(edm::Run&, edm::EventSetup const&){ 
  return true;
}

// ------------ method called when ending the processing of a run  ------------
bool MCGenEventFilter::endRun(edm::Run&, edm::EventSetup const&){
  return true;
}

// ------------ method called when starting to processes a luminosity block  ------------
bool MCGenEventFilter::beginLuminosityBlock(edm::LuminosityBlock&, edm::EventSetup const&){
  return true;
}

// ------------ method called when ending the processing of a luminosity block  ------------
bool MCGenEventFilter::endLuminosityBlock(edm::LuminosityBlock&, edm::EventSetup const&){
  return true;
}

// ------------ method fills 'descriptions' with the allowed parameters for the module  ------------
void MCGenEventFilter::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  //The following says we do not know what parameters are allowed so do no validation
  // Please change this to state exactly what you do use, even if it is no parameters
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  descriptions.addDefault(desc);
}
//define this as a plug-in
DEFINE_FWK_MODULE(MCGenEventFilter);
