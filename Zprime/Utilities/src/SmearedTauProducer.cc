#include "HighMassAnalysis/Utilities/interface/SmearedTauProducer.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "DataFormats/HepMCCandidate/interface/GenParticle.h"

SmearedTauProducer::SmearedTauProducer(const edm::ParameterSet& iConfig){
  produces<std::vector<pat::Tau> >();
   
  _TauSource = iConfig.getParameter<edm::InputTag>("TauSource");
  _GenCollection = iConfig.getUntrackedParameter<edm::InputTag>("GenSource");
  _TauToGenMatchingDeltaR = iConfig.getParameter<double>("TauToGenMatchingDeltaR");
  
  _SmearThePt = iConfig.getParameter<bool>("SmearThePt");
  _PtScaleOffset = iConfig.getParameter<double>("PtScaleOffset");
  _PtSigmaOffset = iConfig.getParameter<double>("PtSigmaOffset");
  
  _SmearTheEta = iConfig.getParameter<bool>("SmearTheEta");
  _EtaScaleOffset = iConfig.getParameter<double>("EtaScaleOffset");
  _EtaSigmaOffset = iConfig.getParameter<double>("EtaSigmaOffset");
  
  _SmearThePhi = iConfig.getParameter<bool>("SmearThePhi");
  _PhiScaleOffset = iConfig.getParameter<double>("PhiScaleOffset");
  _PhiSigmaOffset = iConfig.getParameter<double>("PhiSigmaOffset");
}


SmearedTauProducer::~SmearedTauProducer(){
 
   // do anything here that needs to be done at desctruction time
   // (e.g. close files, deallocate resources etc.)

}


//
// member functions
//

// ------------ method called to produce the data  ------------
void SmearedTauProducer::produce(edm::Event& iEvent, const edm::EventSetup& iSetup){
  using namespace edm;
  													     
  if(iEvent.isRealData()) return;
 
  Handle<pat::TauCollection> theTauCollection; 
  iEvent.getByLabel(_TauSource, theTauCollection);
  
  iEvent.getByLabel(_GenCollection, _theGenParticleCollection);
  std::cout << " Collection size " << _theGenParticleCollection->size();
  //if(!iEvent.getByLabel(_GenCollection, _theGenParticleCollection)){
  //  edm::LogError("") << ">>> Tau-GenParticle match map does not exist !!!"; 
  //  return; 
  //}
  
  std::auto_ptr<pat::TauCollection> smearedTaus (new pat::TauCollection);
												     
  for (unsigned int vIt = 0; vIt < theTauCollection->size(); vIt++) {
    edm::Ref<std::vector<pat::Tau> > tau(theTauCollection, vIt);
		    	 
    double smearedPt;
    double smearedEta;
    double smearedPhi;
  
    double ptgen = tau->pt();
    double etagen = tau->eta(); 
    double phigen = tau->phi();  
    
    std::pair<bool, reco::Candidate::LorentzVector> theGenMatchedPair = matchToGen(*tau);								    	 
    reco::Candidate::LorentzVector gen = theGenMatchedPair.second;							    	 
    
    if(theGenMatchedPair.first) {										    	 
      ptgen = gen.pt();										      
      etagen = gen.eta();										      
      phigen = gen.phi();										      
      LogTrace("") << ">>> Tau-GenParticle match found; pttau= " << tau->pt() << ", ptgen= " << ptgen;	      
    } 
    else{
      LogTrace("") << ">>> Tau-GENPARTICLE MATCH NOT FOUND!!!";
    }
    
    if(_SmearThePt){
      smearedPt = (ptgen * _PtScaleOffset) + ((tau->pt() -  ptgen) * _PtSigmaOffset);
    }
    else
      smearedPt = tau->pt();
    
    if(_SmearTheEta){
      smearedEta = (etagen - _EtaScaleOffset) + ((tau->eta() - etagen) * _EtaSigmaOffset);
    }
    else 
      smearedEta = tau->eta();
    
    if(_SmearThePhi){
      smearedPhi = (phigen - _PhiScaleOffset) + ((tau->phi() - phigen) * _PhiSigmaOffset);
    }
    else
      smearedPhi = tau->phi();
      
    pat::Tau *theSmearedTau =  tau->clone();
    theSmearedTau->setP4(reco::Particle::PolarLorentzVector(smearedPt, smearedEta, smearedPhi, tau->mass()));
    smearedTaus->push_back(*theSmearedTau);

  }
  iEvent.put(smearedTaus);													     
}

std::pair<bool,reco::Candidate::LorentzVector> SmearedTauProducer::matchToGen(const pat::Tau& theObject){
  bool foundMatch = false;
  reco::Candidate::LorentzVector theGenObject(0,0,0,0);
  std::pair<bool, reco::Candidate::LorentzVector> theOutpair;
  reco::Candidate::LorentzVector MChadtau;
  const reco::Candidate *daughterCand;
  theOutpair = std::make_pair<bool, reco::Candidate::LorentzVector>(foundMatch, theGenObject);
  
  for(reco::GenParticleCollection::const_iterator genParticle = _theGenParticleCollection->begin();genParticle != _theGenParticleCollection->end();++genParticle){
    if((abs(genParticle->pdgId()) == 15) && (genParticle->status() != 3)){
      int neutrinos = 0;
      MChadtau = genParticle->p4();
      for(int ii=0; ii<(int)(genParticle->numberOfDaughters()); ii++) {
        daughterCand = genParticle->daughter(ii);
        if( (abs(daughterCand->pdgId()) == 12) || (abs(daughterCand->pdgId()) == 14) || (abs(daughterCand->pdgId()) == 16) ) {
          neutrinos++;
          MChadtau = MChadtau - daughterCand->p4();
        }
      }
      if(neutrinos == 1){
        if(reco::deltaR(MChadtau.eta(), MChadtau.phi(), theObject.eta(), theObject.phi()) < _TauToGenMatchingDeltaR) {
	  foundMatch = true;
	  theGenObject = MChadtau;
        }
      }
    }
  }
  theOutpair = std::make_pair<bool, reco::Candidate::LorentzVector>(foundMatch, theGenObject);

  return theOutpair;
}

// ------------ method called once each job just before starting event loop  ------------
void SmearedTauProducer::beginJob(){
}

// ------------ method called once each job just after ending the event loop  ------------
void SmearedTauProducer::endJob() {
}

// ------------ method called when starting to processes a run  ------------
void SmearedTauProducer::beginRun(edm::Run&, edm::EventSetup const&){
}

// ------------ method called when ending the processing of a run  ------------
void SmearedTauProducer::endRun(edm::Run&, edm::EventSetup const&){
}

// ------------ method called when starting to processes a luminosity block  ------------
void SmearedTauProducer::beginLuminosityBlock(edm::LuminosityBlock&, edm::EventSetup const&){
}

// ------------ method called when ending the processing of a luminosity block  ------------
void SmearedTauProducer::endLuminosityBlock(edm::LuminosityBlock&, edm::EventSetup const&){
}

// ------------ method fills 'descriptions' with the allowed parameters for the module  ------------
void SmearedTauProducer::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  //The following says we do not know what parameters are allowed so do no validation
  // Please change this to state exactly what you do use, even if it is no parameters
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  descriptions.addDefault(desc);
}

//define this as a plug-in
DEFINE_FWK_MODULE(SmearedTauProducer);
